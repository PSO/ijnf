

.PHONY: all
all:
	make -C cpp

.PHONY: logo
logo:
	cd logo ; pdflatex matrix.tex
	convert logo/matrix.pdf logo/matrix.png

.PHONY: clean
clean:
	make -C cpp clean

.PHONY: uninstall
uninstall:
	rm /usr/local/bin/ijnf

.PHONY: install
install: uninstall
	cp bin/jnf.opt /usr/local/bin/ijnf
