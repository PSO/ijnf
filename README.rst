Interval Jordan Normal Form (IJNF)
==================================

Tool for characterising the n-th power of a matrix.

The tool turns a square matrix M of numeric values into a triplet of square matrices P, J, Q of numeric intervals, such that the matrix P.(J^k).Q both:

- contains M^k,
- has coefficients which are nice functions of k.

Compilation
***********

For the C++ tool:

.. code::

   sudo apt install g++ libgmp-dev
   make
   sudo make install

For the OCaml interface:

.. code::

   sudo apt install opam m4 libmpfr-dev
   opam init
   opam install gmp mlgmpidl menhir ocamlfind ocamlbuid
   make -C ocaml
