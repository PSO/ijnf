#include "jenkins_traub.hpp"
#include "interval.hpp"
//#include "polynomial.hpp"
#include "complex.hpp"
#include <cassert>
#include <vector>

using real_t                  = float;
using interval_t              = interval;
using complex_interval_t      = cartesian_complex<interval_t>;
using complex_t               = cartesian_complex<real_t>;
using real_poly_t             = std::vector<real_t>;
using complex_interval_poly_t = std::vector<complex_interval_t>;

interval_t expand(const interval_t& i, real_t r) {
	return i + interval_t(0, r) - interval_t(0, r);
}

complex_t center(const complex_interval_t& z) {
	return complex_t(center(real(z)), center(imag(z)));
}

complex_interval_t expand(const complex_interval_t& i, real_t r) {
	return complex_interval_t(expand(real(i), r), expand(imag(i), r));
}

bool intersect(const complex_interval_t& lhs, const complex_interval_t& rhs) {
	return intersect(real(lhs), real(rhs)) && intersect(imag(lhs), imag(rhs));
}

complex_interval_t meet
	(const complex_interval_t& lhs, const complex_interval_t& rhs) {
	return complex_interval_t
		       (meet(real(lhs), real(rhs)), meet(imag(lhs), imag(rhs)));
}

template<class ForwardIterator>
bool pairwise_empty_intersection(ForwardIterator lo, ForwardIterator hi) {
	if(lo == hi) return true;
	for(auto i = lo ; i != hi ; ++i) {
		auto j = i;
		for(++j ; j != hi ; ++j)
			if(intersect(*i, *j)) return false;
	}
	return true;
}

template<class T, class U>
U eval(const std::vector<T>& p, const U& x) {
	using std::fma;
	const std::size_t n = p.size();
	if(n == 0) return U(0);
	U result = U(p[n - 1]);
	for(std::size_t i = n - 1 ; i != 0 ; ) {
		--i;
		result = fma(result, U(x), U(p[i]));
	}
	return result;
}

// assumes p monic and square_free
std::pair<std::vector<interval_t>, std::vector<complex_interval_t>> gershgorin
    (const std::vector<complex_t>& eigenvalue_estimate
    ,const real_poly_t& p
    )
{
	using std::abs;
	const std::size_t n = eigenvalue_estimate.size();
	std::vector<complex_interval_t> pn;
	pn.reserve(n);
	for(std::size_t i = 0 ; i < n ; ++i) {
		complex_interval_t delta(1);
		for(std::size_t j = 0 ; j < n ; ++j)
			if(j != i)
				delta *=
					complex_interval_t(eigenvalue_estimate[i])
					- complex_interval_t(eigenvalue_estimate[j]);
		pn.emplace_back(complex_interval_t(1)/delta);
	}
	std::vector<complex_interval_t> gershgorin_row_disk, gershgorin_col_disk;
	gershgorin_row_disk.reserve(n);
	for(std::size_t i = 0 ; i < n ; ++i) {
		interval_t row_radius = 0;
		for(std::size_t j = 0 ; j < n ; ++j) {
			if(j == i) continue;
			row_radius += abs(pn[j]);
		}
		interval_t col_radius = 0;
		for(std::size_t j = 0 ; j < n ; ++j) {
			if(j == i) continue;
			col_radius +=
				abs(eval(p, complex_interval_t(eigenvalue_estimate[j])));
		}
		const complex_interval_t estimate_image =
			eval(p, complex_interval_t(eigenvalue_estimate[i]));
		row_radius *= abs(estimate_image);
		col_radius *= abs(pn[i]);
		const complex_interval_t center =
			complex_interval_t(eigenvalue_estimate[i]) - pn[i] * estimate_image;
		gershgorin_row_disk.emplace_back(expand(center, supremum(row_radius)));
		gershgorin_col_disk.emplace_back(expand(center, supremum(col_radius)));
	}
	if(!pairwise_empty_intersection
		   (gershgorin_row_disk.cbegin(), gershgorin_row_disk.cend())
	  )
		throw std::runtime_error
			      ("some gershgorin disks intersect, fix not implemented !");
	if(pairwise_empty_intersection
		   (gershgorin_col_disk.cbegin(), gershgorin_col_disk.cend())
	  )
		for(std::size_t i = 0 ; i < n ; ++i) {
			gershgorin_row_disk[i] =
				meet(gershgorin_row_disk[i], gershgorin_col_disk[i]);
		}
	std::sort
		(gershgorin_row_disk.begin()
		,gershgorin_row_disk.end()
		,[](const complex_interval_t& lhs, const complex_interval_t& rhs) {
			if(imag(lhs).contains_zero() && imag(rhs).contains_zero())
				return infimum(real(lhs)) < infimum(real(rhs));
			if(imag(lhs).contains_zero() != imag(rhs).contains_zero())
				return imag(lhs).contains_zero();
			if(std::abs(infimum(imag(lhs))) != std::abs(infimum(imag(rhs))))
				return std::abs(infimum(imag(lhs)))
				       < std::abs(infimum(imag(rhs)));
			return infimum(real(lhs)) < infimum(real(rhs));
		}
		);
	std::size_t m = 0;
	for(std::size_t m_hi = n
	   ;m_hi - m != 1
	   ;(imag(gershgorin_row_disk[(m + m_hi)/2]).contains_zero()
		    ?m:m_hi) = (m + m_hi) / 2
	   );
	// check real roots
	std::vector<interval_t> real_root;
	if(imag(gershgorin_row_disk[m]).contains_zero() || m != 0) {
		if(imag(gershgorin_row_disk[m]).contains_zero()) ++m;
		int sign_left = ((n % 2 == 0)?1:-1) * ((p[n] > 0)?1:-1);
		const int sign_right = (p[n] > 0)?1:-1;
		for(std::size_t i = 0, j = 1 ; j < m ; ++i, ++j) {
			const real_t midpoint =
				(supremum(real(gershgorin_row_disk[i]))
				 + infimum(real(gershgorin_row_disk[j]))
				) / 2;
			const interval_t image = eval(p, interval_t(midpoint));
			if(image.contains_zero())
				throw std::runtime_error("unable to check real roots !");
			const int sign = (image.infimum() > 0)?1:-1;
			if(sign == sign_left)
				throw std::runtime_error("unable to check real roots !");
			sign_left = sign;
		}
		if(sign_left == sign_right)
			throw std::runtime_error("unable to check real roots !");
		real_root.reserve(m);
		for(std::size_t i = 0 ; i < m ; ++i)
			real_root.emplace_back(real(gershgorin_row_disk[i]));
	}
	// check complex roots
	std::vector<complex_interval_t> complex_root;
	assert((n - m) % 2 == 0);
	complex_root.reserve((n - m)/2);
	for(std::size_t i = m ; i < n ; ++i) {
		if(gershgorin_row_disk[i] == complex_interval_t(0)) continue;
		std::size_t conj_i = n;
		for(std::size_t j = i + 1 ; j < n ; ++j) {
			if(intersect(gershgorin_row_disk[i], conj(gershgorin_row_disk[j]))) {
				if(conj_i != n)
					throw std::runtime_error("unable to check complex roots !");
				conj_i = j;
			}
		}
		if(conj_i == n)
			throw std::runtime_error("unable to check complex roots !");
		complex_root.emplace_back
			(meet(gershgorin_row_disk[i], conj(gershgorin_row_disk[conj_i])));
		gershgorin_row_disk[conj_i] = complex_interval_t(0);
	}
	return std::make_pair(real_root, complex_root);
}

int main() {
	real_poly_t p({15, -32, 24, -8, 1});
	auto roots = gershgorin
		(std::vector<complex_t>
			({complex_t(2.0000001, 1.000001)
			 ,complex_t(3.000001, 0)
			 ,complex_t(2.0000001, -1.0000001)
			 ,complex_t(1.0000001, 0.0000001)
			 })
		, p
		);
	for(const auto& r : roots.first)
		std::cout << r << " (" << center(r) << ')' << std::endl;
	for(const auto& z : roots.second)
		std::cout << z << " (" << center(z) << ')' << std::endl;
}
