#include "domain/interval.hpp"
#include "types.hpp"
#include <cassert>
#include <vector>

interval_type expand(const interval_type& i, real_type r) {
	return i + interval_type(0, r) - interval_type(0, r);
}

complex_type center(const complex_interval_type& z) {
	return complex_type(center(real(z)), center(imag(z)));
}

complex_interval_type expand(const complex_interval_type& i, real_type r) {
	return complex_interval_type(expand(real(i), r), expand(imag(i), r));
}

bool intersect(const complex_interval_type& lhs, const complex_interval_type& rhs) {
	return intersect(real(lhs), real(rhs)) && intersect(imag(lhs), imag(rhs));
}

complex_interval_type meet
	(const complex_interval_type& lhs, const complex_interval_type& rhs) {
	return complex_interval_type
		       (meet(real(lhs), real(rhs)), meet(imag(lhs), imag(rhs)));
}

template<class ForwardIterator>
bool pairwise_empty_intersection(ForwardIterator lo, ForwardIterator hi) {
	if(lo == hi) return true;
	for(auto i = lo ; i != hi ; ++i) {
		auto j = i;
		for(++j ; j != hi ; ++j)
			if(intersect(*i, *j)) return false;
	}
	return true;
}

struct gershgorin_exception : std::runtime_error {
	using std::runtime_error::runtime_error;
};

// assumes p monic and square_free
std::pair<std::vector<interval_type>, std::vector<complex_interval_type>> gershgorin
    (const std::vector<complex_type>& eigenvalue_estimate
    ,const real_polynomial_type& p
    )
{
	using std::abs;
	const std::size_t n = eigenvalue_estimate.size();
	std::vector<complex_interval_type> pn;
	pn.reserve(n);
	for(std::size_t i = 0 ; i < n ; ++i) {
		complex_interval_type delta(1);
		for(std::size_t j = 0 ; j < n ; ++j)
			if(j != i)
				delta *=
					complex_interval_type(eigenvalue_estimate[i])
					- complex_interval_type(eigenvalue_estimate[j]);
		pn.emplace_back(complex_interval_type(1)/delta);
	}
	std::vector<complex_interval_type> gershgorin_row_disk, gershgorin_col_disk;
	gershgorin_row_disk.reserve(n);
	for(std::size_t i = 0 ; i < n ; ++i) {
		interval_type row_radius = 0;
		for(std::size_t j = 0 ; j < n ; ++j) {
			if(j == i) continue;
			row_radius += abs(pn[j]);
		}
		interval_type col_radius = 0;
		for(std::size_t j = 0 ; j < n ; ++j) {
			if(j == i) continue;
			col_radius +=
				abs(p.evaluate(complex_interval_type(eigenvalue_estimate[j])));
		}
		const complex_interval_type estimate_image =
			p.evaluate(complex_interval_type(eigenvalue_estimate[i]));
		row_radius *= abs(estimate_image);
		col_radius *= abs(pn[i]);
		const complex_interval_type center =
			complex_interval_type(eigenvalue_estimate[i]) - pn[i] * estimate_image;
		gershgorin_row_disk.emplace_back(expand(center, supremum(row_radius)));
		gershgorin_col_disk.emplace_back(expand(center, supremum(col_radius)));
	}
	if(!pairwise_empty_intersection
		   (gershgorin_row_disk.cbegin(), gershgorin_row_disk.cend())
	  )
		throw gershgorin_exception
			      ("some gershgorin disks intersect, fix not implemented !");
	if(pairwise_empty_intersection
		   (gershgorin_col_disk.cbegin(), gershgorin_col_disk.cend())
	  )
		for(std::size_t i = 0 ; i < n ; ++i) {
			gershgorin_row_disk[i] =
				meet(gershgorin_row_disk[i], gershgorin_col_disk[i]);
		}
	std::sort
		(gershgorin_row_disk.begin()
		,gershgorin_row_disk.end()
		,[](const complex_interval_type& lhs, const complex_interval_type& rhs) {
			if(imag(lhs).contains_zero() && imag(rhs).contains_zero())
				return infimum(real(lhs)) < infimum(real(rhs));
			if(imag(lhs).contains_zero() != imag(rhs).contains_zero())
				return imag(lhs).contains_zero();
			if(std::abs(infimum(imag(lhs))) != std::abs(infimum(imag(rhs))))
				return std::abs(infimum(imag(lhs)))
				       < std::abs(infimum(imag(rhs)));
			return infimum(real(lhs)) < infimum(real(rhs));
		}
		);
	std::size_t m = 0;
	for(std::size_t m_hi = n
	   ;m_hi - m != 1
	   ;(imag(gershgorin_row_disk[(m + m_hi)/2]).contains_zero()
		    ?m:m_hi) = (m + m_hi) / 2
	   );
	// check real roots
	std::vector<interval_type> real_root;
	if(imag(gershgorin_row_disk[m]).contains_zero() || m != 0) {
		if(imag(gershgorin_row_disk[m]).contains_zero()) ++m;
		int sign_left = ((n % 2 == 0)?1:-1) * ((p[n] > 0)?1:-1);
		const int sign_right = (p[n] > 0)?1:-1;
		for(std::size_t i = 0, j = 1 ; j < m ; ++i, ++j) {
			const real_type midpoint =
				(supremum(real(gershgorin_row_disk[i]))
				 + infimum(real(gershgorin_row_disk[j]))
				) / 2;
			const interval_type image = p.evaluate(interval_type(midpoint));
			if(image.contains_zero())
				throw gershgorin_exception("unable to check real roots !");
			const int sign = (image.infimum() > 0)?1:-1;
			if(sign == sign_left)
				throw gershgorin_exception("unable to check real roots !");
			sign_left = sign;
		}
		if(sign_left == sign_right)
			throw gershgorin_exception("unable to check real roots !");
		real_root.reserve(m);
		for(std::size_t i = 0 ; i < m ; ++i)
			real_root.emplace_back(real(gershgorin_row_disk[i]));
	}
	// check complex roots
	std::vector<complex_interval_type> complex_root;
	assert((n - m) % 2 == 0);
	complex_root.reserve((n - m)/2);
	for(std::size_t i = m ; i < n ; ++i) {
		if(gershgorin_row_disk[i] == complex_interval_type(0)) continue;
		std::size_t conj_i = n;
		for(std::size_t j = i + 1 ; j < n ; ++j) {
			if(intersect(gershgorin_row_disk[i], conj(gershgorin_row_disk[j]))) {
				if(conj_i != n)
					throw gershgorin_exception("unable to check complex roots !");
				conj_i = j;
			}
		}
		if(conj_i == n)
			throw gershgorin_exception("unable to check complex roots !");
		complex_root.emplace_back
			(meet(gershgorin_row_disk[i], conj(gershgorin_row_disk[conj_i])));
		gershgorin_row_disk[conj_i] = complex_interval_type(0);
	}
	return std::make_pair(real_root, complex_root);
}
