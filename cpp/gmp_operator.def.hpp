/* gmp_operator.def ***********************************************************/
#define PRIM_CAT(l, r) l ## r
#define CAT(l, r) PRIM_CAT(l, r)
#define EVAL(...) __VA_ARGS__
#define PREFIX_OF(prefix, name, op) prefix
#define OPERATOR_OF(prefix, name, op) op
#define OPERATOR_NAME_OF(prefix, name, op) name
#define GMP_FUNCTION_NAME\
	CAT(CAT(EVAL(PREFIX_OF GMP_OPERATOR_DEF_tuple),_)\
	   ,EVAL(OPERATOR_NAME_OF GMP_OPERATOR_DEF_tuple)\
	   )
#ifndef CANONICALIZE
#	define CANONICALIZE(obj)
#else
#	undef CANONICALIZE
#	define CANONICALIZE(obj) obj. canonicalize()
#endif
#define GMP_WRAPPER_NAME CAT(EVAL(PREFIX_OF GMP_OPERATOR_DEF_tuple), _wrapper)

GMP_WRAPPER_NAME operator EVAL(OPERATOR_OF GMP_OPERATOR_DEF_tuple)
	(const GMP_WRAPPER_NAME & rhs) const & noexcept {
	GMP_WRAPPER_NAME result;
	EVAL(GMP_FUNCTION_NAME (result.content, content, rhs.content));
	CANONICALIZE(result);
	return result;
}
GMP_WRAPPER_NAME /*&&*/ operator EVAL(OPERATOR_OF GMP_OPERATOR_DEF_tuple)
	(const GMP_WRAPPER_NAME & rhs) && noexcept {
	EVAL(GMP_FUNCTION_NAME (content, content, rhs.content));
	CANONICALIZE((*this));
	return std::move(*this);
}
GMP_WRAPPER_NAME /*&&*/ operator EVAL(OPERATOR_OF GMP_OPERATOR_DEF_tuple)
	(GMP_WRAPPER_NAME && rhs) const & noexcept {
	EVAL(GMP_FUNCTION_NAME (rhs.content, content, rhs.content));
	CANONICALIZE(rhs);
	return std::move(rhs);
}
GMP_WRAPPER_NAME /*&&*/ operator EVAL(OPERATOR_OF GMP_OPERATOR_DEF_tuple)
	(GMP_WRAPPER_NAME && rhs) && noexcept {
	return std::move(*this) EVAL(OPERATOR_OF GMP_OPERATOR_DEF_tuple)
	       static_cast<const GMP_WRAPPER_NAME &>(rhs);
}
GMP_WRAPPER_NAME & operator CAT(EVAL(OPERATOR_OF GMP_OPERATOR_DEF_tuple), =)
	(const GMP_WRAPPER_NAME & rhs) noexcept {
	std::move(*this) EVAL(OPERATOR_OF GMP_OPERATOR_DEF_tuple) rhs;
	return *this;
}

#undef GMP_WRAPPER_NAME
#undef CANONICALIZE
#undef GMP_FUNCTION_NAME
#undef OPERATOR_NAME_OF
#undef OPERATOR_OF
#undef PREFIX_OF
#undef EVAL
#undef CAT
#undef PRIM_CAT
#undef GMP_OPERATOR_DEF_tuple
/******************************************************************************/
