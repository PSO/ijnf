#ifndef JORDAN_COMMON_HPP_
#define JORDAN_COMMON_HPP_

#include "lmpl/filter.hpp"
#include "lmpl/mem.hpp"
#include "lmpl/cat.hpp"
#include "lmpl/voider.hpp"
#include <type_traits>

using lmpl::void_t;

namespace detail_common_hpp_ {
	template
		<class BaseTraits
		,class Base
		,class Final
		,class /*= void*/
		,template<class...> class... Mixin
		>
	struct implement_traits_aux : BaseTraits {};
	
	template
		<class BaseTraits
		,class Base
		,class Final
		,template<class...> class MixinHead
		,template<class...> class... MixinTail
		>
	struct implement_traits_aux
		<BaseTraits
		,Base
		,Final
		,typename std::enable_if<BaseTraits::valid>::type
		,MixinHead
		,MixinTail...
		>
	: implement_traits_aux
		  <MixinHead<BaseTraits, Base, Final>
		  ,Base
		  ,Final
		  ,void
		  ,MixinTail...
		  > {};
	
	template
		<class BaseTraits
		,class Base
		,class Final
		,template<class...> class... Mixin
		>
	struct implement_traits
	: implement_traits_aux<BaseTraits, Base, Final, void, Mixin...> {};
	
	template<class S, class = void>
	struct underlying_structure_if {
		using type = S;
	};
	
	template<class S>
	struct underlying_structure_if<S, void_t<typename S::structure_type>> {
		using type = typename S::structure_type;
	};
	
	template<class S>
	struct underlying_structure : underlying_structure_if<S> {};
	
	template<class L>
	struct belongs_to {
		template<class X>
		struct invoke : lmpl::mem<L, X> {};
	};
	
	template<class L1, class L2>
	struct merge_mixins
	: lmpl::cat<L1, lmpl::filter_out_t<L2, belongs_to<L1>>> {};
} // namespace detail_common_hpp_

using detail_common_hpp_::underlying_structure;
using detail_common_hpp_::implement_traits;

template<class S>
using underlying_structure_t = typename underlying_structure<S>::type;

template<class S>
using underlying_structure_type_t = typename underlying_structure_t<S>::type;

template<class From, class To>
struct decays_to : std::is_same<typename std::decay<From>::type, To> {};

template<class...>
struct delay {
	template<class T>
	struct invoke { using type = T; };
};

#endif // JORDAN_COMMON_HPP_
