#ifndef JORDAN_CONFIGURATION_HPP_
#define JORDAN_CONFIGURATION_HPP_

#define LEXICOGRAPHIC_GREATER_EQUAL_3(a1, b1, c1, a2, b2, c2)                  \
	((a1 > a2) || (a1 == a2 && (b1 > b2 || (b1 == b2 && c1 >= c2))))

#ifdef __clang__
#	define CLANG_COMPILER
#	define REQUIRE_LATER_COMPILER_VERSION(major, minor, patchlevel)            \
		LEXICOGRAPHIC_GREATER_EQUAL_3(__clang_major__                          \
		                             ,__clang_minor__                          \
		                             ,__clang_patchlevel__                     \
		                             ,major                                    \
		                             ,minor                                    \
		                             ,patchlevel                               \
		                             )
#elif defined(__GNUC__) || defined(__GNUG__)
#	define GCC_COMPILER
#	define REQUIRE_LATER_COMPILER_VERSION(major, minor, patchlevel)            \
		LEXICOGRAPHIC_GREATER_EQUAL_3(__GNUC__                                 \
		                             ,__GNUC_MINOR__                           \
		                             ,__GNUC_PATCHLEVEL__                      \
		                             ,major                                    \
		                             ,minor                                    \
		                             ,patchlevel                               \
		                             )
#else
#	define REQUIRE_LATER_COMPILER_VERSION(a, b, c) false
#endif

#endif // JORDAN_CONFIGURATION_HPP_
