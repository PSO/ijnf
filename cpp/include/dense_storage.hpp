#ifndef JORDAN_DENSE_STORAGE_HPP_
#define JORDAN_DENSE_STORAGE_HPP_

#include <cstddef>
#include <memory>
#include <cassert>
#include <algorithm>

template<class T, class Allocator = std::allocator<T>>
class dense_table {
private:
	class IData;
public:
	using size_t           = std::size_t;
	using type             = T;
	using allocator_type   =
		typename std::allocator_traits<Allocator>::template rebind_alloc<type>;
	using allocator_traits = std::allocator_traits<allocator_type>;
	using pointer          = typename allocator_traits::pointer;
	using const_pointer    = typename allocator_traits::const_pointer;
	using reference        = type&;
	using const_reference  = const type&;
	using iterator         = pointer;
	using const_iterator   = const_pointer;

	/**
	 * Allocate a dense two dimensional table without constructing its elements.
	 * The table's elements must be built or released before the deleter's call!
	 */
	template<class AllocArg>
	dense_table(size_t hg, size_t wd, AllocArg&& alloc)
	noexcept(false)
	: internal_data(hg, wd, std::forward<AllocArg>(alloc)) {}
	
	/**
	 * Constructs a dense two dimensional table and affects v to each of its
	 * elements.
	 */
	template<class U, class AllocArg>
	dense_table(size_t hg, size_t wd, U&& v, AllocArg&& alloc)
	noexcept(false)
	: internal_data
		  (hg, wd, std::forward<U>(v), std::forward<AllocArg>(alloc)) {}
	
	/**
	 * Constructs a dense two dimensional table and affects f(args...) to each
	 * of its elements in increasing lexicographic order:
	 * (i, j) < (i', j') <=> i < i' || (i == i' && j < j')
	 */
	template<class AllocArg, class Functor, class... Args>
	dense_table
		(size_t hg
		,size_t wd
		,AllocArg&& alloc
		,Functor&& f
		,Args&&... args)
	noexcept(false)
	: internal_data(hg, wd, std::forward<AllocArg>(alloc)) {
		try {
			inplace_build
				(begin()
				,hg
				,wd
				,allocator()
				,std::forward<Functor>(f)
				,std::forward<Args>(args)...
				);
		} catch(...) { internal_data.release(); throw; }
	}
	
	template<class U, class AllocArg>
	dense_table
		(std::initializer_list<std::initializer_list<U>> array
		,AllocArg&& alloc
		)
	noexcept(false)
	: internal_data(array, std::forward<AllocArg>(alloc)) {}
	
	template<class U, class AllocArg>
	dense_table
		(size_t hg
		,size_t wd
		,std::initializer_list<U> array
		,AllocArg&& alloc
		)
	noexcept(false)
	: internal_data(hg, wd, array, std::forward<AllocArg>(alloc)) {}
	
	template<class U, class AllocArg>
	dense_table(std::initializer_list<U> array, AllocArg&& alloc)
	noexcept(false)
	: internal_data(array, std::forward<AllocArg>(alloc)) {}
	
	/**
	 * Size accessors.
	 */
	size_t width()  const noexcept { return internal_data.width(); }
	size_t height() const noexcept { return internal_data.height(); }
	size_t size()   const noexcept { return internal_data.size(); }
	
	/**
	 * Allocator accessors.
	 */
	const allocator_type& allocator() const noexcept
		{ return mutable_self().allocator(); }
	allocator_type& allocator()       noexcept
		{ return internal_data.allocator(); }
	
	/**
	 * Returns iterators allowing to iterate through the table's elements in
	 * increasing lexicographic order.
	 */
	const_iterator cbegin() const noexcept { return mutable_self().begin(); }
	const_iterator cend()   const noexcept { return mutable_self().end(); }
	const_iterator begin()  const noexcept { return cbegin(); }
	const_iterator end()    const noexcept { return cend(); }
	iterator begin()        noexcept { return internal_data.begin(); }
	iterator end()          noexcept { return internal_data.end(); }
	
	/**
	 * Returns iterators allowing to iterate through the elements of the
	 * table's rows.
	 */
	iterator row_cbegin(size_t i) const noexcept
		{ return mutable_self().row_begin(i); }
	iterator row_cend(size_t i)   const noexcept
		{ return mutable_self().row_end(i); }
	iterator row_begin(size_t i)  const noexcept { return row_cbegin(i); }
	iterator row_end(size_t i)    const noexcept { return row_cend(i); }
	iterator row_begin(size_t i)  noexcept
		{ assert(i < height()); return begin() + i * width(); }
	iterator row_end(size_t i)    noexcept
		{ assert(i < height()); return row_begin(i) + width(); }
	
	/**
	 * Returns the element at coordinates (i, j)
	 */
	const_reference cat(size_t i, size_t j) const noexcept
		{ return mutable_self().at(i, j); }
	const_reference at(size_t i, size_t j)  const noexcept { return cat(i, j); }
	reference at(size_t i, size_t j) noexcept
		{ assert(i < height() && j < width()); return begin()[i* width() + j]; }
	
	template<class Functor, class... Args>
	static void inplace_build
		(pointer dst
		,size_t hg
		,size_t wd
		,allocator_type& allocator
		,Functor&& f
		,Args&&... args
		)
	noexcept(noexcept(
		allocator_traits::construct
			(allocator
			,pointer()
			,std::forward<Functor>(f)(std::forward<Args>(args)...)
			)
	)) {
		pointer cell = dst;
		try {
			const_pointer past_the_end = cell + (wd * hg);
			for( ; cell != past_the_end ; ++cell)
				allocator_traits::construct
					(allocator
					,cell
					,std::forward<Functor>(f)(std::forward<Args>(args)...)
					);
		} catch(...) {
			for( ; dst < cell ; ++dst)
				allocator_traits::destroy(allocator, dst);
			throw;
		}
	}
	
	/**
	 * Destroys each of the table's elements.
	 * The table's elements must be built or released before the deleter's call!
	 */
	void destroy() noexcept { internal_data.destroy(); }
	
	/**
	 * Deallocate the table's memory.
	 * The table's elements must have been previously destroyed before calling
	 * this method!
	 */
	void release() noexcept { internal_data.release(); }
	
private:
	explicit dense_table(IData&& internal_data_arg) noexcept
	: internal_data(std::move(internal_data_arg)) {}
	
	dense_table& mutable_self() const noexcept {
		return const_cast<dense_table&>(*this);
	}
	
	class IData : allocator_type {
	public:
		IData(const IData& other) noexcept(false)
		: IData
			(other.height()
			,other.width()
			,allocator_traits::select_on_container_copy_construction
				 (other.allocator())
			) {
			copy_construction(other);
		}
		
		template<class U, class AllocArg>
		IData
			(const std::initializer_list<std::initializer_list<U>>& array
			,AllocArg&& alloc
			)
		noexcept(false)
		: allocator_type(std::forward<AllocArg>(alloc))
		, hg(0)
		, wd(0)
		, coefficient(nullptr) {
			size_t hg_tmp = array.size();
			size_t wd_tmp = 0;
			for(const std::initializer_list<U>& row : array) {
				assert(wd_tmp == row.size() || wd_tmp == 0);
				wd_tmp = std::max(wd_tmp, row.size());
			}
			acquire(hg_tmp, wd_tmp);
			iterator dst = begin();
			try {
				for(const std::initializer_list<U>& row : array)
					for(const U& x : row) {
						allocator_traits::construct(allocator(), dst, x);
						++dst;
					}
			} catch(...) { destroy_until(dst); release(); throw; }
		}
		
		template<class U, class AllocArg>
		IData
			(size_t hg_arg
			,size_t wd_arg
			,const std::initializer_list<U>& array
			,AllocArg&& alloc
			)
		noexcept(false)
		: IData(hg_arg, wd_arg, std::forward<AllocArg>(alloc))
			{ copy_from_range(array.begin(), array.end()); }
		
		template<class U, class AllocArg>
		IData(const std::initializer_list<U>& array, AllocArg&& alloc)
		noexcept(false)
		: IData(1, array.size(), std::forward<AllocArg>(alloc))
		{ copy_from_range(array.begin(), array.end()); }
		
		IData(IData&& other) noexcept
		: allocator_type(std::move(other.allocator()))
		, hg(other.height())
		, wd(other.width())
		, coefficient(other.begin()) { other.width() = 0; }
		
		template<class AllocArg>
		IData(size_t hg_arg, size_t wd_arg, AllocArg&& alloc) noexcept(false)
		: allocator_type(std::forward<AllocArg>(alloc))
		, hg(0)
		, wd(0)
		, coefficient(nullptr) { acquire(hg_arg, wd_arg); }
		
		template<class U, class AllocArg>
		IData(size_t hg_arg, size_t wd_arg, U&& v, AllocArg&& alloc)
		noexcept(false)
		: IData(hg_arg, wd_arg, std::forward<AllocArg>(alloc))
			{ construct(std::forward<U>(v)); }
		
		size_t&   width()  noexcept { return wd; };
		size_t&   height() noexcept { return hg; };
		size_t    width()  const noexcept { return wd; };
		size_t    height() const noexcept { return hg; };
		size_t    size()   const noexcept { return width() * height(); }
		iterator& begin()  noexcept { return coefficient; }
		iterator  end()    noexcept { return coefficient + size(); }
		const_iterator begin() const noexcept { return coefficient; }
		const_iterator end()   const noexcept { return coefficient + size(); }
		
		void resize(size_t hg_arg, size_t wd_arg) {
			if(size() != wd_arg * hg_arg) {
				release();
				acquire(hg_arg, wd_arg);
			}
			else { width() = wd_arg; height() = hg_arg; }
		}
		
		IData& operator=(const IData& other) noexcept(false) {
			destroy();
			if(allocator_traits::propagate_on_container_copy_assignment::value
#ifdef CXX17
			   && !allocator_traits::is_always_equal::value
#endif //CXX17
			   && allocator() != other.allocator()) {
				release();
				allocator() = other.allocator();
				acquire(other.width(), other.height());
			} else resize(other.height(), other.width());
			copy_construction(other);
			return *this;
		}
		
		IData& operator=(IData&& other)
		noexcept(
			(allocator_traits::propagate_on_container_move_assignment::value
			 && noexcept(
				    std::declval<allocator_type&>() =
					    std::move_if_noexcept(std::declval<allocator_type&>())
			    )
			 )
#ifdef CXX17
			|| allocator_traits::is_always_equal::value
#endif //CXX17
		) {
#ifdef CXX17
			if(allocator_traits::is_always_equal::value
			   || allocator() == other.allocator()) {
				using std::swap;
				swap(width(), other.width());
				swap(height(), other.height());
				swap(begin(), other.begin());
			} else
#endif //CXX17
			if(allocator_traits::propagate_on_container_move_assignment::value
			  ) {
				destroy(); release();
				allocator() = std::move_if_noexcept(other.allocator());
				width()     = other.width();
				height()    = other.height();
				begin()     = other.begin();
				other.width() = 0;
			} else {
				destroy(); resize(other.height(), other.width());
				pointer it1 = begin();
				try {
					const_pointer past_the_end = end();
					for(const_pointer it2 = other.begin()
					   ;it1 != past_the_end
					   ;++it1, ++it2)
						allocator_traits::construct
							(allocator(), it1, std::move_if_noexcept(*it2));
				} catch(...) { destroy_until(it1); release(); throw; }
			}
			return *this;
		}
		
		void swap(IData& other)
		noexcept(allocator_traits::propagate_on_container_swap::value
#ifdef CXX17
		         || allocator_traits::is_always_equal::value
#endif //CXX17
		) {
			if(noexcept(allocator_traits::propagate_on_container_swap::value)) {
				using std::swap;
				swap(allocator(), other.allocator());
				swap(width(), other.width());
				swap(height(), other.height());
				swap(begin(), other.begin());
				return;
			}
#ifdef CXX17
			assert(allocator_traits::is_always_equal::value
			       || allocator() == other.allocator());
#else
			assert(allocator() == other.allocator());
#endif //CXX17
			swap(width(), other.width());
			swap(height(), other.height());
			swap(coefficient, other.coefficient);
		}
		
		void acquire(size_t hg_arg, size_t wd_arg) noexcept(false) {
			assert(width() == 0);
			width() = wd_arg; height() = hg_arg;
			if(size() != 0)
				begin() = allocator_traits::allocate(allocator(), size());
		}
		
		template<class U>
		void construct(U&& v)
		noexcept(noexcept(
			allocator_traits::construct
				(std::declval<allocator_type&>(), pointer(), v)
		)) {
			iterator it = begin();
			try {
				for(const_iterator past_the_end = end()
				   ;it != past_the_end
				   ;++it)
					allocator_traits::construct(allocator(), it, v);
					// don't forward here !
			} catch(...) { destroy_until(it); release(); throw; }
		}
		
		void destroy_until(pointer past_the_end) noexcept {
			for(iterator it = begin() ; it != past_the_end ; ++it)
				allocator_traits::destroy(allocator(), it);
		}
		
		void destroy() noexcept { destroy_until(end()); }
		void release() noexcept {
			if(width() != 0)
				allocator_traits::deallocate(allocator(), begin(), size());
			width() = 0;
			height() = 0;
		}
		
		allocator_type& allocator() noexcept
			{ return static_cast<allocator_type&>(*this); }
		const allocator_type& allocator() const noexcept
			{ return static_cast<const allocator_type&>(*this); }
		~IData() { destroy(); release(); }
		
	private:
		template<class ConstIterator>
		void copy_from_range(ConstIterator lo, ConstIterator hi)
		noexcept(noexcept(
			allocator_traits::construct
				(std::declval<IData&>().allocator()
				,pointer()
				,*ConstIterator()
				)
		)) {
			pointer dst = begin();
			try {
				for( ; lo != hi ; ++dst, ++lo)
					allocator_traits::construct(allocator(), dst, *lo);
			} catch(...) { destroy_until(dst); release(); throw; }
		}
		
		
		void copy_construction(const IData& other)
		noexcept(noexcept(
			std::declval<IData&>().copy_from_range(other.begin(), other.end())
		)) {
			copy_from_range(other.begin(), other.end());
		}
		
		size_t hg, wd;
		pointer coefficient;
	} internal_data;
};

#endif // JORDAN_DENSE_STORAGE_HPP_
