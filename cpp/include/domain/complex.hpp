#ifndef JORDAN_COMPLEX_HPP_
#define JORDAN_COMPLEX_HPP_

#include <complex>
#include <cmath>

template<class T>
T square(T x) { return x * x; }

template<class T>
class cartesian_complex {
public:
	using type = T;
	
	constexpr cartesian_complex() = default;
	
	constexpr explicit cartesian_complex(type re_arg, type im_arg = type(0))
	: re(re_arg)
	, im(im_arg) {}
	
	template<class U>
	constexpr explicit cartesian_complex(const cartesian_complex<U>& other)
	: re(other.real())
	, im(other.imag()) {}
	
	constexpr bool operator==(const cartesian_complex& other) const noexcept {
		return real() == other.real() && imag() == other.imag();
	}
	
	constexpr bool operator!=(const cartesian_complex& other) const noexcept {
		return real() != other.real() || imag() != other.imag();
	}
	
	constexpr cartesian_complex operator+(const cartesian_complex& z) const {
		return cartesian_complex(real() + real(z), imag() + imag(z));
	}
	
	cartesian_complex& operator+=(const cartesian_complex& z) {
		return *this = *this + z;
	}
	
	constexpr cartesian_complex operator-(const cartesian_complex& z) const {
		return cartesian_complex(real() - real(z), imag() - imag(z));
	}
	
	cartesian_complex& operator-=(const cartesian_complex& z) {
		return *this = *this - z;
	}
	
	constexpr cartesian_complex operator*(const cartesian_complex& z) const {
		return cartesian_complex
			       (real() * real(z) - imag() * imag(z)
			       ,real() * imag(z) + imag() * real(z)
			       );
	}
	
	cartesian_complex& operator*=(const cartesian_complex& z) {
		return *this = *this * z;
	}
	
	cartesian_complex operator/(const cartesian_complex& rhs) const {
		const type r = rhs.norm();
		return cartesian_complex
		           ((real() * real(rhs) + imag() * imag(rhs)) / r
		           ,(real(rhs) * imag() - imag(rhs) * real()) / r
		           );
	}
	
	cartesian_complex& operator/=(const cartesian_complex& z) {
		return *this = *this / z;
	}
	
	constexpr cartesian_complex operator-() const {
		return cartesian_complex(-real(), -imag());
	}
	
	constexpr cartesian_complex conjugate() const {
		return cartesian_complex(real(), -imag());
	}
	
	constexpr type norm() const noexcept {
		return (square(real())) + square(imag());
	}
	
	constexpr type abs() const {
		using std::sqrt;
		return sqrt(norm());
	}
	
	constexpr type real_part() const noexcept { return re; }
	constexpr type imaginary_part() const noexcept { return im; }
	constexpr type real() const noexcept { return real_part(); }
	constexpr type imag() const noexcept { return imaginary_part(); }
	constexpr cartesian_complex conj() const noexcept { return conjugate(); }
	static constexpr type real(const cartesian_complex& z) noexcept
		{ return z.real(); }
	static constexpr type imag(const cartesian_complex& z) noexcept
		{ return z.imag(); }
	static constexpr cartesian_complex conj(const cartesian_complex& z) noexcept
		{ return z.conj(); }
	static constexpr type abs(const cartesian_complex& z)
		{ return z.abs(); }
	static constexpr cartesian_complex fma
		(const cartesian_complex& x
		,const cartesian_complex& y
		,const cartesian_complex& z
		) {
		return x * y + z;
	}
private:
	type re, im;
};

/*
template <>
class cartesian_complex<float> : std::complex<float> { using type = float; }
template <>
class cartesian_complex<double> : std::complex<double> { using type = double; }
template <>
class cartesian_complex<long double> : std::complex<long double>
	{ using type = long double; }
*/

template<class T>
constexpr auto real(const cartesian_complex<T>& z)
noexcept -> decltype(z.real()) {
	return z.real();
}

template<class T>
constexpr auto imag(const cartesian_complex<T>& z)
noexcept -> decltype(z.imag()) {
	return z.imag();
}

template<class T>
constexpr auto conj(const cartesian_complex<T>& z)
noexcept -> decltype(z.conj()) {
	return z.conj();
}

template<class T>
constexpr auto abs(const cartesian_complex<T>& z)
noexcept -> decltype(z.abs()) {
	return z.abs();
}

template<class T>
constexpr auto norm(const cartesian_complex<T>& z)
noexcept -> decltype(z.norm()) {
	return z.norm();
}

template<class T>
constexpr auto fma
	(const cartesian_complex<T>& x
	,const cartesian_complex<T>& y
	,const cartesian_complex<T>& z
	)
noexcept -> decltype(cartesian_complex<T>::fma(x, y, z)) {
	return cartesian_complex<T>::fma(x, y, z);
}

#include <iostream>

template<class T>
std::ostream& operator<<(std::ostream& o, const cartesian_complex<T>& z) {
	return o << '(' << real(z) << " + " << imag(z) << "i)";
}

template<class T>
cartesian_complex<T> sqrt(const cartesian_complex<T>& z) {
	using std::sqrt;
	if(imag(z) == T(0))
		return real(z)>0
			? cartesian_complex<T>(sqrt(real(z)))
			: cartesian_complex<T>(0, sqrt(-real(z)));
	if(real(z) == T(0)) {
		auto a = sqrt(abs(imag(z)/2));
		return cartesian_complex<T>(a, imag(z) > T(0) ? a : -a);
	}
	auto a = sqrt((abs(z)+real(z))/2);
	auto b = sqrt((abs(z)-real(z))/2);
	return cartesian_complex<T>(a, imag(z) > T(0) ? b : -b);
}

#endif // JORDAN_COMPLEX_HPP_
