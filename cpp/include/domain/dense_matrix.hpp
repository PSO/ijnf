#ifndef JORDAN_DENSE_MATRIX_HPP_
#define JORDAN_DENSE_MATRIX_HPP_

#include "traits/field_traits.hpp"
#include "dense_storage.hpp"
#include "common.hpp"
#include <cassert>
#include <memory>
#include <algorithm>

template <class T>
T sum(T hd) { return hd; }

template <class T, class... U>
T sum(T hd, U... tl) { return hd + sum(tl...); }

template<class Field, class Allocator>
class polynomial;

template
	<class Field
	,class Allocator = std::allocator<typename field_traits<Field>::type>
	>
class dense_matrix {
public:
	using coefficient_traits = field_traits<Field>;
	using set_type         = typename coefficient_traits::set_type;
	using type             = typename coefficient_traits::type;
	using storage_type     = dense_table<type, Allocator>;
	using allocator_type   = typename storage_type::allocator_type;
	using allocator_traits = typename storage_type::allocator_traits;
	using size_t           = typename storage_type::size_t;
	using pointer          = typename storage_type::pointer;
	using const_pointer    = typename storage_type::const_pointer;
	using reference        = typename storage_type::reference;
	using const_reference  = typename storage_type::const_reference;
	using iterator         = typename storage_type::iterator;
	using const_iterator   = typename storage_type::const_iterator;
	
private:
	/**
	 * Puts this matrix in reduced row echelon form.
	 */
	template<bool OutputTransformation>
	dense_matrix& inplace_reduced_row_echelon_form_aux
		(dense_matrix* transformation = nullptr)
	noexcept(false /*todo*/) {
		assert(!OutputTransformation || transformation != nullptr);
		if(OutputTransformation)
			*transformation = identity(height(), coefficient_structure());
		size_t first_pivot_candidate = 0;
		const size_t min_dim = std::min(height(), width());
		for(size_t row = 0, col = 0 ; row < min_dim && col < width() ; ++row, ++col) {
			size_t pivot;
			do {
				for(pivot = first_pivot_candidate
				   ;pivot < height()
				    && coefficient_traits::is_zero
					       (coefficient_structure(), cat(pivot, col))
				   ;++pivot
				   );
				if(pivot >= height()) { ++col; }
			} while(pivot >= height() && col < width());
			if(pivot != height()) {
				{	const auto alpha =
						coefficient_traits::inverse
							(coefficient_structure(), cat(pivot, col));
					row_scale(alpha, pivot, col + 1);
					swap_row(pivot, first_pivot_candidate);
					if(OutputTransformation) {
						transformation->row_scale(alpha, pivot);
						transformation->swap_row(pivot, first_pivot_candidate);
					}
				}
				pivot = first_pivot_candidate++;
				at(pivot, col) =
					coefficient_traits::one(coefficient_structure());
				for(size_t eliminated_row = 0
				   ;eliminated_row < height()
				   ;++eliminated_row
				   ) {
					if(eliminated_row == pivot) continue;
					const auto alpha = cat(eliminated_row, col);
					if(coefficient_traits::is_zero
						   (coefficient_structure(), alpha)
					  )
						continue;
					row_axmy(alpha, pivot, eliminated_row, col + 1);
					at(eliminated_row, col) =
						coefficient_traits::zero(coefficient_structure());
					if(OutputTransformation)
						transformation->row_axmy(alpha, pivot, eliminated_row);
				}
			}
		}
		return *this;
	}


	template
		<class Structure
		,class =
			 typename std::enable_if
				 <decays_to<Structure, set_type>::value>::type
		>
	dense_matrix(Structure&& s, storage_type&& internal) noexcept
	: data(std::forward<Structure>(s), std::move(internal)) {}

	template<class CustomAllocator>
	void transpose_src(pointer dst, CustomAllocator& alloc)
	const noexcept(noexcept(
		std::allocator_traits
			<typename std::allocator_traits<CustomAllocator>::template
				 rebind_alloc<type>
			>::construct(alloc, pointer(), std::declval<const_reference>())
	)) {
		using custom_allocator_type   =
			typename std::allocator_traits<CustomAllocator>::template
				rebind_alloc<type>;
		using custom_allocator_traits =
			std::allocator_traits<custom_allocator_type>;
		size_t i, j;
		try {
			for(i = 0 ; i < height() ; ++i)
				for(j = 0 ; j < width() ; ++j)
					custom_allocator_traits::construct
						(alloc, dst + (j * height() + i), cat(i, j));
		} catch(...) {
			const size_t lasti = i, lastj = j;
			for(i = 0 ; i < lasti ; ++i)
				for(j = 0; j < width() ; ++j)
					custom_allocator_traits::destroy
						(alloc, dst + (j * width() + i));
			for(j = 0 ; j < lastj ; ++j)
				custom_allocator_traits::destroy
					(alloc, dst + (j * width() + lasti));
			throw;
		}
	}

	template
		<class CustomSemiring
		,class CustomAllocator
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto inplace_multiply
		(typename semiring_traits<CustomSemiring>::type* dst
		,const dense_matrix<RightField, RightAllocator>& right
		,CustomAllocator& alloc
		,CustomStructure&& s
		)
	const noexcept(
		noexcept(
			std::allocator_traits
				<typename std::allocator_traits<CustomAllocator>::template
					 rebind_alloc<type>
				>::construct(alloc, pointer(), std::declval<const_reference>())
		)
		&& noexcept(
			   semiring_traits<CustomSemiring>::add
				   (s
				   ,semiring_traits<CustomSemiring>::multiply
					   (s
					   ,std::declval<const_reference>()
					   ,std::declval
						    <typename dense_matrix
							     <RightField, RightAllocator>::const_reference
						    >()
					   )
				   ,std::declval<typename coefficient_traits::type&>()
				   )
		   )
	) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename semiring_traits<CustomSemiring>::set_type
			 >::value
		,void
		>::type {
		assert(width() == right.height());
		if(width() == 0)
			return;
		using custom_type = typename semiring_traits<CustomSemiring>::type;
		using custom_allocator_type   =
			typename std::allocator_traits<CustomAllocator>::template
				rebind_alloc<custom_type>;
		using custom_allocator_traits =
			std::allocator_traits<custom_allocator_type>;
		const auto transposed_right = right.transpose();
		size_t i, j;
		try {
			for(i = 0 ; i < height() ; ++i)
				for(j = 0 ; j < right.width() ; ++j) {
					custom_allocator_traits::construct
						(alloc
						,dst + (i * right.width() + j)
						,semiring_traits<CustomSemiring>::multiply
							 (s
							 ,at(i, 0)
							 ,transposed_right.at(j, 0)
							 )
						);
					for(size_t k = 1 ; k < width() ; ++k)
						dst[i * right.width() + j] =
							semiring_traits<CustomSemiring>::add
								(s
								,semiring_traits<CustomSemiring>::multiply
									(s
									,at(i, k)
									,transposed_right.at(j, k)
									)
								,dst[i * right.width() + j]
								);
				}
		} catch(...) {
			const size_t lasti = i, lastj = j;
			for(i = 0 ; i < lasti ; ++i)
				for(j = 0 ; j < right.width() ; ++j)
					custom_allocator_traits::destroy
						(alloc, dst + (i * right.width() + j));
			for(j = 0 ; j < lastj ; ++j)
				custom_allocator_traits::destroy
					(alloc, dst + (lasti * right.width() + j));
			throw;
		}
	}
public:
	
	template
		<class CustomField
		,class CustomAllocator = allocator_type
		>
	using rebind_alloc =
		typename std::allocator_traits<CustomAllocator>::
			template rebind_alloc<typename field_traits<CustomField>::type>;
	
	template
		<class CustomField
		,class CustomAllocator = rebind_alloc<CustomField>
		>
	using rebind =
		dense_matrix<CustomField, typename std::decay<CustomAllocator>::type>;
	
	dense_matrix() noexcept(false)
	: dense_matrix(0, 0) {}
	
	dense_matrix(size_t hg, size_t wd) noexcept(false)
	: data(set_type(), hg, wd, coefficient_traits::zero(set_type()), Allocator()) {}
	
	template<class T, class = decltype(type(std::declval<const T&>()))>
	dense_matrix(size_t hg, size_t wd, const T& v) noexcept(false)
	: data(set_type(), hg, wd, v, Allocator()) {}
	
	dense_matrix(size_t hg, size_t wd, const_pointer src) noexcept(false)
	: data(set_type()
	      ,hg
	      ,wd
	      ,Allocator()
	      ,[](const_pointer& ptr) { return *ptr++; }
	      ,src
	      )
	{}
	
	template<class T>
	dense_matrix(std::initializer_list<std::initializer_list<T>> array)
	noexcept(false)
	: data(set_type(), array, Allocator()) {}
	
	template<class T>
	dense_matrix(size_t hg, size_t wd, std::initializer_list<T> array)
	noexcept(false)
	: data(set_type(), hg, wd, array, Allocator()) {}
	
	template<class T>
	dense_matrix(std::initializer_list<T> array)
	noexcept(false)
	: data(set_type(), array, Allocator()) {}
	
	const set_type& coefficient_structure() const noexcept
		{ return static_cast<const set_type&>(data); }
	
	/**
	 * Size accessors.
	 */
	size_t width()  const noexcept { return data.content.width(); }
	size_t height() const noexcept { return data.content.height(); }
	
	/**
	 * Allocator accessors.
	 */
	const allocator_type& allocator() const noexcept
		{ return data.content.allocator(); }
	allocator_type& allocator()       noexcept
		{ return data.content.allocator(); }
	
	/**
	 * Returns iterators allowing to iterate through the table's elements in
	 * increasing lexicographic order.
	 */
	const_iterator cbegin() const noexcept { return data.content.cbegin(); }
	const_iterator cend()   const noexcept { return data.content.cend(); }
	const_iterator begin()  const noexcept { return data.content.begin(); }
	const_iterator end()    const noexcept { return data.content.end(); }
	iterator begin()        noexcept { return data.content.begin(); }
	iterator end()          noexcept { return data.content.end(); }
	
	/**
	 * Returns iterators allowing to iterate through the elements of the
	 * table's rows.
	 */
	iterator row_cbegin(size_t i) const noexcept
		{ return data.content.row_cbegin(i); }
	iterator row_cend(size_t i)   const noexcept
		{ return data.content.row_cend(i); }
	iterator row_begin(size_t i)  const noexcept
		{ return data.content.row_begin(i); }
	iterator row_end(size_t i)    const noexcept
		{ return data.content.row_end(i); }
	iterator row_begin(size_t i)  noexcept
		{ return data.content.row_begin(i); }
	iterator row_end(size_t i)    noexcept
		{ return data.content.row_end(i); }

	
	/**
	 * Returns the element at coordinates (i, j)
	 */
	const_reference cat(size_t i, size_t j) const noexcept
		{ return data.content.cat(i, j); }
	const_reference at(size_t i, size_t j)  const noexcept
		{ return data.content.at(i, j); }
	reference at(size_t i, size_t j) noexcept
		{ return data.content.at(i, j); }
	
	/**
	 * Returns the transposed matrix.
	 */
	template<class CustomAllocator>
	rebind<Field, CustomAllocator> transpose(CustomAllocator&& alloc)
	const noexcept(false) {
		using custom_allocator_type =
			typename std::decay<CustomAllocator>::type;
		using custom_storage_type = dense_table<type, custom_allocator_type>;
		custom_storage_type transposed_table
			(width(), height(), std::forward<CustomAllocator>(alloc));
		try {
			transpose_src
				(transposed_table.begin(), transposed_table.allocator());
		} catch(...) { transposed_table.release(); throw; }
		return dense_matrix<Field, custom_allocator_type>
			       (coefficient_structure(), std::move(transposed_table));
	}

	dense_matrix transpose() const noexcept(false) {
		return transpose
			       (allocator_traits::select_on_container_copy_construction
				        (allocator())
			       );
	}
	
	dense_matrix kernel() const noexcept(false) {
		assert(width() == height());
		return identity(width()).substract(left_projector());
	}
	
	size_t pick_nonzero_column_id() const {
		std::size_t col;
		for(std::size_t i = 0 ; i < height() ; ++i)
			for(col = 0 ; col < width() ; ++col)
				if(!coefficient_traits::is_zero(coefficient_structure(), at(i, col)))
					{ i = height(); break; }
		return col;
	}
	
	
	dense_matrix& fill_with(size_t) {
		return *this;
	}
	
	template<class... Args>
	dense_matrix& fill_with(size_t col_dst, const dense_matrix& hd, Args&&... args) {
		size_t dst_j;
		for(size_t i = 0 ; i < hd.height() ; ++i) {
			dst_j = col_dst;
			for(size_t j = 0 ; j < hd.width() ; ++j, ++dst_j)
				at(i, dst_j) = hd.at(i, j);
		}
		return fill_with(dst_j, std::forward<Args>(args)...);
	}
	
	dense_matrix column(size_t j) const noexcept(false) {
		dense_matrix result(height(), 1);
		for(size_t i = 0 ; i < height() ; ++i)
			result.at(i, 0) = at(i, j);
		return result;
	}
	
	dense_matrix crop(size_t sh, size_t sw, size_t eh, size_t ew) const {
		assert(eh <= height());
		assert(ew <= width());
		assert(eh > sh);
		assert(ew > sw);
		dense_matrix cropped(eh - sh, ew - sw);
		for(size_t dst_i = 0, src_i = sh ; src_i < eh ; ++src_i, ++dst_i)
			for(size_t dst_j = 0, src_j = sw ; src_j < ew ; ++src_j, ++dst_j)
				cropped.at(dst_i, dst_j) = at(src_i, src_j);
		return cropped;
	}
	
	dense_matrix exclude() const& { return *this; }
	dense_matrix& exclude() & { return *this; }
	dense_matrix&& exclude() && { return std::move(*this); }
	
	template<class... Args>
	dense_matrix exclude(const dense_matrix& basis, Args&&... args) {
		assert(height() == width());
		assert(basis.height() == height());
		const size_t w = sum(basis.width(), args.width()...);
		dense_matrix large_family(basis.height(), w);
		large_family.fill_with(0, basis, std::forward<Args>(args)...);
		dense_matrix proj =
			large_family.projector().crop(0, 0, width(), width());
		return identity(width()).substract(proj).multiply(*this);
	}
	
	template<class... Args>
	bool pick_kernel_vector(dense_matrix& output, Args&&... excluding)
	const noexcept(false) {
		dense_matrix ker = kernel();
		const size_t i =
			ker.exclude(std::forward<Args>(excluding)...)
				.pick_nonzero_column_id();
		if(i == width())
			return false;
		output = ker.column(i);
		return true;
	}
	
	static void intersection_basis(const dense_matrix& u, const dense_matrix& v)
	{
		assert(u.height() == v.height());
		dense_matrix large_family(u.height(), u.width() + v.width());
		large_family.fill_with(0, u, v);
		dense_matrix ker = large_family.kernel();
		return u.multiply(ker.crop(0, 0, u.width(), ker.width()));
	}
	
	static void pick_intersection_vector
		(dense_matrix& output, const dense_matrix& u, const dense_matrix& v)
	{
		assert(u.height() == v.height());
		dense_matrix large_family(u.height(), u.width() + v.width());
		large_family.fill_with(0, u, v);
		large_family.pick_kernel_vector(output);
		output = u.multiply(output.crop(0, 0, u.width(), 1));
	}
	
	template<class CustomField, class CastFunctor>
	rebind<CustomField> cast_with(CastFunctor&& f) const noexcept(false) {
		rebind<CustomField> result(width(), width());
		auto it = result.begin();
		for(const auto& c : data.content) {
			*it = std::forward<CastFunctor>(f)(c);
			++it;
		}
		return result;
	}
	
	/**
	 * Returns the product of two matrices.
	 */
	template
		<class CustomSemiring
		,class CustomAllocator
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto multiply
		(const dense_matrix<RightField, RightAllocator>& right
		,CustomAllocator&& alloc
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename semiring_traits<CustomSemiring>::set_type
			 >::value
		,rebind<CustomSemiring, CustomAllocator>
		>::type {
		using custom_allocator_type =
			typename std::decay<CustomAllocator>::type;
		using custom_type = typename semiring_traits<CustomSemiring>::type;
		using custom_storage_type =
			dense_table<custom_type, custom_allocator_type>;
		custom_storage_type result
			(height(), right.width(), std::forward<CustomAllocator>(alloc));
		try {
			inplace_multiply<CustomSemiring>
				(result.begin(), right, result.allocator(), s);
		} catch(...) { result.release(); throw; }
		return rebind<CustomSemiring, CustomAllocator>
			       (std::forward<CustomStructure>(s), std::move(result));
	}
	
	template
		<class CustomSemiring
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto multiply
		(const dense_matrix<RightField, RightAllocator>& right
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename semiring_traits<CustomSemiring>::set_type
			 >::value
		,rebind<CustomSemiring>
		>::type {
		return multiply<CustomSemiring>
			       (right
			       ,allocator_traits::select_on_container_copy_construction
				        (allocator())
			       ,std::forward<CustomStructure>(s)
			       );
	}
	
	dense_matrix<Field, allocator_type> multiply
		(const dense_matrix<Field, allocator_type>& right)
	const noexcept(false)
		{ return multiply<Field>(right, coefficient_structure()); }
	
	/**
	 * Returns the sum of two matrices.
	 */
	template
		<class CustomSemiring
		,class CustomAllocator
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto add
		(const dense_matrix<RightField, RightAllocator>& right
		,CustomAllocator&& alloc
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename semiring_traits<CustomSemiring>::set_type
			 >::value
		,rebind<CustomSemiring, CustomAllocator>
		>::type {
		assert(width() == right.width() && height() == right.height());
		using custom_allocator_type =
			typename std::decay<CustomAllocator>::type;
		using custom_type = typename semiring_traits<CustomSemiring>::type;
		using custom_storage_type =
			dense_table<custom_type, custom_allocator_type>;
		auto add =
			[](CustomStructure& customs
			  ,const_iterator&& left_coeff
			  ,const_iterator&& right_coeff
			  ) {
				return semiring_traits<CustomSemiring>::add
					       (customs, *(left_coeff++), *(right_coeff++));
			};
		return dense_matrix
			       (std::forward<CustomStructure>(s)
			       ,custom_storage_type
				        (height()
				        ,width()
				        ,std::forward<CustomAllocator>(alloc)
				        ,add
				        ,s
				        ,cbegin()
				        ,right.cbegin()
				        )
			       );
	}
	
	template
		<class CustomSemiring
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto add
		(const dense_matrix<RightField, RightAllocator>& right
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename semiring_traits<CustomSemiring>::set_type
			 >::value
		,rebind<CustomSemiring>
		>::type {
		return add<CustomSemiring>
			       (right
			       ,allocator_traits::select_on_container_copy_construction
				        (allocator())
			       ,std::forward<CustomStructure>(s)
			       );
	}
	
	dense_matrix add(const dense_matrix<Field, allocator_type>& right)
	const noexcept(false) { return add<Field>(right, coefficient_structure()); }
	
	/**
	 * Returns the difference of two matrices.
	 */
	template
		<class CustomRing
		,class CustomAllocator
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto substract
		(const dense_matrix<RightField, RightAllocator>& right
		,CustomAllocator&& alloc
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename ring_traits<CustomRing>::set_type
			 >::value
		,rebind<CustomRing, CustomAllocator>
		>::type {
		assert(width() == right.width() && height() == right.height());
		using custom_allocator_type =
			typename std::decay<CustomAllocator>::type;
		using custom_type = typename ring_traits<CustomRing>::type;
		using custom_storage_type =
		dense_table<custom_type, custom_allocator_type>;
		auto substract =
			[](CustomStructure& customs
			  ,const_iterator&& left_coeff
			  ,const_iterator&& right_coeff
			  ) {
			return ring_traits<CustomRing>::substract
				       (customs, *(left_coeff++), *(right_coeff++));
			};
		return dense_matrix
			      (std::forward<CustomStructure>(s)
			      ,custom_storage_type
				       (height()
				       ,width()
				       ,std::forward<CustomAllocator>(alloc)
				       ,substract
				       ,s
				       ,cbegin()
				       ,right.cbegin()
				       )
			      );
	}
	
	template
		<class CustomRing
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto substract
		(const dense_matrix<RightField, RightAllocator>& right
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename ring_traits<CustomRing>::set_type
			 >::value
		,rebind<CustomRing>
		>::type {
		return substract<CustomRing>
			       (right
			       ,allocator_traits::select_on_container_copy_construction
				        (allocator())
			       ,std::forward<CustomStructure>(s)
			       );
	}
	
	dense_matrix substract(const dense_matrix<Field, allocator_type>& right)
	const noexcept(false)
		{ return substract<Field>(right, coefficient_structure()); }
	
	/**
	 * Returns alpha*X + Y where alpha is a scalar and X and Y are two
	 * (hg,wd) matrices.
	 */
	template
		<class CustomSemiring
		,class CustomAllocator
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto axpy
		(const typename semiring_traits<CustomSemiring>::type& alpha
		,const dense_matrix<RightField, RightAllocator>& right
		,CustomAllocator&& alloc
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename semiring_traits<CustomSemiring>::set_type
			 >::value
		,rebind<CustomSemiring, CustomAllocator>
		>::type {
		assert(width() == right.width() && height() == right.height());
		using custom_allocator_type =
			typename std::decay<CustomAllocator>::type;
		using custom_type = typename semiring_traits<CustomSemiring>::type;
		using custom_storage_type =
			dense_table<custom_type, custom_allocator_type>;
		auto add =
			[](CustomStructure& customs
			  ,const custom_type& a
			  ,const_iterator&& left_coeff
			  ,const_iterator&& right_coeff
			  ) {
				return semiring_traits<CustomSemiring>::add
					       (customs
					       ,semiring_traits<CustomSemiring>::multiply
						        (customs, a, *(left_coeff++))
					       ,*(right_coeff++)
					       );
			};
		return dense_matrix
			       (std::forward<CustomStructure>(s)
			       ,custom_storage_type
				        (height()
				        ,width()
				        ,std::forward<CustomAllocator>(alloc)
				        ,add
				        ,s
				        ,alpha
				        ,cbegin()
				        ,right.cbegin()
				        )
			       );
	}
	
	template
		<class CustomSemiring
		,class RightField
		,class RightAllocator
		,class CustomStructure
		>
	auto axpy
		(const typename semiring_traits<CustomSemiring>::type& alpha
		,const dense_matrix<RightField, RightAllocator>& right
		,CustomStructure&& s
		)
	const noexcept(false) ->
	typename std::enable_if
		<decays_to
			 <CustomStructure
			 ,typename semiring_traits<CustomSemiring>::set_type
			 >::value
		,rebind<CustomSemiring>
		>::type {
		return axpy<CustomSemiring>
			       (alpha
			       ,right
			       ,allocator_traits::select_on_container_copy_construction
				        (allocator())
			       ,std::forward<CustomStructure>(s)
			       );
	}
	
	dense_matrix axpy(const_reference alpha, const dense_matrix& right)
	const noexcept(false)
		{ return axpy<Field>(alpha, right, coefficient_structure()); }
	
	void swap_row(size_t i, size_t j) noexcept(true /*todo*/) {
		if(i == j) return;
		using std::swap;
		const iterator past_the_end = row_end(i);
		for(iterator coef1 = row_begin(i), coef2 = row_begin(j)
		   ;coef1 != past_the_end
		   ;++coef1, ++coef2
		   )
			swap(*coef1, *coef2);
	}
	
	/**
	 * R(x) <- aR(x)
	 * starting at offset
	 */
	void row_scale(const_reference alpha, size_t x, size_t offset)
	noexcept(false /*todo*/) {
		const iterator past_the_end = row_end(x);
		for(iterator coef = row_begin(x) + offset
		   ;coef != past_the_end
		   ;++coef
		   )
			*coef = coefficient_traits::multiply
				        (coefficient_structure(), alpha, std::move(*coef));
	}
	
	
	void row_scale(const_reference alpha, size_t x)
	noexcept(noexcept(
		std::declval<dense_matrix&>().row_scale(alpha, x, size_t())
	))
		{ row_scale(alpha, x, size_t(0)); }
	
	/**
	 * R(y) <- R(y) - aR(x)
	 * starting at offset
	 */
	void row_axmy(const_reference alpha, size_t x, size_t y, size_t offset)
	noexcept(false /*todo*/) {
		const iterator past_the_end = row_end(x);
		for(iterator coefx = row_begin(x) + offset
		   ,coefy = row_begin(y) + offset
		   ;coefx != past_the_end
		   ;++coefx, ++coefy
		   )
			*coefy =
				coefficient_traits::substract
					(coefficient_structure()
					,std::move(*coefy)
					,coefficient_traits::multiply
						 (coefficient_structure(), alpha, *coefx)
					);
	}
	
	void row_axmy(const_reference alpha, size_t x, size_t y)
	noexcept(noexcept(
		std::declval<dense_matrix&>().row_axmy(alpha, x, y, size_t())
	)) { row_axmy(alpha, x, y, size_t(0)); }
	
	void row_axpy(const_reference alpha, size_t x, size_t y, size_t offset)
	noexcept(false /*todo*/) {
		const iterator past_the_end = row_end(x);
		for(iterator coefx = row_begin(x) + offset
		   ,coefy = row_begin(y) + offset
		   ;coefx != past_the_end
		   ;++coefx, ++coefy
		   )
			*coefy =
				coefficient_traits::add
					(coefficient_structure()
					,*coefy
					,coefficient_traits::multiply
						 (coefficient_structure(), alpha, std::move(*coefx))
					);
	}
	
	void row_axpy(const_reference alpha, size_t x, size_t y)
	noexcept(noexcept(
		std::declval<dense_matrix&>().row_axpy(alpha, x, y, size_t())
	)) { row_axpy(alpha, x, y, size_t(0)); }
	
	template<class Structure = set_type>
	static auto identity(size_t n, Structure&& s = set_type())
	noexcept(false) ->
	typename std::enable_if
		<decays_to<Structure, set_type>::value, dense_matrix>::type {
		dense_matrix id(n, n);
		for(size_t i = 0 ; i < n ; ++i)
			id.at(i, i) = coefficient_traits::one(std::forward<Structure>(s));
		return id;
	}
	
	/**
	 * Puts the matrix in reduced row echelon form.
	 * If given a reference to a matrix, also returns the left transformation
	 * matrix.
	 */
	dense_matrix& inplace_reduced_row_echelon_form()
	noexcept(noexcept(
		std::declval<dense_matrix&>()
			.template inplace_reduced_row_echelon_form_aux<false>()
	)) {
		return inplace_reduced_row_echelon_form_aux<false>();
	}
	
	dense_matrix& inplace_reduced_row_echelon_form(dense_matrix& transformation)
	noexcept(noexcept(
		std::declval<dense_matrix&>()
			.template inplace_reduced_row_echelon_form_aux<true>
				 (&transformation)
	)) {
		return inplace_reduced_row_echelon_form_aux<true>(&transformation);
	}
	
	/**
	 * Returns the reduced row echelon form of this matrix.
	 * If given a reference to a matrix, also returns the left transformation
	 * matrix.
	 */
	dense_matrix reduced_row_echelon_form() const & noexcept(false) {
		dense_matrix result(*this);
		result.inplace_reduced_row_echelon_form();
		return result;
	}
	
	dense_matrix reduced_row_echelon_form(dense_matrix& transformation)
	const & noexcept(false) {
		dense_matrix result(*this);
		result.inplace_reduced_row_echelon_form(transformation);
		return result;
	}
	
	dense_matrix&& reduced_row_echelon_form() && noexcept(false) {
		return std::move(inplace_reduced_row_echelon_form());
	}
	
	dense_matrix reduced_row_echelon_form(dense_matrix& transformation)
	&& noexcept(false) {
		return std::move(inplace_reduced_row_echelon_form(transformation));
	}
	
	/**
	 * Returns the matrix rank.
	 */
	size_t rank() const noexcept(false) {
		dense_matrix rref = reduced_row_echelon_form();
		size_t rank = 0, rank_bound = std::min(width(), height());
		const_iterator coef = rref.cbegin();
		const_iterator past_the_end = rref.row_cend(0);
		
		while(coef != past_the_end) {
			if(!coefficient_traits::is_zero(coefficient_structure(), *coef)) {// down one row
				if(++rank == rank_bound) return rank;
				coef         = coef + rref.width();
				past_the_end = past_the_end + rref.width();
			} else ++coef;
		}
		return rank;
	}
	
	/**
	 * Return's the matrix trace.
	 */
	type trace() const noexcept(false) {
		size_t min_dim = std::min(width(), height());
		if(min_dim == 0) return coefficient_traits::zero(coefficient_structure());
		type tr = *cbegin();
		for(size_t i = 1 ; i < min_dim ; ++i)
			tr = coefficient_traits::add(coefficient_structure(), std::move(tr), cat(i, i));
		return tr;
	}
	
	/**
	 * Transforms the matrix into a projection matrix on the image of the
	 * transposed matrix.
	 */
	dense_matrix& inplace_left_projector() noexcept(false) {
		inplace_reduced_row_echelon_form();
		const size_t min_dim = std::min(height(), width());
		for(size_t row = min_dim - 1 ; row != (size_t(0) - size_t(1)) ; --row)
			for(size_t col = row ; col < min_dim ; ++col)
				if(!coefficient_traits::is_zero(coefficient_structure(), at(row, col)))
					{ swap_row(row, col); break; }
		return *this;
	}
	
	/**
	 * Returns a projection matrix on the image of the transposed matrix.
	 */
	dense_matrix left_projector() const & noexcept(false) {
		dense_matrix result(*this);
		return result.inplace_left_projector();
	}
	
	dense_matrix&& left_projector() && noexcept(false) {
		return std::move(inplace_left_projector());
	}
	
	/**
	 * Returns a projection matrix on the image of the matrix.
	 */
	dense_matrix projector() const noexcept(false) {
		return transpose().left_projector().transpose();
	}
	
	/**
	 * Returns the characteristic polynomial.
	 * Currently uses Faddeev-LeVerrier's algorithm
	 */
	polynomial<Field, Allocator> characteristic_polynomial()
	const noexcept(false) {
		assert(width() == height());
		polynomial<Field, Allocator> charpoly(width());
		dense_matrix m(height(), width(), coefficient_traits::zero(coefficient_structure()));
		charpoly[width()] = coefficient_traits::one(coefficient_structure());
		for(size_t i = 0, k = width(); i < width() ; ) {
			for(size_t j = 0 ; j < width() ; ++j)
				m.at(j,j) =
					coefficient_traits::add(coefficient_structure(), m.at(j,j), charpoly[k]);
			m = multiply(m);
			--k, ++i;
			charpoly[k] =
				coefficient_traits::divide
					(coefficient_structure()
					,coefficient_traits::opposite(coefficient_structure(), m.trace())
					,type(i)
					);
		}
		return charpoly;
	}
	
private:
	struct internal_data : set_type {
		template<class... Args>
		internal_data(const set_type& s, Args&&... args)
		: set_type(s)
		, content(std::forward<Args>(args)...) {}

		storage_type content;
	} data;
};

template<class Matrix>
struct matrix_ring {
	using type = Matrix;
	
	matrix_ring(std::size_t n_arg) noexcept : n(n_arg) {}
	
	bool eq(const type& lhs, const type& rhs) const noexcept {
		return lhs == rhs;
	}
	
	template<class T>
	typename std::enable_if<std::is_same<T, type>::value, type>::type one()
	const {
		return type::identity(n);
	}
	
	template<class T>
	typename std::enable_if<std::is_same<T, type>::value, type>::type zero()
	const {
		return type(n, n, 0);
	}
	
	type add(const type& lhs, const type& rhs) const {
		return lhs.add(rhs);
	}
	
	type multiply(const type& lhs, const type& rhs) const {
		return lhs.multiply(rhs);
	}
	
	type opposite(const type& m) const {
		return -m;
	}
private:
	std::size_t n;
};

template<class Matrix>
struct matrix_set
: field_of<typename Matrix::type>
, matrix_ring<Matrix> {
	using scalar_type = typename field_of<typename Matrix::type>::type;
	using type = typename matrix_ring<Matrix>::type;
	
	matrix_set(std::size_t n_arg) noexcept
	: matrix_ring<Matrix>(n_arg) {}
	
	using field_of<typename Matrix::type>::one;
	using matrix_ring<Matrix>::one;
	type one() const { return matrix_set<Matrix>::template one<type>(); }
	
	using field_of<typename Matrix::type>::zero;
	using matrix_ring<Matrix>::zero;
	type zero() const { return matrix_set<Matrix>::template zero<type>(); }
	
	using matrix_ring<Matrix>::eq;
	using field_of<typename Matrix::type>::eq;
	
	using matrix_ring<Matrix>::multiply;
	using field_of<typename Matrix::type>::multiply;
	
	using matrix_ring<Matrix>::add;
	using field_of<typename Matrix::type>::add;
	
	using matrix_ring<Matrix>::opposite;
	using field_of<typename Matrix::type>::opposite;
	
	type multiply(const scalar_type& lhs, const type& rhs) const {
		throw 42;
		// return rhs.scale(lhs);
	}
	type axpy(const scalar_type& a, const type& x, const type& y) const {
		return x.axpy(a, y);
	}
};

#include <ostream>

template<class Field, class Allocator>
std::ostream& operator<<
	(std::ostream& o, const dense_matrix<Field, Allocator>& m)
noexcept(noexcept(false)) {
	using index_t = typename dense_matrix<Field, Allocator>::size_t;
	o << '[';
	for(index_t i = 0 ; i < m.height() ; ++i) {
		if(i != 0)
			o << ',';
		o << "\t[";
		auto coeff = m.row_cbegin(i);
		auto past_the_end = m.row_cend(i);
		if(coeff != past_the_end) {
			o << *coeff;
			for(++coeff ; coeff != past_the_end ; ++coeff)
				o << ", " << *coeff;
		}
		o << "]\n";
	}
	return o << ']';
}

#include "polynomial.hpp"

#endif
