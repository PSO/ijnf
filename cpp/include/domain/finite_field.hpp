#ifndef JORDAN_FINITE_FIELD_HPP_
#define JORDAN_FINITE_FIELD_HPP_

#include "lmpl/common_operators.hpp"
#include "lmpl/reduce.hpp"
#include "lmpl/map.hpp"
#include "lmpl/range.hpp"
#include "lmpl/constant.hpp"
#include "lmpl/repack.hpp"
#include <cstdint>
#include <iostream>

namespace detail_finite_field_hpp_ {
	template<std::uintmax_t LargestVal, class CandidateL, class = void>
	struct first_large_enough_uint_among_aux;
	
	template
		<std::uintmax_t LargestVal
		,template<class...> class List
		,class FirstChoice
		,class... SecondChoice
		>
	struct first_large_enough_uint_among_aux
		<LargestVal
		,List<FirstChoice, SecondChoice...>
		,typename std::enable_if
			 <std::numeric_limits<FirstChoice>::max() >= LargestVal>::type
		>
	: lmpl::type_constant<FirstChoice> {};
	
	template
		<std::uintmax_t LargestVal
		,template<class...> class List
		,class FirstChoice
		,class... SecondChoice
		>
	struct first_large_enough_uint_among_aux
		<LargestVal
		,List<FirstChoice, SecondChoice...>
		,typename std::enable_if
			 <std::numeric_limits<FirstChoice>::max() < LargestVal>::type
			 >
	: first_large_enough_uint_among_aux<LargestVal, List<SecondChoice...>> {};
	
	
	template<std::uintmax_t LargestVal, class CandidateL>
	struct first_large_enough_uint_among
	: first_large_enough_uint_among_aux<LargestVal, CandidateL> {};
	
	template<std::uintmax_t LargestVal>
	struct smallest_large_enough_uint
	: first_large_enough_uint_among
		  <LargestVal
		  ,lmpl::list
			   <std::uint_least8_t
			   ,std::uint_least16_t
			   ,std::uint_least32_t
			   ,std::uint_least64_t
			   ,std::uintmax_t
			   >
		  > {};
	
	template<std::uintmax_t LargestVal>
	struct fastest_large_enough_uint
	: first_large_enough_uint_among
		  <LargestVal
		  ,lmpl::list
			  <std::uint_fast8_t
			  ,std::uint_fast16_t
			  ,std::uint_fast32_t
			  ,std::uint_fast64_t
			  ,std::uintmax_t
			  >
		  > {};

	template<std::uintmax_t LargestVal>
	using smallest_large_enough_uint_t =
		typename smallest_large_enough_uint<LargestVal>::type;
	template<std::uintmax_t LargestVal>
	using fastest_large_enough_uint_t =
		typename smallest_large_enough_uint<LargestVal>::type;
	
	template<std::size_t N>
	struct not_divisible_by {
		template<class I>
		struct invoke : std::integral_constant<bool, (N % (I::value)) != 0> {};
	};
	
	template<std::size_t N>
	struct is_prime
	: lmpl::reduce_t
		  <lmpl::cat_t
			   <lmpl::map_t
				    <lmpl::range_t<lmpl::index_t<1>, lmpl::index_t<N>>
				    ,not_divisible_by<N>
				    >
			   ,lmpl::list<std::integral_constant<bool, N != 1>>
			   >
		  ,lmpl::metafunction<lmpl::boolean_and>
		  > {};
	
	constexpr std::uintmax_t previous_power_2(std::uintmax_t n) {
		return n <= 1 ? n : 2 * previous_power_2(n / 2);
	}
	
	constexpr std::uintmax_t next_power_2(std::uintmax_t n) {
		return n == previous_power_2(n) ? n : 2 * previous_power_2(n);
	}
} // namespace detail_finite_field_hpp_

template<std::size_t Characteristic>
struct static_finite_field {
	static_assert
		(!detail_finite_field_hpp_::is_prime<Characteristic>::value
		,"The characteristic of a finite field must be prime!"
		);
	
	static constexpr std::uintmax_t barrett_pow2n_big =
		detail_finite_field_hpp_::next_power_2(Characteristic);
	using fast_uint_t =
		detail_finite_field_hpp_::fastest_large_enough_uint_t
			<barrett_pow2n_big * Characteristic>;
	static constexpr fast_uint_t barrett_pow2n = barrett_pow2n_big;
	static constexpr fast_uint_t barrett_u =
		(barrett_pow2n * barrett_pow2n) / Characteristic;
	
	constexpr fast_uint_t barrett_reduction(fast_uint_t x) const noexcept {
		// assert(x <= (Characteristic - 1) * (Characteristic - 1));
		return x - ((((x / barrett_pow2n) * barrett_u) / barrett_pow2n) * Characteristic);
	}
	
	constexpr fast_uint_t
	substract_characteristic_if_needed(fast_uint_t x, int times = 1)
	const noexcept {
		return
			times == 0
				? x
				: substract_characteristic_if_needed
					  (x > Characteristic ? x - Characteristic : x, times - 1);
	}
	
	constexpr fast_uint_t mod_characteristic(fast_uint_t x) const noexcept {
		// assert(x <= (Characteristic - 1) * (Characteristic - 1));
		return substract_characteristic_if_needed(barrett_reduction(x), 3);
	}
	
	
public:
	struct value_type {
	public:
		constexpr value_type() noexcept
		: content() {};
	private:
		friend static_finite_field<Characteristic>;
		using underlying_type =
		detail_finite_field_hpp_::smallest_large_enough_uint_t
		<Characteristic - 1>;
		template<class UInt>
		constexpr explicit value_type(UInt n) noexcept
		: content(static_cast<underlying_type>(n)) {}
		
		underlying_type content;
		
		friend std::ostream& operator<<(std::ostream& o, value_type n) {
			return o << static_cast<std::uintmax_t>(n.content);
		}
	};
	
	using type = value_type;
	
	constexpr static_finite_field() noexcept {};
	
	template<class T>
	constexpr typename std::enable_if<std::is_same<T, type>::value, type>::type
	zero() const noexcept { return type(0); }
	
	constexpr type zero() const noexcept { return zero<type>(); }
	
	template<class T>
	constexpr typename std::enable_if<std::is_same<T, type>::value, type>::type
	one() const noexcept { return type(1); }
	
	constexpr type one() const noexcept { return one<type>(); }
	
	constexpr std::size_t characteristic() const noexcept {
		return Characteristic;
	}
	
	constexpr type add(type lhs, type rhs) const noexcept {
		return type(substract_characteristic_if_needed
			            (static_cast<fast_uint_t>(lhs.content)
			             + static_cast<fast_uint_t>(rhs.content)
			            ));
	}
	
	constexpr type opposite(type op) const noexcept {
		return eq(op, zero()) ? zero() : type(Characteristic - op.content);
	}
	
	constexpr type multiply(type lhs, type rhs) const noexcept {
		return type(multiply_uint(lhs.content, rhs.content));
	}
	
	constexpr type inverse(type x) const noexcept {
		return type(inverse_uint(x.content));
	}
	
	constexpr type divide(type lhs, type rhs) const noexcept {
		return type(multiply_uint(lhs.content, inverse_uint(rhs.content)));
	}
	
	constexpr type pow(type x, int e) const noexcept {
		return
			e == 0
				? one()
			: e > 0
				? type(positive_pow(x.content, e))
				: type(positive_pow(inverse_uint(x.content), e, one()));
	}
	
	constexpr bool eq(type lhs, type rhs) const noexcept {
		return lhs.content == rhs.content;
	}
	
	constexpr bool neq(type lhs, type rhs) const noexcept {
		return lhs.content != rhs.content;
	}
private:
	constexpr fast_uint_t
	multiply_uint(fast_uint_t lhs, fast_uint_t rhs) const noexcept {
		return mod_characteristic(lhs * rhs);
	}
	
	constexpr fast_uint_t inverse_uint(fast_uint_t op) const noexcept {
		return Characteristic == 2
			       ? 1 : positive_pow(op, Characteristic - 2, 1);
	}
	
	constexpr fast_uint_t
	positive_pow(fast_uint_t x, int e, fast_uint_t acc) const noexcept {
		// assert(e > 0);
		return
			e == 0
				? acc
			: e == 1
				? multiply_uint(acc, x)
				: positive_pow
					  (multiply_uint(x, x)
					  ,e / 2
					  ,e % 2 == 0 ? acc : multiply_uint(acc, x)
					  );
	}
};

#endif // JORDAN_FINITE_FIELD_HPP_
