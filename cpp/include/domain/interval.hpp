#ifndef JORDAN_INTERVAL_HPP_
#define JORDAN_INTERVAL_HPP_

#include <cfenv>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <limits>
#include <stdexcept>

#pragma STDC FENV_ACCESS ON

struct roundmode_error : std::runtime_error {
	roundmode_error()
	: std::runtime_error("fesetround failure") {}
};

template<class FloatingPointType>
class interval {
public:
	using type = FloatingPointType;
	static_assert(std::numeric_limits<FloatingPointType>::is_iec559
	             ,"requires IEEE 754 floats"
	             );
	
private:
	static auto get_round_mode() noexcept(false) ->
	decltype(std::fegetround()) {
		auto rmode = std::fegetround();
		if(rmode < 0) throw std::runtime_error("can't determine rounding mode");
		return rmode;
	}
	
	static void round_up_mode() noexcept(false) {
		if(std::fesetround(FE_UPWARD)) throw roundmode_error();
	}
	
	static void round_down_mode() noexcept(false) {
		if(std::fesetround(FE_DOWNWARD)) throw roundmode_error();
	}
	
	template<class T>
	static auto round_down(T&& x) ->
	decltype(type(std::forward<T>(x))) {
		auto default_rounding_mode = get_round_mode();
		round_down_mode();
		type r(std::forward<T>(x));
		std::fesetround(default_rounding_mode);
		return r;
	}
	
	template<class T>
	static auto round_up(T&& x) ->
	decltype(type(std::forward<T>(x))) {
		auto default_rounding_mode = get_round_mode();
		round_up_mode();
		type r(std::forward<T>(x));
		std::fesetround(default_rounding_mode);
		return r;
	}
public:
	template<class T, class = decltype(round_down(std::declval<T&&>()))>
	interval(T&& x)
	: lo(round_down(x))
	, hi(round_up(x)) { assert(lo <= hi); }
	
	template<class T, class U>
	interval(T&& lo_arg, U&& hi_arg)
	: lo(round_down(std::forward<T>(lo_arg)))
	, hi(round_up(std::forward<U>(hi_arg))) { assert(lo <= hi); }
	
	interval() : interval(0) {}
	
	interval(const interval&) = default;
	interval(interval&&) = default;
	interval& operator=(const interval&) = default;
	interval& operator=(interval&&) = default;
	
	
	static interval top() noexcept {
		return interval(-std::numeric_limits<type>::infinity()
						,std::numeric_limits<type>::infinity()
						);
	}
	
	static interval bottom() {
		throw std::runtime_error("bottom interval not implemented yet. ");
	}
	
	interval operator*(const interval& right) const {
		auto default_rounding_mode = get_round_mode();
		round_down_mode();
		type result_lo =
			std::min
				(std::min(lo * right.lo, lo * right.hi)
				,std::min(hi * right.lo, hi * right.hi)
				);
		round_up_mode();
		type result_hi =
			std::max
				(std::max(lo * right.lo, lo * right.hi)
				,std::max(hi * right.lo, hi * right.hi)
				);
		std::fesetround(default_rounding_mode);
		return interval(result_lo, result_hi);
	}
	
	bool contains_zero() const noexcept {
		return (std::signbit(lo) != std::signbit(hi))
		       | (std::fpclassify(lo) == FP_ZERO)
		       | (std::fpclassify(hi) == FP_ZERO);
	}
	
	bool contains_zero_strict() const noexcept {
		return (std::signbit(lo) != std::signbit(hi))
		       & (std::fpclassify(lo) != FP_ZERO)
		       & (std::fpclassify(hi) != FP_ZERO);
	}
	
	interval& operator*=(const interval& rhs) {
		return *this = *this * rhs;
	}
	
	interval operator/(const interval& right) const {
		if(right.contains_zero()) return top();
		auto default_rounding_mode = get_round_mode();
		round_down_mode();
		type result_lo =
			std::min
				(std::min(lo / right.lo, lo / right.hi)
				,std::min(hi / right.lo, hi / right.hi)
				);
		round_up_mode();
		type result_hi =
			std::max
				(std::max(lo / right.lo, lo / right.hi)
				,std::max(hi / right.lo, hi / right.hi)
				);
		std::fesetround(default_rounding_mode);
		return interval(result_lo, result_hi);
	}
	
	interval& operator/=(const interval& rhs) {
		interval copy = *this / rhs;
		return *this = copy;
	}
	
	interval operator+(const interval& right) const {
		auto default_rounding_mode = get_round_mode();
		round_down_mode();
		type result_lo = lo + right.lo;
		round_up_mode();
		type result_hi = hi + right.hi;
		std::fesetround(default_rounding_mode);
		return interval(result_lo, result_hi);
	}
	
	interval& operator+=(const interval& rhs) {
		return *this = *this + rhs;
	}
	
	interval operator-(const interval& right) const {
		auto default_rounding_mode = get_round_mode();
		round_down_mode();
		type result_lo = lo - right.hi;
		round_up_mode();
		type result_hi = hi - right.lo;
		std::fesetround(default_rounding_mode);
		return interval(result_lo, result_hi);
	}
	
	interval& operator-=(const interval& rhs) {
		return *this = *this - rhs;
	}
	
	interval pseudo_inverse() const { return interval(1) / *this; }
	interval operator-() const {
		auto default_rounding_mode = get_round_mode();
		round_down_mode();
		const type result_lo = -hi;
		round_up_mode();
		const type result_hi = -lo;
		std::fesetround(default_rounding_mode);
		return interval(result_lo, result_hi);
	}
	
	interval square() const {
		using std::swap;
		auto default_rounding_mode = get_round_mode();
		type abslo = std::copysign(lo, type(1));
		type abshi = std::copysign(hi, type(1));
		if(abslo > abshi) swap(abslo, abshi);
		round_down_mode();
		const type squared_lo = abslo * abslo;
		round_up_mode();
		const type squared_hi = abshi * abshi;
		std::fesetround(default_rounding_mode);
		return interval(squared_lo, squared_hi);
	}
	
	interval sqrt() const {
		assert(lo >= type(0));
		using std::sqrt;
		auto default_rounding_mode = get_round_mode();
		round_down_mode();
		const type sqrt_lo = sqrt(lo);
		
		const type sqrt_hi = sqrt(hi);
		std::fesetround(default_rounding_mode);
		return interval(sqrt_lo, sqrt_hi);
	}
	
	interval meet(const interval& other) const {
		const type result_lo = std::max(lo, other.lo);
		const type result_hi = std::min(hi, other.hi);
		if(result_lo > result_hi) return bottom();
		return interval(result_lo, result_hi);
	}
	
	const type& infimum() const noexcept { return lo; }
	const type& supremum() const noexcept { return hi; }
	type center() const noexcept { return (lo + hi)/2; }
	
	bool operator==(const interval& other) const noexcept
		{ return lo == other.lo && hi == other.hi; }
	
	static interval fma
		(const interval& x, const interval& y, const interval& z) {
		return x * y + z;
	}
private:
	type lo, hi;
};

template<class T>
bool intersect(const interval<T>& lhs, const interval<T>& rhs) noexcept {
	return lhs.infimum() <= rhs.supremum() && rhs.infimum() <= lhs.supremum();
}

template<class T>
interval<T> square(const interval<T>& i) {
	return i.square();
}

template<class T>
interval<T> sqrt(const interval<T>& i) {
	return i.sqrt();
}

template<class T>
interval<T> meet(const interval<T>& lhs, const interval<T>& rhs) {
	return lhs.meet(rhs);
}

template<class T>
interval<T> fma
	(const interval<T>& x, const interval<T>& y, const interval<T>& z) {
	return interval<T>::fma(x, y, z);
}

template<class T>
auto supremum(const interval<T>& i) noexcept -> decltype(i.supremum()) {
	return i.supremum();
}

template<class T>
auto infimum(const interval<T>& i) noexcept -> decltype(i.infimum()) {
	return i.infimum();
}

template<class T>
auto center(const interval<T>& i) noexcept -> decltype(i.center()) {
	return i.center();
}

template<class T>
auto contains_zero(const interval<T>& i) noexcept ->
decltype(i.contains_zero()) {
	return i.contains_zero();
}

template<class T>
auto contains_zero_strict(const interval<T>& i) noexcept ->
decltype(i.contains_zero_strict()) {
	return i.contains_zero_strict();
}

template<class T, class U>
interval<U> operator*(T x, const interval<U>& y) {
	return interval<U>(x) * y;
}

#include <iostream>

template<class T>
std::ostream& operator<<(std::ostream& o, const interval<T>& i) {
	if(i.infimum() == i.supremum())
		return o << i.infimum();
	return o << "interval(" << i.infimum() << ", " << i.supremum() << ')';
}

#endif // JORDAN_INTERVAL_HPP_
