#ifndef JORDAN_POLYNOMIAL_HPP_
#define JORDAN_POLYNOMIAL_HPP_

#include "dense_storage.hpp"
#include "traits/field_traits.hpp"
#include <vector>

template
	<class Field
	,class Allocator = std::allocator<typename field_traits<Field>::type>
	>
class polynomial {
public:
	using coefficient_domain = Field;
	using coefficient_traits = field_traits<coefficient_domain>;
	using set_type         = typename coefficient_traits::set_type;
	using type             = typename coefficient_traits::type;
	using storage_type     = dense_table<type, Allocator>;
	using allocator_type   = typename storage_type::allocator_type;
	using allocator_traits = typename storage_type::allocator_traits;
	using size_t           = typename storage_type::size_t;
	using pointer          = typename storage_type::pointer;
	using const_pointer    = typename storage_type::const_pointer;
	using reference        = typename storage_type::reference;
	using const_reference  = typename storage_type::const_reference;
	using iterator         = typename storage_type::iterator;
	using const_iterator   = typename storage_type::const_iterator;
	
	template
		<class CustomField
		,class CustomAllocator = allocator_type
		>
	using rebind_alloc =
		typename std::allocator_traits<CustomAllocator>::
			template rebind_alloc<typename field_traits<CustomField>::type>;
	
	template
		<class CustomField
		,class CustomAllocator = rebind_alloc<CustomField>
		>
	using rebind =
		polynomial<CustomField, typename std::decay<CustomAllocator>::type>;
	
	polynomial() noexcept(false)
	: polynomial(set_type()) {}
	
	explicit polynomial(const set_type& s) noexcept(false)
	: data(s
	      ,size_t(0)
	      ,size_t(0)
	      ,coefficient_traits::zero(s)
	      ,Allocator()
	      ) {}
	
	explicit polynomial(size_t dg) noexcept(false)
	: polynomial(dg, set_type()) {}
	
	polynomial(size_t dg, const set_type& s) noexcept(false)
	: data(s
	      ,size_t(1)
	      ,dg + size_t(1)
	      ,coefficient_traits::zero(s)
	      ,Allocator()
	      ) { (*this)[dg] = coefficient_traits::one(structure()); }
	
	template<class T>
	polynomial(std::initializer_list<T> array) noexcept(false)
	: data(set_type(), array, Allocator()) {
		shrink(array.size() - 1);
	}
	
	template<class CustomField, class CustomAllocator, class CastFunctor>
	rebind<CustomField, CustomAllocator> cast_with
		(CustomAllocator&& alloc, CastFunctor&& f)
	const noexcept(false) {
		const auto cast_coefficient =
			[](const_iterator& coef, CastFunctor&& castf) {
				return std::forward<CastFunctor>(castf)(*(coef++));
			};
		const_iterator coef = cbegin();
		return rebind<CustomField, CustomAllocator>
			       (typename rebind<CustomField, CustomAllocator>::storage_type
				        (1
				        ,degree() + 1
				        ,std::forward<CustomAllocator>(alloc)
				        ,cast_coefficient
				        ,coef
				        ,std::forward<CastFunctor>(f)
				        )
			       );
	}
	
	template<class CustomField, class CastFunctor>
	rebind<CustomField> cast_with(CastFunctor&& f)
	const noexcept(false) {
		return cast_with<CustomField>
			       (typename rebind<CustomField>::allocator_type
				        (allocator_traits::select_on_container_copy_construction
					         (allocator())
				        )
			       ,std::forward<CastFunctor>(f)
			       );
	}
	
	template<class CustomField, class CustomAllocator>
	rebind
		<CustomField
		,rebind_alloc<CustomField, CustomAllocator>
		> cast(CustomAllocator&& alloc)
	const noexcept(false) {
		const auto f =
			[](const type& x) {
				return static_cast<typename field_traits<CustomField>::type>(x);
			};
		return cast_with<CustomField>
			       (rebind_alloc<CustomField, CustomAllocator>
				        (std::forward<CustomAllocator>(alloc))
			       ,f
			       );
	}
	
	template<class CustomField>
	rebind<CustomField> cast() const noexcept(false) {
		return cast<CustomField>
			       (allocator_traits::select_on_container_copy_construction
				        (allocator())
			       );
	}
	
	polynomial formal_derivative() const noexcept(false) {
		auto derivative_coefficient =
			[&](const_iterator& coeff, size_t& order) {
				return coefficient_traits::multiply
					       (structure(), *(coeff++), type(order++));
			};
		const_iterator coef = cbegin();
		size_t order = 1;
		storage_type derivative
			(1
			,degree()
			,allocator_traits::select_on_container_copy_construction
				 (allocator())
			,derivative_coefficient
			,++coef
			,order
			);
		return polynomial(std::move(derivative));
	}
	
	polynomial monic() const noexcept(false) {
		auto monic_coefficient =
			[&](const_iterator& coeff, const_reference leading_coeff) {
				return coefficient_traits::divide
					       (structure(), *(coeff++), leading_coeff);
			};
		const_iterator coef = cbegin();
		std::size_t k = degree();
		for(; k <= degree() && coefficient_traits::is_zero(structure(), (*this)[k]) ; --k);
		if(k == minus_infinite_degree()) { return polynomial(); }
		const_reference leading_coeff = (*this)[k];
		storage_type monic
			(1
			,degree() + 1
			,allocator_traits::select_on_container_copy_construction
				 (allocator())
			,monic_coefficient
			,coef
			,leading_coeff
			);
		return polynomial(std::move(monic));
	}
	
	polynomial simplify_zero_roots() const noexcept(false) {
		if(degree() == minus_infinite_degree())
			return *this;
		std::size_t offset;
		for(offset = 0
		   ;offset < degree()
		    && coefficient_traits::is_zero(structure(), (*this)[offset])
		   ;++offset
		   );
		auto copy_coef = [](const_iterator& coef) { return *(coef++); };
		const_iterator coef = cbegin() + offset;
		storage_type shifted
			(1
			,degree() + 1 - offset
			,allocator_traits::select_on_container_copy_construction(allocator())
			,copy_coef
			,coef
			);
		return polynomial(std::move(shifted));
	}
	
	polynomial euclidean_division
		(const polynomial& divisor, polynomial& remainder)
	const noexcept(false) {
		remainder = *this;
		if(degree() == minus_infinite_degree()
		   || divisor.degree() == minus_infinite_degree()
		   || degree() < divisor.degree()
		  )
			{ return polynomial(); }
		
		const size_t quotient_dg = degree() - divisor.degree();
		polynomial q(quotient_dg);
		const_reference alpha = divisor[divisor.degree()];
		assert(!coefficient_traits::is_zero(structure(), alpha));
		for(size_t k = quotient_dg ; k != minus_infinite_degree() ; --k) {
			q[k] =
				coefficient_traits::divide
					(structure(), remainder[k + divisor.degree()], alpha);
			for(size_t i = 0 ; i <= divisor.degree() ; ++i)
				remainder[i + k] =
					coefficient_traits::substract
						(structure()
						,remainder[i + k]
						,coefficient_traits::multiply
							 (structure(), q[k], divisor[i])
						);
		}
		remainder.shrink(divisor.degree() - 1);
		return q;
	}
	
	polynomial remainder(const polynomial& divisor) const noexcept(false) {
		polynomial r;
		euclidean_division(divisor, r);
		return r;
	}
	
	polynomial operator%(const polynomial& divisor) const noexcept(false) {
		return remainder(divisor);
	}
	
	static polynomial zero() noexcept(false) { return polynomial(); }
	static polynomial one()  noexcept(false) { return polynomial(0); }
	
	polynomial add(const polynomial& other) const noexcept(false) {
		if(degree() < other.degree()) return other.add(*this);
		
		polynomial result(*this);
		for(size_t i = 0 ; i < other.degree() ; ++i)
			result[i] = coefficient_traits::add(structure(), result[i], other[i]);
		result.shrink();
		return result;
	}
	
	polynomial operator+(const polynomial& other) const noexcept(false) {
		return add(other);
	}
	
	polynomial substract(const polynomial& other) const noexcept(false) {
		if(degree() < other.degree()) return other.add(*this);
			
		polynomial result(*this);
		for(size_t i = 0 ; i < other.degree() ; ++i)
			result[i] =
				coefficient_traits::substract(structure(), result[i], other[i]);
		result.shrink();
		return result;
	}
	
	polynomial operator-(const polynomial& other) const noexcept(false) {
		return substract(other);
	}
	
	polynomial additive_inverse() const noexcept(false) {
		polynomial result(*this);
		for(size_t i = 0 ; i < result.degree() ; ++i)
			result[i] = coefficient_traits::additive_inverse(structure(), result[i]);
	}
	
	polynomial operator-() const noexcept(false) {
		return additive_inverse();
	}
	
	polynomial quotient(const polynomial& divisor)
	const noexcept(false)
		{ polynomial rem; return euclidean_division(divisor, rem); }
	
	polynomial operator/(const polynomial& divisor) const noexcept(false) {
		return quotient(divisor);
	}
	
	polynomial multiply(const polynomial& other) const noexcept(false) {
		if(degree() == minus_infinite_degree()
		   || other.degree() == minus_infinite_degree()
		  )
			return polynomial();
		
		const size_t result_dg = degree() + other.degree();
		polynomial result(result_dg);
		result[result_dg] = coefficient_traits::zero(structure());
		for(size_t i = 0 ; i <= degree() ; ++i)
			for(size_t j = 0 ; j <= other.degree() ; ++j)
				result[i + j] =
					coefficient_traits::add
						(structure()
						,result[i + j]
						,coefficient_traits::multiply
							 (structure(), (*this)[i], other[j])
						);
		return result;
	}
	
	polynomial operator*(const polynomial& other) const noexcept(false) {
		return multiply(other);
	}
	
	iterator begin()        noexcept { return data.coefficient.begin(); }
	iterator end()          noexcept { return data.coefficient.end(); }
	const_iterator begin()  const noexcept { return data.coefficient.begin(); }
	const_iterator end()    const noexcept { return data.coefficient.end(); }
	const_iterator cbegin() const noexcept { return data.coefficient.cbegin(); }
	const_iterator cend()   const noexcept { return data.coefficient.cend(); }
	
	bool operator==(const polynomial& other) const noexcept {
		auto i1 = begin(), i2 = other.begin();
		auto end1 = end(), end2 = other.end();
		for( ; i1 != end1 && i2 != end2 ; ++i1, ++i2)
			if(*i1 != *i2) return false;
		return i1 == end1 && i2 == end2;
	}
	
	allocator_type&       allocator() noexcept
		{ return data.coefficient.allocator(); }
	const allocator_type& allocator() const noexcept
		{ return data.coefficient.allocator(); }
	size_t degree() const noexcept
		{ return data.coefficient.width() - 1; }
	reference       operator[](size_t i) noexcept
		{ return data.coefficient.at(0, i); }
	const_reference operator[](size_t i) const noexcept
		{ return data.coefficient.at(0, i); }
	
	static size_t minus_infinite_degree() noexcept
		{ return size_t(0) - size_t(1); }
	
	type evaluate(const type& x) {
		if(degree() == minus_infinite_degree())
			return coefficient_traits::zero(structure());
		type result = (*this)[degree()];
		auto x_s      = mksymb(x);
		auto result_s = mksymb(std::move(result));
		for(size_t i = degree() ; i != 0 ; ) {
			--i;
			auto coef_i_s = mksymb((*this)[i]);
			result = coefficient_traits::eval
				         (structure(), result_s * x_s + coef_i_s);
		}
		return result;
	}
	
	template<class TT>
	TT evaluate(const TT& x) const {
		if(degree() == minus_infinite_degree())
			return TT(0);
		TT result = TT((*this)[degree()]);
		for(size_t i = degree() ; i != 0 ; ) {
			--i;
			result = std::move(result) * x + TT((*this)[i]);
		}
		return result;
	}
	
	template<class VectorSpace>
	typename VectorSpace::type evaluate
		(const std::vector<typename VectorSpace::type>& powers
		,const typename VectorSpace::set_type& s =
			 typename VectorSpace::set_type()
		)
	const noexcept(false /*todo*/) {
		assert(powers.size() > degree() || degree() == minus_infinite_degree());
		if(degree() == minus_infinite_degree())
			return VectorSpace::zero(s);
		typename VectorSpace::type result = VectorSpace::zero(s);
		for(size_t i = 0 ; i <= degree() ; ++i)
			result = VectorSpace::axpy
				         (s, (*this)[i], powers[i], std::move(result));
		return result;
	}
	
	// const qualification violation !
	set_type& structure() const noexcept
		{ return const_cast<set_type&>(static_cast<const set_type&>(data)); }
private:
	template<class CustomField, class CustomAllocator>
	friend class polynomial;
	
	void shrink(size_t max_degree) {
		size_t d = 0;
		if(max_degree == minus_infinite_degree())
			{ *this = polynomial(); return; }
		for(size_t i = 0 ; i <= max_degree ; )
			if(!coefficient_traits::is_zero(structure(), (*this)[i]))
				d = std::max(d, ++i);
			else ++i;
		if(--d == minus_infinite_degree()) { *this = polynomial(); return; }
		polynomial shrinked(d);
		for(size_t i = 0 ; i <= d ; ++i)
			shrinked[i] = (*this)[i];
		*this = std::move(shrinked);
	}
	
	polynomial(storage_type&& internals) noexcept
	: data(set_type(), std::move(internals)) {}
	
	struct internal_data : set_type {
		template<class... Args>
		internal_data(const set_type& s, Args&&... args)
		: set_type(s)
		, coefficient(std::forward<Args>(args)...) {}
		
		storage_type coefficient;
	} data;
};

template<class Field, class Allocator>
bool nonzero(const polynomial<Field, Allocator>& p) {
	return p.degree() != polynomial<Field, Allocator>::minus_infinite_degree();
}

template<class Poly>
using polynomial_euclidean_domain = euclidean_domain_of<Poly>;
/*
template<class Poly>
struct polynomial_euclidean_domain
: euclidean_domain_of<Poly>
, Poly::coefficient_domain {
	using scalar_type = typename Poly::coefficient_domain::type;
	using type = typename euclidean_domain_of<Poly>::type;
	
	using Poly::coefficient_domain::eq;
	using Poly::coefficient_domain::add;
	using Poly::coefficient_domain::opposite;
	using Poly::coefficient_domain::multiply;
	using Poly::coefficient_domain::one;
	using Poly::coefficient_domain::zero;
	type add(const type& lhs, const type& rhs) const {
		return euclidean_domain_of<Poly>::add(lhs, rhs);
	}
	bool eq(const type& lhs, const type& rhs) const {
		return euclidean_domain_of<Poly>::eq(lhs, rhs);
	}
	template<class T>
	typename std::enable_if<std::is_same<T, type>::value, type>::type
	zero() const {
		return euclidean_domain_of<Poly>::zero();
	}
	template<class T>
	typename std::enable_if<std::is_same<T, type>::value, type>::type
	one() const {
		return euclidean_domain_of<Poly>::one();
	}
	type multiply(const type& lhs, const type& rhs) const {
		return euclidean_domain_of<Poly>::multiply(lhs, rhs);
	}
};*/

#include <ostream>

template<class Field, class Allocator>
std::ostream& operator<<
	(std::ostream& o, const polynomial<Field, Allocator>& p)
noexcept(false) {
	using poly_t = polynomial<Field, Allocator>;
	if(p.degree() == poly_t::minus_infinite_degree())
		return o << '0';
	o << *p.begin();
	typename poly_t::size_t k = 0;
	typename poly_t::const_iterator c = p.begin();
	for(++c ; c != p.end() ; ++c) {
		++k;
		if(!poly_t::coefficient_traits::is_zero(p.structure(), *c)) {
			o << " + (" << *c << ")X^" << k;
		}
	}
	return o;
}
		
template<class Poly>
std::vector<Poly> simple_polynomial_factorization(const Poly& p) {
	if(p.degree() == 0)
		return std::vector<Poly>();
	using euclidean_domain_traits =
		euclidean_domain_traits<polynomial_euclidean_domain<Poly>>;
	using set_type = typename euclidean_domain_traits::set_type;
	set_type s;
	std::vector<Poly> factorization;
	Poly developped = p;
	while(developped.degree() != 0) {
		factorization.push_back(developped);
		developped =
			euclidean_domain_traits::gcd
				(s, developped, developped.formal_derivative().monic()).monic();
	}
	Poly step = euclidean_domain_traits::one(s), q = euclidean_domain_traits::one(s);
	for(size_t i = factorization.size() - 1 ; i > 0; --i) {
		step = euclidean_domain_traits::multiply(s, step, factorization[i]);
		q = euclidean_domain_traits::multiply
			   (s, q, euclidean_domain_traits::multiply(s, step, factorization[i]));
		factorization[i - 1] =
			euclidean_domain_traits::quotient
				(s, factorization[i - 1], q).monic();
	}
	return factorization;
}

#endif // JORDAN_POLYNOMIAL_HPP_
