#ifndef JORDAN_RATIONAL_HPP_
#define JORDAN_RATIONAL_HPP_

//#include "integer_traits_traits.hpp"
#include "traits/field_traits.hpp"
#include <cassert>
#include <ostream>
#include <limits>
#include <cmath>

namespace detail_rational_field_hpp_ {
	uint64_t hamming(uint64_t v) {
		v = v - ((v>>1) & 0x5555555555555555);
		v = (v & 0x3333333333333333) + ((v>>2) & 0x3333333333333333);
		v = (v + (v >> 4)) & 0x0F0F0F0F0F0F0F0F;
		v = (v + (v >> 8)) & 0x00FF00FF00FF00FF;
		return (v * 0x0001000100010001) >> (64-16);
	}
	
	int first_nonzero(uint64_t v) {
		v = v & (-v);
		int r = (v & 0xFFFFFFFF00000000)?32:0;
		r += (v & 0xFFFF0000FFFF0000)?16:0;
		r += (v & 0xFF00FF00FF00FF00)?8:0;
		r += (v & 0xF0F0F0F0F0F0F0F0)?4:0;
		r += (v & 0xCCCCCCCCCCCCCCCC)?2:0;
		r += (v & 0xAAAAAAAAAAAAAAAA)?1:0;
		return r;
	}
	
	template <class FloatT, class UIntT>
	void guess_rational(FloatT x, UIntT& num, UIntT& denom) {
		const int n =
			static_cast<int>
				(std::min(std::numeric_limits<FloatT>::digits - 2
				         ,std::numeric_limits<UIntT>::digits
				         )
				);
		int e;
		x = copysign(x, 1.);
		x = std::frexp(x, &e);
		UIntT q = static_cast<UIntT>(std::trunc(std::ldexp(x, n)));
		int argmin = 0;
		if(q == 0) {
			num = UIntT(0);
			denom = UIntT(1);
			return;
		}
		if(first_nonzero(q) > (n/3)) {
			num = q >> first_nonzero(q);
			denom = UIntT(2) << (n - first_nonzero(q) - e - 1);
			denom = (denom == UIntT(0))?UIntT(1):denom;
			return;
		}
		UIntT mask = static_cast<UIntT>(-1);
		if(n < std::numeric_limits<UIntT>::digits)
			mask = ~(mask << n);
		UIntT shiftedq = q;
		int min_hamming = n;
		for(int i = 0 ; i < (n*2/3) ; ++i) {
			shiftedq >>= 1;
			mask >>= 1;
			const int tmp = hamming((q ^ shiftedq) & mask);
			if(tmp < min_hamming) {
				min_hamming = tmp;
				argmin = i + 1;
			}
		}
		num = std::round(std::ldexp((std::ldexp(x, argmin) - x), e));
		denom = (UIntT(1) << argmin) - UIntT(1);
		denom = (denom == UIntT(0))?UIntT(1):denom;
		return;
	}
} // namespace detail_rational_field_hpp_

template<class Integer>
Integer remainder(Integer x, Integer y) {
	assert(y != Integer(0));
	return x % y;
}

template<class Integer = int64_t>
class rational;

template<class Integer>
std::ostream& operator<<(std::ostream&, const rational<Integer>&)
noexcept(false);

template<class Integer>
class rational {
	struct internal_data;
public:
	using integer_traits =
		euclidean_domain_traits<euclidean_domain_of<Integer>>;
	using set_type     = typename integer_traits::set_type;
	using integer_type = typename integer_traits::type;
	
	rational()
	: rational(0) {}
	
	explicit rational(const Integer& numerator)
	: data(numerator, integer_type(1)) {}
	
	rational(const Integer& numerator, const Integer& denominator)
	: data(numerator, denominator, simplify_tag()) {}
	
	const integer_type& numerator() const noexcept { return data.numerator; }
	const integer_type& denominator()  const noexcept { return data.denominator; }
	
	rational operator*(const rational& other) const noexcept {
		const auto gcd1 =
			integer_traits::gcd
				(structure(), std::abs(numerator()), other.denominator());
		const auto gcd2 =
			integer_traits::gcd
				(structure(), std::abs(other.numerator()), denominator());
		auto gcd1_s = mksymb(gcd1);
		auto gcd2_s = mksymb(gcd2);
		auto p1_s   = mksymb(numerator());
		auto p2_s   = mksymb(other.numerator());
		auto q1_s   = mksymb(denominator());
		auto q2_s   = mksymb(other.denominator());
		const auto numerator =
			integer_traits::eval
				(structure(), (p1_s / gcd1_s) * (p2_s / gcd2_s));
		const auto denominator =
			integer_traits::eval
				(structure(), (q1_s / gcd2_s) * (q2_s / gcd1_s));
		return rational(internal_data(numerator, denominator, structure()));
	}
	
	rational operator/(const rational& other) const noexcept {
		assert(other.numerator() != Integer(0));
		return (*this) * other.inverse();
	}
	
	rational operator-(const rational& other) const noexcept {
		const auto lcm =
			integer_traits::lcm(structure(), denominator(), other.denominator());
		auto lcm_s = mksymb(lcm);
		auto p1_s  = mksymb(numerator());
		auto p2_s  = mksymb(other.numerator());
		auto q1_s  = mksymb(denominator());
		auto q2_s  = mksymb(other.denominator());
		const auto numerator =
			integer_traits::eval
				(structure(), p1_s * (lcm_s / q1_s) - p2_s * (lcm_s / q2_s));
		return rational
			       (internal_data(numerator, lcm, structure(), simplify_tag()));
	}
	
	rational operator-() const noexcept {
		return rational
			       (internal_data
				        (integer_traits::opposite(structure(), numerator())
				        ,denominator()
				        ,structure()
				        )
			       );
	}
	
	rational operator+(const rational& other) const noexcept {
		const auto lcm =
		integer_traits::lcm(structure(), denominator(), other.denominator());
		auto lcm_s = mksymb(lcm);
		auto p1_s  = mksymb(numerator());
		auto p2_s  = mksymb(other.numerator());
		auto q1_s  = mksymb(denominator());
		auto q2_s  = mksymb(other.denominator());
		const auto numerator =
			integer_traits::eval
				(structure(), p1_s * (lcm_s / q1_s) + p2_s * (lcm_s / q2_s));
		return rational
			       (internal_data(numerator, lcm, structure(), simplify_tag()));
	}
	
	rational inverse() const noexcept {
		assert(numerator() != Integer(0));
		return rational
			       (internal_data
				        (integer_traits::multiply
					        (structure()
					        ,denominator()
					        ,integer_type(numerator() > 0?1:-1)
					        )
				       ,std::abs(numerator())
				       )
			       );
	}
	
	bool operator==(const rational& other) const noexcept {
		return integer_traits::eq(structure(), numerator(), other.numerator())
		       && integer_traits::eq(structure(), denominator(), other.denominator());
	}
	
	bool operator!=(const rational& other) const noexcept {
		return integer_traits::neq(structure(), numerator(), other.numerator())
		       || integer_traits::neq(structure(), denominator(), other.denominator());
	}
	
	bool is_zero() const noexcept {
		return integer_traits::is_zero(structure(), numerator());
	}
	
private:
	template<class U>
	friend std::ostream& operator<<(std::ostream&, const rational<U>&);
	
	// const qualification violation !
	set_type& structure() const noexcept
		{ return const_cast<set_type&>(static_cast<const set_type&>(data)); }
	
	struct simplify_tag {};
	
	rational(internal_data&& d) noexcept
	: data(std::move(d)) {}
	
	struct internal_data
	: set_type {
		internal_data(Integer numerator_arg, Integer denominator_arg)
		: internal_data
			  (numerator_arg
			  ,denominator_arg
			  ,set_type()
			  ) {}
		
		internal_data
			(Integer numerator_arg
			,Integer denominator_arg
			,const set_type& s
			)
		: set_type(s)
		, numerator(numerator_arg)
		, denominator(denominator_arg) {
			assert(!integer_traits::is_zero(s, denominator));
			assert(denominator > 0);
		}
		
		internal_data(Integer numerator_arg, Integer denominator_arg, simplify_tag)
		: internal_data
			  (numerator_arg
			  ,denominator_arg
			  ,set_type()
			  ,simplify_tag()
			  ) {}
		
		internal_data
			(Integer numerator_arg
			,Integer denominator_arg
			,const set_type& s
			,simplify_tag
			)
		: internal_data
			  (numerator_arg
			  ,denominator_arg
			  ,integer_traits::gcd(s, std::abs(numerator_arg), denominator_arg)
			  ,s
			  ) {}
		
		internal_data
			(Integer numerator_arg
			,Integer denominator_arg
			,Integer common_denominator
			,const set_type& s
			)
		: internal_data
			(integer_traits::quotient(s, numerator_arg, common_denominator)
			,integer_traits::quotient(s, denominator_arg, common_denominator)
			,s) {}
		
		Integer numerator, denominator;
	} data;
};

template<class Integer>
std::ostream& operator<<
	(std::ostream& o, const rational<Integer>& q)
noexcept(false) {
	if(q.denominator() == Integer(1))
		return o << q.numerator();
	return o << q.numerator() << '/' << q.denominator();
}

/*
template<class Integer, class = void>
struct rational_field_aux
: satisfies_field_element<Integer>::template compile_time_check<void> {};
		
template<class Integer>
struct rational_field_aux
	<Integer
	,typename std::enable_if<satisfies_field_element<Integer>::value>::type
	>
: field_of<rational<Integer>> {};

template<class Integer = int64_t>
struct rational_field : rational_field_aux<Integer> {};
//*/
		
template<class Rational, class FloatT>
Rational guess_rational(FloatT x) {
	using uint_t =
		typename std::make_unsigned<typename Rational::integer_type>::type;
	uint_t num, denom;
	detail_rational_field_hpp_::guess_rational(x, num, denom);
	num = std::signbit(x)?-num:num;
	return Rational(num, denom);
}

#endif // JORDAN_RATIONAL_HPP_
