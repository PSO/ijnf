#ifndef JORDAN_JENKINS_TRAUB_HPP_
#define JORDAN_JENKINS_TRAUB_HPP_

#include <cmath>
#include <initializer_list>
#include <cassert>
#include "utility"

namespace detail_jenkins_traub_hpp_ {

	template<class PolynomialT, class RealApproximationT>
	struct algorithm {
		enum {
			FIRST_STAGE_ITERATION = 5,
			MAX_RETRIES = 20,
			MAX_SECOND_STAGE_ITERATION = 10,
			ADDITIONAL_ITERATION_PER_RETRY = 5,
			MAX_THIRD_STAGE_ITERATION = 10
			
		};
		using polynomial_type = PolynomialT;
		using real_type       = RealApproximationT;
		using index_t = decltype(std::declval<polynomial_type>().degree());
		
		algorithm(const polynomial_type& p_arg)
		: p(p_arg)
		, sigma(2) {}
		
		template<class F1, class F2>
		bool run(F1&& consume_real_root, F2&& consume_quadratic_factor) {
			while(p.degree() > 2) {
				first_stage();
				if(!second_stage
					   (std::forward<F1>(consume_real_root)
					   ,std::forward<F2>(consume_quadratic_factor)
					   )
				  ) return false;
			}
			if(p.degree() == 1) std::forward<F1>(consume_real_root)(-p[0]/p[1]);
			else if(p.degree() == 2)
				std::forward<F2>(consume_quadratic_factor)
					(p[1]/p[2], p[0]/p[2]);
			return true;
		}
		
	private:
		void first_stage() {
			k = p.formal_derivative();
			const index_t deg = k.degree();
			for(index_t i = 0 ; i < deg ; ++i)
				k[i] /= real_type(k[deg]);
			k[deg] = real_type(1);
			for(int i = 0 ; i < FIRST_STAGE_ITERATION ; ++i) {
				// k(z) = (k(z) - k(0)/p(0) * p(z))/z
				const real_type alpha = k[0]/p[0];
				for(index_t j = 0 ; j < deg ; ++j)
					k[j] = k[j + 1] - alpha * p[j];
				k[deg] = alpha * p[deg + 1];
			}
			for(index_t i = 0 ; i < deg ; ++i)
				k[i] /= k[deg];
			k[deg] = real_type(1);
		}
		
		template<class F1, class F2>
		bool second_stage
			(F1&& consume_real_root, F2&& consume_quadratic_factor) {
			polynomial_type k_save = k;
			for(int i = 0 ; i <= MAX_RETRIES ; ++i) {
				pick_sigma(i);
				notify_sigma_update();
				notify_k_update();
				update_determinants();
				if(second_stage_attempt
					   (std::forward<F1>(consume_real_root), std::forward<F2>(consume_quadratic_factor), i)
				  ) return true;
				k = k_save;
			}
			return false;
		}
		
		static bool has_converged(real_type x, real_type xm1, real_type xm2) {
			const real_type relative_epsilon(1e-3);
			const real_type epsilon(1e-7);
			const real_type threshold(1e-4);
			const real_type deltam1 = std::abs(xm2 - xm1);
			const real_type delta   = std::abs(xm1 - x);
			return delta > deltam1
			       && ((std::abs(xm1) <= threshold)
				           ?delta <= epsilon
				           :std::abs(delta/xm1) <= relative_epsilon
			          );
		}
		
		template
			<bool TryLinear = true
			,bool TryQuadratic = true
			,class F1
			,class F2
			>
		bool second_stage_attempt
			(F1&& consume_real_root
			,F2&& consume_quadratic_factor
			,int n
			,int start_iteration = 0
			,real_type vm1 = real_type()
			,real_type vm2 = real_type()
			,real_type tm1 = real_type()
			,real_type tm2 = real_type()
			) {
			if(!TryQuadratic && !TryLinear) return false;
			const int max_iteration =
				MAX_SECOND_STAGE_ITERATION + ADDITIONAL_ITERATION_PER_RETRY * n;
			for(int i = start_iteration ; i < max_iteration ; ++i) {
				real_type u, v;
				compute_next_sigma_coefficients(u, v);
				// check whether t = Im(s1)^2 -> 0
				if(TryLinear) {
					real_type t = (v - u * u / real_type(4))*(1 - detpk/prodk);
					if(i > 1
					   && std::abs(tm2 - tm1) <= std::abs(tm1)/2
					   && std::abs(tm1 - t) <= std::abs(t)/2
					  ) {
						real_type sigma0_save = sigma[0];
						real_type sigma1_save = sigma[1];
						sigma[0] = v;
						sigma[1] = u;
						if(linear_third_stage
							   (std::forward<F1>(consume_real_root))
						  ) return true;
						sigma[0] = sigma0_save;
						sigma[1] = sigma1_save;
						return second_stage_attempt<false, TryQuadratic>
							       (std::forward<F1>(consume_real_root)
							       ,std::forward<F2>(consume_quadratic_factor)
							       ,n, i, vm1, vm2, tm1, tm2
							       );
					}
					tm2 = tm1;
					tm1 = t;
				}
				// check whether s1*s2 -> l
				if(TryQuadratic) {
					if(i > 1
					   && std::abs(vm2 - vm1) <= std::abs(vm1)/2
					   && std::abs(vm1 - v) <= v/2
					  ) {
						if(quadratic_third_stage
							   (std::forward<F2>(consume_quadratic_factor))
						  ) return true;
						return second_stage_attempt<TryLinear, false>
							       (std::forward<F1>(consume_real_root)
							       ,std::forward<F2>(consume_quadratic_factor)
							       ,n, i, vm1, vm2, tm1, tm2
							       );
					}
					vm2 = vm1;
					vm1 = v;
				}
				// next k
				update_k();
				notify_k_update();
				update_determinants();
			}
			return false;
		}
		
		template<class F>
		bool linear_third_stage(F&& consume_real_root) {
			notify_sigma_update();
			notify_k_update();
			update_determinants();
			const real_type half_u = sigma[1] / real_type(2);
			const index_t deg = k.degree();
			real_type s = ((half_u * detpk) - detskp) / prodk - half_u;
			real_type sm1, sm2;
			for(int i = 0 ; i < MAX_THIRD_STAGE_ITERATION ; ++i) {
				const real_type alpha = p.evaluate(s)/k.evaluate(s);
				// k = (k - alpha * p)/(z - s)
				for(index_t j = 0 ; j < deg ; ++j)
					k[j] = k[j + 1] - alpha * p[j + 1];
				k[deg] = - alpha * p[deg + 1];
				for(index_t j = deg ; j > 0 ; --j)
					k[j - 1] += k[j] * s;
				s = s - (p.evaluate(s) / k.evaluate(s));
				if(i > 1 && has_converged(s, sm1, sm2)) {
					std::forward<F>(consume_real_root)(sm1);
					p = p.quotient(polynomial_type(std::initializer_list<real_type>({1, -sm1})));
					return true;
				}
				sm2 = sm1;
				sm1 = s;
			}
			return false;
		}
		
		template<class F>
		bool quadratic_third_stage(F&& consume_quadratic_factor) {
			real_type u, vm1, vm2;
			for(int i = 0 ; i < MAX_THIRD_STAGE_ITERATION ; ++i) {
				compute_next_sigma_coefficients(sigma[1], sigma[0]);
				notify_sigma_update();
				update_determinants();
				update_k();
				notify_k_update();
				update_determinants();
				if(i > 1 && has_converged(sigma[0], vm1, vm2)) {
					std::forward<F>(consume_quadratic_factor)(u, vm1);
					p = p.quotient(polynomial_type(std::initializer_list<real_type>({1, u, vm1})));
					return true;
				}
				vm2 = vm1;
				vm1 = sigma[0];
				u = sigma[1];
			}
			return false;
		}
		
		static real_type root_radius_lower_bound(const polynomial_type& p) {
			const real_type epsilon(1e-3);
			const int n = 10;
			polynomial_type p2 = p;
			for(index_t i = 1 ; i < p2.degree() ; ++i)
				p2[i] = std::abs(p2[i]);
			polynomial_type p2diff = p2.formal_derivative();
			p2[0] = -std::abs(p2[0]);
			real_type r(1), y;
			for(int i = 0
			   ;i < n && std::abs(y = p2diff.evaluate(r)) > epsilon
			   ;++i
			   ) {
				r -= p2.evaluate(r) / y;
			}
			return r;
		}
		
		void pick_sigma(int n) noexcept {
			const real_type shift = 0.7;
			const real_type step  = 0.473;
			const real_type beta  = root_radius_lower_bound(p);
			double costheta = shift + (real_type(n) * step);
			costheta -= std::floor(costheta / 2.) * 2. + 1.;
			sigma[0] = beta * beta;
			sigma[1] = real_type(-2) * beta * real_type(costheta);
		}
		
		void update_k() noexcept {
			const real_type alpha_k = prodp/detpk;
			const real_type alpha_p = detskp/detpk - sigma[1];
			const index_t deg = k.degree();
			assert(deg >= 2);
			// k = quotient_k * alpha_k + (z - alpha_p) * quotient_p + b;
			k[0] = b;
			for(index_t i = 1 ; i <= deg - 2 ; ++i)
				k[i] =
					(alpha_k * quotient_k[i])
					- (alpha_p * quotient_p[i])
					+ quotient_p[i - 1];
			k[deg - 1] = quotient_p[deg - 2] - alpha_p * quotient_p[deg - 1];
			k[deg] = quotient_p[deg - 1];
		}
		
		void compute_next_sigma_coefficients
			(real_type& next_u, real_type& next_v) const noexcept {
			// b01 =  B_{01} * v^2/(s1-s2)
			// b02 = -B_{02} * v^2/(s1-s2)
			// b12 =  B_{12} * v^2/(s1-s2)
			// where B_{ij} is K_{i}(s1)K_{j}(s2) - K_{i}(s2)K_{j}(s1)
			const real_type& v = sigma[0];
			const real_type& u = sigma[1];
			const real_type k1p0v = k1p0 * v;
			const real_type kp0u  = kp0 * u;
			const real_type b01   = v * (prodk - kp0 * detskp);
			const real_type b02   =
				(u * prodk) + (kp0 * v * detpk) + (detskp * (k1p0v - kp0u));
			const real_type b12 =
				detpk * (k1p0v + kp0u)
				- real_type(2) * kp0 * detskp
				+ (kp0 * kp0) * prodp
				+ prodk;
			// b12 might be 0 ! TODO
			next_u = b02 / b12;
			next_v = b01 / b12;
		}
		
		void notify_sigma_update() noexcept {
			polynomial_type remainder;
			quotient_p = p.euclidean_division(sigma, remainder);
			if(remainder.degree() == polynomial_type::minus_infinite_degree())
				assert(false); // not implemented
			apub  = remainder[0];
			b     = remainder.degree() == 2?remainder[1]:0;;
			a     = apub - sigma[1] * b;
			prodp = (sigma[0] * b * b) + (a * apub);
		}
		
		void notify_k_update() noexcept {
			polynomial_type remainder;
			quotient_k = k.euclidean_division(sigma, remainder);
			cpud  = remainder[0];
			d     = remainder[1];
			c     = cpud - sigma[1] * d;
			prodk = (sigma[0] * d * d) + (c * cpud);
			kp0   = k[0]/p[0];
			k1p0  = (k[1] - (kp0 * p[1]))/p[0];
		}
		
		void update_determinants() noexcept {
			detpk  = (b * c) - (a * d);
			detskp = (sigma[0] * b * d) + (c * apub);
		}
		
		polynomial_type p;
		polynomial_type k;
		polynomial_type sigma;
		polynomial_type quotient_p;
		polynomial_type quotient_k;
		
		// as defined in Jenkins and Traub's paper, those quantities define the
		// remainders of the real polynomials P and K divided by the real
		// quadratic polynomial sigma
		// P(z) = Q_{P}(z) * \sigma(z) + (b * z + u) + a
		// K(z) = Q_{K}(z) * \sigma(z) + (d * z + u) + c
		// where \sigma(z) = z^2 + uz + v = (z-s1)(z-s2)
		// (note that sigma being real, s1 = conj(s2))
		real_type a;
		real_type b;
		real_type c;
		real_type d;
		
		// commonly reused quantities
		real_type apub;   // a + u * b
		real_type cpud;   // a + u * d
		real_type prodp;  // P(s1)*P(s2)
		real_type prodk;  // K(s1)*K(s2)
		real_type detpk;  // (P(s1)*K(s2) - P(s2)*K(s1))/(s1-s2)
		real_type detskp; // (s1K(s1)*P(s2) - s2K(s2)*P(s1))/(s1-s2)
		real_type kp0;    // K(0)/P(0)
		real_type k1p0;   // K_{1}(0)/P(0) where K_{1}(z) = (K(z) - kp0*P(z))/z
	};
} // namespace detail_jenkins_traub_hpp_

template<class RealApproximationT, class PolynomialT, class F1, class F2>
bool jenkins_traub
	(const PolynomialT& p
	,F1&& consume_real_root
	,F2&& consume_quadratic_factor
	) {
	return detail_jenkins_traub_hpp_::algorithm
		       <PolynomialT, RealApproximationT>(p).run
			       (std::forward<F1>(consume_real_root)
			       ,std::forward<F2>(consume_quadratic_factor)
			       );
}

#endif // JORDAN_JENKINS_TRAUB_HPP_
