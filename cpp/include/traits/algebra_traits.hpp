#ifndef JORDAN_ALGEBRA_TRAITS_HPP_
#define JORDAN_ALGEBRA_TRAITS_HPP_

#include "vector_space_traits.hpp"
#include "../common.hpp"
#include <type_traits>

namespace detail_algebra_traits_hpp_ {
	template
		<class S
		,class Scalar = typename S::scalar_type
		,class T = typename S::type
		,template<class> class AdmissibleScalar =
			 curried_decays_to<Scalar>::template invoke
		,template<class> class AdmissibleT =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_algebra
	: all_valid
		  <satisfies_vector_space<S, Scalar, T, AdmissibleScalar, AdmissibleT>
		  ,satisfies_multiplicative_monoid<S, T, AdmissibleT>
		  > {};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		,class = void
		>
	struct minimal_algebra_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct minimal_algebra_traits_aux
		<S
		,Scalar
		,T
		,AdmissibleScalar
		,AdmissibleT
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_algebra
				  <S, Scalar, T, AdmissibleScalar, AdmissibleT>::value
			 >::type
		>
	: vector_space_traits_builder<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>
	, multiplicative_monoid_traits_builder<S, T, AdmissibleT, DerivedTraits> {
		static constexpr bool valid = true;
		
		using set_type =
			typename vector_space_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>::
					set_type;
		using type =
			typename vector_space_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>::
				type;
		template<class U>
		using is_admissible_type =
			typename vector_space_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>::
					template is_admissible_type<U>;
		using scalar_type =
			typename vector_space_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>::
					scalar_type;
		template<class U>
		using is_admissible_scalar_type =
			typename vector_space_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>::
					template is_admissible_scalar_type<U>;
		
		using vector_space_traits_builder
			      <S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>::
				      one;
		using multiplicative_monoid_traits_builder
			      <S, T, AdmissibleT, DerivedTraits>::one;
		
		using vector_space_traits_builder
			      <S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>::
				      multiply;
		using multiplicative_monoid_traits_builder
			      <S, T, AdmissibleT, DerivedTraits>::multiply;
	};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct algebra_traits_builder
	: minimal_algebra_traits_aux
		  <S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits> {};
	
	template
		<class S
		,class Scalar = typename S::scalar_type
		,class T = typename S::type
		,template<class> class AdmissibleScalar =
			 curried_decays_to<Scalar>::template invoke
		,template<class> class AdmissibleT =
			 curried_decays_to<T>::template invoke
		>
	struct algebra_traits
	: vector_space_traits_builder
		<S
		,Scalar
		,T
		,AdmissibleScalar
		,AdmissibleT
		,vector_space_traits<S, Scalar, T, AdmissibleScalar, AdmissibleT>
		> {};
} // namespace detail_algebra_traits_hpp_

using detail_algebra_traits_hpp_::satisfies_algebra;
using detail_algebra_traits_hpp_::algebra_traits;
using detail_algebra_traits_hpp_::algebra_traits_builder;

#endif // JORDAN_ALGEBRA_TRAITS_HPP_
