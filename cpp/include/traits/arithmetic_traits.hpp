#ifndef JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_
#define JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_

#include <type_traits>

namespace detail_arithmetic_traits_hpp_ {
#define STRIP_PARENTHESES_local_aux(...) __VA_ARGS__
#define STRIP_PARENTHESES_local(a) STRIP_PARENTHESES_local_aux a
/** Implements a trait class defining a static constexpr bool 'value' member
 *  whose value is true iff 'expression' is legal and can either be
 *  implicitly converted to a value of type 'expression_type' or
 *  'expression''s type and 'expression_type' are both possibly cv-qualified
 *  'void'.
 *  The types used must be complete.
 *
 *  Those traits contain a template class "compile_time_check" which, if
 *  instantiated with void, will cause a compilation failure hopefully
 *  displaying the string litteral description in the error messages.
 */
#define JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait(             \
	         trait_name                                                        \
	        ,template_list                                                     \
	        ,template_args                                                     \
	        ,expression                                                        \
	        ,expression_type                                                   \
	        ,description                                                       \
	        )                                                                  \
	template<STRIP_PARENTHESES_local(template_list), class = void>             \
	struct trait_name ## _aux : std::false_type {                              \
		template<class U>                                                      \
		struct compile_time_check {                                            \
			static_assert(!std::is_same<U, void>::value, description);         \
		};                                                                     \
	};                                                                         \
	                                                                           \
	template<STRIP_PARENTHESES_local(template_list)>                           \
	struct trait_name ## _aux                                                  \
		<STRIP_PARENTHESES_local(template_args)                                \
		,typename std::enable_if                                               \
			 <std::is_convertible                                              \
				  <decltype(expression)                                        \
				  ,expression_type                                             \
				  >::value                                                     \
			 >::type                                                           \
		>                                                                      \
	: std::true_type { template<class U> struct compile_time_check {}; };      \
	                                                                           \
	template<STRIP_PARENTHESES_local(template_list)>                           \
	struct trait_name                                                          \
	: trait_name ## _aux<STRIP_PARENTHESES_local(template_args)> {}

	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(has_static_one
		,(class T)
		,(T)
		,T::one()
		,T
		,"static one is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(has_static_zero
		,(class T)
		,(T)
		,T::zero()
		,T
		,"static zero is not defined."
		);
	
	template<class T, class = void>
	struct one_default_definition /* undefined */;
	
#define JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()                       \
std::decay<T>::type::one()
	template<class T>
	struct one_default_definition
		<T
		,typename std::enable_if
			 <has_static_one<typename std::decay<T>::type>::value>::type
		> {
		static auto one()
		noexcept(noexcept(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl();
		}
	};
#undef JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl
	template<class T>
	struct one_default_definition
		<T
		,typename std::enable_if
			 <std::is_constructible<typename std::decay<T>::type, int>::value
			  && !has_static_one<typename std::decay<T>::type>::value
			 >::type
		> {
		static typename std::decay<T>::type one() noexcept {
			return static_cast<typename std::decay<T>::type>(1);
		}
	};
	
	template<class T, class = void>
	struct zero_default_definition /* undefined */;
#define JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()                       \
std::decay<T>::type::zero()
	template<class T>
	struct zero_default_definition
		<T
		,typename std::enable_if
			 <has_static_zero<typename std::decay<T>::type>::value>::type
		> {
		static auto zero()
		noexcept(noexcept(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl();
		}
	};
#undef JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl
	
	template<class T>
	struct zero_default_definition
		<T
		,typename std::enable_if
			 <std::is_constructible<typename std::decay<T>::type, int>::value
			  && !has_static_zero<typename std::decay<T>::type>::value
			 >::type
		> {
		static typename std::decay<T>::type zero() noexcept {
			return static_cast<typename std::decay<T>::type>(0);
		}
	};
}

namespace default_arithmetic_definition {
	template<class T>
	struct one_definition
	: detail_arithmetic_traits_hpp_::one_default_definition<T> {};
	template<class T>
	struct zero_definition
	: detail_arithmetic_traits_hpp_::zero_default_definition<T> {};
} // namespace extend_monoid

namespace detail_arithmetic_traits_hpp_ {

	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_equality_comparable
		,(class Lhs, class Rhs)
		,(Lhs, Rhs)
		,std::declval<Lhs>() == std::declval<Rhs>()
		,bool
		,"operator== is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_inequality_comparable
		,(class Lhs, class Rhs)
		,(Lhs, Rhs)
		,std::declval<Lhs>() != std::declval<Rhs>()
		,bool
		,"operator!= is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_eq
		,(class S, class Lhs, class Rhs)
		,(S, Lhs, Rhs)
		,std::declval<S>().eq(std::declval<Lhs>(), std::declval<Rhs>())
		,bool
		,"eq is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_neq
		,(class S, class Lhs, class Rhs)
		,(S, Lhs, Rhs)
		,std::declval<S>().neq(std::declval<Lhs>(), std::declval<Rhs>())
		,bool
		,"neq is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_summable
		,(class Lhs, class Rhs, class R)
		,(Lhs, Rhs, R)
		,std::declval<Lhs>() + std::declval<Rhs>()
		,R
		,"operator+ is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_add
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().add(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"add is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_multipliable
		,(class Lhs, class Rhs, class R)
		,(Lhs, Rhs, R)
		,std::declval<Lhs>() * std::declval<Rhs>()
		,R
		,"operator* is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_multiply
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().multiply(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"multiply is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(has_nontrivial_remainder
		,(class Lhs, class Rhs, class R)
		,(Lhs, Rhs, R)
		,std::declval<Lhs>() % std::declval<Rhs>()
		,R
		,"operator% is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_remainder
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().remainder(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"remainder is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(has_quotient
		,(class Lhs, class Rhs, class R)
		,(Lhs, Rhs, R)
		,std::declval<Lhs>() / std::declval<Rhs>()
		,R
		,"operator/ is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_quotient
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().quotient(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"quotient is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_inverse
		,(class S, class T, class R)
		,(S, T, R)
		,std::declval<S>().inverse(std::declval<T>())
		,R
		,"inverse is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_invertible_class
		,(class T, class R)
		,(T, R)
		,std::declval<T>().inverse()
		,R
		,"member inverse is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_invertible_number
		,(class T, class R)
		,(T, R)
		,R(1)/std::declval<T>()
		,R
		,"t(1)/x is not legal."
		);
	
	template<class T, class R, class = void>
	struct is_invertible_aux : std::false_type {};
	template<class T, class R>
	struct is_invertible_aux
		<T
		,R
		,typename std::enable_if<is_invertible_class<T, R>::value>::type
		>
	: std::true_type {
#	define JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()                    \
		static_cast<T>(x).inverse()
		static R inverse(T x)
		noexcept(noexcept(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl
	};
	
	template<class T, class R>
	struct is_invertible_aux
		<T
		,R
		,typename std::enable_if
			<!is_invertible_class<T, R>::value
			 && is_invertible_number<T, R>::value
			>::type
		>
	: std::true_type {
#	define JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()                    \
		R(1)/static_cast<T>(x)
		static R inverse(T x)
		noexcept(noexcept(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl
	};
	
	template<class T, class R>
	struct is_invertible
	: std::integral_constant
		  <bool
		  ,is_invertible_aux<T, R>::value
		   && !has_nontrivial_remainder<T, T, R>::value
		  > {
		template<class U>
		struct compile_time_check {
			static_assert
				(is_invertible<T, R>::value || !std::is_same<U, void>::value
				,"Type is not invertible."
				);
		};
	};
	
#define JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()                       \
	is_invertible_aux<T&&, R>::inverse(std::forward<T>(x))
	template<class R, class T>
	auto convertible_inverse(T&& x)
	noexcept(noexcept(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl())) ->
	decltype(JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl()) {
		return JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl();
	}
#undef JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_impl
	
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_opposite
		,(class S, class T, class R)
		,(S, T, R)
		,std::declval<S>().opposite(std::declval<T>())
		,R
		,"opposite is not defined"
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_additively_invertible
		,(class T, class R)
		,(T, R)
		,-std::declval<T>()
		,R
		,"unary operator- is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_divide
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().divide(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"divide is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_divisible
		,(class Lhs, class Rhs, class R)
		,(Lhs, Rhs, R)
		,std::declval<Lhs>() / std::declval<Rhs>()
		,R
		,"operator/ is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_substract
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().substract(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"substract is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(is_substractible
		,(class Lhs, class Rhs, class R)
		,(Lhs, Rhs, R)
		,std::declval<Lhs>() - std::declval<Rhs>()
		,R
		,"operator- is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_euclidean_divison
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().euclidean_divison
			 (std::declval<Lhs>(), std::declval<Rhs>(), std::declval<R&>())
		,R
		,"euclidean_division is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(has_euclidean_divison
		,(class Lhs, class Rhs, class R)
		,(Lhs, Rhs, R)
		,std::declval<Lhs>().euclidean_divison
			 (std::declval<Rhs>(), std::declval<R&>())
		,R
		,"euclidean division is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_gcd
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().gcd(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"gcd is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_lcm
		,(class S, class Lhs, class Rhs, class R)
		,(S, Lhs, Rhs, R)
		,std::declval<S>().gcd(std::declval<Lhs>(), std::declval<Rhs>())
		,R
		,"lcm is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(has_global_one
		,(class T)
		,(T)
		,default_arithmetic_definition::one_definition<T>::one()
		,T
		,"static one is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(has_global_zero
		,(class T)
		,(T)
		,default_arithmetic_definition::zero_definition<T>::zero()
		,T
		,"static zero is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_one
		,(class S, class R)
		,(S, R)
		,std::declval<S>().template one<R>()
		,R
		,"one is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_zero
		,(class S, class R)
		,(S, R)
		,std::declval<S>().template zero<R>()
		,R
		,"zero is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_axpy
		,(class S, class A, class X, class Y, class R)
		,(S, A, X, Y, R)
		,std::declval<S>().axpy
			 (std::declval<A>(), std::declval<X>(), std::declval<Y>())
		,R
		,"axpy is not defined."
		);
	
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_monomial
		,(class S, class SizeT, class Scalar, class R)
		,(S, SizeT, Scalar, R)
		,std::declval<S>().monomial
			 (std::declval<SizeT>(), std::declval<Scalar>())
		,R
		,"monomial is not defined."
		);
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_size
		,(class S, class T, class SizeT)
		,(S, T, SizeT)
		,std::declval<S>().size(std::declval<T>())
		,SizeT
		,"size is not defined."
		);
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_degree
		,(class S, class T, class SizeT)
		,(S, T, SizeT)
		,std::declval<S>().degree(std::declval<T>())
		,SizeT
		,"degree is not defined."
		);
	JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
		(does_define_nth
		,(class S, class SizeT, class R)
		,(S, SizeT, R)
		,std::declval<S>().nth(std::declval<SizeT>())
		,R
		,"nth is not defined."
		);
#undef JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_local_implement_trait
#undef STRIP_PARENTHESES_local_aux
#undef STRIP_PARENTHESES_local
} // namespace detail_arithmetic_traits_hpp_

using detail_arithmetic_traits_hpp_::is_equality_comparable;
using detail_arithmetic_traits_hpp_::is_inequality_comparable;
using detail_arithmetic_traits_hpp_::does_define_eq;
using detail_arithmetic_traits_hpp_::does_define_neq;
using detail_arithmetic_traits_hpp_::is_summable;
using detail_arithmetic_traits_hpp_::is_multipliable;
using detail_arithmetic_traits_hpp_::does_define_add;
using detail_arithmetic_traits_hpp_::does_define_multiply;
using detail_arithmetic_traits_hpp_::has_nontrivial_remainder;
using detail_arithmetic_traits_hpp_::does_define_remainder;
using detail_arithmetic_traits_hpp_::has_quotient;
using detail_arithmetic_traits_hpp_::does_define_quotient;
using detail_arithmetic_traits_hpp_::is_invertible;
using detail_arithmetic_traits_hpp_::convertible_inverse;
using detail_arithmetic_traits_hpp_::is_additively_invertible;
using detail_arithmetic_traits_hpp_::does_define_inverse;
using detail_arithmetic_traits_hpp_::does_define_opposite;
using detail_arithmetic_traits_hpp_::is_divisible;
using detail_arithmetic_traits_hpp_::is_substractible;
using detail_arithmetic_traits_hpp_::does_define_divide;
using detail_arithmetic_traits_hpp_::does_define_substract;
using detail_arithmetic_traits_hpp_::does_define_euclidean_divison;
using detail_arithmetic_traits_hpp_::has_euclidean_divison;
using detail_arithmetic_traits_hpp_::does_define_gcd;
using detail_arithmetic_traits_hpp_::does_define_lcm;
using detail_arithmetic_traits_hpp_::has_global_one;
using detail_arithmetic_traits_hpp_::has_global_zero;
using detail_arithmetic_traits_hpp_::does_define_one;
using detail_arithmetic_traits_hpp_::does_define_zero;
using detail_arithmetic_traits_hpp_::does_define_axpy;
using detail_arithmetic_traits_hpp_::does_define_monomial;
using detail_arithmetic_traits_hpp_::does_define_size;
using detail_arithmetic_traits_hpp_::does_define_degree;
using detail_arithmetic_traits_hpp_::does_define_nth;
#endif // JORDAN_TRAITS_ARITHMETIC_TRAITS_HPP_
