#ifndef JORDAN_TRAITS_FIELD_TRAITS_HPP_
#define JORDAN_TRAITS_FIELD_TRAITS_HPP_

#include "group_traits.hpp"
#include "monoid_traits.hpp"
#include "../common.hpp"
#include <type_traits>

namespace extend_wrapper {
	struct gcd_tag {};
	struct lcm_tag {};
	
#define JORDAN_FIELD_TRAITS_HPP_local_impl()                                   \
	mkexpr<gcd_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto gcd(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_FIELD_TRAITS_HPP_local_impl()) {
		return JORDAN_FIELD_TRAITS_HPP_local_impl();
	}
#undef JORDAN_FIELD_TRAITS_HPP_local_impl
	
#define JORDAN_FIELD_TRAITS_HPP_local_impl()                                   \
	mkexpr<lcm_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto lcm(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_FIELD_TRAITS_HPP_local_impl()) {
		return JORDAN_FIELD_TRAITS_HPP_local_impl();
	}
#undef JORDAN_FIELD_TRAITS_HPP_local_impl
} // namespace extend_wrapper

namespace detail_field_traits_hpp_ {
	template<class Traits1, class Traits2, class = void>
	struct have_same_set_type_aux : std::false_type {
		template<class U>
		struct compile_time_check {
			static_assert
				(!std::is_same<U, void>::value
				,"traits apply to different sets."
				);
		};
	};
	
	template<class Traits1, class Traits2>
	struct have_same_set_type_aux
		<Traits1
		,Traits2
		,typename std::enable_if
			 <std::is_same
				  <typename Traits1::set_type, typename Traits2::set_type
				  >::value
			 >::type
		>
	: std::true_type { template<class U> struct compile_time_check {}; };
	
	template<class Traits1, class Traits2>
	struct have_same_set_type : have_same_set_type_aux<Traits1, Traits2> {};
	
	
	template<class Traits1, class Traits2, class = void>
	struct have_same_type_aux : std::false_type {
		template<class U>
		struct compile_time_check {
			static_assert
				(!std::is_same<U, void>::value
				,"traits apply to different types."
				);
		};
	};
	
	template<class Traits1, class Traits2>
	struct have_same_type_aux
		<Traits1
		,Traits2
		,typename std::enable_if
			 <std::is_same
				  <typename Traits1::type, typename Traits2::type>::value
			 >::type
		>
	: std::true_type { template<class U> struct compile_time_check {}; };
	
	template<class Traits1, class Traits2>
	struct have_same_type : have_same_type_aux<Traits1, Traits2> {};
	
	template<class Traits1, class Traits2>
	struct compatible_traits
	: all_valid
		  <have_same_set_type<Traits1, Traits2>
		  ,have_same_type<Traits1, Traits2>
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_semiring
	: all_valid
		  <satisfies_additive_monoid<S, T, Admissible>
		  ,satisfies_multiplicative_monoid<S, T, Admissible>
		  ,compatible_traits
			    <additive_monoid_traits<S, T, Admissible>
			    ,multiplicative_monoid_traits<S, T, Admissible>
			    >
		  > {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_semiring_element
	: all_valid
		  <satisfies_additive_monoid_element<T, Admissible>
		  ,satisfies_multiplicative_monoid_element<T, Admissible>
		  ,have_same_type
			   <additive_monoid_traits
				    <additive_monoid_of<T, Admissible>, T, Admissible>
			   ,multiplicative_monoid_traits
				    <multiplicative_monoid_of<T, Admissible>, T, Admissible>
			   >
		  > {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_semiring_traits_aux { static constexpr bool valid = false; };
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_semiring_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_semiring<S, T, Admissible>::value>::type
		>
	: additive_monoid_traits_builder<S, T, Admissible, DerivedTraits>
	, multiplicative_monoid_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename additive_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::type;
		using typename additive_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		static constexpr bool valid = true;
		template<class U>
		using is_admissible_type = Admissible<U>;
		
		using additive_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
		using multiplicative_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_semiring_traits
	: minimal_semiring_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template<class T, template<class> class Admissible, class = void>
	struct semiring_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct semiring_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_semiring_element<T, Admissible>::value>::type
		>
	: virtual additive_monoid_of<T, Admissible>
	, virtual multiplicative_monoid_of<T, Admissible> {
		using typename additive_monoid_of<T, Admissible>::type;
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct semiring_of : virtual semiring_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct semiring_traits_builder
	: minimal_semiring_traits<S, T, Admissible, DerivedTraits> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct semiring_traits
	: semiring_traits_builder
		  <S, T, Admissible, semiring_traits<S, T, Admissible>> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_ring
	: all_valid
		  <satisfies_additive_group<S, T, Admissible>
		  ,satisfies_semiring<S, T, Admissible>
		  ,compatible_traits
			   <additive_group_traits<S, T, Admissible>
			   ,semiring_traits<S, T, Admissible>
			   >
		  > {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_ring_element
	: all_valid
		  <satisfies_additive_group_element<T, Admissible>
		  ,satisfies_semiring_element<T, Admissible>
		  ,have_same_type
			   <additive_group_traits<additive_group_of<T, Admissible>>
			   ,semiring_traits<semiring_of<T, Admissible>>
			   >
		  > {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_ring_traits_aux { static constexpr bool valid = false; };
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_ring_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if<satisfies_ring<S, T, Admissible>::value>::type
		>
	: additive_group_traits_builder<S, T, Admissible, DerivedTraits>
	, semiring_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename semiring_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		using typename semiring_traits_builder
			      <S, T, Admissible, DerivedTraits>::type;
		static constexpr bool valid = true;
		template<class U>
		using is_admissible_type = Admissible<U>;
		
		using additive_group_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
		using semiring_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_ring_traits
	: minimal_ring_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template<class T, template<class> class Admissible, class = void>
	struct ring_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct ring_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_ring_element<T, Admissible>::value>::type
		>
	: virtual additive_group_of<T, Admissible>
	, virtual semiring_of<T, Admissible> {
		using typename semiring_of<T, Admissible>::type;
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct ring_of : virtual ring_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct ring_traits_builder
	: minimal_ring_traits<S, T, Admissible, DerivedTraits> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct ring_traits
	: ring_traits_builder<S, T, Admissible, ring_traits<S, T, Admissible>> {};
	
	
	template<class S, class T, template<class> class Admissible>
	struct has_quotient_and_remainder
	: all_valid
		  <check_for
			   <does_define_quotient
			   ,S
			   ,any_cref<T>
			   ,any_cref<T>
			   ,T
			   >
		  ,check_for
			   <does_define_remainder
			   ,S
			   ,any_cref<T>
			   ,any_cref<T>
			   ,T
			   >
		  > {};
	
	template<class S, class T, template<class> class Admissible>
	struct has_divide
	: check_for
		  <does_define_divide
		  ,S
		  ,any_cref<T>
		  ,any_cref<T>
		  ,T
		  > {};
	
	template<class S, class T, template<class> class Admissible, class = void>
	struct satisfies_euclidean_domain_aux : std::false_type {};
	
	template<class S, class T, template<class> class Admissible>
	struct satisfies_euclidean_domain_aux
		<S
		,T
		,Admissible
		,typename std::enable_if
			 <satisfies_ring<S, T, Admissible>::value
			  && has_quotient_and_remainder<S, T, Admissible>::value
			 >::type
		>
	: std::true_type {
		using set_type = typename ring_traits<S, T, Admissible>::set_type;
		static constexpr bool trivial_remainder = false;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		s.quotient(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto quotient(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		s.remainder(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto remainder(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template<class S, class T, template<class> class Admissible>
	struct satisfies_euclidean_domain_aux
		<S
		,T
		,Admissible
		,typename std::enable_if
			 <satisfies_ring<S, T, Admissible>::value
			  && has_divide<S, T, Admissible>::value
			 >::type
		>
	: std::true_type {
		using set_type = typename ring_traits<S, T, Admissible>::set_type;
		static constexpr bool trivial_remainder = true;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		s.divide(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto quotient(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		s.zero()
		template<class Lhs, class Rhs>
		static auto remainder(const set_type& s, Lhs&&, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			assert(!s.is_zero(rhs));
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_euclidean_domain
	: all_valid
		  <satisfies_ring<S, T, Admissible>
		  ,one_valid
			   <has_divide<S, T, Admissible>
			   ,has_quotient_and_remainder<S, T, Admissible>
			   >
		  > {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_euclidean_domain_element
	: all_valid
		<satisfies_ring_element<T, Admissible>
		,check_for
			 <has_quotient
			 ,any_cref<T>
			 ,any_cref<T>
			 ,T
			 >
		,one_valid
			 <check_for<has_nontrivial_remainder, any_cref<T>, any_cref<T>, T>
			 ,check_for<is_invertible, any_cref<T>, T>
			 >
		> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_euclidean_domain_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_euclidean_domain_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_euclidean_domain<S, T, Admissible>::value>::type
		>
	: ring_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename ring_traits_builder
			      <S, T, Admissible, DerivedTraits>::type;
		using typename ring_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		static constexpr bool valid = true;
		static constexpr bool trivial_remainder =
			satisfies_euclidean_domain_aux<S, T, Admissible>::
				trivial_remainder;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		satisfies_euclidean_domain_aux<S, T, Admissible>::quotient             \
			(s, std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto quotient(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		satisfies_euclidean_domain_aux<S, T, Admissible>::remainder            \
			(s, std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto remainder(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
		
		using ring_traits_builder<S, T, Admissible, DerivedTraits>::eval;
#	define JORDAN_FIELD_TRAITS_HPP_local_impl()                                \
		quotient                                                               \
			(s                                                                 \
			,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval  \
				 (s, lhs(std::move(e)))                                        \
			,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval  \
				 (s, rhs(std::move(e)))                                        \
			)
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::quotient_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_FIELD_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_euclidean_domain_traits
	: minimal_euclidean_domain_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template<class Traits, class S, class = void>
	struct implement_euclidean_divison_aux : Traits {
		using typename Traits::type;
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_quotient_impl()                \
		Traits::quotient(s, std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_remainder_impl()               \
		Traits::remainder(s, lhs, rhs)
		template<class Lhs, class Rhs>
		static auto euclidean_divison
			(const set_type& s, Lhs&& lhs, Rhs&& rhs, type& rem)
		noexcept(
			noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_remainder_impl())
			&& noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_quotient_impl())
		) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_quotient_impl()) {
			rem = JORDAN_TRAITS_FIELD_TRAITS_HPP_local_remainder_impl();
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_quotient_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_remainder_impl
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_quotient_impl
	};
	
	template<class Traits, class S>
	struct implement_euclidean_divison_aux
		<Traits
		,S
		,typename std::enable_if
			 <check_for
				  <does_define_euclidean_divison
				  ,S
				  ,any_cref<typename Traits::type>
				  ,any_cref<typename Traits::type>
				  ,typename Traits::type
				  >::value
			 >::type
		>
	: Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		s.euclidean_divison                                                    \
			(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs), std::forward<R>(r))
		template<class Lhs, class Rhs, class R>
		static auto euclidean_divison
			(const set_type& s, Lhs&& lhs, Rhs&& rhs, R&& r)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_euclidean_divison
	: implement_euclidean_divison_aux<Traits, S> {};
	
	template<class Traits, class S, class = void>
	struct implement_gcd_aux : Traits {
		using typename Traits::type;
		using typename Traits::set_type;
		
		template<class Lhs, class Rhs>
		static type gcd(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(
			noexcept(Traits::is_zero(s, static_cast<const type&>(lhs)))
			&& noexcept
				   (Traits::remainder
					   (s, std::declval<type>(), std::declval<type>())
				   )
			&& noexcept(type(std::forward<Lhs>(lhs)))
			&& noexcept(type(std::forward<Rhs>(rhs)))
			&& noexcept(std::declval<type&>() = std::declval<type>())
		) {
			if(Traits::is_zero(s, static_cast<const type&>(lhs)))
				return std::move(rhs);
			type dividend(std::forward<Lhs>(lhs));
			type quotient(std::forward<Rhs>(rhs));
			while(!Traits::is_zero(s, quotient)) {
				type tmp(quotient);
				quotient =
					Traits::remainder
						(s, std::move(dividend), std::move(quotient));
				dividend = std::move(tmp);
			}
			return dividend;
		}
	};
	
	template<class Traits, class S>
	struct implement_gcd_aux
		<Traits
		,S
		,typename std::enable_if
			 <check_for
				  <does_define_gcd
				  ,S
				  ,any_cref<typename Traits::type>
				  ,any_cref<typename Traits::type>
				  ,typename Traits::type
				  >::value
			 >::type
		>
	: Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		s.gcd(std::forward<T>(lhs), std::forward<U>(rhs))
		template<class T, class U>
		static auto gcd(const set_type& s, T&& lhs, U&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_gcd : implement_gcd_aux<Traits, S> {};
	
	template<class Traits, class S, class = void>
	struct implement_lcm_aux
	: Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		Traits::multiply                                                       \
			(s                                                                 \
			,Traits::quotient                                                  \
				 (s                                                            \
				 ,std::forward<Lhs>(lhs)                                       \
				 ,Traits::gcd                                                  \
					  (s, std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))      \
				 )                                                             \
			,std::forward<Rhs>(rhs)                                            \
			)
		template<class Lhs, class Rhs>
		static auto lcm(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S>
	struct implement_lcm_aux
		<Traits
		,S
		,typename std::enable_if
			 <check_for
				  <does_define_gcd
				  ,S
				  ,any_cref<typename Traits::type>
				  ,any_cref<typename Traits::type>
				  ,typename Traits::type
				  >::value
			 >::type
		>
	: Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		s.lcm(std::forward<T>(lhs), std::forward<U>(rhs))
		template<class T, class U>
		static auto lcm(const set_type& s, T&& lhs, U&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_lcm : implement_lcm_aux<Traits, S> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct euclidean_domain_traits_builder
	: implement_traits
		  <minimal_euclidean_domain_traits<S, T, Admissible, DerivedTraits>
		  ,S
		  ,DerivedTraits
		  ,implement_euclidean_divison
		  ,implement_gcd
		  ,implement_lcm
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct euclidean_domain_traits
	: euclidean_domain_traits_builder
		  <S, T, Admissible, euclidean_domain_traits<S, T, Admissible>> {};
	
	template<class T, template<class> class Admissible, class = void>
	struct euclidean_domain_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct euclidean_domain_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_euclidean_domain_element<T, Admissible>::value
			  && check_for
				     <has_nontrivial_remainder, any_cref<T>, any_cref<T>, T
				     >::value
			  && !check_for<is_invertible, any_cref<T>, T>::value
			 >::type
		>
	: virtual ring_of<T, Admissible> {
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		std::forward<Lhs>(lhs) / std::forward<Rhs>(rhs)
		template<class Lhs, class Rhs>
		auto quotient(Lhs&& lhs, Rhs&& rhs) const
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		std::forward<Lhs>(lhs) % std::forward<Rhs>(rhs)
		template<class Lhs, class Rhs>
		auto remainder(Lhs&& lhs, Rhs&& rhs) const
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template<class T, template<class> class Admissible>
	struct euclidean_domain_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_euclidean_domain_element<T, Admissible>::value
			  && check_for<is_invertible, any_cref<T>, T>::value
			 >::type
		>
	: virtual ring_of<T, Admissible> {
#	define JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()                         \
		std::forward<Lhs>(lhs) / std::forward<Rhs>(rhs)
		template<class Lhs, class Rhs>
		auto divide(Lhs&& lhs, Rhs&& rhs) const
		noexcept(noexcept(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_FIELD_TRAITS_HPP_local_impl
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct euclidean_domain_of
	: virtual euclidean_domain_of_aux<T, Admissible> {};
	
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_field
	: all_valid
		  <satisfies_multiplicative_group<S, T, Admissible>
		  ,satisfies_euclidean_domain<S, T, Admissible>
		  ,compatible_traits
			   <euclidean_domain_traits<S, T, Admissible>
			   ,multiplicative_group_traits<S, T, Admissible>
			   >
		  ,compatible_traits
			   <euclidean_domain_traits<S, T, Admissible>
			   ,additive_group_traits<S, T, Admissible>
			   >
		  > {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_field_element
	: all_valid
		  <satisfies_euclidean_domain_element<T, Admissible>
		  ,satisfies_multiplicative_monoid_element<T, Admissible>
		  ,have_same_type
			   <euclidean_domain_traits<euclidean_domain_of<T, Admissible>>
			   ,multiplicative_group_traits
				    <multiplicative_group_of<T, Admissible>>
			   >
		  > {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_field_traits_aux { static constexpr bool valid = false; };
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_field_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_field<S, T, Admissible>::value>::type
		>
	: multiplicative_group_traits_builder<S, T, Admissible, DerivedTraits>
	, euclidean_domain_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename euclidean_domain_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		using typename euclidean_domain_traits_builder
			      <S, T, Admissible, DerivedTraits>::type;
		static constexpr bool valid = true;
		template<class U>
		using is_admissible_type = Admissible<U>;
		//*
		// conflict with the evaluation of quotients from euclidean_domain
		// still necessary for the evaluation inverses
		// TODO
		using multiplicative_group_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
		//*/
		// required for +,-,%
		/*
		using euclidean_domain_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
		/*/
		using euclidean_domain_traits_builder
			<S, T, Admissible, DerivedTraits>::
				template additive_group_traits_builder
					<S, T, Admissible, DerivedTraits>::eval;
		//*/
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_field_traits
	: minimal_field_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template<class T, template<class> class Admissible, class = void>
	struct field_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct field_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_field_element<T, Admissible>::value>::type
		>
	: virtual euclidean_domain_of<T, Admissible>
	, virtual multiplicative_group_of<T, Admissible> {
		using typename euclidean_domain_of<T, Admissible>::type;
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct field_of : virtual field_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct field_traits_builder
	: minimal_field_traits<S, T, Admissible, DerivedTraits> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct field_traits
	: field_traits_builder<S, T, Admissible, field_traits<S, T, Admissible>> {};
} // namespace detail_field_traits_hpp_

using detail_field_traits_hpp_::satisfies_semiring;
using detail_field_traits_hpp_::satisfies_ring;
using detail_field_traits_hpp_::satisfies_euclidean_domain;
using detail_field_traits_hpp_::satisfies_field;
using detail_field_traits_hpp_::satisfies_semiring_element;
using detail_field_traits_hpp_::satisfies_ring_element;
using detail_field_traits_hpp_::satisfies_euclidean_domain_element;
using detail_field_traits_hpp_::satisfies_field_element;
using detail_field_traits_hpp_::semiring_of;
using detail_field_traits_hpp_::ring_of;
using detail_field_traits_hpp_::euclidean_domain_of;
using detail_field_traits_hpp_::field_of;
using detail_field_traits_hpp_::semiring_traits;
using detail_field_traits_hpp_::semiring_traits_builder;
using detail_field_traits_hpp_::ring_traits;
using detail_field_traits_hpp_::ring_traits_builder;
using detail_field_traits_hpp_::euclidean_domain_traits;
using detail_field_traits_hpp_::euclidean_domain_traits_builder;
using detail_field_traits_hpp_::field_traits;
using detail_field_traits_hpp_::field_traits_builder;

#endif // JORDAN_TRAITS_FIELD_TRAITS_HPP_
