#ifndef JORDAN_TRAITS_GROUP_TRAITS_HPP_
#define JORDAN_TRAITS_GROUP_TRAITS_HPP_

#include "monoid_traits.hpp"
#include "arithmetic_traits.hpp"
#include "../common.hpp"
#include <type_traits>

namespace extend_wrapper {
	struct quotient_tag {};
	struct difference_tag {};
	struct opposite_tag {};
	struct inverse_tag {};
	
#define JORDAN_GROUP_TRAITS_HPP_local_impl()                                   \
	mkexpr<quotient_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator/(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
		return JORDAN_GROUP_TRAITS_HPP_local_impl();
	}
#undef JORDAN_GROUP_TRAITS_HPP_local_impl
	
#define JORDAN_GROUP_TRAITS_HPP_local_impl()                                   \
	mkexpr<difference_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator-(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
		return JORDAN_GROUP_TRAITS_HPP_local_impl();
	}
#undef JORDAN_GROUP_TRAITS_HPP_local_impl
	
#define JORDAN_GROUP_TRAITS_HPP_local_impl()                                   \
	mkexpr<opposite_tag>(std::forward<Op>(op))
	template<class Op>
	auto operator-(Op&& op) noexcept ->
	decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
		return JORDAN_GROUP_TRAITS_HPP_local_impl();
	}
#undef JORDAN_GROUP_TRAITS_HPP_local_impl
	
#define JORDAN_GROUP_TRAITS_HPP_local_impl()                                   \
mkexpr<inverse_tag>(std::forward<Op>(op))
	template<class Op>
	auto inv(Op&& op) noexcept ->
	decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
		return JORDAN_GROUP_TRAITS_HPP_local_impl();
	}
#undef JORDAN_GROUP_TRAITS_HPP_local_impl
} // namespace extend_wrapper

namespace detail_group_traits_hpp_ {
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_multiplicative_group
	: all_valid
		  <check_for
			   <does_define_inverse
			   ,S
			   ,any_cref<T>
			   ,T
			   >
		  ,satisfies_multiplicative_monoid<S, T, Admissible>
		> {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_multiplicative_group_element
	: all_valid
		  <satisfies_multiplicative_monoid_element<T, Admissible>
		  ,check_for<is_invertible, any_cref<T>, T>
		  > {};
	
	template<class T, template<class> class Admissible, class = void>
	struct multiplicative_group_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct multiplicative_group_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_multiplicative_group_element<T, Admissible>::value>::type
		>
	: virtual multiplicative_monoid_of<T, Admissible> {
		using typename multiplicative_monoid_of<T, Admissible>::type;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()                         \
		convertible_inverse<T>(std::forward<U>(x))
		template<class U>
		auto inverse(U&& x) const
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct multiplicative_group_of
	: virtual multiplicative_group_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_multiplicative_group_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_multiplicative_group_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_multiplicative_group<S, T, Admissible>::value>::type
		>
	: multiplicative_monoid_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename multiplicative_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		static constexpr bool valid = true;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()                         \
		s.inverse(std::forward<Op>(x))
		template<class Op>
		static auto inverse(const set_type& s, Op&& x)
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
		using multiplicative_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
#	define JORDAN_GROUP_TRAITS_HPP_local_impl()                                \
		inverse                                                                \
			(s                                                                 \
			,delay<Op>::template invoke<DerivedTraits>::type::eval             \
				(s, extend_wrapper::nth<0>(std::move(e)))                      \
			)
		template<class Op>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression<extend_wrapper::inverse_tag, Op>&& e
			)
		noexcept(noexcept(JORDAN_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_GROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_multiplicative_group_traits
	: minimal_multiplicative_group_traits_aux
		  <S, T, Admissible, DerivedTraits> {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_additive_group_element
	: all_valid
		  <satisfies_additive_monoid_element<T, Admissible>
		  ,check_for<is_additively_invertible, any_cref<T>, T>
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_additive_group
	: all_valid
		  <satisfies_additive_monoid<S, T, Admissible>
		  ,check_for
			   <does_define_opposite
			   ,S
			   ,any_cref<T>
			   ,T
			   >
		  > {};
	
	template<class T, template<class> class Admissible, class = void>
	struct additive_group_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct additive_group_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_additive_group_element<T, Admissible>::value>::type
		>
	: virtual additive_monoid_of<T, Admissible> {
		using typename additive_monoid_of<T, Admissible>::type;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl() -std::forward<U>(x)
		template<class U>
		auto opposite(U&& x) const
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct additive_group_of : virtual additive_group_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_additive_group_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_additive_group_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_additive_group<S, T, Admissible>::value>::type
		>
	: additive_monoid_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename additive_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		static constexpr bool valid = true;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()                         \
		s.opposite(std::forward<Op>(x))
		template<class Op>
		static auto opposite(const set_type& s, Op&& x)
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
		using additive_monoid_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
#	define JORDAN_GROUP_TRAITS_HPP_local_impl()                                \
		opposite                                                               \
			(s                                                                 \
			,delay<Op>::template invoke<DerivedTraits>::type::eval             \
				(s, extend_wrapper::nth<0>(std::move(e)))                      \
			)
		template<class Op>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression<extend_wrapper::opposite_tag, Op>&& e
			)
		noexcept(noexcept(JORDAN_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_GROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_additive_group_traits
	: minimal_additive_group_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template<class Traits, class S, class = void>
	struct implement_divide_aux : Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()                         \
		Traits::multiply                                                       \
			(s                                                                 \
			,std::forward<Lhs>(lhs)                                            \
			,Traits::inverse(s, std::forward<Rhs>(rhs))                        \
			)
		template<class Lhs, class Rhs>
		static auto divide(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S>
	struct implement_divide_aux
		<Traits
		,S
		,typename std::enable_if
			 <check_for
				  <does_define_divide
				  ,S
				  ,any_cref<typename Traits::type>
				  ,any_cref<typename Traits::type>
				  ,typename Traits::type
				  >::value
			 >::type
		>
	: Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()                         \
		s.divide(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto divide(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_divide : implement_divide_aux<Traits, S> {
		using typename implement_divide_aux<Traits, S>::set_type;
		using implement_divide_aux<Traits, S>::eval;
#	define JORDAN_GROUP_TRAITS_HPP_local_impl()                                \
		implement_divide_aux<Traits, S>::divide(s                              \
		      ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval\
			       (s, lhs(std::move(e)))                                      \
		      ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval\
			       (s, rhs(std::move(e)))                                      \
		      )
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::quotient_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_GROUP_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class = void>
	struct implement_substract_aux : Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()                         \
		Traits::add                                                            \
			(s, std::forward<T>(lhs), Traits::opposite(s, std::forward<U>(rhs)))
		template<class T, class U>
		static auto substract(const set_type& s, T&& lhs, U&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S>
	struct implement_substract_aux
		<Traits
		,S
		,typename std::enable_if
			 <check_for
				  <does_define_substract
				  ,S
				  ,any_cref<typename Traits::type>
				  ,any_cref<typename Traits::type>
				  ,typename Traits::type
				  >::value
			 >::type
		>
	: Traits {
		using typename Traits::set_type;
		
#	define JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()                         \
		s.substract(std::forward<T>(lhs), std::forward<U>(rhs))
		template<class T, class U>
		static auto substract(const set_type& s, T&& lhs, U&& rhs)
		noexcept(noexcept(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_GROUP_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_substract : implement_substract_aux<Traits, S> {
		using typename implement_substract_aux<Traits, S>::set_type;
		using implement_substract_aux<Traits, S>::eval;
#	define JORDAN_GROUP_TRAITS_HPP_local_impl()                                \
		implement_substract_aux<Traits, S>::substract                          \
			(s                                                                 \
			,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval  \
				 (s, lhs(std::move(e)))                                        \
			,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval  \
				 (s, rhs(std::move(e)))                                        \
			)
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::difference_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_GROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_GROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_GROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_GROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct multiplicative_group_traits_builder
	: implement_traits
		  <minimal_multiplicative_group_traits<S, T, Admissible, DerivedTraits>
		  ,S
		  ,DerivedTraits
		  ,implement_divide
		  >
	{};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct additive_group_traits_builder
	: implement_traits
		  <minimal_additive_group_traits<S, T, Admissible, DerivedTraits>
		  ,S
		  ,DerivedTraits
		  ,implement_substract
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct multiplicative_group_traits
	: multiplicative_group_traits_builder
		  <S, T, Admissible, multiplicative_group_traits<S, T, Admissible>> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct additive_group_traits
	: additive_group_traits_builder
		  <S, T, Admissible, additive_group_traits<S, T, Admissible>> {};
} // namespace detail_group_traits_hpp_

using detail_group_traits_hpp_::satisfies_additive_group;
using detail_group_traits_hpp_::satisfies_multiplicative_group;
using detail_group_traits_hpp_::satisfies_additive_group_element;
using detail_group_traits_hpp_
	      ::satisfies_multiplicative_group_element;
using detail_group_traits_hpp_::additive_group_of;
using detail_group_traits_hpp_::multiplicative_group_of;
using detail_group_traits_hpp_::additive_group_traits;
using detail_group_traits_hpp_::multiplicative_group_traits;
using detail_group_traits_hpp_::additive_group_traits_builder;
using detail_group_traits_hpp_::multiplicative_group_traits_builder;
#endif // JORDAN_TRAITS_GROUP_TRAITS_HPP_
