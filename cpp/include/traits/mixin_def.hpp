#ifndef MIXIN_DEF_HPP_
#define MIXIN_DEF_HPP_

#include "lmpl/list.hpp"
#include "lmpl/range.hpp"
#include "lmpl/repack.hpp"
#include "lmpl/reduce.hpp"
#include "lmpl/map.hpp"
#include "lmpl/common_operators.hpp"

#include "wrapper.hpp"

namespace detail_mixin_def_hpp_ {
	template<class Traits, class ExpressionTypeL, class TypeL, class = void>
	struct is_correctly_typed_aux {};
	
	template
		<class Traits
		,template<class...> class ExpressionTypeHolder
		,class... ExpressionType
		,template<class...> class TypeHolder
		,class... Type
		>
	struct is_correctly_typed_aux
		<Traits
		,ExpressionTypeHolder<ExpressionType...>
		,TypeHolder<Type...>
		,typename std::enable_if
			 <sizeof...(ExpressionType) != sizeof...(Type)>::type
		>
	: std::false_type {};
	
	template
		<class Traits
		,template<class...> class ExpressionTypeHolder
		,class... ExpressionType
		,template<class...> class TypeHolder
		,class... Type
		>
	struct is_correctly_typed_aux
		<Traits
		,ExpressionTypeHolder<ExpressionType...>
		,TypeHolder<Type...>
		,typename std::enable_if
			 <sizeof...(ExpressionType) == sizeof...(Type)>::type
		>
	: lmpl::reduce_t
		<lmpl::list
			 <std::is_same
				  <typename Traits::template type_expression_type_t
					   <ExpressionType>
				  ,Type
				  >...
			 >
		,lmpl::pack_template<lmpl::boolean_and>
		> {};
	
	template<class Traits, class ExpressionTypeL, class TypeL>
	struct is_correctly_typed
	: is_correctly_typed_aux<Traits, ExpressionTypeL, TypeL> {};
	
	template
		<class F
		,F Function
		,class Domain
		,class Derived
		,class ArgL
		,class IdxL
		>
	struct interpret_helper /* undefined */ ;

	template
		<class F
		,F Function
		,class Derived
		,class Domain
		,template<class...> class ArgHolder
		,class... Args
		,template<class...> class IdxHolder
		,class... Indices
		>
	struct interpret_helper
		<F
		,Function
		,Derived
		,Domain
		,ArgHolder<Args...>
		,IdxHolder<Indices...>
		> {
	
#	define MIXIN_DEF_HPP_local_impl()\
	Function(domain\
	        ,Derived::interpret\
		         (std::forward<Args>\
			          (extend_wrapper::nth<Indices::value>(std::move(e)))\
		         )...\
	        )
		template<class Tag>
		static auto invoke
			(const Domain& domain, extend_wrapper::expression<Tag, Args...>&& e)
		noexcept(noexcept(MIXIN_DEF_HPP_local_impl())) ->
		decltype(MIXIN_DEF_HPP_local_impl()) {
			return MIXIN_DEF_HPP_local_impl();
		}
#	undef MIXIN_DEF_HPP_local_impl
	};

	template<class...>
	struct dependent_name {
		template<class T>
		struct invoke { using type = T; };
	};
	
	template<class T, class... Dep>
	using dependent_name_t =
		typename dependent_name<Dep...>::template invoke<T>::type;
	
	// the higher N, the greater the precedence
	template<std::size_t N>
	struct precedence : precedence<N - 1> {};
	
	template<>
	struct precedence<0> {};
} // namespace detail_mixin_def_hpp_

#endif // MIXIN_DEF_HPP_
