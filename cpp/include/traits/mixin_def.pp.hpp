/** Defines macros easing the definition of template mixin classes used by
 * traits classes.
 *
 * A second inclusion of this file undefines all macros defined by its first
 * inclusion.
 */

#ifndef MIXIN_DEF_PP_HPP_
#define MIXIN_DEF_PP_HPP_

#include "../utility.pp.hpp"

#	define IS_TEMPLATE_PARAMETER_AUX_param() 0
#	define IS_TEMPLATE_PARAMETER_AUX_checked_param() 0
#	define IS_TEMPLATE_PARAMETER_AUX_uref() 1
#	define IS_TEMPLATE_PARAMETER_AUX_checked_uref() 1
#	define IS_TEMPLATE_PARAMETER_AUX_checked_tpl() 1
#	define IS_TEMPLATE_PARAMETER_AUX_tpl() 1
#	define IS_TEMPLATE_PARAMETER_AUX(kind, ...)\
	MACRO_UTILITY_CAT(IS_TEMPLATE_PARAMETER_AUX_, kind)()
#	define IS_TEMPLATE_PARAMETER(parameter_tuple)\
	IS_TEMPLATE_PARAMETER_AUX parameter_tuple

#	define IS_PARAMETER_AUX_param() 1
#	define IS_PARAMETER_AUX_checked_param() 1
#	define IS_PARAMETER_AUX_uref() 1
#	define IS_PARAMETER_AUX_checked_uref() 1
#	define IS_PARAMETER_AUX_checked_tpl() 0
#	define IS_PARAMETER_AUX_tpl() 0
#	define IS_PARAMETER_AUX(kind, ...)\
	MACRO_UTILITY_CAT(IS_PARAMETER_AUX_, kind)()
#	define IS_PARAMETER(parameter_tuple)\
	IS_PARAMETER_AUX parameter_tuple

#	define IS_CHECKED_AUX_param() 0
#	define IS_CHECKED_AUX_checked_param() 1
#	define IS_CHECKED_AUX_uref() 0
#	define IS_CHECKED_AUX_checked_uref() 1
#	define IS_CHECKED_AUX_checked_tpl() 0
#	define IS_CHECKED_AUX_tpl() 1
#	define IS_CHECKED_AUX(kind, ...)\
	MACRO_UTILITY_CAT(IS_CHECKED_AUX_, kind)()
#	define IS_CHECKED(parameter_tuple)\
	IS_CHECKED_AUX parameter_tuple

#	define IS_UNIVERSAL_REFERENCE_AUX_param() 0
#	define IS_UNIVERSAL_REFERENCE_AUX_checked_param() 0
#	define IS_UNIVERSAL_REFERENCE_AUX_uref() 1
#	define IS_UNIVERSAL_REFERENCE_AUX_checked_uref() 1
#	define IS_UNIVERSAL_REFERENCE_AUX_checked_tpl() 0
#	define IS_UNIVERSAL_REFERENCE_AUX_tpl() 0
#	define IS_UNIVERSAL_REFERENCE_AUX(kind, ...)\
	MACRO_UTILITY_CAT(IS_UNIVERSAL_REFERENCE_AUX_, kind)()
#	define IS_UNIVERSAL_REFERENCE(parameter_tuple)\
	IS_UNIVERSAL_REFERENCE_AUX parameter_tuple

#	define ADD_DUMMY(...) (__VA_ARGS__, dummy)
#	define GET_FIRST_PARAMETER(fst, ...) fst
#	define DISCARD_FIRST_PARAMETER_IF_0(...) (fst, __VA_ARGS__)
#	define DISCARD_FIRST_PARAMETER_IF_1(fst, ...) (__VA_ARGS__)

#	define GET_TYPE_NAME_OF(parameter_tuple)\
	GET_FIRST_PARAMETER\
	DISCARD_FIRST_PARAMETER_IF_1\
	ADD_DUMMY parameter_tuple
#	define GET_PARAMETER_NAME_OF(parameter_tuple)\
	GET_FIRST_PARAMETER\
	DISCARD_FIRST_PARAMETER_IF_1 DISCARD_FIRST_PARAMETER_IF_1\
	ADD_DUMMY parameter_tuple

#	define GET_PARAMETER_TYPE_OF_0(parameter_tuple)\
	GET_TYPE_NAME_OF(parameter_tuple)

#	define GET_PARAMETER_TYPE_OF_1(parameter_tuple)\
	GET_TYPE_NAME_OF(parameter_tuple)&&

#	define GET_PARAMETER_TYPE_OF(parameter_tuple)\
	MACRO_UTILITY_CAT(GET_PARAMETER_TYPE_OF_\
	                 ,IS_UNIVERSAL_REFERENCE(parameter_tuple)\
	                 )(parameter_tuple)

#	define GET_EXPECTED_TYPE_OF(parameter_tuple) \
	GET_FIRST_PARAMETER\
	MACRO_UTILITY_CAT(DISCARD_FIRST_PARAMETER_IF_\
	                 ,IS_PARAMETER(parameter_tuple)\
	                 )\
	DISCARD_FIRST_PARAMETER_IF_1 DISCARD_FIRST_PARAMETER_IF_1\
	ADD_DUMMY parameter_tuple

#	define MAKE_TEMPLATE_PARAMETER(parameter_tuple)\
	class GET_TYPE_NAME_OF(parameter_tuple)

#	define MAKE_TEMPLATE_ARGUMENT(parameter_tuple)\
	GET_TYPE_NAME_OF(parameter_tuple)

#	define MAKE_EXPECTED_TYPE(parameter_tuple)\
	GET_EXPECTED_TYPE_OF(parameter_tuple)

#	define MAKE_FORWARDED_PARAMETER(parameter_tuple)\
	std::forward<GET_TYPE_NAME_OF(parameter_tuple)>\
		(GET_PARAMETER_NAME_OF(parameter_tuple))

#	define MAKE_FUNCTION_PARAMETER(parameter_tuple)\
	GET_PARAMETER_TYPE_OF(parameter_tuple)\
	GET_PARAMETER_NAME_OF(parameter_tuple)

#	define MAKE_DUMMY_PARAMETER_INSTANCE(parameter_tuple)\
	std::declval<GET_TYPE_NAME_OF(parameter_tuple)>()

#	define MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE_0(parameter_tuple)\
	std::declval<GET_TYPE_NAME_OF(parameter_tuple)>()
#	define MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE_1(parameter_tuple)\
	std::declval<GET_EXPECTED_TYPE_OF(parameter_tuple)>()
#	define MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE(parameter_tuple)\
	MACRO_UTILITY_CAT(MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE_\
	                 ,IS_CHECKED(parameter_tuple)\
	                 )(parameter_tuple)

#	define MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((MAKE_TEMPLATE_PARAMETER,)\
			 ,IS_TEMPLATE_PARAMETER\
			 ,MACRO_UTILITY_IDENTITY parameter_tuple_list\
			 )\
		)

#	define MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
	(MACRO_UTILITY_MAP_LIST\
	 MACRO_UTILITY_FILTER_LIST_ACC\
		 ((GET_TYPE_NAME_OF,)\
		 ,IS_TEMPLATE_PARAMETER\
		 ,MACRO_UTILITY_IDENTITY parameter_tuple_list\
		 )\
	)


#	define MAKE_CANDIDATE_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((MAKE_TEMPLATE_ARGUMENT,)\
			 ,IS_CHECKED\
			 ,MACRO_UTILITY_IDENTITY parameter_tuple_list\
			 )\
		)

#	define MAKE_EXPECTED_TYPE_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((MAKE_EXPECTED_TYPE,)\
			 ,IS_CHECKED\
			 ,MACRO_UTILITY_IDENTITY parameter_tuple_list\
			 )\
		)

#	define MAKE_FORWARDED_PARAMETER_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((MAKE_FORWARDED_PARAMETER,)\
			 ,IS_PARAMETER\
			 ,MACRO_UTILITY_STRIP_PARENTHESES(parameter_tuple_list)\
			 )\
		)

#	define MAKE_PARAMETER_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((MAKE_FUNCTION_PARAMETER,)\
			 ,IS_PARAMETER\
			 ,MACRO_UTILITY_STRIP_PARENTHESES(parameter_tuple_list)\
			 )\
		)

#	define MAKE_UNNAMED_PARAMETER_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((GET_PARAMETER_TYPE_OF,)\
			 ,IS_PARAMETER\
			 ,MACRO_UTILITY_STRIP_PARENTHESES(parameter_tuple_list)\
			 )\
		)

#	define MAKE_DUMMY_ARGUMENT_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((MAKE_DUMMY_PARAMETER_INSTANCE,)\
			 ,IS_PARAMETER\
			 ,MACRO_UTILITY_STRIP_PARENTHESES(parameter_tuple_list)\
			 )\
		)

#	define MAKE_DEFAULT_DUMMY_ARGUMENT_LIST(parameter_tuple_list)\
	MACRO_UTILITY_EVAL\
		(MACRO_UTILITY_MAP_LIST\
		 MACRO_UTILITY_FILTER_LIST_ACC\
			 ((MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE,)\
			 ,IS_PARAMETER\
			 ,MACRO_UTILITY_STRIP_PARENTHESES(parameter_tuple_list)\
			 )\
		 )

// note: all parameters must be checked
// or must not depend on a template parameter for this definition to be legal
#	define SELECTOR_HELPER_DEF(prefix\
	                          ,parameter_tuple_list\
	                          ,method_impl\
	                          ,expected_return_type\
	                          )\
namespace detail_mixin_pp_def_hpp_ {\
	template\
		<class Traits\
		,class Derived\
		,MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)\
		>\
	auto prefix ## _specialization_selector_helper_aux\
		(detail_mixin_def_hpp_::precedence<1>\
		,MAKE_PARAMETER_LIST(parameter_tuple_list)\
		) ->\
	detail_mixin_def_hpp_::is_correctly_typed\
			 <Traits\
			 ,lmpl::list<decltype method_impl>\
			 ,lmpl::list<MACRO_UTILITY_STRIP_PARENTHESES(expected_return_type)>\
			 > /* undefined */;\
	\
	template\
		<class Traits\
		,class Derived\
		,MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)\
		>\
	std::false_type prefix ## _specialization_selector_helper_aux\
		(detail_mixin_def_hpp_::precedence<0>\
		,MAKE_PARAMETER_LIST(parameter_tuple_list)\
		) /* undefined */; \
	\
	template<class Traits, class Derived>\
	constexpr bool prefix ## _specialization_selector_helper() {\
		return decltype\
			       (prefix ## _specialization_selector_helper_aux\
				        <Traits, Derived>\
					        (detail_mixin_def_hpp_::precedence<1>()\
					        ,MAKE_DEFAULT_DUMMY_ARGUMENT_LIST\
						         (parameter_tuple_list)\
					        )\
			       )::value;\
	}\
	\
	template\
		<class Traits\
		,class Derived\
		,MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)\
		>\
	constexpr auto prefix ## _overload_selector_helper_aux\
		(detail_mixin_def_hpp_::precedence<1>\
		,MAKE_UNNAMED_PARAMETER_LIST(parameter_tuple_list)\
		) ->\
	typename std::enable_if\
		<detail_mixin_def_hpp_::is_correctly_typed\
			 <Traits\
			 ,lmpl::list<MAKE_CANDIDATE_LIST(parameter_tuple_list)>\
			 ,lmpl::list<MAKE_EXPECTED_TYPE_LIST(parameter_tuple_list)>\
			 >::value\
		,bool\
		>::type { return true; }\
	\
	template\
		<class Traits\
		,class Derived\
		,MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)\
		>\
	constexpr bool prefix ## _overload_selector_helper_aux\
		(detail_mixin_def_hpp_::precedence<0>\
		,MAKE_UNNAMED_PARAMETER_LIST(parameter_tuple_list)\
		) { return false; }\
	\
	template\
		<class Traits\
		,class Derived\
		,MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)\
		>\
	constexpr bool prefix ## _overload_selector_helper() {\
		return prefix ## _overload_selector_helper_aux<Traits, Derived>\
		(detail_mixin_def_hpp_::precedence<1>()\
		,MAKE_DUMMY_ARGUMENT_LIST(parameter_tuple_list)\
		);\
	}\
}

/** Defines a mixin intended to introduce static methods to a traits class.
 *
 * The base traits class is expected to define the templated type
 * `type_expression_type_t<T>` such that if `T` is an expression template type,
 * `type_expression_type_t<T>` is the type it is convertible to.
 * Otherwise, `type_expression_type_t<T>` is `typename std::decay<T>::type`.
 *
 * @param mixin_name name of the defined class
 * @param method_name name of the introduced static method
 * @param parameter_tuple_list parenthesized list of parenthesized tuples
 * each tuple must have one of the following forms:
 * - (param, [type], [parameter identifier])
 * - (uref, [type identifier], [parameter identifier], [type])
 * @param method_impl parenthesized expression returned by a static method call
 * @expected_return_type parenthesized expected type of `method_impl`
 * @failure_message string literal to display in compilation failures if
 * `method_impl` is ill-formed or isn't of the expected type
 */
#	define INTRODUCTION_MIXIN_DEF(mixin_name\
	                             ,method_name\
	                             ,parameter_tuple_list\
	                             ,method_impl\
	                             ,expected_return_type\
	                             ,failure_message\
	                             )\
	INTRODUCTION_MIXIN_DEF_AUX\
		(mixin_name\
		,method_name\
		,parameter_tuple_list\
		,method_impl\
		,expected_return_type\
		,failure_message\
		,1\
		)

#	define OPTIONAL_INTRODUCTION_MIXIN_DEF(mixin_name\
	                                      ,method_name\
	                                      ,parameter_tuple_list\
	                                      ,method_impl\
	                                      ,expected_return_type\
	                                      )\
	INTRODUCTION_MIXIN_DEF_AUX\
		(mixin_name\
		,method_name\
		,parameter_tuple_list\
		,method_impl\
		,expected_return_type\
		,dummy\
		,0\
		)

#	define COMPILE_TIME_ERROR_DEF_0(failure_message)
#	define COMPILE_TIME_ERROR_DEF_1(failure_message)\
template<class U>\
struct compile_time_check\
: Traits::template compile_time_check<U> {\
	static_assert(!std::is_same<U, void>::value, failure_message);\
};\

#	define INTRODUCTION_MIXIN_DEF_AUX(mixin_name\
	                                 ,method_name\
	                                 ,parameter_tuple_list\
	                                 ,method_impl\
	                                 ,expected_return_type\
	                                 ,failure_message\
	                                 ,fail_if_unavailable\
	                                 )\
SELECTOR_HELPER_DEF\
	(mixin_name\
	,parameter_tuple_list\
	,method_impl\
	,expected_return_type\
	)\
namespace detail_mixin_pp_def_hpp_ {\
	template<class Traits, class Derived, class = void>\
	struct mixin_name ## _aux : Traits {\
		MACRO_UTILITY_CAT(COMPILE_TIME_ERROR_DEF_, fail_if_unavailable)\
			(failure_message)\
	};\
	\
	template<class Traits, class Derived>\
	struct mixin_name ## _aux\
		<Traits\
		,Derived\
		,typename std::enable_if\
			 <mixin_name ## _specialization_selector_helper<Traits, Derived>()\
			 >::type\
		>\
	: Traits {\
		template<class U> struct compile_time_error\
		: Traits::template compile_time_error<U> {};\
		\
		template<MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)>\
		static auto method_name(MAKE_PARAMETER_LIST(parameter_tuple_list))\
		noexcept(noexcept method_impl) ->\
		typename std::enable_if\
			<mixin_name ## _overload_selector_helper\
				 <Traits\
				 ,Derived\
				 ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				 >()\
			,decltype method_impl\
			>::type {\
			return method_impl;\
		}\
		\
		template<MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)>\
		static auto method_name(MAKE_PARAMETER_LIST(parameter_tuple_list))\
		noexcept(noexcept(\
			detail_mixin_def_hpp_::dependent_name_t\
				<Traits\
				,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				>::template method_name\
					    (MAKE_FORWARDED_PARAMETER_LIST(parameter_tuple_list))\
		)) ->\
		typename std::enable_if\
			<!mixin_name ## _overload_selector_helper\
				  <Traits\
				  ,Derived\
				  ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				  >()\
			,decltype\
				 (detail_mixin_def_hpp_::dependent_name_t\
					  <Traits\
					  ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
					  >::template method_name\
						     (MAKE_FORWARDED_PARAMETER_LIST\
							      (parameter_tuple_list)\
						     )\
				 )\
			>::type {\
			return detail_mixin_def_hpp_::dependent_name_t\
				       <Traits\
				       ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				       >::template method_name\
					           (MAKE_FORWARDED_PARAMETER_LIST\
						            (parameter_tuple_list)\
					           );\
		}\
	};\
}\
struct mixin_name {\
	template<class Traits, class Derived>\
	struct invoke\
	: detail_mixin_pp_def_hpp_:: mixin_name ## _aux<Traits, Derived> {};\
};

/** Helper macro, used in INTERPRET_MIXIN_DEF.
 */
#	define MIXIN_DEF_HPP_interpret_impl(method_name)\
	detail_mixin_def_hpp_::interpret_helper\
		<decltype\
			 (&detail_mixin_def_hpp_::dependent_name_t\
				   <Derived, Args...>::template method_name<Args...>\
			 )\
		,&detail_mixin_def_hpp_::dependent_name_t\
			  <Derived, Args...>::template method_name<Args...>\
		,domain_type\
		,detail_mixin_def_hpp_::dependent_name_t<Derived, Args...>\
		,lmpl::list<Args...>\
		,lmpl::range_t\
			 <lmpl::index_t<0>, lmpl::index_t<sizeof...(Args)>>\
		>::invoke(domain, std::move(e))
/**
 * TODO
 */
#	define INTERPRET_MIXIN_DEF(mixin_name\
	                          ,method_name\
	                          ,operator_tag\
	                          )\
struct mixin_name {\
	template<class Traits, class Derived>\
	struct invoke : Traits {\
		using typename Traits::domain_type;\
		using Traits::interpret;\
		\
		template<class... Args>\
		static auto interpret\
			(const domain_type& domain\
			,extend_wrapper::expression<operator_tag, Args...>&& e\
			)\
		noexcept(noexcept(\
			MIXIN_DEF_HPP_interpret_impl(method_name)\
		)) ->\
		decltype(MIXIN_DEF_HPP_interpret_impl(method_name)) {\
			return MIXIN_DEF_HPP_interpret_impl(method_name);\
		}\
	};\
};

/** TODO
 */
#	define FUNCTIONALITY_MIXIN_DEF(mixin_name\
	                              ,method_name\
	                              ,parameter_tuple_list\
	                              ,default_method_impl\
	                              ,alternative_method_impl\
	                              ,expected_return_type\
	                              ,failure_message\
	                              )\
SELECTOR_HELPER_DEF\
	(mixin_name ## _default\
	,parameter_tuple_list\
	,default_method_impl\
	,expected_return_type\
	)\
SELECTOR_HELPER_DEF\
	(mixin_name ## _alternative\
	,parameter_tuple_list\
	,alternative_method_impl\
	,expected_return_type\
	)\
namespace detail_mixin_pp_def_hpp_ {\
	template<class Traits, class Derived, class = void>\
	struct mixin_name ## _aux : Traits {\
		template<class U>\
		struct compile_time_error\
		: Traits::template compile_time_error<U> {\
			static_assert(std::is_same<U, void>::value, failure_message);\
		};\
	};\
	\
	template<class Traits, class Derived>\
	struct mixin_name ## _aux\
		<Traits\
		,Derived\
		,typename std::enable_if\
			 <mixin_name ## _default_specialization_selector_helper\
				  <Traits, Derived>()\
			 && mixin_name ## _alternative_specialization_selector_helper\
				    <Traits, Derived>()\
			 >::type\
		>\
	: Traits {\
		template<MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)>\
		static auto method_name(MAKE_PARAMETER_LIST(parameter_tuple_list))\
		noexcept(noexcept default_method_impl) ->\
		typename std::enable_if\
			<mixin_name ## _default_overload_selector_helper\
				 <Traits\
				 ,Derived\
				 ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				 >()\
			,decltype default_method_impl\
			>::type {\
			return default_method_impl;\
		}\
		\
		template<MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)>\
		static auto method_name(MAKE_PARAMETER_LIST(parameter_tuple_list))\
		noexcept(noexcept(\
			Traits::method_name(MAKE_FORWARDED_PARAMETER_LIST\
				(parameter_tuple_list))\
		)) ->\
		typename std::enable_if\
			<!mixin_name ## _default_overload_selector_helper\
				  <Traits\
				  ,Derived\
				  ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				  >()\
			,decltype\
				 (Traits::method_name(MAKE_FORWARDED_PARAMETER_LIST\
					  (parameter_tuple_list))\
				 )\
			>::type {\
			return Traits::method_name(MAKE_FORWARDED_PARAMETER_LIST\
				       (parameter_tuple_list));\
		}\
	};\
	\
	template<class Traits, class Derived>\
	struct mixin_name ## _aux\
		<Traits\
		,Derived\
		,typename std::enable_if\
			 <!mixin_name ## _default_specialization_selector_helper\
				   <Traits, Derived>()\
			  && mixin_name ## _alternative_specialization_selector_helper\
				   <Traits, Derived>()\
			 >::type\
		>\
	: Traits {\
		template<MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)>\
		static auto method_name(MAKE_PARAMETER_LIST(parameter_tuple_list))\
		noexcept(noexcept alternative_method_impl) ->\
		typename std::enable_if\
			<mixin_name ## _alternative_overload_selector_helper\
				 <Traits\
				 ,Derived\
				 ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				 >()\
			>::type {\
			return alternative_method_impl;\
		}\
		\
		template<MAKE_TEMPLATE_PARAMETER_LIST(parameter_tuple_list)>\
		static auto method_name(MAKE_PARAMETER_LIST(parameter_tuple_list))\
		noexcept(noexcept(\
			Traits::method_name\
				(MAKE_FORWARDED_PARAMETER_LIST(parameter_tuple_list))\
		)) ->\
		typename std::enable_if\
			<!mixin_name ## _alternative_overload_selector_helper\
				  <Traits\
				  ,Derived\
				  ,MAKE_TEMPLATE_ARGUMENT_LIST(parameter_tuple_list)\
				  >()\
			,decltype\
				 (Traits::method_name\
					  (MAKE_FORWARDED_PARAMETER_LIST(parameter_tuple_list))\
				 )\
			>::type {\
			return Traits::method_name\
				       (MAKE_FORWARDED_PARAMETER_LIST(parameter_tuple_list));\
		}\
	};\
}\
struct mixin_name {\
	template<class Traits, class Derived>\
	struct invoke\
	: detail_mixin_pp_def_hpp_::mixin_name ## _aux<Traits, Derived> {};\
};

#else // MIXIN_DEF_PP_HPP_
// clean defined macros on second inclusion
#	undef FUNCTIONALITY_MIXIN_DEF
#	undef INTERPRET_MIXIN_DEF
#	undef MIXIN_DEF_HPP_interpret_impl
#	undef INTRODUCTION_MIXIN_DEF_AUX
#	undef COMPILE_TIME_ERROR_DEF_1
#	undef COMPILE_TIME_ERROR_DEF_0
#	undef OPTIONAL_INTRODUCTION_MIXIN_DEF
#	undef INTRODUCTION_MIXIN_DEF
#	undef SELECTOR_HELPER_DEF
#	undef MAKE_DEFAULT_DUMMY_ARGUMENT_LIST
#	undef MAKE_DUMMY_ARGUMENT_LIST
#	undef MAKE_UNNAMED_PARAMETER_LIST
#	undef MAKE_PARAMETER_LIST
#	undef MAKE_FORWARDED_PARAMETER_LIST
#	undef MAKE_EXPECTED_TYPE_LIST
#	undef MAKE_CANDIDATE_LIST
#	undef MAKE_TEMPLATE_ARGUMENT_LIST
#	undef MAKE_TEMPLATE_PARAMETER_LIST
#	undef MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE
#	undef MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE_1
#	undef MAKE_DEFAULT_DUMMY_PARAMETER_INSTANCE_0
#	undef MAKE_DUMMY_PARAMETER_INSTANCE_AUX
#	undef MAKE_FUNCTION_PARAMETER
#	undef MAKE_FUNCTION_PARAMETER_AUX
#	undef MAKE_FUNCTION_PARAMETER_AUX_uref
#	undef MAKE_FUNCTION_PARAMETER_AUX_param
#	undef MAKE_FORWARDED_PARAMETER
#	undef MAKE_FORWARDED_PARAMETER_AUX
#	undef MAKE_FORWARDED_PARAMETER_AUX_uref
#	undef MAKE_FORWARDED_PARAMETER_AUX_param
#	undef MAKE_EXPECTED_TYPE
#	undef MAKE_EXPECTED_TYPE_AUX
#	undef MAKE_TEMPLATE_ARGUMENT
#	undef MAKE_TEMPLATE_ARGUMENT_AUX
#	undef MAKE_TEMPLATE_PARAMETER
#	undef MAKE_TEMPLATE_PARAMETER_AUX
#	undef GET_EXPECTED_TYPE_OF
#	undef GET_PARAMETER_TYPE_OF
#	undef GET_PARAMETER_TYPE_OF_1
#	undef GET_PARAMETER_TYPE_OF_0
#	undef GET_PARAMETER_NAME_OF
#	undef GET_TYPE_NAME_OF
#	undef DISCARD_FIRST_PARAMETER_IF_1
#	undef DISCARD_FIRST_PARAMETER_IF_0
#	undef GET_FIRST_PARAMETER
#	undef ADD_DUMMY
#	undef IS_UNIVERSAL_REFERENCE
#	undef IS_UNIVERSAL_REFERENCE_AUX
#	undef IS_UNIVERSAL_REFERENCE_AUX_tpl
#	undef IS_UNIVERSAL_REFERENCE_AUX_checked_tpl
#	undef IS_UNIVERSAL_REFERENCE_AUX_checked_uref
#	undef IS_UNIVERSAL_REFERENCE_AUX_uref
#	undef IS_UNIVERSAL_REFERENCE_AUX_checked_param
#	undef IS_UNIVERSAL_REFERENCE_AUX_param
#	undef IS_CHECKED
#	undef IS_CHECKED_AUX
#	undef IS_CHECKED_AUX_tpl
#	undef IS_CHECKED_AUX_checked_tpl
#	undef IS_CHECKED_AUX_checked_uref
#	undef IS_CHECKED_AUX_uref
#	undef IS_CHECKED_AUX_checked_param
#	undef IS_CHECKED_AUX_param
#	undef IS_PARAMETER
#	undef IS_PARAMETER_AUX
#	undef IS_PARAMETER_AUX_tpl
#	undef IS_PARAMETER_AUX_checked_tpl
#	undef IS_PARAMETER_AUX_checked_uref
#	undef IS_PARAMETER_AUX_uref
#	undef IS_PARAMETER_AUX_checked_param
#	undef IS_PARAMETER_AUX_param
#	undef IS_TEMPLATE_PARAMETER
#	undef IS_TEMPLATE_PARAMETER_AUX
#	undef IS_TEMPLATE_PARAMETER_AUX_tpl
#	undef IS_TEMPLATE_PARAMETER_AUX_checked_tpl
#	undef IS_TEMPLATE_PARAMETER_AUX_checked_uref
#	undef IS_TEMPLATE_PARAMETER_AUX_uref
#	undef IS_TEMPLATE_PARAMETER_AUX_checked_param
#	undef IS_TEMPLATE_PARAMETER_AUX_param
#	include "../utility.pp.hpp"
#undef MIXIN_DEF_PP_HPP_
#endif // MIXIN_DEF_PP_HPP_
