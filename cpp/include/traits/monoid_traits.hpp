#ifndef JORDAN_TRAITS_MONOID_TRAITS_HPP_
#define JORDAN_TRAITS_MONOID_TRAITS_HPP_

#include "semigroup_traits.hpp"
#include "arithmetic_traits.hpp"
#include "../common.hpp"
#include <utility>

namespace extend_wrapper {
	struct one_symbol_tag {};
	struct zero_symbol_tag {};
	
	constexpr auto onesymb = mkexpr<one_symbol_tag>();
	constexpr auto zerosymb = mkexpr<zero_symbol_tag>();
} // namespace extend_wrapper

using extend_wrapper::onesymb;
using extend_wrapper::zerosymb;

namespace detail_monoid_traits_hpp_ {
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_multiplicative_monoid_element
	: all_valid
		  <satisfies_multiplicative_semigroup_element<T, Admissible>
		  ,has_global_one<T>
		  > {};
	
	template<class T, template<class> class Admissible, class = void>
	struct multiplicative_monoid_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct multiplicative_monoid_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_multiplicative_monoid_element<T, Admissible>::value
			 >::type
		>
	: virtual multiplicative_semigroup_of<T, Admissible> {
		using typename multiplicative_semigroup_of<T, Admissible>::type;
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()                        \
		default_arithmetic_definition::one_definition<type>::one()
		template<class U>
		typename std::enable_if<std::is_same<U, type>::value, type>::type
		one() const
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
		
		type one() const
		noexcept(noexcept(
			std::declval
				<const multiplicative_monoid_of_aux<T, Admissible>&
				>().template one<type>()
		)) {
			return one<type>();
		}
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct multiplicative_monoid_of
	: virtual multiplicative_monoid_of_aux<T, Admissible> {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_additive_monoid_element
	: all_valid
		  <satisfies_additive_semigroup_element<T, Admissible>
		  ,has_global_zero<T>
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_additive_monoid
	: all_valid
		  <satisfies_additive_semigroup<S, T, Admissible>
		  ,does_define_zero<S, T>
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_multiplicative_monoid
	: all_valid
		  <satisfies_multiplicative_semigroup<S, T, Admissible>
		  ,does_define_one<S, T>
		  > {};
	
	template<class T, template<class> class Admissible, class = void>
	struct additive_monoid_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct additive_monoid_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_additive_monoid_element<T, Admissible>::value>::type
		>
	: virtual additive_semigroup_of<T, Admissible> {
		using typename additive_semigroup_of<T, Admissible>::type;
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()                        \
		default_arithmetic_definition::zero_definition<type>::zero()
		template<class U>
		typename std::enable_if<std::is_same<U, type>::value, type>::type
		zero() const
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
		
		type zero() const
		noexcept(noexcept(
			std::declval
				<const additive_monoid_of_aux<T, Admissible>&
				>().template zero<type>()
		)) {
			return zero<type>();
		}
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct additive_monoid_of
	: virtual additive_monoid_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_additive_monoid_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_additive_monoid_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_additive_monoid<S, T, Admissible>::value>::type
		>
	: additive_semigroup_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename additive_semigroup_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		using typename additive_semigroup_traits_builder
			      <S, T, Admissible, DerivedTraits>::type;
		static constexpr bool valid = true;
		
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl() s.template zero<type>()
		static auto zero(const set_type& s)
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
		
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
		using additive_semigroup_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
		
#	define JORDAN_MONOID_TRAITS_HPP_local_impl() zero(s)
		static auto eval
			(const set_type& s
			,const extend_wrapper::expression<extend_wrapper::zero_symbol_tag>&
			)
		noexcept(noexcept(JORDAN_MONOID_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_MONOID_TRAITS_HPP_local_impl()) {
			return JORDAN_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_MONOID_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_additive_monoid_traits
	: minimal_additive_monoid_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_multiplicative_monoid_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_multiplicative_monoid_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_multiplicative_monoid<S, T, Admissible>::value>::type
		>
	: multiplicative_semigroup_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename multiplicative_semigroup_traits_builder
			      <S, T, Admissible, DerivedTraits>::set_type;
		using typename multiplicative_semigroup_traits_builder
			      <S, T, Admissible, DerivedTraits>::type;
		static constexpr bool valid = true;
		
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl() s.template one<type>()
		static auto one(const set_type& s)
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
		using multiplicative_semigroup_traits_builder
			      <S, T, Admissible, DerivedTraits>::eval;
		
#	define JORDAN_MONOID_TRAITS_HPP_local_impl() one(s)
		static auto eval
			(const set_type& s
			,const extend_wrapper::expression<extend_wrapper::one_symbol_tag>&
			)
		noexcept(noexcept(JORDAN_MONOID_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_MONOID_TRAITS_HPP_local_impl()) {
			return JORDAN_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_MONOID_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_multiplicative_monoid_traits
	: minimal_multiplicative_monoid_traits_aux
		  <S, T, Admissible, DerivedTraits> {};
	
	template<class S, class T, class = void>
	struct does_define_is_zero_aux : std::false_type {};
	
	template<class S, class T>
	struct does_define_is_zero_aux
		<S
		,T
		,typename std::enable_if
			 <std::is_convertible
				  <decltype(std::declval<S>().is_zero(std::declval<T>()))
				  ,bool
				  >::value
			 >::type
		>
	: std::true_type {};
	
	template<class S, class T>
	struct does_define_is_zero : does_define_is_zero_aux<S, T> {};
	
	template<class Traits, class S, class = void>
	struct implement_is_zero_aux : Traits {
		using typename Traits::set_type;
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()                        \
		Traits::eq(s, std::forward<T>(x), Traits::zero(s))
		template<class T>
		static bool is_zero(const set_type& s, T&& x)
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S>
	struct implement_is_zero_aux
		<Traits
		,S
		,typename std::enable_if
			 <does_define_is_zero<S, typename Traits::type>::value>::type
		>
	: Traits {
		using typename Traits::set_type;
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()                        \
		s.is_zero(std::forward<T>(x))
		template<class T>
		static bool is_zero(const set_type& s, T&& x)
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_is_zero : implement_is_zero_aux<Traits, S> {};
	
	template<class S, class T, class = void>
	struct does_define_is_one_aux : std::false_type {};
	
	template<class S, class T>
	struct does_define_is_one_aux
		<S
		,T
		,typename std::enable_if
			 <std::is_convertible
				  <decltype(std::declval<S>().is_one(std::declval<T>()))
				  ,bool
				  >::value
			 >::type
		>
	: std::true_type {};
	
	template<class S, class T>
	struct does_define_is_one : does_define_is_one_aux<S, T> {};
	
	template<class Traits, class S, class = void>
	struct implement_is_one_aux : Traits {
		using typename Traits::set_type;
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()                        \
		Traits::eq(s, std::forward<T>(x), Traits::one(s))
		template<class T>
		static bool is_one(const set_type& s, T&& x)
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S>
	struct implement_is_one_aux
		<Traits
		,S
		,typename std::enable_if
			 <does_define_is_one<S, typename Traits::type>::value>::type
		>
	: Traits {
		using typename Traits::set_type;
#	define JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl()                        \
		s.is_one(std::forward<T>(x))
		template<class T>
		static bool is_one(const set_type& s, T&& x)
		noexcept(noexcept(JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl())) {
			return JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_TRAITS_MONOID_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_is_one : implement_is_one_aux<Traits, S> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct additive_monoid_traits_builder
	: implement_traits
		  <minimal_additive_monoid_traits<S, T, Admissible, DerivedTraits>
		  ,S
		  ,DerivedTraits
		  ,implement_is_zero
		  > {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct multiplicative_monoid_traits_builder
	: implement_traits
		  <minimal_multiplicative_monoid_traits<S, T, Admissible, DerivedTraits>
		  ,S
		  ,DerivedTraits
		  ,implement_is_one
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct additive_monoid_traits
	: additive_monoid_traits_builder
		  <S, T, Admissible, additive_monoid_traits<S, T, Admissible>> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct multiplicative_monoid_traits
	: multiplicative_monoid_traits_builder
		  <S, T, Admissible, multiplicative_monoid_traits<S, T, Admissible>> {};
} // namespace detail_monoid_traits_hpp_

using detail_monoid_traits_hpp_::satisfies_additive_monoid;
using detail_monoid_traits_hpp_::satisfies_multiplicative_monoid;
using detail_monoid_traits_hpp_::satisfies_additive_monoid_element;
using detail_monoid_traits_hpp_::
	      satisfies_multiplicative_monoid_element;
using detail_monoid_traits_hpp_::additive_monoid_of;
using detail_monoid_traits_hpp_::multiplicative_monoid_of;
using detail_monoid_traits_hpp_::additive_monoid_traits;
using detail_monoid_traits_hpp_::multiplicative_monoid_traits;
using detail_monoid_traits_hpp_::additive_monoid_traits_builder;
using detail_monoid_traits_hpp_::multiplicative_monoid_traits_builder;

#endif // JORDAN_TRAITS_MONOID_TRAITS_HPP_
