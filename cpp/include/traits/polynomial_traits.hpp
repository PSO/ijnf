#ifndef JORDAN_POLYNOMIAL_TRAITS_HPP_
#define JORDAN_POLYNOMIAL_TRAITS_HPP_

#include "algebra_traits.hpp"

namespace extend_wrapper {
	struct nth_tag {};
	
#define JORDAN_POLYNOMIAL_TRAITS_HPP_local_impl()                              \
	mkexpr<nth_tag>(std::forward<T>(v), n)
	template<class T>
	auto nth(T&& v, std::size_t n) noexcept ->
	decltype(JORDAN_POLYNOMIAL_TRAITS_HPP_local_impl()) {
		return JORDAN_POLYNOMIAL_TRAITS_HPP_local_impl();
	}
#undef JORDAN_POLYNOMIAL_TRAITS_HPP_local_impl
	
} // namespace extend_wrapper

namespace detail_polynomial_traits_hpp_ {
	template
		<class S
		,class Scalar = typename S::scalar_type
		,class T = typename S::type
		,template<class> class AdmissibleScalar =
			 curried_decays_to<Scalar>::template invoke
		,template<class> class AdmissibleT =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_polynomial_domain
	: all_valid
		<satisfies_euclidean_domain<S, T, AdmissibleT>
		,satisfies_algebra<S, Scalar, T, AdmissibleScalar, AdmissibleT>
		,does_define_nth<S, std::size_t, T>
		,does_define_size<S, T, std::size_t>
		,check_for<does_define_monomial, S, std::size_t, any_cref<Scalar>, T>
		> {};
	
	/*
	template<class Lhs, class Rhs>
	type multiply(const set_type& s, Lhs&& lhs, Rhs&& rhs) {
		type result = zero<type>();
		auto r_s = mksymb(result);
		for(std::size_t n = size(s, lhs) ; n != 0 ; ) {
			--n;
			for(std::size_t m = size(s, rhs) ; m != 0 ; ) {
				--m;
				result +=
					monomial
						(s
						,n + m
						,multiply
							 (s
							 ,std::forward<Lhs>(lhs)[n]
							  * std::forward<Rhs>(rhs)[m]
							 )
						);
			}
		}
		return result;
	}
	//*/
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		,class = void
		>
	struct minimal_polynomial_domain_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct minimal_polynomial_domain_traits_aux
		<S
		,Scalar
		,T
		,AdmissibleScalar
		,AdmissibleT
		,DerivedTraits
		,typename std::enable_if
			<satisfies_polynomial_domain
				 <S, Scalar, T, AdmissibleScalar, AdmissibleT>::value
			>::type
		>
	: euclidean_domain_traits_builder<S, T, AdmissibleT, DerivedTraits>
	, algebra_traits_builder
		  <S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>
	{
		using set_type =
			typename algebra_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits
				>::set_type;
		using scalar_type =
			typename algebra_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits
				>::scalar_type;
		using type =
			typename algebra_traits_builder
				<S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits
				>::type;
		
#	define POLYNOMIAL_TRAITS_HPP_local_impl() s.nth(std::forward<F>(f), n)
		template<class F>
		static auto nth(const set_type& s, F&& f, std::size_t n)
		noexcept(noexcept(POLYNOMIAL_TRAITS_HPP_local_impl())) ->
		typename std::enable_if
			<AdmissibleT<F>::value
			,decltype(POLYNOMIAL_TRAITS_HPP_local_impl())
			>::type {
			return POLYNOMIAL_TRAITS_HPP_local_impl();
		}
#	undef POLYNOMIAL_TRAITS_HPP_local_impl
		
#	define POLYNOMIAL_TRAITS_HPP_local_impl() s.size(std::forward<F>(f))
		template<class F>
		static auto nth(const set_type& s, F&& f, std::size_t n)
		noexcept(noexcept(POLYNOMIAL_TRAITS_HPP_local_impl())) ->
		typename std::enable_if
			<AdmissibleT<F>::value
			,decltype(POLYNOMIAL_TRAITS_HPP_local_impl())
			>::type {
			return POLYNOMIAL_TRAITS_HPP_local_impl();
		}
#	undef POLYNOMIAL_TRAITS_HPP_local_impl
		
#	define POLYNOMIAL_TRAITS_HPP_local_impl() s.monomial(n)
		static auto polynomial_type(const set_type& s, std::size_t n)
		noexcept(noexcept(POLYNOMIAL_TRAITS_HPP_local_impl())) ->
		decltype(POLYNOMIAL_TRAITS_HPP_local_impl()) {
			return POLYNOMIAL_TRAITS_HPP_local_impl();
		}
#	undef POLYNOMIAL_TRAITS_HPP_local_impl
		
		using euclidean_domain_traits_builder
			      <S, T, AdmissibleT, DerivedTraits>::eval;
		using algebra_traits_builder
			      <S
			      ,Scalar
			      ,T
			      ,AdmissibleScalar
			      ,AdmissibleT
			      ,DerivedTraits
			      >::eval;
#	define POLYNOMIAL_TRAITS_HPP_local_impl()                                  \
		nth(s                                                                  \
		   ,delay<U>::template invoke<DerivedTraits>::type::eval               \
			    (s, lhs(std::move(e)))                                         \
		   ,rhs(std::move(e))                                                  \
		   )
		template<class U>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
			     <extend_wrapper::nth_tag, U, std::size_t>&& e
			)
		noexcept(noexcept(POLYNOMIAL_TRAITS_HPP_local_impl())) ->
		decltype(POLYNOMIAL_TRAITS_HPP_local_impl()) {
			return POLYNOMIAL_TRAITS_HPP_local_impl();
		}
#	undef POLYNOMIAL_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct minimal_polynomial_domain_traits
	: minimal_polynomial_domain_traits_aux
		  <S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits> {};
	
	template<class Traits, class S, class DerivedTraits, class = void>
	struct implement_evaluate_aux : Traits {
		using typename Traits::type;
		using typename Traits::scalar_type;
		using typename Traits::set_type;
		
		template<class F>
		static scalar_type evaluate
			(const set_type& s, F&& f, const scalar_type& x)
		noexcept(
			noexcept(Traits::template zero<scalar_type>(s))
			&& noexcept(scalar_type(std::declval<scalar_type>()))
			&& noexcept
				 (std::declval<scalar_type&>() =
					  Traits::eval
						  (s
						  ,(mksymb(std::declval<scalar_type&&>())
						    * mksymb(std::declval<const scalar_type&>())
						   ) + mksymb(std::forward<F>(f)[std::size_t(0)])
						  )
				 )
		) {
			scalar_type result = Traits::template zero<scalar_type>(s);
			auto r_s = mksymb(std::move(result));
			auto x_s = mksymb(x);
			for(std::size_t n = Traits::size(s, f) ; n != 0 ; ) {
				auto&& nth_coef = Traits::nth(s, std::forward<F>(f), --n);
				auto nth_coef_s = mksymb(std::move(nth_coef));
				result = Traits::eval(s, (r_s * x_s) + nth_coef_s);
			}
			return result;
		}
		
		template
			<class CustomS
			,class Scalar = typename CustomS::scalar_type
			,class T = typename CustomS::type
			,template<class> class AdmissibleScalar =
				 curried_decays_to<Scalar>::template invoke
			,template<class> class AdmissibleT =
				 curried_decays_to<T>::template invoke
			,class F
			>
		static T evaluate
			(const set_type& s
			,const CustomS& opset
			,F&& f
			,const typename std::remove_reference<T>::type& op
			) {
			using operand_traits =
				algebra_traits
					<CustomS, Scalar, T, AdmissibleScalar, AdmissibleT>;
			T result = operand_traits::template zero<T>(opset);
			auto r_s  = mksymb(std::move(result));
			auto op_s = mksymb(op);
			for(std::size_t n = size(s, f) ; n != 0 ;) {
				auto&& nth_coef = Traits::nth(s, std::forward<F>(f), --n);
				auto nth_coef_s = mksymb(std::move(nth_coef));
				result = operand_traits::eval
					         (opset, (r_s * op_s) + (nth_coef_s * onesymb));
			}
			return result;
		}
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_evaluate
	: implement_evaluate_aux<Traits, S, DerivedTraits> {};
	
	template
		<class Domain
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct polynomial_domain_traits_builder
	: implement_traits
		  <minimal_polynomial_domain_traits
			   <Domain, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>
		  ,Domain
		  ,DerivedTraits
		  ,implement_evaluate
		  > {};
	
	template
		<class Domain
		,class Scalar = typename Domain::scalar_type
		,class T = typename Domain::type
		,template<class> class AdmissibleScalar =
			 curried_decays_to<Scalar>::template invoke
		,template<class> class AdmissibleT =
			 curried_decays_to<T>::template invoke
		>
	struct polynomial_domain_traits
	: polynomial_domain_traits_builder
		  <Domain
		  ,Scalar
		  ,T
		  ,AdmissibleScalar
		  ,AdmissibleT
		  ,polynomial_domain_traits
			   <Domain, Scalar, T, AdmissibleScalar, AdmissibleT>
		  > {};
} // namespace detail_polynomial_traits_hpp_

using detail_polynomial_traits_hpp_::satisfies_polynomial_domain;
using detail_polynomial_traits_hpp_::polynomial_domain_traits_builder;
using detail_polynomial_traits_hpp_::polynomial_domain_traits;

#endif // JORDAN_POLYNOMIAL_TRAITS_HPP_
