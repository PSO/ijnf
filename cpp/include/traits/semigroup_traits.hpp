#ifndef JORDAN_SEMIGROUP_TRAITS_HPP_
#define JORDAN_SEMIGROUP_TRAITS_HPP_

#include "set_traits.hpp"
#include "arithmetic_traits.hpp"
#include "../common.hpp"
#include <type_traits>
#include <utility>

namespace extend_wrapper {
	struct product_tag {};
	struct sum_tag {};
	
#define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                               \
	mkexpr<product_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator*(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()) {
		return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
	}
#undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
	
#define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                               \
	mkexpr<sum_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator+(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()) {
		return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
	}
#undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
} // namespace extend_wrapper

namespace detail_semigroup_traits_hpp_ {
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_additive_semigroup_element
	: all_valid
		  <check_for<is_summable, any_cref<T>, any_cref<T>, T>
		  ,satisfies_set_element<T>
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_additive_semigroup
	: all_valid
		  <check_for
			   <does_define_add
			   ,S
			   ,any_cref<T>
			   ,any_cref<T>
			   ,T
			   >
		  ,satisfies_set<S, T, Admissible>
		  > {};
	
	template<class T, template<class> class Admissible, class = void>
	struct additive_semigroup_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct additive_semigroup_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_additive_semigroup_element<T, Admissible>::value>::type
		>
	: virtual set_of<T, Admissible> {
#	define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                            \
		std::forward<Lhs>(lhs) + std::forward<Rhs>(rhs)
		template<class Lhs, class Rhs>
		auto add(Lhs&& lhs, Rhs&& rhs) const
		noexcept(noexcept(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct additive_semigroup_of : virtual additive_semigroup_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_additive_semigroup_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_additive_semigroup_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_additive_semigroup<S, T, Admissible>::value>::type
		>
	: set_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename set_traits_builder<S, T, Admissible, DerivedTraits>::set_type;
		using typename set_traits_builder<S, T, Admissible, DerivedTraits>::type;
		template<class U>
		using is_admissible_type =
			typename set_traits_builder<S, T, Admissible, DerivedTraits>::
				template is_admissible_type<U>;
		static constexpr bool valid = true;
		
		
#	define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                            \
		s.add(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto add(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())) ->
		typename std::enable_if
			<is_admissible_type<Lhs&&>::value
			 && is_admissible_type<Rhs&&>::value
			,decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())
			>::type {
			return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
		
		using set_traits_builder<S, T, Admissible, DerivedTraits>::eval;
#	define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                            \
		add(s                                                                  \
		   ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval   \
			    (s, lhs(std::move(e)))                                         \
		   ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval   \
			    (s, rhs(std::move(e)))                                         \
		   )
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::sum_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_additive_semigroup_traits
	: minimal_additive_semigroup_traits_aux
		  <S, T, Admissible, DerivedTraits> {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_multiplicative_semigroup_element
	: all_valid
		  <check_for<is_multipliable, any_cref<T>, any_cref<T>, T>
		  ,satisfies_set_element<T, Admissible>
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_multiplicative_semigroup
	: all_valid
		  <check_for
			   <does_define_multiply
			   ,S
			   ,any_cref<T>
			   ,any_cref<T>
			   ,T
			   >
		  ,satisfies_set<S, T, Admissible>
		  > {};
	
	template<class T, template<class> class Admissible, class = void>
	struct multiplicative_semigroup_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct multiplicative_semigroup_of_aux
		<T
		,Admissible
		,typename std::enable_if
			 <satisfies_multiplicative_semigroup_element<T, Admissible>::value
			 >::type
		>
	: virtual set_of<T, Admissible> {
#	define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                            \
		std::forward<U>(lhs) * std::forward<V>(rhs)
		template<class U, class V>
		auto multiply(U&& lhs, V&& rhs) const
		noexcept(noexcept(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct multiplicative_semigroup_of
	: virtual multiplicative_semigroup_of_aux<T, Admissible> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_multiplicative_semigroup_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_multiplicative_semigroup_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_multiplicative_semigroup<S, T, Admissible>::value
			 >::type
		>
	: set_traits_builder<S, T, Admissible, DerivedTraits> {
		using typename set_traits_builder<S, T, Admissible, DerivedTraits>::set_type;
		using typename set_traits_builder<S, T, Admissible, DerivedTraits>::type;
		
		template<class U>
		using is_admissible_type =
			typename set_traits_builder<S, T, Admissible, DerivedTraits>::
				template is_admissible_type<U>;
		static constexpr bool valid = true;
		
#	define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                            \
		s.multiply(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
		template<class Lhs, class Rhs>
		static auto multiply(const set_type& s, Lhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())) ->
		typename std::enable_if
			<is_admissible_type<Lhs&&>::value
			 && is_admissible_type<Rhs&&>::value
			,decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())
			>::type {
			return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
		
		using set_traits_builder<S, T, Admissible, DerivedTraits>::eval;
#	define JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()                            \
		multiply                                                               \
			(s                                                                 \
			,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval  \
				 (s, lhs(std::move(e)))                                        \
			,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval  \
				 (s, rhs(std::move(e)))                                        \
			)
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
			 <extend_wrapper::product_tag
			 ,LhsExpression
			 ,RhsExpression
			 >&& e
			)
		noexcept(noexcept(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SEMIGROUP_TRAITS_HPP_local_impl()) {
			return JORDAN_SEMIGROUP_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SEMIGROUP_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_multiplicative_semigroup_traits
	: minimal_multiplicative_semigroup_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct additive_semigroup_traits_builder
	: minimal_additive_semigroup_traits<S, T, Admissible, DerivedTraits> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct multiplicative_semigroup_traits_builder
	: minimal_multiplicative_semigroup_traits<S, T, Admissible, DerivedTraits> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct additive_semigroup_traits
	: additive_semigroup_traits_builder
		  <S, T, Admissible, additive_semigroup_traits<S, T, Admissible>> {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct multiplicative_semigroup_traits
	: multiplicative_semigroup_traits_builder
		  <S, T, Admissible, multiplicative_semigroup_traits<S, T, Admissible>> {};
} // namespace detail_semigroup_traits_hpp_

using detail_semigroup_traits_hpp_::
	      satisfies_additive_semigroup_element;
using detail_semigroup_traits_hpp_::
	      satisfies_multiplicative_semigroup_element;
using detail_semigroup_traits_hpp_::satisfies_additive_semigroup;
using detail_semigroup_traits_hpp_::satisfies_multiplicative_semigroup;
using detail_semigroup_traits_hpp_::additive_semigroup_of;
using detail_semigroup_traits_hpp_::multiplicative_semigroup_of;
using detail_semigroup_traits_hpp_::additive_semigroup_traits;
using detail_semigroup_traits_hpp_::additive_semigroup_traits_builder;
using detail_semigroup_traits_hpp_::multiplicative_semigroup_traits;
using detail_semigroup_traits_hpp_::multiplicative_semigroup_traits_builder;

#endif // JORDAN_SEMIGROUP_TRAITS_HPP_
