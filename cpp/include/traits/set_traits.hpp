#ifndef JORDAN_SET_TRAITS_HPP_
#define JORDAN_SET_TRAITS_HPP_


#include "traits_base.hpp"
#include "mixin_def.hpp"

#include "trait_composition.hpp"
#include "arithmetic_traits.hpp"
#include "wrapper.hpp"
#include "../common.hpp"
#include <type_traits>

namespace extend_wrapper {
	struct is_equal_tag {};
	struct is_not_equal_tag {};
	struct and_tag {};
	struct or_tag {};
	struct not_tag {};
	
#define JORDAN_SET_TRAITS_HPP_local_impl()                                     \
	mkexpr<is_equal_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator==(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
		return JORDAN_SET_TRAITS_HPP_local_impl();
	}
#undef JORDAN_SET_TRAITS_HPP_local_impl
	
#define JORDAN_SET_TRAITS_HPP_local_impl()                                     \
	mkexpr<is_not_equal_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator!=(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
		return JORDAN_SET_TRAITS_HPP_local_impl();
	}
#undef JORDAN_SET_TRAITS_HPP_local_impl
	
#define JORDAN_SET_TRAITS_HPP_local_impl()                                     \
	mkexpr<and_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator&&(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
		return JORDAN_SET_TRAITS_HPP_local_impl();
	}
#undef JORDAN_SET_TRAITS_HPP_local_impl
	
#define JORDAN_SET_TRAITS_HPP_local_impl()                                     \
	mkexpr<or_tag>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs))
	template<class Lhs, class Rhs>
	auto operator||(Lhs&& lhs, Rhs&& rhs) noexcept ->
	decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
		return JORDAN_SET_TRAITS_HPP_local_impl();
	}
#undef JORDAN_SET_TRAITS_HPP_local_impl
	
#define JORDAN_SET_TRAITS_HPP_local_impl()                                     \
	mkexpr<not_tag>(std::forward<Op>(op))
	template<class Op>
	auto operator!(Op&& op) noexcept ->
	decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
		return JORDAN_SET_TRAITS_HPP_local_impl();
	}
#undef JORDAN_SET_TRAITS_HPP_local_impl
} // namespace extend_wrapper

namespace detail_set_traits_hpp_ {
	template<class To>
	struct curried_decays_to {
		template<class From>
		struct invoke : std::is_same<typename std::decay<From>::type, To> {};
	};
	
	//*
#	include "mixin_def.pp.hpp"
	INTRODUCTION_MIXIN_DEF
		(intro_eq
		,eq
		,((param, const typename Traits::domain_type&, domain)
		 ,(checked_uref, Lhs, lhs, typename Traits::value_type)
		 ,(checked_uref, Rhs, rhs, typename Traits::value_type)
		 )
		,(domain.eq(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)))
		,(bool)
		,"domain.eq(lhs, rhs) is not legal"
		)
	FUNCTIONALITY_MIXIN_DEF
		(impl_neq
		,neq
		,((param, const typename Traits::domain_type&, domain)
		 ,(checked_uref, Lhs, lhs, typename Traits::value_type)
		 ,(checked_uref, Rhs, rhs, typename Traits::value_type)
		 )
		,(domain.neq(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)))
		,(!Traits::eq(domain, std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)))
		,(bool)
		,"Traits::eq(domain, lhs, rhs) is not legal"
		)
	INTERPRET_MIXIN_DEF(impl_interpret_eq, eq, extend_wrapper::is_equal_tag)
	INTERPRET_MIXIN_DEF
		(impl_interpret_neq, neq, extend_wrapper::is_not_equal_tag)
#	include "mixin_def.pp.hpp"
	
	struct impl_interpret_symbol {
		template<class Traits, class Derived>
		struct invoke : Traits {
			using typename Traits::domain_type;
#		define JORDAN_SET_TRAITS_HPP_local_impl() std::forward<U>(symb.value)
			template<class U>
			static auto interpret
				(const domain_type&, extend_wrapper::symbol<U>&& symb)
			noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
			decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
				return JORDAN_SET_TRAITS_HPP_local_impl();
			}
#		undef JORDAN_SET_TRAITS_HPP_local_impl
			
#		define JORDAN_SET_TRAITS_HPP_local_impl() std::forward<U>(symb.value)
			template<class U>
			static auto interpret
				(const domain_type&, extend_wrapper::symbol<U>& symb)
				noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
			decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
				return JORDAN_SET_TRAITS_HPP_local_impl();
			}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
		};
	};
	
	struct impl_interpret_boolean_or {
		template<class Traits, class Derived>
		struct invoke : Traits {
			using typename Traits::domain_type;
			using Traits::interpret;
#		define JORDAN_SET_TRAITS_HPP_local_impl()\
			delay<LhsExpression>::template invoke<Derived>::type::interpret\
				(domain, std::forward<LhsExpression>(lhs(std::move(e))))\
			|| delay<LhsExpression>::template invoke<Derived>::type::interpret\
				   (domain, std::forward<RhsExpression>(rhs(std::move(e))))
			template<class LhsExpression, class RhsExpression>
			static auto interpret
				(const domain_type& domain
				,extend_wrapper::expression
					 <extend_wrapper::or_tag
					 ,LhsExpression
					 ,RhsExpression
					 >&& e
				)
			noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
			decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
				return JORDAN_SET_TRAITS_HPP_local_impl();
			}
#		undef JORDAN_SET_TRAITS_HPP_local_impl
		};
	};
	
	struct impl_interpret_boolean_and {
		template<class Traits, class Derived>
		struct invoke : Traits {
			using typename Traits::domain_type;
			using Traits::interpret;
#		define JORDAN_SET_TRAITS_HPP_local_impl()\
			delay<LhsExpression>::template invoke<Derived>::type::interpret\
				(domain, std::forward<LhsExpression>(lhs(std::move(e))))\
			&& delay<LhsExpression>::template invoke<Derived>::type::interpret\
				   (domain, std::forward<RhsExpression>(rhs(std::move(e))))
			template<class LhsExpression, class RhsExpression>
			static auto interpret
				(const domain_type& domain
				,extend_wrapper::expression
					 <extend_wrapper::and_tag
					 ,LhsExpression
					 ,RhsExpression
					 >&& e
				)
			noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
			decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
				return JORDAN_SET_TRAITS_HPP_local_impl();
			}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
		};
	};
	
	struct intro_value_type {
		template<class Traits, class Derived>
		struct invoke : Traits {
			using value_type = typename Traits::domain_type::value_type;
		};
	};
	
	using set_constraints =
		lmpl::list
			<intro_value_type
			,intro_eq
			,impl_neq
			,impl_interpret_symbol
			,impl_interpret_boolean_or
			,impl_interpret_boolean_and
			,impl_interpret_eq
			,impl_interpret_neq
			>;
	
#	include "mixin_def.pp.hpp"
	INTRODUCTION_MIXIN_DEF
		(intro_operator_eq
		,eq
		,((checked_uref, Lhs, lhs, typename Traits::value_type)
		 ,(checked_uref, Rhs, rhs, typename Traits::value_type)
		 )
		,(std::forward<Lhs>(lhs) == std::forward<Rhs>(rhs))
		,(bool)
		,"lhs == rhs is not legal"
		)
	OPTIONAL_INTRODUCTION_MIXIN_DEF
		(intro_operator_neq
		,neq
		,((checked_uref, Lhs, lhs, typename Traits::value_type)
		 ,(checked_uref, Rhs, rhs, typename Traits::value_type)
		 )
		,(std::forward<Lhs>(lhs) != std::forward<Rhs>(rhs))
		,(bool)
		)
#	include "mixin_def.pp.hpp"
	
	template<class ValueType>
	using set_domain_constraints =
		lmpl::list
			<with_value_type<ValueType>
			,intro_operator_eq
			,intro_operator_neq
			>;
	//*/
	
	
	/** Trait class defining a member static constexpr bool `value` which is
	 *  true only if `S` defines all necessary methods for the set concept
	 *  (with respect to the type T and the admissibility condition Admissible)
	 */
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_set
	: all_valid
		<check_for
			<does_define_eq
			,S
			,any_cref<T>
			,any_cref<T>
			>
		> {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		,class = void
		>
	struct minimal_set_traits_aux {};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_set_traits_aux
		<S
		,T
		,Admissible
		,DerivedTraits
		,typename std::enable_if<satisfies_set<S, T, Admissible>::value>::type
		> {
		using set_type = S;
		using type     = T;
		template<class U>
		using is_admissible_type = Admissible<U>;
		static constexpr bool valid = true;
		
		template<class Lhs, class Rhs>
		static typename std::enable_if
			<is_admissible_type<Lhs&&>::value
			 && is_admissible_type<Rhs&&>::value
			,bool
			>::type
		eq(const set_type& s, Lhs&& lhs, Rhs&& rhs) noexcept {
			return s.eq(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs));
		}
		
#	define JORDAN_SET_TRAITS_HPP_local_impl()                                  \
		eq(s                                                                   \
		  ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval    \
			   (s, std::forward<LhsExpression>(lhs(std::move(e))))             \
		  ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval    \
			   (s, std::forward<RhsExpression>(rhs(std::move(e))))             \
		  )
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::is_equal_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
			return JORDAN_SET_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
			
#	define JORDAN_SET_TRAITS_HPP_local_impl()                                  \
		delay<LhsExpression>::template invoke<DerivedTraits>::type::eval       \
			(s, std::forward<LhsExpression>(lhs(std::move(e))))                \
		|| delay<LhsExpression>::template invoke<DerivedTraits>::type::eval    \
			(s, std::forward<RhsExpression>(rhs(std::move(e))))
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::or_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
			return JORDAN_SET_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
			
#	define JORDAN_SET_TRAITS_HPP_local_impl()                                  \
		delay<LhsExpression>::template invoke<DerivedTraits>::type::eval       \
			(s, std::forward<LhsExpression>(lhs(std::move(e))))                \
		&& delay<LhsExpression>::template invoke<DerivedTraits>::type::eval    \
			(s, std::forward<RhsExpression>(rhs(std::move(e))))
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::and_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
			return JORDAN_SET_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
			
#	define JORDAN_SET_TRAITS_HPP_local_impl()                                  \
		!delay<Op>::template invoke<DerivedTraits>::type::eval                 \
			 (s, extend_wrapper::nth<0>(std::move(e)))
		template<class Op>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression<extend_wrapper::not_tag, Op>&& e
			)
		noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
			return JORDAN_SET_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
			
#	define JORDAN_SET_TRAITS_HPP_local_impl() std::forward<U>(symb.value)
		template<class U>
		static auto eval(const set_type&, extend_wrapper::symbol<U>&& symb)
		noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
			return JORDAN_SET_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
			
#	define JORDAN_SET_TRAITS_HPP_local_impl() std::forward<U>(symb.value)
		template<class U>
		static auto eval
			(const set_type&, extend_wrapper::symbol<U>& symb)
		noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
			return JORDAN_SET_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct minimal_set_traits
	: minimal_set_traits_aux<S, T, Admissible, DerivedTraits> {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_set_element
	: check_for<is_equality_comparable, any_cref<T>, any_cref<T>> {};
	
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		,class = void
		>
	struct set_of_aux {};
	
	template<class T, template<class> class Admissible>
	struct set_of_aux
		<T
		,Admissible
		,typename std::enable_if<satisfies_set_element<T>::value>::type
		> {
		using type = T;
		template<class U, class V>
		bool eq(U&& lhs, V&& rhs) const noexcept {
			return std::forward<U>(lhs) == std::forward<V>(rhs);
		}
	};
	
	/** Class defining a default set corresponding to a type `T` and an
	 *  admissibility condition `Admissible`.
	 */
	template
		<class T
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct set_of : set_of_aux<T, Admissible> {};
	
	template<class Trait, class S, class = void>
	struct implement_neq_aux : Trait {
		using typename Trait::set_type;
		template<class T, class U>
		static bool neq(const set_type& s, T&& lhs, U&& rhs) noexcept {
			return !Trait::eq(s, std::forward<T>(lhs), std::forward<U>(rhs));
		}
	};
	
	template<class Trait, class S>
	struct implement_neq_aux
		<Trait
		,S
		,typename std::enable_if
			 <!check_for
				  <does_define_neq
				  ,S
				  ,any_cref<typename Trait::type>
				  ,any_cref<typename Trait::type>
				  >::value
			  && check_for
				     <is_inequality_comparable
				     ,any_cref<typename Trait::type>
				     ,any_cref<typename Trait::type>
				     >::value
			 >::type
		>
	: Trait {
		using typename Trait::set_type;
		template<class T, class U>
		static bool neq(const set_type&, T&& lhs, U&& rhs) noexcept {
			return std::forward<T>(lhs) != std::forward<U>(rhs);
		}
	};
	
	template<class Trait, class S>
	struct implement_neq_aux
		<Trait
		,S
		,typename std::enable_if
			 <check_for
				  <does_define_neq
				  ,S
				  ,any_cref<typename Trait::type>
				  ,any_cref<typename Trait::type>
				  >::value
			 >::type
		>
	: Trait {
		using typename Trait::set_type;
		template<class T, class U>
		static bool neq(const set_type& s, T&& lhs, U&& rhs) noexcept {
			return s.neq(std::forward<T>(lhs), std::forward<U>(rhs));
		}
	};
	
	template<class Trait, class S, class DerivedTraits>
	struct implement_neq : implement_neq_aux<Trait, S> {
		using typename implement_neq_aux<Trait, S>::set_type;
		
#	define JORDAN_SET_TRAITS_HPP_local_impl()                                  \
		implement_neq_aux<Trait, S>::neq(s                                     \
		   ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval   \
			    (s, std::forward<LhsExpression>(lhs(std::move(e))))            \
		   ,delay<LhsExpression>::template invoke<DerivedTraits>::type::eval   \
			    (s, std::forward<RhsExpression>(rhs(std::move(e))))            \
		   )
		template<class LhsExpression, class RhsExpression>
		static auto eval
			(const set_type& s
			,extend_wrapper::expression
				 <extend_wrapper::is_not_equal_tag
				 ,LhsExpression
				 ,RhsExpression
				 >&& e
			)
		noexcept(noexcept(JORDAN_SET_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_SET_TRAITS_HPP_local_impl()) {
			return JORDAN_SET_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_SET_TRAITS_HPP_local_impl
		using implement_neq_aux<Trait, S>::eval;
	};
	
	template
		<class S
		,class T
		,template<class> class Admissible
		,class DerivedTraits
		>
	struct set_traits_builder
	: implement_traits
		  <minimal_set_traits<S, T, Admissible, DerivedTraits>
		  ,S
		  ,DerivedTraits
		  ,implement_neq
		  > {};
	
	template
		<class S
		,class T = typename S::type
		,template<class> class Admissible =
			 curried_decays_to<T>::template invoke
		>
	struct set_traits
	: set_traits_builder<S, T, Admissible, set_traits<S, T, Admissible>> {};
} // namespace detail_set_traits_hpp_

using detail_set_traits_hpp_::curried_decays_to;
using detail_set_traits_hpp_::satisfies_set_element;
using detail_set_traits_hpp_::satisfies_set;
using detail_set_traits_hpp_::set_traits_builder;
using detail_set_traits_hpp_::set_traits;
using detail_set_traits_hpp_::set_of;

using detail_set_traits_hpp_::set_constraints;
using detail_set_traits_hpp_::set_domain_constraints;
#endif // JORDAN_SET_TRAITS_HPP_
