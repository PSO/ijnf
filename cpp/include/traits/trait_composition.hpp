#ifndef JORDAN_TRAITS_TRAIT_COMPOSITION_HPP_
#define JORDAN_TRAITS_TRAIT_COMPOSITION_HPP_

#include "lmpl/cartesian_product.hpp"
#include "lmpl/repack.hpp"
#include "lmpl/common_operators.hpp"
#include "lmpl/list.hpp"
#include "lmpl/reduce.hpp"
#include "lmpl/map.hpp"
#include "lmpl/unique.hpp"
#include <type_traits>

namespace detail_trait_composition_hpp_ {
	template<class... Traits>
	struct all_valid
	: lmpl::reduce_t
		  <lmpl::list<Traits...>
		  ,lmpl::metafunction<lmpl::boolean_and>
		  > {
		template<class U>
		struct compile_time_check
		: Traits::template compile_time_check<U>... {};
	};
	
	template<class... Traits>
	struct first_fail;
	
	template<>
	struct first_fail<> {
		using type = first_fail<>;
		template<class U>
		struct compile_time_check {};
	};
	
	template<class Head, class... Tail>
	struct first_fail<Head, Tail...> {
		using type =
			typename std::conditional
				<Head::value
				,typename first_fail<Tail...>::type
				,Head
				>::type;
	};
	
	template<class... Traits>
	struct all_valid_fail_once
	: lmpl::reduce_t
		  <lmpl::list<Traits...>, lmpl::metafunction<lmpl::boolean_and>> {
		template<class U>
		struct compile_time_check
		: first_fail<Traits...>::type::template compile_time_check<U>{};
	};
	
	template<class... Traits>
	struct one_valid
	: lmpl::reduce_t
		  <lmpl::list<Traits...>
		  ,lmpl::metafunction<lmpl::boolean_or>
		  > {
		template<class U>
		struct compile_time_check
		: Traits::template compile_time_check
			  <typename std::conditional
				   <one_valid<Traits...>::value, void*, U>::type
			  >... {};
	};
	
	template<class T>
	struct any_cref {};
	
	template<class... T>
	struct any_of {};
	
	template<class T>
	struct cref_qualification_list
	: lmpl::unique<lmpl::list<T&, const T&, T&&>> {};
	
	template<>
	struct cref_qualification_list<void>
	: lmpl::type_constant<lmpl::list<void>> {};
	
	template<class T>
	struct argument_choices : lmpl::type_constant<lmpl::list<T>> {};
	
	template<class T>
	struct argument_choices<any_cref<T>>
	: cref_qualification_list<typename std::decay<T>::type> {};
	
	template<class... T>
	struct argument_choices<any_of<T...>>
	: lmpl::type_constant<lmpl::list<T...>> {};
	
	template<template<class...> class Trait>
	struct apply_to_list {
		template<class L>
		struct invoke
		: lmpl::repack<L, lmpl::pack_template<Trait>> {};
	};
	
	template<template<class...> class Trait, class ArgL>
	struct check_for_aux
	: lmpl::repack_t
		  <lmpl::map_t<ArgL, apply_to_list<Trait>>
		  ,lmpl::pack_template<all_valid_fail_once>
		  > {};
	
	
	template<template<class...> class Trait, class... Args>
	struct check_for
	: check_for_aux
		  <Trait
		  ,typename lmpl::repack_t
			   <lmpl::map_t
				    <lmpl::list<Args...>, lmpl::metafunction<argument_choices>>
				    ,lmpl::pack_template<lmpl::cartesian_product>
				    >::type
		  > {};
} // namespace detail_trait_composition_hpp_

#define TRAIT_COMPOSITION_DELAYED_CAT_AUX(x, y) x ## y
#define TRAIT_COMPOSITION_DELAYED_CAT(x, y) TRAIT_COMPOSITION_DELAYED_CAT_AUX(x, y)
#define JORDAN_COMPILE_TIME_CHECK(...)                                         \
	struct TRAIT_COMPOSITION_DELAYED_CAT\
		       (detail_trait_composition_hpp_check, __LINE__)\
	: __VA_ARGS__ ::template compile_time_check<void> {}

using detail_trait_composition_hpp_::any_cref;
using detail_trait_composition_hpp_::any_of;
using detail_trait_composition_hpp_::check_for;
using detail_trait_composition_hpp_::all_valid;
using detail_trait_composition_hpp_::one_valid;

#endif // JORDAN_TRAITS_TRAIT_COMPOSITION_HPP_
