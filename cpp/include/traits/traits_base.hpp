#ifndef TRAITS_BASE_HPP_
#define TRAITS_BASE_HPP_

#include <type_traits>

namespace detail_traits_base_hpp_ {
	template<class Derived, class Domain>
	struct traits_base {
		using domain_type = Domain;
		template<class T>
		using type_expression_type =
			typename domain_type::template type_expression_type<T>;
		template<class T>
		using type_expression_type_t = typename type_expression_type<T>::type;
		
		template<class T>
		struct compile_time_check {};
		
		traits_base() = delete;
	};
	
	template<class Derived, template<class> class Typer = std::decay>
	struct domain_base {
		template<class T>
		using type_expression_type = Typer<T>;
		template<class T>
		using type_expression_type_t = typename type_expression_type<T>::type;
		
		template<class T>
		struct compile_time_check {};
	};
	
	template<class Derived>
	struct domain_default_base : domain_base<Derived> {};
	
	template
		<class Base
		,class Derived
		,class... Mixin
		>
	struct traits_builder_helper;
	
	template<class Base, class Derived>
	struct traits_builder_helper<Base, Derived> { using type = Base; };
	
	template
		<class Base
		,class Derived
		,class MixinHead
		,class... MixinTail
		>
	struct traits_builder_helper<Base, Derived, MixinHead, MixinTail...>
	: traits_builder_helper
		  <typename MixinHead::template invoke<Base, Derived>
		  ,Derived
		  ,MixinTail...
		  > {};
	
	template<class MixinL, class Domain>
	struct traits_builder {};
	
	template
		<template<class...> class Holder
		,class... Mixin
		,class Domain
		>
	struct traits_builder<Holder<Mixin...>, Domain>
	: traits_builder_helper
		  <traits_base
			   <traits_builder<Holder<Mixin...>, Domain>
			   ,Domain
			   >
		  ,traits_builder<Holder<Mixin...>, Domain>
		  ,Mixin...
		  >::type {};
	
	template
		<class MixinL
		,template<class> class BaseDomain = domain_default_base
		>
	struct domain_builder {};
	
	template
		<template<class...> class Holder
		,class... Mixin
		,template<class> class BaseDomain
		>
	struct domain_builder<Holder<Mixin...>, BaseDomain>
	: traits_builder_helper
		  <BaseDomain<domain_builder<Holder<Mixin...>, BaseDomain>>
		  ,domain_builder<Holder<Mixin...>, BaseDomain>
		  ,Mixin...
		  >::type {};
	
	template<class ValueType>
	struct with_value_type {
		template<class Base, class Derived>
		struct invoke : Base { using value_type = ValueType; };
	};
} // namespace detail_traits_base_hpp_

using detail_traits_base_hpp_::domain_base;
using detail_traits_base_hpp_::traits_builder;
using detail_traits_base_hpp_::domain_builder;
using detail_traits_base_hpp_::with_value_type;

#endif // TRAITS_BASE_HPP_
