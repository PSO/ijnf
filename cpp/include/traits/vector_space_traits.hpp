#ifndef JORDAN_VECTOR_SPACE_TRAITS_HPP_
#define JORDAN_VECTOR_SPACE_TRAITS_HPP_

#include "group_traits.hpp"
#include "field_traits.hpp"
#include "../common.hpp"
#include <type_traits>

namespace detail_vector_space_traits_hpp_ {
	template
		<class S
		,class Scalar = typename S::scalar_type
		,class T = typename S::type
		,template<class> class AdmissibleScalar =
			 curried_decays_to<Scalar>::template invoke
		,template<class> class AdmissibleT =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_vector_space
	: all_valid
		  <satisfies_field<S, Scalar, AdmissibleScalar>
		  ,satisfies_additive_group<S, T, AdmissibleT>
		  ,check_for
			   <does_define_multiply
			   ,S
			   ,any_cref<Scalar>
			   ,any_cref<T>
			   ,T
			   >
		  > {};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		,class = void
		>
	struct minimal_vector_space_traits_aux {
		static constexpr bool valid = false;
	};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct minimal_vector_space_traits_aux
		<S
		,Scalar
		,T
		,AdmissibleScalar
		,AdmissibleT
		,DerivedTraits
		,typename std::enable_if
			 <satisfies_vector_space
				  <S, Scalar, T, AdmissibleScalar, AdmissibleT>::value
			 >::type
		>
	: field_traits_builder<S, Scalar, AdmissibleScalar, DerivedTraits>
	, additive_group_traits_builder<S, T, AdmissibleT, DerivedTraits> {
		using set_type =
			typename field_traits_builder
				<S, Scalar, AdmissibleScalar, DerivedTraits>::set_type;
		using type =
			typename additive_group_traits_builder
				<S, T, AdmissibleT, DerivedTraits>::type;
		template<class U>
		using is_admissible_type =
			typename additive_group_traits_builder
				<S, T, AdmissibleT, DerivedTraits>::
					template is_admissible_type<U>;
		
		using scalar_type =
			typename field_traits_builder
				<S, Scalar, AdmissibleScalar, DerivedTraits>::type;
		template<class U>
		using is_admissible_scalar_type =
			typename field_traits_builder
				<S, Scalar, AdmissibleScalar, DerivedTraits>::
					template is_admissible_type<U>;
		
		using field_traits_builder
			      <S, Scalar, AdmissibleScalar, DerivedTraits>::zero;
		using additive_group_traits_builder
			      <S, T, AdmissibleT, DerivedTraits>::zero;
		using field_traits_builder
			      <S, Scalar, AdmissibleScalar, DerivedTraits>::add;
		using additive_group_traits_builder
			      <S, T, AdmissibleT, DerivedTraits>::add;
		using field_traits_builder
			      <S, Scalar, AdmissibleScalar, DerivedTraits>::opposite;
		using additive_group_traits_builder
			      <S, T, AdmissibleT, DerivedTraits>::opposite;
		
		using field_traits_builder
			<S, Scalar, AdmissibleScalar, DerivedTraits>::multiply;
		
#	define JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl()                         \
		s.multiply(std::forward<ScalarLhs>(lhs), std::forward<Rhs>(rhs))
		template<class ScalarLhs, class Rhs>
		static auto multiply(const set_type& s, ScalarLhs&& lhs, Rhs&& rhs)
		noexcept(noexcept(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl())) ->
		typename std::enable_if
			<is_admissible_scalar_type<ScalarLhs>::value
			 && is_admissible_type<Rhs>::value
			,decltype(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl())
			>::type {
			return JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl
		
		static constexpr bool valid = true;
		
#	define JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl()                         \
		s.template zero<type>()
		static auto zero(const set_type& s)
		noexcept(noexcept(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl())) ->
		decltype(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl()) {
			return JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl
	};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct minimal_vector_space_traits
	: minimal_vector_space_traits_aux
		  <S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits> {};
	
	template<class Traits, class S, class = void>
	struct implement_axpy_aux : Traits {
		using typename Traits::set_type;
		
#	define JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl()                         \
		Traits::add                                                            \
			(s                                                                 \
			,Traits::multiply(s, std::forward<A>(a), std::forward<X>(x))       \
			,std::forward<Y>(y)                                                \
			)
		template<class A, class X, class Y>
		static auto axpy(const set_type& s, A&& a, X&& x, Y&& y)
		noexcept(noexcept(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl())) ->
		typename std::enable_if
			<Traits::template is_admissible_scalar_type<A>::value
			 && Traits::template is_admissible_type<X>::value
			 && Traits::template is_admissible_type<Y>::value
			,decltype(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl())
			>::type {
			return JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S>
	struct implement_axpy_aux
		<Traits
		,S
		,typename std::enable_if
			 <check_for
				 <does_define_axpy
				 ,S
				 ,any_cref<typename Traits::scalar_type>
				 ,any_cref<typename Traits::type>
				 ,any_cref<typename Traits::type>
				 ,typename Traits::type
				 >::value
			 >::type
		>
	: Traits {
		using typename Traits::set_type;
		
#	define JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl()                         \
		s.axpy(std::forward<A>(a), std::forward<X>(x), std::forward<Y>(y))
		template<class A, class X, class Y>
		static auto axpy(const set_type& s, A&& a, X&& x, Y&& y)
		noexcept(noexcept(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl())) ->
		typename std::enable_if
			<Traits::template is_admissible_scalar_type<A>::value
			 && Traits::template is_admissible_type<X>::value
			 && Traits::template is_admissible_type<Y>::value
			,decltype(JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl())
			>::type {
			return JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl();
		}
#	undef JORDAN_VECTOR_SPACE_TRAITS_HPP_local_impl
	};
	
	template<class Traits, class S, class DerivedTraits>
	struct implement_axpy : implement_axpy_aux<Traits, S> {};
	
	template
		<class S
		,class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class DerivedTraits
		>
	struct vector_space_traits_builder
	: implement_traits
		  <minimal_vector_space_traits
			   <S, Scalar, T, AdmissibleScalar, AdmissibleT, DerivedTraits>
		  ,S
		  ,DerivedTraits
		  ,implement_axpy
		  > {};
	
	template
		<class S
		,class Scalar = typename S::scalar_type
		,class T = typename S::type
		,template<class> class AdmissibleScalar =
			 curried_decays_to<Scalar>::template invoke
		,template<class> class AdmissibleT =
			 curried_decays_to<T>::template invoke
		>
	struct vector_space_traits
	: vector_space_traits_builder
		  <S
		  ,Scalar
		  ,T
		  ,AdmissibleScalar
		  ,AdmissibleT
		  ,vector_space_traits<S, Scalar, T, AdmissibleScalar, AdmissibleT>
		  > {};
	
	template
		<class Scalar
		,class T
		,template<class> class AdmissibleScalar =
			 curried_decays_to<Scalar>::template invoke
		,template<class> class AdmissibleT =
			 curried_decays_to<T>::template invoke
		>
	struct satisfies_vector_space_element
	: all_valid
		<satisfies_field_element<Scalar, AdmissibleScalar>
		,satisfies_additive_group_element<T, AdmissibleT>
		,check_for<is_multipliable, any_cref<Scalar>, any_cref<T>, T>
		> {};
	
	template
		<class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		,class = void
		>
	struct vector_space_of_aux {};
	
	template
		<class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		>
	struct vector_space_of_aux
		<Scalar
		,T
		,AdmissibleScalar
		,AdmissibleT
		,typename std::enable_if
			 <satisfies_vector_space_element
				  <Scalar, T, AdmissibleScalar, AdmissibleT>::value
			 >::type
		>
	: field_of<Scalar, AdmissibleScalar> {};
	
	template
		<class Scalar
		,class T
		,template<class> class AdmissibleScalar
		,template<class> class AdmissibleT
		>
	struct vector_space_of
	: vector_space_of_aux<Scalar, T, AdmissibleScalar, AdmissibleT> {};
} // namespace detail_vector_space_traits_hpp_

using detail_vector_space_traits_hpp_::satisfies_vector_space;
using detail_vector_space_traits_hpp_::vector_space_traits;
using detail_vector_space_traits_hpp_::vector_space_of;
using detail_vector_space_traits_hpp_::vector_space_traits_builder;

#endif // JORDAN_VECTOR_SPACE_TRAITS_HPP_
