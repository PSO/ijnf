#ifndef JORDAN_TRAITS_WRAPPER_HPP_
#define JORDAN_TRAITS_WRAPPER_HPP_

#include "lmpl/range.hpp"
#include "lmpl/get.hpp"

namespace extend_wrapper {
	template<class T>
	struct symbol {
		constexpr explicit symbol(T&& value_arg)
		: value(std::forward<T>(value_arg)) {}
		
		symbol() = delete;
		symbol& operator=(const symbol&) = delete;
		
		symbol(const symbol& other) : value(std::forward<T>(other.value)) {}
		template<class U>
		friend symbol<U&&> mksymb(U&& x);
		
		T value;
	};
	
	template<class T>
	symbol<T&&> mksymb(T&& x) { return symbol<T&&>(std::forward<T>(x)); }
} // namespace extend_wrapper

#include <iostream>

namespace detail_wrapper_hpp_ {
	using extend_wrapper::symbol;
	
	template<class IndexList, class... Operands>
	struct expression_aux /* undefined */;
	
	template<class Idx, class T>
	struct leaf : symbol<T> { using symbol<T>::symbol; };
	
	template<class... Indices, class... Operands>
	struct expression_aux<lmpl::list<Indices...>, Operands...>
	: private leaf<Indices, Operands>... {
		constexpr explicit expression_aux(Operands&&... op)
		: leaf<Indices, Operands>(std::forward<Operands>(op))... {};
		
		template<int N>
		lmpl::get_t<lmpl::list<Operands...>, lmpl::index_t<N>>&& nth()
		noexcept {
			return std::forward
					   <lmpl::get_t<lmpl::list<Operands...>, lmpl::index_t<N>>
					   >(static_cast
					         <leaf<lmpl::get_t
							           <lmpl::list<Indices...>
							           ,lmpl::index_t<N>
							           >
						          ,lmpl::get_t
							           <lmpl::list<Operands...>
							           ,lmpl::index_t<N>
							           >
						          >&
					         >(*this).value
				        );
		}
	};
} // namespace detail_wrapper_hpp_

namespace extend_wrapper {
	template<class Tag, class... Operands>
	struct expression {
	private:
		using content_t =
			detail_wrapper_hpp_::expression_aux
				<lmpl::range_t
					 <lmpl::index_t<0>, lmpl::index_t<sizeof...(Operands)>>
				,Operands...
				>;
	public:
		constexpr explicit expression(Operands&&... op)
		: content(std::forward<Operands>(op)...) {};
		
		expression& operator=(const expression&) = delete;
		expression(const expression&) = delete;
		expression(expression&&) = default;
		
		template<int N>
		auto nth()
		noexcept(noexcept(std::declval<content_t>().template nth<N>())) ->
		decltype(std::declval<content_t>().template nth<N>()) {
			return content.template nth<N>();
		}
		
	private:
		content_t content;
	};
	
	template<class Tag, class... Operands>
	constexpr expression<Tag, Operands&&...> mkexpr(Operands&&... op) {
		return expression<Tag, Operands&&...>(std::forward<Operands>(op)...);
	}
} // namespace extend_wrapper

namespace detail_wrapper_hpp_ {
	using extend_wrapper::expression;
	
	template<int Idx, class Tag, class... Operands>
	lmpl::get_t<lmpl::list<Operands...>, lmpl::index_t<Idx>>&&
	nth(expression<Tag, Operands...>&& e) noexcept {
		return e.template nth<Idx>();
	}
	
	template<class Tag, class Lhs, class Rhs>
	Lhs&& lhs(expression<Tag, Lhs, Rhs>&& e) noexcept {
		return nth<0>(std::move(e));
	}
	
	template<class Tag, class Lhs, class Rhs>
	Rhs&& rhs(expression<Tag, Lhs, Rhs>&& e) noexcept {
		return nth<1>(std::move(e));
	}
} // namespace detail_wrapper_hpp_

namespace extend_wrapper {
	using detail_wrapper_hpp_::lhs;
	using detail_wrapper_hpp_::rhs;
	using detail_wrapper_hpp_::nth;
} // namespace extend

using extend_wrapper::mksymb;

#endif // JORDAN_TRAITS_WRAPPER_HPP_
