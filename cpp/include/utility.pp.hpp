#ifndef MACRO_UTILITY_HPP_
#define MACRO_UTILITY_HPP_

/******************************************************************************/
/* utilities ******************************************************************/
/******************************************************************************/

#	define MACRO_UTILITY_IDENTITY(...) __VA_ARGS__
/** Expects a parenthesized argument x and removes the parentheses.
 */
#	define MACRO_UTILITY_STRIP_PARENTHESES(x)                                  \
	MACRO_UTILITY_IDENTITY x

/* credits goes to Paul Fultz's
 * https://github.com/pfultz2/Cloak/wiki/C-Preprocessor-tricks,-tips,-and-idioms
 */
#	define MACRO_UTILITY_GET_8TH_ARG(x1, x2, x3, x4, x5, x6, x7, x8, ...) x8
#	define MACRO_UTILITY_HAS_TWO_OR_MORE_ARGUMENTS(...)                        \
	MACRO_UTILITY_GET_8TH_ARG(__VA_ARGS__, 1, 1, 1, 1, 1, 1, 0, dummy)

#	define MACRO_UTILITY_EVAL0(...) __VA_ARGS__
#	define MACRO_UTILITY_EVAL1(...)                                            \
	MACRO_UTILITY_EVAL0(MACRO_UTILITY_EVAL0(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL2(...)                                            \
	MACRO_UTILITY_EVAL1(MACRO_UTILITY_EVAL1(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL3(...)                                            \
	MACRO_UTILITY_EVAL2(MACRO_UTILITY_EVAL2(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL4(...)                                            \
	MACRO_UTILITY_EVAL3(MACRO_UTILITY_EVAL3(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL5(...)                                            \
	MACRO_UTILITY_EVAL4(MACRO_UTILITY_EVAL4(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL6(...)                                            \
	MACRO_UTILITY_EVAL5(MACRO_UTILITY_EVAL5(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL7(...)                                            \
	MACRO_UTILITY_EVAL6(MACRO_UTILITY_EVAL6(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL8(...)                                            \
	MACRO_UTILITY_EVAL7(MACRO_UTILITY_EVAL7(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL9(...)                                            \
	MACRO_UTILITY_EVAL8(MACRO_UTILITY_EVAL8(__VA_ARGS__))
#	define MACRO_UTILITY_EVAL(...)                                             \
	MACRO_UTILITY_EVAL9(__VA_ARGS__)

#	define MACRO_UTILITY_EMPTY()
#	define MACRO_UTILITY_DEFER(...) __VA_ARGS__ MACRO_UTILITY_EMPTY()
#	define MACRO_UTILITY_OBSTRUCT(...) \
	__VA_ARGS__ MACRO_UTILITY_DEFER(MACRO_UTILITY_EMPTY)()
#	define MACRO_UTILITY_PRIMITIVE_CAT(x, y) x ## y
#	define MACRO_UTILITY_CAT(x, y) MACRO_UTILITY_PRIMITIVE_CAT(x, y)

#	define MACRO_UTILITY_NOT_1() 0
#	define MACRO_UTILITY_NOT_0() 1
#	define MACRO_UTILITY_NOT(x) MACRO_UTILITY_CAT(MACRO_UTILITY_NOT_, x)()

/******************************************************************************/
/* list mapping ***************************************************************/
/******************************************************************************/

#	define MACRO_UTILITY_MAP_LIST_ACC_1(acc,macro, h, ...)\
	MACRO_UTILITY_OBSTRUCT(MACRO_UTILITY_MAP_LIST_ACC_INDIRECT)()\
		((MACRO_UTILITY_IDENTITY acc macro(h),), macro, __VA_ARGS__)
#	define MACRO_UTILITY_MAP_LIST_ACC_0(acc, macro, h)\
	(MACRO_UTILITY_IDENTITY acc macro(h))

#	define MACRO_UTILITY_MAP_LIST_ACC(acc, macro, ...)\
	MACRO_UTILITY_CAT(MACRO_UTILITY_MAP_LIST_ACC_\
	                 ,MACRO_UTILITY_HAS_TWO_OR_MORE_ARGUMENTS(__VA_ARGS__)     \
	                 )(acc, macro, __VA_ARGS__)
#	define MACRO_UTILITY_MAP_LIST_ACC_INDIRECT()\
	MACRO_UTILITY_DEFER(MACRO_UTILITY_MAP_LIST_ACC)

#	define MACRO_UTILITY_MAP_LIST(macro, ...)                                  \
	MACRO_UTILITY_IDENTITY MACRO_UTILITY_MAP_LIST_ACC((), macro, __VA_ARGS__)
#	define MACRO_UTILITY_MAP_LIST_INDIRECT() \
	MACRO_UTILITY_DEFER(MACRO_UTILITY_MAP_LIST)

/******************************************************************************/
/* list filtering *************************************************************/
/******************************************************************************/

#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX_01(acc, h)\
	(MACRO_UTILITY_IDENTITY acc, h)
#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX_00(acc, h) acc
#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX_0(acc, predicate, h)\
	MACRO_UTILITY_CAT(MACRO_UTILITY_FILTER_LIST_ACC_AUX_0, predicate(h))(acc, h)

#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX_11(acc, predicate, h, ...)\
	MACRO_UTILITY_OBSTRUCT(MACRO_UTILITY_FILTER_LIST_ACC_AUX_INDIRECT)()\
		((MACRO_UTILITY_IDENTITY acc, h ), predicate, __VA_ARGS__)
#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX_10(acc, predicate, h, ...)\
	MACRO_UTILITY_OBSTRUCT(MACRO_UTILITY_FILTER_LIST_ACC_AUX_INDIRECT)()\
		(acc, predicate, __VA_ARGS__)

#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX_1(acc, predicate, h, ...)\
	MACRO_UTILITY_CAT(MACRO_UTILITY_FILTER_LIST_ACC_AUX_1, predicate(h))\
		(acc, predicate, h, __VA_ARGS__)
#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX(acc, predicate, ...)\
	MACRO_UTILITY_CAT(MACRO_UTILITY_FILTER_LIST_ACC_AUX_\
	                 ,MACRO_UTILITY_HAS_TWO_OR_MORE_ARGUMENTS(__VA_ARGS__)\
	                 )(acc, predicate, __VA_ARGS__)

#	define MACRO_UTILITY_FILTER_LIST_ACC_AUX_INDIRECT()\
	MACRO_UTILITY_DEFER(MACRO_UTILITY_FILTER_LIST_ACC_AUX)
#	define MACRO_UTILITY_FILTER_LIST_ACC_01(acc, h)\
	(MACRO_UTILITY_IDENTITY acc h)
#	define MACRO_UTILITY_FILTER_LIST_ACC_00(acc, h) acc
#	define MACRO_UTILITY_FILTER_LIST_ACC_0(acc, predicate, h)\
	MACRO_UTILITY_CAT(MACRO_UTILITY_FILTER_LIST_ACC_0, predicate(h))(acc, h)

#	define MACRO_UTILITY_FILTER_LIST_ACC_11(acc, predicate, h, ...)\
	MACRO_UTILITY_OBSTRUCT(MACRO_UTILITY_FILTER_LIST_ACC_AUX_INDIRECT)()\
		((MACRO_UTILITY_IDENTITY acc h), predicate, __VA_ARGS__)
#	define MACRO_UTILITY_FILTER_LIST_ACC_10(acc, predicate, h, ...)\
	MACRO_UTILITY_OBSTRUCT(MACRO_UTILITY_FILTER_LIST_ACC_INDIRECT)()\
		(acc, predicate, __VA_ARGS__)
#	define MACRO_UTILITY_FILTER_LIST_ACC_1(acc, predicate, h, ...)\
	MACRO_UTILITY_CAT(MACRO_UTILITY_FILTER_LIST_ACC_1, predicate(h))\
		(acc, predicate, h, __VA_ARGS__)

#	define MACRO_UTILITY_FILTER_LIST_ACC(acc, predicate, ...)\
	MACRO_UTILITY_CAT(MACRO_UTILITY_FILTER_LIST_ACC_\
	                 ,MACRO_UTILITY_HAS_TWO_OR_MORE_ARGUMENTS(__VA_ARGS__)\
	                 )(acc, predicate, __VA_ARGS__)
#	define MACRO_UTILITY_FILTER_LIST_ACC_INDIRECT()\
	MACRO_UTILITY_DEFER(MACRO_UTILITY_FILTER_LIST_ACC)

#	define MACRO_UTILITY_FILTER_LIST(predicate, ...)\
	MACRO_UTILITY_IDENTITY MACRO_UTILITY_FILTER_LIST_ACC((), predicate, __VA_ARGS__)
#	define MACRO_UTILITY_FILTER_LIST_INDIRECT()\
	MACRO_UTILITY_DEFER(MACRO_UTILITY_FILTER_LIST)

#else // MACRO_UTILITY_HPP_
// clean defined macros on second inclusion
#	undef MACRO_UTILITY_FILTER_LIST_INDIRECT
#	undef MACRO_UTILITY_FILTER_LIST
#	undef MACRO_UTILITY_FILTER_LIST_ACC_INDIRECT
#	undef MACRO_UTILITY_FILTER_LIST_ACC
#	undef MACRO_UTILITY_FILTER_LIST_ACC_1
#	undef MACRO_UTILITY_FILTER_LIST_ACC_10
#	undef MACRO_UTILITY_FILTER_LIST_ACC_11
#	undef MACRO_UTILITY_FILTER_LIST_ACC_0
#	undef MACRO_UTILITY_FILTER_LIST_ACC_00
#	undef MACRO_UTILITY_FILTER_LIST_ACC_01
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX_INDIRECT
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX_1
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX_10
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX_11
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX_0
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX_00
#	undef MACRO_UTILITY_FILTER_LIST_ACC_AUX_01
#	undef MACRO_UTILITY_MAP_LIST_INDIRECT
#	undef MACRO_UTILITY_MAP_LIST
#	undef MACRO_UTILITY_MAP_LIST_INDIRECT_ACC
#	undef MACRO_UTILITY_MAP_LIST_ACC
#	undef MACRO_UTILITY_MAP_LIST_ACC_0
#	undef MACRO_UTILITY_MAP_LIST_ACC_1
#	undef MACRO_UTILITY_CAT
#	undef MACRO_UTILITY_PRIMITIVE_CAT
#	undef MACRO_UTILITY_OBSTRUCT
#	undef MACRO_UTILITY_DEFER
#	undef MACRO_UTILITY_EMPTY
#	undef MACRO_UTILITY_EVAL
#	undef MACRO_UTILITY_EVAL9
#	undef MACRO_UTILITY_EVAL8
#	undef MACRO_UTILITY_EVAL7
#	undef MACRO_UTILITY_EVAL6
#	undef MACRO_UTILITY_EVAL5
#	undef MACRO_UTILITY_EVAL4
#	undef MACRO_UTILITY_EVAL3
#	undef MACRO_UTILITY_EVAL2
#	undef MACRO_UTILITY_EVAL1
#	undef MACRO_UTILITY_EVAL0
#	undef MACRO_UTILITY_HAS_TWO_OR_MORE_ARGUMENTS
#	undef MACRO_UTILITY_GET_8TH_ARG
#	undef MACRO_UTILITY_STRIP_PARENTHESES
#	undef MACRO_UTILITY_IDENTITY
#undef MACRO_UTILITY_HPP_
#endif // MACRO_UTILITY_HPP_
