#ifndef INTERMEDIATE_MATRIX_DECOMPOSITION_HPP_
#define INTERMEDIATE_MATRIX_DECOMPOSITION_HPP_

#include "types.hpp"
#include <vector>
#include <iostream>

rational_type binomial_coefficient(rational_type k, rational_type n) {
	return k == rational_type(0)
		?rational_type(1)
		:(n/k * binomial_coefficient(k - rational_type(1), n - rational_type(1)));
}

template<class T>
T poly_gcd(const T& x, const T& y) {
	return y.degree() == T::minus_infinite_degree() ? x.monic() : poly_gcd(y, x % y);
}

struct diagonal_block {
	diagonal_block()
	: size()
	, kind(EMPTY)
	, block() {}
	
	diagonal_block(std::size_t n, const rational_polynomial_type& charpoly)
	: size(n)
	, kind(COMPANION_MATRIX)
	, block(charpoly) {}
	
	diagonal_block(std::size_t n, const rational_type& eigenvalue)
	: size(n)
	, kind(JORDAN_BLOCK)
	, block(eigenvalue) {}
	
	diagonal_block(std::size_t n, const interval_type& r, const interval_type& phi)
	: size(n)
	, kind(REAL_JORDAN_BLOCK)
	, block(r, phi) {}
	
	diagonal_block(const diagonal_block& other)
	: size(other.size)
	, kind(other.kind) {
		switch(kind) {
			case COMPANION_MATRIX:
				new (&block.charpoly) rational_polynomial_type(other.block.charpoly);
				break;
			case JORDAN_BLOCK:
				new (&block.eigenvalue) rational_type(other.block.eigenvalue);
				break;
			case REAL_JORDAN_BLOCK:
				new (&block.trigo) decltype(block.trigo)(other.block.trigo);
				break;
			default:
				break;
		}
	}
	
	~diagonal_block() {
		switch(kind) {
			case COMPANION_MATRIX:
				block.charpoly.~rational_polynomial_type();
				break;
			case JORDAN_BLOCK:
				block.eigenvalue.~rational_type();
				break;
			case REAL_JORDAN_BLOCK:
				block.trigo.norm.~interval_type();
				block.trigo.angle.~interval_type();
				break;
			default: break;
		}
	}
	
	std::size_t size;
	enum { JORDAN_BLOCK, REAL_JORDAN_BLOCK, COMPANION_MATRIX, EMPTY } kind;
	union data_type {
		data_type() {}
		data_type(const rational_type& x)
		: eigenvalue(x) {}
		data_type(const interval_type& r, const interval_type& phi)
		: trigo{r, phi} {}
		data_type(const rational_polynomial_type& x)
		: charpoly(x) {}
		~data_type() {}
		
		rational_type eigenvalue;
		rational_polynomial_type charpoly;
		struct {
			interval_type norm;
			interval_type angle;
		} trigo;
	} block;
};

std::ostream& operator<<(std::ostream& o, const diagonal_block& block) {
	switch(block.kind) {
		case diagonal_block::JORDAN_BLOCK:
			return o << "jordan_block(" << block.block.eigenvalue
			         << ", " << block.size << ')';
		case diagonal_block::REAL_JORDAN_BLOCK:
			return o << "real_jordan_block(" << block.block.trigo.norm
			         << ", " << block.block.trigo.angle
			         << "e^{i2π*" << block.size << "})";
		case diagonal_block::COMPANION_MATRIX:
			return o << "companion_matrix((" << block.block.charpoly
			         << "), " << block.size << ")";
		case diagonal_block::EMPTY:
			return o;
		default:
			// unreachable
			return o;
	}
	// unreachable
	return o;
}

/******************************************************************************/

struct intermediate_decomposition_algorithm {
	struct result_type {
		rational_matrix_type transformation;
		rational_matrix_type inverse_transformation;
		std::vector<diagonal_block> diagonal;
	};
	
	intermediate_decomposition_algorithm(const rational_matrix_type& m)
	: original_matrix(m)
	, squarefree_factorization
		  (simple_polynomial_factorization(m.characteristic_polynomial()))
	, matrix_power()
	, nullspace(1)
	, output_column_index()
	, transformation(m.width(), m.width())
	, diagonal()
	, diagonal_transformation() {
		for(auto& factor : squarefree_factorization) {
			if(factor[0] == rational_type(0)) {
				factor = factor / rational_polynomial_type(1);
				break;
			}
			++nullspace;
		}
		nullspace = nullspace==(squarefree_factorization.size()+1)?0:nullspace;
		
		std::size_t max_degree = nullspace > 0 ? 1 : 0;
		for(const auto& factor : squarefree_factorization)
			max_degree = std::max(max_degree, factor.degree());
		matrix_power.reserve(max_degree + 1);
		matrix_power.emplace_back(rational_matrix_type::identity(m.width()));
		for(std::size_t i = 0 ; i < max_degree ; ++i)
			matrix_power.emplace_back(matrix_power.back().multiply(m));
	}
	
	void refactor_and_resume
		( const rational_polynomial_type& squarefree_factor
		, rational_matrix_type&& linearly_dependent_family
		, std::size_t algebraic_multiplicity
		)
	{
		const std::size_t m = linearly_dependent_family.height();
		rational_matrix_type rref_transformation(m, m);
		linearly_dependent_family.inplace_reduced_row_echelon_form(rref_transformation);
		rational_polynomial_type witness(m - 1);
		for(std::size_t i = 0 ; i < m ; ++i) {
			witness[i] = rref_transformation.cat(m - 1, i);
		}
		const auto witness_squarefree_factorization
			(simple_polynomial_factorization(witness));
		rational_polynomial_type last_factor = squarefree_factor;
		for(const auto& w : witness_squarefree_factorization) {
			if(w.degree() == 0) { continue; }
			const rational_polynomial_type squarefree_subfactor =
				poly_gcd(w, last_factor);
			extract_blocks(squarefree_subfactor, algebraic_multiplicity);
			last_factor = (last_factor / squarefree_subfactor).monic();
		}
		if(last_factor.degree() != 0) {
			extract_blocks(last_factor, algebraic_multiplicity);
		}
	}
	
	// this function will be called if the roots of squarefree_factor don't have
	// jordan blocks of the same form
	//
	// such a situation couldn't happen if squarefree_factor was irreducible in
	// Q[X], but as factoring a polynomial is a bit complex, a mere square-free
	// decomposition is performed
	void refactor_and_resume
		( const rational_polynomial_type& squarefree_factor
		, std::size_t algebraic_multiplicity
		, std::size_t expected_multiplicity
		, const rational_matrix_type& left_proj
		, const rational_matrix_type& prev_left_proj
		)
	{
		rational_matrix_type column;
		const std::size_t n = original_matrix.width();
		const std::size_t save_column_index = output_column_index;
		const std::size_t elevated_deg =
			squarefree_factor.degree() * expected_multiplicity;
		const auto prevker = prev_left_proj.kernel();
		while(left_proj.pick_kernel_vector(column, prevker, transformation)) {
			for(std::size_t j = 0 ; j < n ; ++j) {
				transformation.at(j, output_column_index) = column.at(j, 0);
			}
			++output_column_index;
			for(std::size_t i = 1 ; i < elevated_deg ; ++i) {
				column = original_matrix.multiply(column);
				for(std::size_t j = 0 ; j < n ; ++j) {
					transformation.at(j, output_column_index) = column.at(j, 0);
				}
				++output_column_index;
			}
			if(transformation.rank() != output_column_index) {
				const std::size_t offset = output_column_index - elevated_deg;
				rational_matrix_type vector_family =
					transformation.crop(0, offset, n, output_column_index);
				if(vector_family.rank() == elevated_deg) {
					rational_matrix_type previous_combined_family =
						transformation.crop(0, save_column_index, n, offset);
					rational_matrix_type::pick_intersection_vector(column, previous_combined_family, vector_family);
					for(std::size_t col = 0 ; col < elevated_deg ; ++col) {
						for(std::size_t row = 0 ; row < n ; ++row)
							vector_family.at(row, col) =
								column.at(row, 0);
						column = original_matrix.multiply(column);
					}
				}
				vector_family = vector_family.transpose();
				for(std::size_t row = 0 ; row < n ; ++row) {
					for(std::size_t col = save_column_index ; col < output_column_index ; ++col) {
						transformation.at(row, col) = rational_type(0);
					}
				}
				output_column_index = save_column_index;
				return refactor_and_resume
					( squarefree_factor
					, std::move(vector_family)
					, algebraic_multiplicity
					);
			}
		}
		/* if this function was called, then there must have been a discrepancy
		 * that should have been caught
		 */
		assert(false);
	}
	
	void extract_blocks
		(const rational_polynomial_type& squarefree_factor
		,std::size_t algebraic_multiplicity
		) {
		typename module::set_type matrix_module_parameters(original_matrix.width());
		
		const std::size_t n = original_matrix.width();
		
		std::vector<std::size_t> power_rank(algebraic_multiplicity + 2, 0);
		std::vector<std::size_t> nblocks_of_size(algebraic_multiplicity + 2, 0);
		std::vector<rational_matrix_type> left_projectors;
		left_projectors.reserve(algebraic_multiplicity);
		left_projectors.emplace_back(rational_matrix_type::identity(n));
		
		power_rank[0] = n;
		const rational_matrix_type khi_m =
			squarefree_factor.evaluate<module>(matrix_power, matrix_module_parameters);
		left_projectors.emplace_back(khi_m);
		
		power_rank[1] = left_projectors[1].rank();
		std::size_t max_geometric_multiplicity = 1;
		while(power_rank[max_geometric_multiplicity] != power_rank[max_geometric_multiplicity - 1]) {
			left_projectors.emplace_back
				(left_projectors.back().multiply(khi_m));
			left_projectors[max_geometric_multiplicity].inplace_left_projector();
			++max_geometric_multiplicity;
			power_rank[max_geometric_multiplicity] = left_projectors.back().rank();
		}
		--max_geometric_multiplicity;
		assert(max_geometric_multiplicity <= algebraic_multiplicity);
		
		for(std::size_t i = 1 ; i <= max_geometric_multiplicity ; ++i)
			nblocks_of_size[i] =
				(power_rank[i - 1] + power_rank[i + 1]) - (2 * power_rank[i]);
		
		const std::size_t deg = squarefree_factor.degree();
		if(deg == 1) {
			rational_matrix_type column;
			for(std::size_t mu = max_geometric_multiplicity ; mu != 0 ; --mu) {
				if(nblocks_of_size[mu] == 0) continue;
				while(left_projectors[mu].pick_kernel_vector
					      (column
					      ,left_projectors[mu - 1].kernel()
					      ,transformation
					      )
				     ) {
					output_column_index += mu;
					for(std::size_t i = 0 ; i < mu ; ++i) {
						--output_column_index;
						for(std::size_t j = 0 ; j < n ; ++j)
							transformation.at(j, output_column_index) =
								column.at(j, 0);
						column = khi_m.multiply(column);
					}
					add_jordan_block(mu, -squarefree_factor[0]);
					output_column_index += mu;
				}
			}
		} else {
			for(std::size_t mu = 1 ; mu <= max_geometric_multiplicity ; ++mu) {
				/* early check, not necessary, but can catch some problems
				 * and start to refactor before beginning to compute basis
				 * vectors that would need to be thrown away
				 */
				if(nblocks_of_size[mu] % deg != 0) {
					return refactor_and_resume
						(squarefree_factor
						,algebraic_multiplicity
						,mu
						,left_projectors[mu]
						,left_projectors[mu - 1]
						);
				}
				else nblocks_of_size[mu] /= deg;
			}
			const std::size_t save_column_index = output_column_index;
			const std::size_t save_diagonal_size = diagonal.size();
			rational_matrix_type column;
			for(std::size_t mu = 1 ; mu <= max_geometric_multiplicity ; ++mu) {
				if(nblocks_of_size[mu] == 0) continue;
				while(left_projectors[mu].pick_kernel_vector
					      (column
					      ,left_projectors[mu-1].kernel()
					      ,transformation
					      )
				     ) {
					add_companion_block(mu, squarefree_factor);
					const std::size_t elevated_deg = mu * deg;
					for(std::size_t i = 0 ; i < elevated_deg ; ++i) {
						for(std::size_t j = 0 ; j < n ; ++j)
							transformation.at(j, output_column_index) =
								column.at(j, 0);
						column = original_matrix.multiply(column);
						++output_column_index;
					}
					if(transformation.rank() != output_column_index) {
						const std::size_t offset = output_column_index - elevated_deg;
						rational_matrix_type vector_family =
							transformation.crop(0, offset, n, output_column_index);
						if(vector_family.rank() == elevated_deg) {
							rational_matrix_type previous_combined_family =
								transformation.crop(0, save_column_index, n, offset);
							rational_matrix_type::pick_intersection_vector(column, previous_combined_family, vector_family);
							for(std::size_t col = 0 ; col < elevated_deg ; ++col) {
								for(std::size_t row = 0 ; row < n ; ++row)
									vector_family.at(row, col) =
										column.at(row, 0);
								column = original_matrix.multiply(column);
							}
						}
						diagonal.resize(save_diagonal_size);
						for(std::size_t i = 0 ; i < n ; ++i) {
							for(std::size_t j = save_column_index ; j < output_column_index ; ++j) {
								transformation.at(i, j) = rational_type(0);
								diagonal_transformation.at(i, j) = rational_type(0);
							}
						}
						output_column_index = save_column_index;
						vector_family = vector_family.transpose();
						refactor_and_resume
							(squarefree_factor
							,std::move(vector_family)
							,algebraic_multiplicity
							);
						return;
					}
				}
			}
		}
	}
	
	void add_jordan_block
		(std::size_t multiplicity, const rational_type& eigenvalue) {
		diagonal.emplace_back(multiplicity, eigenvalue);
		const std::size_t n = multiplicity + output_column_index;
		for(std::size_t row = output_column_index ; row < n ; ++row)
			for(std::size_t col = output_column_index ; col < n ; ++col)
				diagonal_transformation.at(row, col) =
					rational_type(row == col);
	}
	
	void add_companion_block
		(std::size_t multiplicity
		,const rational_polynomial_type& squarefree_characteristic
		) {
		diagonal.emplace_back(multiplicity, squarefree_characteristic);
		const std::size_t degree = squarefree_characteristic.degree();
		const std::size_t n = degree * multiplicity;
		rational_polynomial_type remainder(0);
		for(std::size_t col = 0 ; col < n ; ++col) {
			remainder = remainder % squarefree_characteristic;
			const std::size_t local_degree = remainder.degree();
			for(std::size_t blockrow = 0
			   ;blockrow < multiplicity && blockrow < n - col
			   ;++blockrow
			   ) {
				const rational_type alpha =
					binomial_coefficient
						(rational_type(blockrow)
						,rational_type(col + blockrow)
						);
				const std::size_t row_offset =
					output_column_index + (multiplicity - blockrow - 1)*degree;
				const std::size_t real_col =
					output_column_index + col + blockrow;
				for(std::size_t row = 0 ; row <= local_degree ; ++row) {
					diagonal_transformation.at(row_offset + row, real_col) =
						alpha * remainder[row];
				}
				for(std::size_t row = local_degree + 1 ; row < degree ; ++row)
					diagonal_transformation.at
						(row_offset + row, real_col) = rational_type(0);
			}
			remainder = remainder * rational_polynomial_type(1);
		}
	}
	
	result_type operator()() {
		const std::size_t n = original_matrix.width();
		diagonal.reserve(n);
		transformation = rational_matrix_type(n, n);
		diagonal_transformation = rational_matrix_type(n, n, rational_type(0));
		output_column_index = 0;
		std::size_t algebraic_multiplicity = 0;
		if(nullspace > 0) {
			extract_blocks(rational_polynomial_type(1), nullspace);
		}
		for(const auto& squarefree_factor : squarefree_factorization) {
			extract_blocks(squarefree_factor, ++algebraic_multiplicity);
		}
		
		rational_matrix_type inverse_transformation;
		diagonal_transformation.reduced_row_echelon_form(inverse_transformation);
		transformation = transformation.multiply(inverse_transformation);
		transformation.reduced_row_echelon_form(inverse_transformation);
		
		return {std::move(transformation)
		       ,std::move(inverse_transformation)
		       ,std::move(diagonal)
		       };
	}
	
private:
	const rational_matrix_type&           original_matrix;
	std::vector<rational_polynomial_type> squarefree_factorization;
	std::vector<rational_matrix_type>     matrix_power;
	std::size_t                           nullspace;
	
	std::size_t output_column_index;
	rational_matrix_type transformation;
	std::vector<diagonal_block> diagonal;
	rational_matrix_type diagonal_transformation;
};

auto intermediate_decomposition(const rational_matrix_type& m) ->
decltype(intermediate_decomposition_algorithm(m)()) {
	return intermediate_decomposition_algorithm(m)();
}

#endif // INTERMEDIATE_MATRIX_DECOMPOSITION_HPP_
