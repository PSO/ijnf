#ifndef JORDAN_LAGUERRE_HPP_
#define JORDAN_LAGUERRE_HPP_

#include "types.hpp"

struct convergence_error : std::runtime_error {
	convergence_error()
	: std::runtime_error("the root finding algorithm couldn't converge") {};
};

struct laguerre_root_finder {
	static constexpr int max_trials = 100;
	static constexpr int max_iter   = 100;
	static constexpr real_type epsilon = 1e-20; //!
	
	laguerre_root_finder(const complex_polynomial_type& f_arg)
	: f(f_arg) {}
	
	struct iterator {
		using value_type = complex_type;
		using pointer    = const value_type*;
		using reference  = const value_type&;
		using iterator_category = std::input_iterator_tag;
		
		explicit iterator()
		: f()
		, root() {}
		
		iterator& operator++() {
			using std::norm;
			using std::sqrt;
			auto deg = f.degree();
			if(deg == 0) return (*this = iterator());
			if(deg == 1) {
				root = -f[0] / f[1];
				f    = complex_polynomial_type({1});
				return *this;
			}
			// should also handle quadratic factors separately
			if(deg == complex_polynomial_type::minus_infinite_degree()) {
				f = complex_polynomial_type({1});
			}
			
			int trial = 0;
			value_type starting_x(1);
			value_type x;
			value_type y;
			value_type n = complex_type(deg);
			complex_polynomial_type fdiff  = f.formal_derivative();
			complex_polynomial_type fdiff2 = fdiff.formal_derivative();
			do {
				if(trial >= max_trials) throw convergence_error();
				x = starting_x;
				starting_x *= complex_type(1, 0.1);
				++trial;
				y = f.evaluate(x);
				for( int i = 0
				   ; i < max_iter
				     && norm(y) > epsilon
				   ; ++i
				   ) {
					value_type g  = fdiff.evaluate(x) / y;
					value_type g2 = g * g;
					value_type h = g2 - (fdiff.evaluate(x) / y);
					value_type delta = sqrt((n - complex_type(1)) * (n * h - g));
					value_type denom[2] = {g + delta, g - delta};
					value_type real_denom =
						norm(denom[0]) > norm(denom[1])
							? std::move(denom[0])
							: std::move(denom[1]);
					x = std::move(x) - (std::move(n) / std::move(real_denom));
					y = f.evaluate(x);
				}
			} while(!(norm(y) <= epsilon));
			
			root   = std::move(x);
			f      = f / complex_polynomial_type{-root, complex_type(1)};
			return *this;
			
		}
		
		bool operator==(const iterator& other) const noexcept {
			return f.degree() == other.f.degree();
		}
		
		bool operator!=(const iterator& other) const noexcept {
			return !(*this == other);
		}
		
		iterator operator++(int) {
			iterator tmp(*this);
			++(*this);
			return tmp;
		}
		value_type operator*() const noexcept {
			return root;
		}
		
	private:
		friend laguerre_root_finder;
		iterator(complex_polynomial_type f_arg)
		: f(f_arg)
		, root(1) {}
		
		complex_polynomial_type f;
		complex_type root;
	};
	
	iterator begin() const { iterator it(f); ++it; return it; }
	iterator end() const { return iterator(); }
	
private:
	complex_polynomial_type f;
};

#endif // JORDAN_LAGUERRE_HPP_
