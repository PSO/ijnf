light metaprogramming library (lmpl).

This header-only library gather a lot of rather small metaprogramming utilities
that I tend to use regularly. I thought I'd finally make a library out of those
rather than always rewrite them. It requires a c++11-compliant compiler.

The lmpl shares some similarities with the much more expertly written tiny
metaprogramming library of the much more experienced Eric Niebler as well as
Louis Dionne and Peter Dimov's work (from which it is heavily inspired).

Boost.Hana might be a more mature and certainly more reliable alternative,
unfortunately it requires c++14. While c++14 is supported by recent versions
of most compilers, said versions aren't necessarily available easily on some
operating systems. As Boost.Hana's style is fairly different from the usual
c++11 metaprogramming techniques (in particular, Boost.Hana makes heavy use
of automatic inference of return types), translating code using it to something
more manageable for old compilers would be extremely tedious at best.

Requires boost's unit test framework to compile the tests (you may specify your
boost installation path by passing
	-DBOOST_ROOT=[absolute path to your boost installation]
to cmake)
