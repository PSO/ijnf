#ifndef LMPL_APPLY_HPP_
#define LMPL_APPLY_HPP_

#include "get.hpp"
#include "list.hpp"

namespace lmpl {
	template<class... T>
	struct apply {
		template<class Wrapper>
		struct invoke : Wrapper::template invoke<T...> {};
	};
	
	namespace detail_apply_hpp_ {
		template<class IndexList, class Functor, class ArgumentList>
		struct invoker {};
		
		template
			<template<class...> class IndexHolder
			,class... Indices
			,class Functor
			,template<class...> class ArgumentHolder
			,class... Arguments
			>
		struct invoker
			<IndexHolder<Indices...>, Functor, ArgumentHolder<Arguments...>> {
#		define LMPL_APPLY_HPP_local_function_content()                         \
			std::forward<Functor>(fct)                                         \
				(select<Indices>(std::forward(args)...)...)
			static auto invoke(Functor&& fct, Arguments&&... args)
			noexcept(noexcept(LMPL_APPLY_HPP_local_function_content())) ->
			decltype(LMPL_APPLY_HPP_local_function_content()) {
				return LMPL_APPLY_HPP_local_function_content();
			}
#		undef LMPL_APPLY_HPP_local_function_content
		};
	} // namespace detail_apply_hpp_
#define LMPL_APPLY_HPP_local_function_content()                                \
	detail_apply_hpp_::invoker<IndexList, Functor, list<Args...>>              \
		::invoke(std::forward<Functor>(fct), std::forward<Args>(args)... )
	template<class IndexList, class Functor, class... Args>
	auto invoke_with(Functor&& fct, Args&&... args)
	noexcept(noexcept(LMPL_APPLY_HPP_local_function_content())) ->
	decltype(LMPL_APPLY_HPP_local_function_content()) {
		return LMPL_APPLY_HPP_local_function_content();
	}
#undef LMPL_APPLY_HPP_local_function_content
} // namespace lmpl

#endif // LMPL_APPLY_HPP_
