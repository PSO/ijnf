#ifndef LMPL_CARTESIAN_PRODUCT_HPP_
#define LMPL_CARTESIAN_PRODUCT_HPP_

#include "map.hpp"
#include "push_front.hpp"
#include "repack.hpp"
#include "list.hpp"
#include "constant.hpp"
#include "size.hpp"
#include <type_traits>

namespace lmpl {
	namespace detail_cartesian_product_hpp_ {
		template<class... T>
		struct cartesian_product {};
	
		template<template<class...> class HeadHolder, class... HeadContent>
		struct cartesian_product<HeadHolder<HeadContent...>>
		: lmpl::type_constant<lmpl::list<HeadHolder<HeadContent>...>> {};
	
		template
			<template<class...> class HeadHolder
			,class... HeadContent
			,class... Tail
			>
		struct cartesian_product<HeadHolder<HeadContent...>, Tail...> {
			struct binary_cartesian_product {
				template<class L>
				struct invoke
					: lmpl::type_constant
						  <lmpl::list<lmpl::push_front_t<L, HeadContent>...>> {};
			};
		
			using unflattened =
				lmpl::map_t
					<typename cartesian_product<Tail...>::type
					,binary_cartesian_product
					>;
			
			using type =
				typename lmpl::repack_t
					<typename std::conditional
						 <lmpl::empty<unflattened>::value
						 ,lmpl::list<lmpl::empty_list>
						 ,unflattened
						 >::type
					,lmpl::pack_template<lmpl::cat>
					>::type;
		};
	} // namespace detail_cartesian_product_hpp_
	
	template<class... T>
	struct cartesian_product {};
	
	template<template<class...> class Holder, class... Content, class... Tail>
	struct cartesian_product<Holder<Content...>, Tail...>
	: type_constant
		  <typename detail_cartesian_product_hpp_::cartesian_product
			   <Holder<Content...>, Tail...>::type
		  > {};
	
	template<class... T>
	using cartesian_product_t = typename cartesian_product<T...>::type;
} // namespace lmpl

#endif // LMPL_CARTESIAN_PRODUCT_HPP_
