#ifndef LMPL_CAT_HPP_
#define LMPL_CAT_HPP_

#include "constant.hpp"

namespace lmpl {
	template<class... Lists>
	struct cat {};
	
	template<template<class...> class Holder, class... Content>
	struct cat<Holder<Content...>> : type_constant<Holder<Content...>> {};
	
	template
		<template<class...> class Holder
		,class... Content1
		,class... Content2
		,class... Lists
		>
	struct cat<Holder<Content1...>, Holder<Content2...>, Lists...>
	: cat<Holder<Content1..., Content2...>, Lists...> {};
	
	template
		<template<class...> class Holder
		,class... Content1
		,class... Content2
		,class... Content3
		,class... Content4
		,class... Content5
		,class... Content6
		,class... Content7
		,class... Content8
		,class... Content9
		,class... Content10
		,class... Content11
		,class... Content12
		,class... Content13
		,class... Content14
		,class... Content15
		,class... Content16
		,class... Lists
		>
	struct cat
		<Holder<Content1...>
		,Holder<Content2...>
		,Holder<Content3...>
		,Holder<Content4...>
		,Holder<Content5...>
		,Holder<Content6...>
		,Holder<Content7...>
		,Holder<Content8...>
		,Holder<Content9...>
		,Holder<Content10...>
		,Holder<Content11...>
		,Holder<Content12...>
		,Holder<Content13...>
		,Holder<Content14...>
		,Holder<Content15...>
		,Holder<Content16...>
		,Lists...
		>
	: cat
		<Holder
			<Content1...
			,Content2...
			,Content3...
			,Content4...
			,Content5...
			,Content6...
			,Content7...
			,Content8...
			,Content9...
			,Content10...
			,Content11...
			,Content12...
			,Content13...
			,Content14...
			,Content15...
			,Content16...
			>
		,Lists...
	> {};
	
	template<class... Lists>
	using cat_t = typename cat<Lists...>::type;
} // namespace lmpl

#endif // LMPL_CAT_HPP_
