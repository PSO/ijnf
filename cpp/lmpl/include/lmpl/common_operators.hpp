#ifndef LMPL_COMMON_OPERATORS_HPP_
#define LMPL_COMMON_OPERATORS_HPP_

#include "voider.hpp"
#include <type_traits>

namespace lmpl {
	template<class A, class = void>
	struct boolean_not {};
	
	template<class A>
	struct boolean_not<A, void_t<decltype(!A::value)>>
	: std::integral_constant<bool, !A::value> {};
	
	
	template<class A, class B, class = void>
	struct boolean_or {};
	
	template<class A, class B>
	struct boolean_or<A, B, void_t<decltype(A::value || B::value)>>
	: std::integral_constant<bool, A::value || B::value> {};
	
	template<class A, class B, class = void>
	struct boolean_and {};
	
	template<class A, class B>
	struct boolean_and<A, B, void_t<decltype(A::value && B::value)>>
	: std::integral_constant<bool, A::value && B::value> {};
	
	template<class T, class = void>
	struct increment {};
	
	template<class T>
	struct increment
		<T
		,typename std::enable_if
			<std::is_integral<decltype(T::value)>::value>::type
		>
	: std::integral_constant
		<decltype(T::value), decltype(T::value)(T::value + 1)> {};
} // namespace lmpl

#endif // LMPL_COMMON_OPERATORS_HPP_
