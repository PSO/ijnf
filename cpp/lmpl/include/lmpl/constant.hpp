#ifndef LMPL_CONSTANT_HPP_
#define LMPL_CONSTANT_HPP_

namespace lmpl {
	template<class T>
	struct type_constant {
		using type = T;
		template<class...>
		using invoke = type_constant<T>;
	};
} // namespace lmpl

#endif // LMPL_CONSTANT_HPP_
