#ifndef LMPL_DROP_BACK_HPP_
#define LMPL_DROP_BACK_HPP_

#include "reduce.hpp"
#include "map.hpp"
#include "zip.hpp"

#include <type_traits>

namespace lmpl {
	namespace detail_drop_back_hpp_ {
		template<class MaxIdx>
		struct discard_beyond {
			template<class T>
			struct invoke {};
		};
		
		template<class MaxIdx>
		template<class T, class Idx>
		struct discard_beyond<MaxIdx>::invoke<list<T, Idx>>
		: std::conditional<(Idx::value < MaxIdx::value), list<T>, list<>> {};
		
		template<class List, class N, class = void>
		struct drop_back_dispatch {};
		
		template<template<class...> class Holder, class... Content, class N>
		struct drop_back_dispatch
			<Holder<Content...>
			,N
			,typename std::enable_if
				<(N::value >= 0) && (N::value <= sizeof...(Content))>::type
			>
		: reduce
			<map_t
				<number_list_t<Holder<Content...>>
				,discard_beyond
					<std::integral_constant
						<decltype(N::value)
						,decltype(N::value)(sizeof...(Content) - N::value)
						>
					>
				>
			,pack_template<cat_t>
			> {};
	} // namespace detail_drop_back_hpp_
	
	template<class List, class N>
	struct drop_back
	: detail_drop_back_hpp_::drop_back_dispatch<List, N> {};
	
	template<class List, class N>
	using drop_back_t = typename drop_back<List, N>::type;
} // namespace lmpl

#endif // LMPL_DROP_BACK_HPP_
