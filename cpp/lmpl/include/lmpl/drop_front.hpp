#ifndef LMPL_DROP_FRONT_HPP_
#define LMPL_DROP_FRONT_HPP_

#include "repeat.hpp"
#include "voider.hpp"
#include <type_traits>

namespace lmpl {
	template<class List>
	struct pop_front {};
	
	template<template<class...> class Holder, class Head, class... Tail>
	struct pop_front<Holder<Head, Tail...>>
	: type_constant<Holder<Tail...>> {};
	
	template<class List>
	using pop_front_t = typename pop_front<List>::type;
	
	namespace detail_drop_front_hpp_ {
		template<class>
		struct dummy {};
		
		template<class NSizedVoidPtrList, template<class...> class Holder>
		struct drop_front {};
		
		template
			<template<class...> class NSizedVoidPtrListHolder
			,class... Content
			,template<class...> class Holder
			>
		struct drop_front<NSizedVoidPtrListHolder<Content...>, Holder> {
			template<class... Tail>
			static Holder<Tail...> apply(Content..., dummy<Tail>*...);
		};
		
		template<class List, class N, class = void>
		struct drop_front_dispatch {};
		
		template
			<template<class...> class ListHolder
			,class... ListContent
			,class N
			>
		struct drop_front_dispatch
			<ListHolder<ListContent...>, N, void_t<decltype(N::value)>>
		: type_constant
			<decltype
				(drop_front<repeat_t<void*, N>, ListHolder>::apply
					(std::declval<dummy<ListContent>*>()...))> {};
	} // namespace detail_drop_front_hpp_
	
	template<class List, class N>
	struct drop_front : detail_drop_front_hpp_::drop_front_dispatch<List, N> {};
	
	template<class List, class N>
	using drop_front_t = typename drop_front<List, N>::type;
} // namespace lmpl

#endif // LMPL_DROP_FRONT_HPP_
