#ifndef LMPL_FILTER_HPP_
#define LMPL_FILTER_HPP_

#include "voider.hpp"
#include "reduce.hpp"
#include "map.hpp"
#include "repack.hpp"
#include "cat.hpp"
#include "list.hpp"
#include "constant.hpp"

#include <type_traits>

namespace lmpl {
	namespace detail_filter_hpp_ {
		template<class List, class Predicate, bool Out, class = void>
		struct filter_dispatch {};
		
		template<class Predicate, bool Out>
		struct filter_transformer {
			template<class T>
			struct invoke
			: std::conditional
				  <Predicate::template invoke<T>::type::value == Out
				  ,list<T>
				  ,empty_list
				  > {};
		};
		
		template<template<class...> class Holder, class Predicate, bool Out>
		struct filter_dispatch
			<Holder<>
			,Predicate
			,Out
			,template_void_t<Predicate::template invoke>
			>
		: type_constant<Holder<>> {};
		
		template
			<template<class...> class Holder
			,class... Content
			,class Predicate
			,bool Out
			>
		struct filter_dispatch
			<Holder<Content...>
			,Predicate
			,Out
			,template_void_t<Predicate::template invoke>
			>
		: repack
			<reduce_t
				<map_t
					<list<Content...>
					,filter_transformer<Predicate, Out>
					>
				,metafunction<cat>
				>
			,pack_template<Holder>
			>{};
	} // namespace detail_filter_hpp_
	
	template<class List, class Predicate>
	struct filter
	: detail_filter_hpp_::filter_dispatch<List, Predicate, true> {};
	
	template<class List, class Predicate>
	struct filter_out
	: detail_filter_hpp_::filter_dispatch<List, Predicate, false> {};
	
	template<class List, class Predicate>
	using filter_t = typename filter<List, Predicate>::type;
	
	template<class List, class Predicate>
	using filter_out_t = typename filter_out<List, Predicate>::type;
} // namespace lmpl

#endif // LMPL_FILTER_HPP_
