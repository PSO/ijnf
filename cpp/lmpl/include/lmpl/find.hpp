#ifndef LMPL_FIND_HPP_
#define LMPL_FIND_HPP_

#include "constant.hpp"
#include "voider.hpp"

#include <type_traits>

namespace lmpl {
	namespace detail_find_hpp_ {
		template<class Key, class Value>
		struct key_value_pair {};
		
		template<class... T>
		struct helper : T... {};
		
		template<class Key, class Value>
		Value find_helper
			      (const key_value_pair
				      <typename type_constant<Key>::type, Value>&);
		
		template<class Dictionary, class Key, class = void>
		struct find {};
	
		template<class Dictionary, class Key>
		struct find
			<Dictionary
			,Key
			,void_t<decltype(find_helper<Key>(std::declval<Dictionary>()))>
			>
		: type_constant<decltype(find_helper<Key>(std::declval<Dictionary>()))>
		{};
	} // namespace detail_find_hpp_
	
	template<class Dictionary, class Key>
	struct find {};
	
	template
		<template<class...> class DictionaryHolder
		,template<class...> class... Holder
		,class... OriginalKeys
		,class... OriginalValues
		,class Key
		>
	struct find<DictionaryHolder<Holder<OriginalKeys, OriginalValues>...>, Key>
	: detail_find_hpp_::find
		<detail_find_hpp_::helper
			<detail_find_hpp_::key_value_pair<OriginalKeys, OriginalValues>...>
		,Key
		> {};
	
	template<class Dictionary, class Key>
	using find_t = typename find<Dictionary, Key>::type;
	
} // namespace lmpl

#endif // LMPL_FIND_HPP_
