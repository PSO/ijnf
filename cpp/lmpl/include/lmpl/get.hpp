#ifndef LMPL_GET_HPP_
#define LMPL_GET_HPP_

#include "drop_front.hpp"
#include "constant.hpp"

#include <type_traits>
#include <utility>

namespace lmpl {
	template<class List>
	struct front {};
	
	template<template<class...> class ListHolder, class Head, class... Tail>
	struct front<ListHolder<Head, Tail...>>
	: type_constant<Head> {};
	
	template<class List>
	using front_t = typename front<List>::type;
	
	namespace detail_get_hpp_ {
		template<class List, class Index, class = void>
		struct get_dispatch {};
		
		template
			<template<class...> class ListHolder
			,class... ListContent
			,class Index
			>
		struct get_dispatch
			<ListHolder<ListContent...>
			,Index
			,typename std::enable_if
				<   (Index::value >= 0)
				 && (Index::value < sizeof...(ListContent))
				>::type
			>
		: front<drop_front_t<ListHolder<ListContent...>, Index>> {};
		
		template<std::size_t Index, class = void>
		struct select {
			template<class Head, class... Tail>
			static auto invoke(Head&& /* ignored */, Tail&&... tl) noexcept ->
			decltype(select<std::size_t(Index - 1)>
				        ::invoke(std::forward<Tail>(tl)...)) {
				return select<std::size_t(Index - 1)>
					       ::invoke(std::forward<Tail>(tl)...);
			}
		};
		
		template<std::size_t Index>
		struct select<Index, typename std::enable_if<Index >= 16>::type> {
			template
				<class H01, class H02, class H03, class H04
				,class H05, class H06, class H07, class H08
				,class H09, class H10, class H11, class H12
				,class H13, class H14, class H15, class H16
				,class... Tail
				>
			static auto invoke
				(H01&&, H02&&, H03&&, H04&& /* ignored */
				,H05&&, H06&&, H07&&, H08&& /* ignored */
				,H09&&, H10&&, H11&&, H12&& /* ignored */
				,H13&&, H14&&, H15&&, H16&& /* ignored */
				,Tail&&... tl) noexcept ->
			decltype(select<std::size_t(Index - 16)>
				        ::invoke(std::forward<Tail>(tl)...)) {
				return select<std::size_t(Index - 16)>
					       ::invoke(std::forward<Tail>(tl)...);
			}
		};
		
		template<>
		struct select<0, void> {
			template<class Head, class... Tail>
			static Head&& invoke(Head&& h, Tail&&... /* ignored */) noexcept {
				return std::forward<Head>(h);
			}
		};
	} // namespace detail_get_hpp_
	
	template<class List, class Index>
	struct get : detail_get_hpp_::get_dispatch<List, Index> {};
	
	template<class List, class Index>
	using get_t = typename get<List, Index>::type;
	
	template<class Index, class... Args>
	auto select(Args&&... args) noexcept ->
	decltype(detail_get_hpp_::select<Index::value>
		        ::invoke(std::forward<Args>(args)...)) {
		return detail_get_hpp_::select<Index::value>
			       ::invoke(std::forward<Args>(args)...);
	}
} // namespace lmpl

#endif // LMPL_GET_HPP_
