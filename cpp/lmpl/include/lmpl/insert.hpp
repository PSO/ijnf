#ifndef LMPL_INSERT_HPP_
#define LMPL_INSERT_HPP_

#include "constant.hpp"
#include "voider.hpp"
#include "find.hpp"

namespace lmpl {
	namespace detail_insert_hpp_ {
		template<class Dictionary, class Key, class Value, class = void>
		struct insert {};
	
		template
			<template<class...> DictionaryHolder
			,template<class...> class... Holder
			,class... OriginalKeys
			,class... OriginalValues
			,class Key
			,class Value
			>
		struct insert
			<DictionaryHolder
				<Holder<OriginalKeys, OriginalValues>...>
			,Key
			,Value
			,void
			>
		: type_constant
			<DictionaryHolder
				<Holder<OriginalKeys, OriginalValues>...
				,key_value_pair<Key, Value>
				>
			> {};
	
		template
			<template<class...> DictionaryHolder
			,template<class...> class... Holder
			,class... OriginalKeys
			,class... OriginalValues
			,class Key
			,class Value
			>
		struct insert
			<DictionaryHolder<Holder<OriginalKeys, OriginalValues>...>
			,Key
			,Value
			,void_t
				<find_t
					<DictionaryHolder<Holder<OriginalKeys, OriginalValues>...>
					,Key
					>
				>
			>
		: type_constant
			<DictionaryHolder<Holder<OriginalKeys, OriginalValues>...>> {};
	} // namespace detail_insert_hpp_
	
	template<class Dictionary, class Key, class Value>
	struct insert : detail_insert_hpp_::insert<Dictionary, Key, Value> {};
	
	template<class Dictionary, class Key, class Value>
	using insert_t = typename insert<Dictionary, Key, Value>::type
} // namespace lmpl

#endif // LMPL_INSERT_HPP_
