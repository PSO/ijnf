#ifndef LMPL_LAMBDA_HPP_
#define LMPL_LAMBDA_HPP_

#include "repack.hpp"
#include "map.hpp"
#include "zip.hpp"
#include "voider.hpp"
#include "list.hpp"
#include "constant.hpp"
#include "get.hpp"
#include "pop_back.hpp"

#include <type_traits>
#include <cstddef>

namespace lmpl {
	template<std::size_t N>
	struct placeholder : std::integral_constant<std::size_t, N> {};
	
	using _v0 = placeholder<0>;
	using _v1 = placeholder<1>;
	using _v2 = placeholder<2>;
	using _v3 = placeholder<3>;
	using _v4 = placeholder<4>;
	using _v5 = placeholder<5>;
	using _v6 = placeholder<6>;
	using _v7 = placeholder<7>;
	using _v8 = placeholder<8>;
	using _v9 = placeholder<9>;
	
	template<class T>
	struct nested_type {};
	
	namespace detail_lambda_hpp_ {
		template<class T, class = void>
		struct unpack : type_constant<T> {};
		
		template<class T>
		struct unpack
			<nested_type<T>, void_t<typename unpack<T>::type::type>>
		: type_constant<typename unpack<T>::type::type> {};
		
		template
			<template<class...> class Metafunction, class ArgList, class = void>
		struct defer {};
		
		template
			<template<class...> class Metafunction
			,template<class...> class Holder
			,class... Args
			>
		struct defer
			<Metafunction
			,Holder<Args...>
			,void_t<decltype(sizeof(Metafunction<typename unpack<Args>::type...>))>
			>
		: type_constant<Metafunction<typename unpack<Args>::type...>> {};
		
		template<class T, class ReplaceList>
		struct replace_recursively : type_constant<T> {};
		
		template
			<std::size_t N
			,template<class...> class Holder
			,class T
			,class... Tail
			>
		struct replace_recursively
			<placeholder<N>, Holder<list<T, placeholder<N>>, Tail...>>
		: unpack<T> {};
		
		template
			<std::size_t N
			,template<class...> class Holder
			,class T
			,class Placeholder
			,class... Tail
			>
		struct replace_recursively
			<placeholder<N>, Holder<list<T, Placeholder>, Tail...>>
		: replace_recursively<placeholder<N>, Holder<Tail...>> {};
		
		template<class T, class ReplaceList>
		struct replace_recursively<nested_type<T>, ReplaceList>
		: unpack
			<nested_type
				<typename replace_recursively<T, ReplaceList>::type>
			> {};
		
		template
			<template<class...> class Deferred
			,class ArgL
			,class ReplaceList
			>
		struct replace_recursively
			<defer<Deferred, ArgL>, ReplaceList>
		: type_constant
			<defer
				<Deferred
				,typename unpack
					<typename replace_recursively<ArgL, ReplaceList>::type
					>::type
				>
			> {};
		
		template
			<template<class...> class ClosedTemplate
			,class... OriginalArgs
			,class ReplaceList
			>
		struct replace_recursively<ClosedTemplate<OriginalArgs...>, ReplaceList>
		: unpack
			<ClosedTemplate
				<typename replace_recursively
					<OriginalArgs, ReplaceList>::type...
				>
			> {};
		
		template<class ClosedTemplate, class VariableList>
		struct lambda {};
		
		template
			<class ClosedTemplate
			,template<class...> class Holder
			,class... Placeholders
			>
		struct lambda<ClosedTemplate, Holder<Placeholders...>> {
			template<class... Args>
			using invoke =
				typename replace_recursively
					<ClosedTemplate
					,zip_t
						<cat_t
							<list<Args...>
							,drop_front_t
								<list<Placeholders...>
								,std::integral_constant
									<std::size_t, sizeof...(Args)>
								>
							>
						,list<Placeholders...>
						>
					>::type;
		};
	} // namespace detail_lambda_hpp_
	
	template<class... T>
	struct lambda
	: detail_lambda_hpp_::lambda
		<get_t
			<list<T...>
			,std::integral_constant<std::size_t, sizeof...(T) - 1>
			>
		,pop_back_t<list<T...>>
		> {};
	
	template<template<class...> class Metafunction, class... Args>
	using defer =
		nested_type<detail_lambda_hpp_::defer<Metafunction, list<Args...>>>;
	
	template<template<class...> class Metafunction, class... Args>
	using defer_t = typename defer<Metafunction, Args...>::type;
	
} // namespace lmpl

#endif // LMPL_LAMBDA_HPP_
