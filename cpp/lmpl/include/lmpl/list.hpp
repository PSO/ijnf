#ifndef LMPL_LIST_HPP_
#define LMPL_LIST_HPP_

namespace lmpl {
	template<class...>
	struct list {};
	
	using empty_list = list<>;
} // namespace lmpl

#endif // LMPL_LIST_HPP_
