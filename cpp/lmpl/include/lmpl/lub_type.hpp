#ifndef LMPL_LUB_TYPE_HPP_
#define LMPL_LUB_TYPE_HPP_

#include "constant.hpp"
#include "voider.hpp"
#include "list.hpp"
#include "reduce.hpp"

namespace lmpl {
	// special type acting as "any-type"
	// useful for ignoring the return type of always-throwing functions in
	// common return type computations
	struct top_t {
		template<class T>
		explicit operator T() noexcept /* undefined*/;
	};
	
	namespace detail_lub_type_hpp_ {
		template<class T>
		T declprval() /* undefined */;
		
		template<class T, class U, class = void>
		struct lub_type_aux : type_constant<void> {};
		
		template<class T>
		struct lub_type_aux<T, top_t> : lub_type_aux<T, T> {};
		
		template<class T>
		struct lub_type_aux<top_t, T> : lub_type_aux<T, T> {};
		
		template<>
		struct lub_type_aux<top_t, top_t> : type_constant<top_t> {};
		
		template<class T, class U>
		struct lub_type_aux
			<T
			,U
			,void_t<decltype(declprval<bool>()?declprval<T>():declprval<U>())>
			>
		: type_constant
			<decltype(declprval<bool>()?declprval<T>():declprval<U>())> {};
		
		struct lub_type_metafunction {
			template<class T, class U>
			struct invoke : lub_type_aux<T, U> {};
		};
	} // namespace detail_lub_type_hpp_
	
	template<class... T>
	struct lub_type
	: reduce
		<list<T...>
		,detail_lub_type_hpp_::lub_type_metafunction
		> {};
	
	template<class T>
	struct lub_type<T> : lub_type<T, T> {};
	
	template<>
	struct lub_type<> {};
	
	template<class... T>
	using lub_type_t = typename lub_type<T...>::type;
} // namespace lmpl

#endif // LMPL_LUB_TYPE_HPP_
