#ifndef LMPL_MAP_HPP_
#define LMPL_MAP_HPP_

#include "constant.hpp"
#include "voider.hpp"

namespace lmpl {
	
	namespace detail_map_hpp_ {
		template<class List, class Metafunction>
		struct map {};
		
		template
			<template<class...> class Holder
			,class... Content
			,class Metafunction
			>
		struct map<Holder<Content...>, Metafunction>
		: type_constant
			<Holder
				<typename Metafunction::template invoke<Content>::type...>
			>{};
		
		template<class List, class Metafunction, class = void>
		struct map_dispatch {};
		
		template<class List, class Metafunction>
		struct map_dispatch
			<List
			,Metafunction
			,template_void_t<Metafunction::template invoke>
			>
		: map<List, Metafunction> {};
	} // namespace detail_map_hpp_
	
	template<class List, class Metafunction>
	struct map : detail_map_hpp_::map_dispatch<List, Metafunction> {};
	
	template<class List, class Metafunction>
	using map_t = typename map<List, Metafunction>::type;
} // namespace lmpl

#endif // LMPL_MAP_HPP_
