#ifndef LMPL_MEM_HPP_
#define LMPL_MEM_HPP_

#include "zip.hpp"
#include "list.hpp"
#include "reduce.hpp"
#include "repack.hpp"
#include "common_operators.hpp"
#include <type_traits>

namespace lmpl {
	namespace detail_mem_hpp_ {
		template<class>
		struct leaf_type_wrapper {};
		
		template<class T, class /*Index*/>
		struct indexed_type_wrapper : leaf_type_wrapper<T> {};
		
		template<class List>
		struct helper {};
		
		template<class T>
		struct pair_decompose {};
		
		template<template<class...> class Holder, class First, class Second>
		struct pair_decompose<Holder<First, Second>> {
			using first  = First;
			using second = Second;
		};
		
		template<class T>
		using first_t = typename pair_decompose<T>::first;
		
		template<class T>
		using second_t = typename pair_decompose<T>::second;
		
		template<template<class...> class Holder, class... T>
		struct helper<Holder<T...>>
		: indexed_type_wrapper<first_t<T>, second_t<T>>... {};
	} // namespace detail_mem_hpp_
	
	template<class List, class X>
	struct mem {};
	
	template<template<class...> class Holder, class... Content, class X>
	struct mem<Holder<Content...>, X>
	: std::is_base_of
		<detail_mem_hpp_::leaf_type_wrapper<X>
		,detail_mem_hpp_::helper<number_list_t<list<Content...>>>
		> {};
	
	template<class Subset, class Superset>
	struct included_set : std::false_type {};
	
	template
		<template<class...> class Holder
		,class... Content1
		,class... Content2
		>
	struct included_set<Holder<Content1...>, Holder<Content2...>>
	: reduce
		<list
			<std::true_type
			,std::is_base_of
				<detail_mem_hpp_::leaf_type_wrapper<Content1>
				,detail_mem_hpp_::helper<number_list_t<list<Content2...>>>
				>...
			>
		,pack_template<boolean_and>
		>::type {};
	
	
	template<class U, class V>
	struct same_set
	: boolean_and<included_set<U, V>, included_set<V, U>> {};
} // namespace lmpl

#endif // LMPL_MEM_HPP_
