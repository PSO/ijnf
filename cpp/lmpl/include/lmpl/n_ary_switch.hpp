#ifndef BOOST_PP_IS_ITERATING
#	ifndef LMPL_N_ARY_SWITCH_HPP_
#		define LMPL_N_ARY_SWITCH_HPP_

#		include "lub_type.hpp"
#		include <boost/preprocessor/iteration/iterate.hpp>
#		include <boost/preprocessor/iteration/local.hpp>
#		include <boost/preprocessor/repetition/enum_params.hpp>
#		include <boost/preprocessor/arithmetic/dec.hpp>
#		include <boost/preprocessor/config/limits.hpp>
#		include <boost/serialization/static_warning.hpp>
#		include <cassert>

#		ifndef LMPL_SWITCH_ARITY_LIMIT
#			define LMPL_SWITCH_ARITY_LIMIT 10
#		endif // LMPL_SWITCH_ARITY_LIMIT
#		if (LMPL_SWITCH_ARITY_LIMIT > BOOST_PP_LIMIT_ITERATION)
#			undef LMPL_SWITCH_ARITY_LIMIT
#			define LMPL_SWITCH_ARITY_LIMIT BOOST_PP_LIMIT_ITERATION
#			pragma message "warning: LMPL_SWITCH_ARITY_LIMIT exceeds BOOST_PP_LIMIT_ITERATION"
#		endif // LMPL_SWITCH_ARITY_LIMIT > BOOST_PP_LIMIT_ITERATION
#		if (LMPL_SWITCH_ARITY_LIMIT > BOOST_PP_LIMIT_MAG)
#			undef LMPL_SWITCH_ARITY_LIMIT
#			define LMPL_SWITCH_ARITY_LIMIT BOOST_PP_LIMIT_MAG
#			pragma message "warning: LMPL_SWITCH_ARITY_LIMIT exceeds BOOST_PP_LIMIT_MAG"
#		endif // LMPL_SWITCH_ARITY_LIMIT > BOOST_PP_LIMIT_MAG

#		define BOOST_PP_ITERATION_LIMITS (2, LMPL_SWITCH_ARITY_LIMIT)
#		define BOOST_PP_FILENAME_1 "lmpl/n_ary_switch.hpp"

namespace lmpl {
	template<class>
	struct case_id;
	
	namespace detail_n_ary_switch_hpp_ {
		template<class... Actions>
		struct n_ary_switch_helper {
			// won't be compiled with a single switch instruction, you might
			// want to increase LMPL_SWITCH_ARITY_LIMIT
			// should always evaluate to true
			BOOST_STATIC_WARNING(sizeof...(Actions) < LMPL_SWITCH_ARITY_LIMIT)
			template<class... Args>
			using return_type_t =
				lub_type_t<decltype(Actions::invoke(std::declval<Args>()))...>;
	
			/*
			template<class Idx, class... Args>
			static return_type_t<Args...> invoke(Idx&& index, Args&&... args)
			noexcept() {
				// ...
			}
			//*/
		};
		
		template<class Default, class... Actions>
		struct n_ary_switch_default_helper {
			// won't be compiled with a single switch instruction, you might
			// want to increase LMPL_SWITCH_ARITY_LIMIT
			// should always evaluate to true
			BOOST_STATIC_WARNING(sizeof...(Actions) < LMPL_SWITCH_ARITY_LIMIT)
			template<class... Args>
			using return_type_t =
				lub_type_t
					<decltype(Default::invoke(std::declval<Args>()...))
					,decltype(Actions::invoke(std::declval<Args>()...))...
					>;
			
			/*
			 template<class Idx, class... Args>
			 static return_type_t<Args...> invoke(Idx&& index, Args&&... args)
			 noexcept() {
				// ...
			 }
			 //*/
		};

		template<class T, class Ret>
		struct invoker {
			template<class... Args>
			static Ret invoke(Args&&... args)
			noexcept(noexcept(T::invoke(std::forward<Args>(args)...))) {
				return static_cast<Ret>(T::invoke(std::forward<Args>(args)...));
			}
		};

		template<class T>
		struct invoker<T, void> {
			template<class... Args>
			static void invoke(Args&&... args)
			noexcept(noexcept(T::invoke(std::forward<Args>(args)...))) {
				T::invoke(std::forward<Args>(args)...);
			}
		};
	} // namespace detail_n_ary_switch_hpp_
	
	template<class... Actions, class Idx, class... Args>
	auto n_ary_switch(Idx&& index, Args&&... args)
	noexcept
		(noexcept
			(detail_n_ary_switch_hpp_::n_ary_switch_helper<Actions...>::invoke
				(std::forward<Idx>(index), std::forward<Args>(args)...))) ->
	decltype
		(detail_n_ary_switch_hpp_::n_ary_switch_helper<Actions...>::invoke
			(std::forward<Idx>(index), std::forward<Args>(args)...)) {
		return detail_n_ary_switch_hpp_::n_ary_switch_helper<Actions...>::invoke
			       (std::forward<Idx>(index), std::forward<Args>(args)...);
	}
	
	template<class... Actions, class Idx, class... Args>
	auto n_ary_switch_default(Idx&& index, Args&&... args)
	noexcept(noexcept
		(detail_n_ary_switch_hpp_::n_ary_switch_default_helper<Actions...>
			::invoke(std::forward<Idx>(index), std::forward<Args>(args)...))) ->
	decltype(detail_n_ary_switch_hpp_::n_ary_switch_default_helper<Actions...>
		        ::invoke
			        (std::forward<Idx>(index), std::forward<Args>(args)...)) {
		return detail_n_ary_switch_hpp_::n_ary_switch_default_helper<Actions...>
			       ::invoke
				       (std::forward<Idx>(index), std::forward<Args>(args)...);
	}
} // namespace lmpl

#		include BOOST_PP_ITERATE()

#	endif // LMPL_N_ARY_SWITCH_HPP_
#else // BOOST_PP_IS_ITERATING
#	define n BOOST_PP_ITERATION()

namespace lmpl { namespace detail_n_ary_switch_hpp_ {
	
	template<BOOST_PP_ENUM_PARAMS(n, class T)>
	struct n_ary_switch_helper<BOOST_PP_ENUM_PARAMS(n, T)> {
	private:
		template<class... Args>
		using return_type_t =
			lub_type_t
				<decltype(T0::invoke(std::declval<Args>()...))
#			define BOOST_PP_LOCAL_MACRO(k) \
				,decltype(T ## k::invoke(std::declval<Args>()...))
#			define BOOST_PP_LOCAL_LIMITS (1, BOOST_PP_DEC(n))
#			include BOOST_PP_LOCAL_ITERATE()
				>;
	public:
		template<class Idx, class... Args>
		static return_type_t<Args...> invoke(Idx&& index, Args&&... args)
		noexcept
			(   noexcept(T0::invoke(std::forward<Args>(args)...))
#		define BOOST_PP_LOCAL_MACRO(k)                                         \
			 && noexcept(T ## k::invoke(std::forward<Args>(args)...))
#		define BOOST_PP_LOCAL_LIMITS (1, BOOST_PP_DEC(n))
#		include BOOST_PP_LOCAL_ITERATE()
			) {
			switch(std::forward<Idx>(index)) {
#			define BOOST_PP_LOCAL_MACRO(k)                                     \
				case case_id<T ## k>::value():                                 \
				return invoker<T ## k, return_type_t<Args...>>::invoke         \
					       (std::forward<Args>(args)...);
#			define BOOST_PP_LOCAL_LIMITS (0, BOOST_PP_DEC(n))
#			include BOOST_PP_LOCAL_ITERATE()
			}
			assert(false);
		}
	};
	
	template<class Default, BOOST_PP_ENUM_PARAMS(n, class T)>
	struct n_ary_switch_default_helper<Default, BOOST_PP_ENUM_PARAMS(n, T)> {
	private:
		template<class... Args>
		using return_type_t =
			lub_type_t
				<decltype(Default::invoke(std::declval<Args>()...))
#			define BOOST_PP_LOCAL_MACRO(k)                                     \
				,decltype(T ## k::invoke(std::declval<Args>()...))
#			define BOOST_PP_LOCAL_LIMITS (0, BOOST_PP_DEC(n))
#			include BOOST_PP_LOCAL_ITERATE()
				>;
	public:
		template<class Idx, class... Args>
		static return_type_t<Args...> invoke(Idx&& index, Args&&... args)
		noexcept
			(   noexcept(Default::invoke(std::forward<Args>(args)...))
#		define BOOST_PP_LOCAL_MACRO(k)                                         \
			 && noexcept(T ## k::invoke(std::forward<Args>(args)...))
#		define BOOST_PP_LOCAL_LIMITS (0, BOOST_PP_DEC(n))
#		include BOOST_PP_LOCAL_ITERATE()
			) {
			switch(std::forward<Idx>(index)) {
#			define BOOST_PP_LOCAL_MACRO(k)                                     \
				case case_id<T ## k>::value():                                 \
					return invoker<T ## k, return_type_t<Args...>>::invoke     \
					       (std::forward<Args>(args)...);
#			define BOOST_PP_LOCAL_LIMITS (0, BOOST_PP_DEC(n))
#			include BOOST_PP_LOCAL_ITERATE()
				default:
					return invoker<Default, return_type_t<Args...>>::invoke
						       (std::forward<Args>(args)...);
			}
			assert(false);
		}
	};
	
} // namespace detail_n_ary_switch_hpp_
} // namespace lmpl

#	undef n
#endif // BOOST_PP_IS_ITERATING
