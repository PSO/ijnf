#ifndef LMPL_PARTITION_HPP_
#define LMPL_PARTITION_HPP_

#include "zip.hpp"
#include "list.hpp"
#include "constant.hpp"
#include "map.hpp"
#include "reduce.hpp"
#include "cat.hpp"
#include <type_traits>

namespace lmpl {
	namespace detail_partition_hpp_ {
		template<class Pivot>
		struct sieve {
			template<class T>
			struct invoke {};
		};
		
		template<class Pivot>
		template<template<class...> class Holder, class T, class Idx>
		struct sieve<Pivot>::invoke<Holder<T, Idx>>
		: std::conditional
			<Idx::value < Pivot::value
			,list<list<T>, empty_list>
			,list<empty_list, list<T>>
			> {};
		
		struct cat_pair {
			template<class T, class U>
			struct invoke {};
		};
		
		template<class Fst1, class Snd1, class Fst2, class Snd2>
		struct cat_pair::invoke<list<Fst1, Snd1>, list<Fst2, Snd2>>
		: type_constant<list<cat_t<Fst1, Fst2>, cat_t<Snd1, Snd2>>> {};
		
		template<class RawPartition>
		struct partition {};
		
		template<class... Left, class Middle, class... Right>
		struct partition<list<list<Left...>, list<Middle, Right...>>>
		: type_constant<list<list<Left...>, list<Middle, Right...>>> {
			using left = list<Left...>;
			using middle = Middle;
			using right = list<Right...>;
		};
		
		template<class List, class Pivot, class = void>
		struct partition_dispatch {};
		
		template
			<template<class...> class Holder
			,class Head
			,class... Content
			,class Pivot
			>
		struct partition_dispatch
			<Holder<Head, Content...>
			,Pivot
			,typename std::enable_if
				<std::is_integral<decltype(Pivot::value)>::value>::type
			>
		: partition
			<reduce_t
				<map_t<number_list_t<Holder<Head, Content...>>, sieve<Pivot>>
				,cat_pair
				>
			> {};
	} // namespace detail_partition_hpp_
	
	template<class List, class Pivot>
	struct partition
	: detail_partition_hpp_::partition_dispatch<List, Pivot> {};
	
	template<class List, class Pivot>
	using partition_t = typename partition<List, Pivot>::type;
} // namespace lmpl

#endif // LMPL_PARTITION_HPP_
