#ifndef LMPL_POP_BACK_HPP_
#define LMPL_POP_BACK_HPP_

#include "push_front.hpp"
#include "constant.hpp"

namespace lmpl {
	template<class List>
	struct pop_back {};
	
	template
		<template<class...> class ListHolder
		,class ListHead
		,class... ListTail
		>
	struct pop_back<ListHolder<ListHead, ListTail...>>
	: push_front<typename pop_back<ListHolder<ListTail...>>::type, ListHead> {};
	
	template<template<class...> class ListHolder, class ListHead>
	struct pop_back<ListHolder<ListHead>> : type_constant<ListHolder<>> {};
	
	template<class List>
	using pop_back_t = typename pop_back<List>::type;
} // namespace lmpl

#endif // LMPL_POP_BACK_HPP_
