#ifndef LMPL_PUSH_BACK_HPP_
#define LMPL_PUSH_BACK_HPP_

#include "constant.hpp"

namespace lmpl {
	template<class List, class T>
	struct push_back {};
	
	template<template<class...> class ListHolder, class... ListContent, class T>
	struct push_back<ListHolder<ListContent...>, T>
	: type_constant<ListHolder<ListContent..., T>> {};
	
	template<class List, class T>
	using push_back_t = typename push_back<List, T>::type;
} // namespace lmpl

#endif // LMPL_PUSH_BACK_HPP_
