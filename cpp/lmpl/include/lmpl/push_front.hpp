#ifndef LMPL_PUSH_FRONT_HPP_
#define LMPL_PUSH_FRONT_HPP_

#include "constant.hpp"

namespace lmpl {
	template<class List, class T>
	struct push_front {};
	
	template<template<class...> class ListHolder, class... ListContent, class T>
	struct push_front<ListHolder<ListContent...>, T>
	: type_constant<ListHolder<T, ListContent...>> {};
	
	template<class List, class T>
	using push_front_t = typename push_front<List, T>::type;
} // namespace lmpl

#endif // LMPL_PUSH_FRONT_HPP_
