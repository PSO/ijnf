#ifndef LMPL_RANGE_HPP_
#define LMPL_RANGE_HPP_

#include "cat.hpp"
#include "list.hpp"
#include "constant.hpp"
#include <type_traits>

namespace lmpl {
	namespace detail_range_hpp_ {
		template<class T, T FirstIndex, T LastIndex, class = void>
		struct range : type_constant<empty_list> {};
		
		template<class T, T FirstIndex, T LastIndex>
		struct range
			<T
			,FirstIndex
			,LastIndex
			,typename std::enable_if
				<   (LastIndex > FirstIndex)
				 && ((LastIndex - FirstIndex) > 1)
				>::type
			>
		: cat
			<typename range<T, FirstIndex, (LastIndex + FirstIndex) / 2>::type
			,typename range<T, (LastIndex + FirstIndex) / 2, LastIndex>::type
			> {};
		
		template<class T, T FirstIndex, T LastIndex>
		struct range
			<T
			,FirstIndex
			,LastIndex
			,typename std::enable_if
				<   (LastIndex > FirstIndex)
				 && (LastIndex - FirstIndex) == 1
				>::type
			>
		: type_constant<list<std::integral_constant<T, FirstIndex>>> {};
		
		template<class FirstIndex, class LastIndex, class = void>
		struct range_dispatch {};
		
		template<class FirstIndex, class LastIndex>
		struct range_dispatch
			<FirstIndex
			,LastIndex
			,typename std::enable_if
				<std::is_same
					<decltype(FirstIndex::value)
					,decltype(LastIndex::value)
					>::value
				,void
				>::type
			>
		: range
			<decltype(FirstIndex::value)
			,FirstIndex::value
			,LastIndex::value
			> {};
	} // namespace detail_range_hpp_
	
	template<class FirstIndex, class LastIndex>
	struct range : detail_range_hpp_::range_dispatch<FirstIndex, LastIndex> {};
	
	template<class FirstIndex, class LastIndex>
	using range_t = typename range<FirstIndex, LastIndex>::type;
	
	template<int N>
	using index_t = std::integral_constant<std::size_t, std::size_t(N)>;
	
	template<int... Indices>
	using index_list = list<index_t<Indices>...>;
	
} // namespace lmpl

#endif // LMPL_RANGE_HPP_
