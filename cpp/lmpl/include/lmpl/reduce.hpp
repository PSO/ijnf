#ifndef LMPL_REDUCE_HPP_
#define LMPL_REDUCE_HPP_

#include "range.hpp"
#include "cat.hpp"
#include "drop_front.hpp"
#include "list.hpp"
#include "constant.hpp"
#include "voider.hpp"

#include <type_traits>
#include <cstddef>

namespace lmpl {
	namespace detail_reduce_hpp_ {
		struct dummy {};
		
		template<class Transformer, std::size_t Period>
		struct transform_every_period {
			template<class T, class U, class Idx, class = void>
			struct invoke : type_constant<dummy> {};
		};
		
		template<class Transformer, std::size_t Period>
		template<class T, class U, class Idx>
		struct transform_every_period<Transformer, Period>::invoke
			<T, U, Idx, typename std::enable_if<Idx::value % Period == 0>::type>
		: Transformer::template invoke<T, U> {};
		
		template<class Transformer, std::size_t Period>
		template<class T, class Idx>
		struct transform_every_period<Transformer, Period>::invoke
			<T
			,dummy
			,Idx
			,typename std::enable_if<Idx::value % Period == 0>::type
			>
		: type_constant<T> {};
		
		template<class List, class N>
		struct rotate_left
		: cat<drop_front_t<List, N>, repeat_t<dummy, N>> {};
		
		
		template<class List1, class List2, class List3, class Transformer>
		struct apply_on_triples {};
		
		template
			<template<class...> class Holder
			,class... Content1
			,class... Content2
			,class... Content3
			,class Transformer
			>
		struct apply_on_triples
			<Holder<Content1...>
			,Holder<Content2...>
			,Holder<Content3...>
			,Transformer
			>
		: type_constant
			<Holder
				<typename Transformer::template invoke
					<Content1, Content2, Content3>::type...
				>
			> {};
		
		template
			<class List
			,class AssociativeTransformer
			,class Indexes
			,std::size_t Level
			>
		struct step
		: apply_on_triples
			<List
			,typename rotate_left
				<List, std::integral_constant<std::size_t, Level / 2>>::type
			,Indexes
			,transform_every_period<AssociativeTransformer, Level>
			> {};
		
		template
			<class List
			,class AssociativeTransformer
			,class Indexes
			,std::size_t Level
			>
		struct reduce
		: step
			<typename reduce
				<List
				,AssociativeTransformer
				,Indexes
				,Level / 2
				>::type
			,AssociativeTransformer
			,Indexes
			,Level
			> {};
		
		template
			<class List
			,class AssociativeTransformer
			,class Indexes
			>
		struct reduce<List, AssociativeTransformer, Indexes, std::size_t(1)>
		: type_constant<List> {};
		
		template<class List>
		struct head {};
		
		template<template<class...> class Holder, class Head, class... Tail>
		struct head<Holder<Head, Tail...>> : type_constant<Head> {};
		
		template<class T>
		constexpr T next_highest_power_of_two(T n) noexcept {
			return n <= 1
				       ? 1
				       : 2 * next_highest_power_of_two<T>(n - (n / 2));
		}
		
		template<class List, class AssociativeTransformer, class = void>
		struct reduce_dispatch {};
		
		template<class AssociativeTransformer, class... Elements>
		struct naive_reduce;
		
		template
			<class AssociativeTransformer
			,class  E1, class  E2, class  E3, class  E4
			,class  E5, class  E6, class  E7, class  E8
			,class  E9, class E10, class E11, class E12
			,class E13, class E14, class E15, class E16
			,class... Tail
			>
		struct naive_reduce
			<AssociativeTransformer
			, E1,  E2,  E3,  E4,  E5,  E6,  E7,  E8
			, E9, E10, E11, E12, E13, E14, E15, E16
			,Tail...
			>
		: naive_reduce
			  <AssociativeTransformer
			  ,typename naive_reduce
				   <AssociativeTransformer
				   ,E1, E2, E3, E4, E5, E6, E7, E8
				   >::type
			  ,typename naive_reduce
				   <AssociativeTransformer
				   ,E9, E10, E11, E12, E13, E14, E15, E16
				   >::type
			  ,Tail...
			  > {};
			
		template
			<class AssociativeTransformer
			,class E1, class E2, class E3, class E4
			,class E5, class E6, class E7, class E8
			,class... Tail
			>
		struct naive_reduce
			<AssociativeTransformer, E1, E2, E3, E4, E5, E6, E7, E8, Tail...>
		: naive_reduce
			<AssociativeTransformer
			,typename naive_reduce<AssociativeTransformer, E1, E2, E3, E4>::type
			,typename naive_reduce<AssociativeTransformer, E5, E6, E7, E8>::type
			,Tail...
			> {};
			
		template
			<class AssociativeTransformer
			,class E1, class E2, class E3, class E4
			,class... Tail
			>
		struct naive_reduce<AssociativeTransformer, E1, E2, E3, E4, Tail...>
		: naive_reduce
			<AssociativeTransformer
			,typename naive_reduce<AssociativeTransformer, E1, E2>::type
			,typename naive_reduce<AssociativeTransformer, E3, E4>::type
			,Tail...
			> {};
		
		template<class AssociativeTransformer, class E1, class E2, class E3>
		struct naive_reduce<AssociativeTransformer, E1, E2, E3>
		: naive_reduce
			<AssociativeTransformer
			,typename naive_reduce<AssociativeTransformer, E1, E2>::type
			,E3
			> {};
		
		template<class AssociativeTransformer, class E1, class E2>
		struct naive_reduce<AssociativeTransformer, E1, E2>
		: type_constant
			  <typename AssociativeTransformer::template invoke<E1, E2>::type
			  > {};
		
		template<class AssociativeTransformer, class E>
		struct naive_reduce<AssociativeTransformer, E> : type_constant<E> {};
			
		template
			<template<class...> class Holder
			,class... Content
			,class AssociativeTransformer
			>
		struct reduce_dispatch
			<Holder<Content...>
			,AssociativeTransformer
			,typename std::enable_if
			<(sizeof...(Content) > 0) && (sizeof...(Content) <= 16)
			,template_void_t<AssociativeTransformer::template invoke>
			>::type
			>
		: naive_reduce<AssociativeTransformer, Content...> {};
			
		template
			<template<class...> class Holder
			,class... Content
			,class AssociativeTransformer
			>
		struct reduce_dispatch
			<Holder<Content...>
			,AssociativeTransformer
			,typename std::enable_if
				<(sizeof...(Content) > 16)
				,template_void_t<AssociativeTransformer::template invoke>
				>::type
			>
		: head
			<typename reduce
				<list<Content...>
				,AssociativeTransformer
				,range_t
					<std::integral_constant<std::size_t, 0>
					,std::integral_constant<std::size_t, sizeof...(Content)>
					>
				,next_highest_power_of_two(sizeof...(Content))
				>::type
			> {};
	} // namespace detail_reduce_hpp_
	
	template<class List, class AssociativeTransformer>
	struct reduce
	: detail_reduce_hpp_::reduce_dispatch<List, AssociativeTransformer> {};
	
	template<class List, class AssociativeTransformer>
	using reduce_t = typename reduce<List, AssociativeTransformer>::type;
} // namespace lmpl

#endif // LMPL_REDUCE_HPP_
