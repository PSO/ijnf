#ifndef LMPL_REPACK_HPP_
#define LMPL_REPACK_HPP_

#include "apply.hpp"
#include "constant.hpp"

namespace lmpl {
	template<class List, class HolderMetafunction>
	struct repack {};
	
	template
		<template<class...> class ListHolder
		,class... ListContent
		,class HolderMetafunction
		>
	struct repack<ListHolder<ListContent...>, HolderMetafunction>
	: apply<ListContent...>::template invoke<HolderMetafunction> {};
	
	template<class List, class HolderMetafunction>
	using repack_t = typename repack<List, HolderMetafunction>::type;
	
	template<template<class...> class Tpl>
	struct pack_template {
		template<class... T>
		struct invoke : type_constant<Tpl<T...>> {};
	};
	
	template<template<class...> class Metafunction>
	struct metafunction {
		template<class... T>
		struct invoke : Metafunction<T...> {};
	};
} // namespace lmpl

#endif // LMPL_REPACK_HPP_
