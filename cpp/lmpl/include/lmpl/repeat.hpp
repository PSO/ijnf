#ifndef LMPL_REPEAT_HPP_
#define LMPL_REPEAT_HPP_

#include "cat.hpp"
#include "list.hpp"
#include "voider.hpp"

#include <type_traits>

namespace lmpl {
	namespace detail_repeat_hpp_ {
		template<class T, class Index, Index I, class = void>
		struct repeat : type_constant<empty_list> {};
		
		template<class T, class Index, Index I>
		struct repeat
			<T, Index, I, typename std::enable_if<(I > Index(1))>::type>
		: cat
			<typename repeat<T, Index, Index(I / Index(2))>::type
			,typename repeat<T, Index, Index(I - (I/Index(2)))>::type
			> {};
		
		template<class T, class Index, Index I>
		struct repeat<T, Index, I, typename std::enable_if<I == Index(1)>::type>
		: type_constant<list<T>> {};
		
		template<class T, class Index, class = void>
		struct repeat_dispatch{};
		
		template<class T, class Index>
		struct repeat_dispatch<T, Index, void_t<decltype(Index::value)>>
		: repeat<T, decltype(Index::value), Index::value> {};
	} // namespace detail_repeat_hpp_
	
	template<class T, class Index>
	struct repeat : detail_repeat_hpp_::repeat_dispatch<T, Index> {};
	
	template<class T, class Index>
	using repeat_t = typename repeat<T, Index>::type;
} // namespace lmpl

#endif // LMPL_REPEAT_HPP_
