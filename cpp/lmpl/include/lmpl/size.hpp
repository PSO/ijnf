#ifndef LMPL_SIZE_HPP_
#define LMPL_SIZE_HPP_

#include <type_traits>

namespace lmpl {
	template<class List>
	struct size {};
	
	template<template<class...> class ListHolder, class... ListContent>
	struct size<ListHolder<ListContent...>>
	: std::integral_constant
		<decltype(sizeof...(ListContent)), sizeof...(ListContent)> {};
	
	template<class List>
	struct empty {};
	
	template<template<class...> class ListHolder, class... ListContent>
	struct empty<ListHolder<ListContent...>> : std::false_type {};
	
	template<template<class...> class ListHolder>
	struct empty<ListHolder<>> : std::true_type {};
	
} // namespace lmpl

#endif // LMPL_SIZE_HPP_
