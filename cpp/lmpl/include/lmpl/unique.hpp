#ifndef LMPL_UNIQUE_HPP_
#define LMPL_UNIQUE_HPP_

#include "mem.hpp"
#include "push_front.hpp"
#include "constant.hpp"

namespace lmpl {
	template<class List>
	struct unique {};
	
	template
		<template<class...> class ListHolder
		,class ListHead
		,class... ListTail
		>
	struct unique<ListHolder<ListHead, ListTail...>>
	: std::conditional
		<mem<ListHolder<ListTail...>, ListHead>::value
		,typename unique<ListHolder<ListTail...>>::type
		,push_front_t<typename unique<ListHolder<ListTail...>>::type, ListHead>
		> {};
	
	template<template<class...> class ListHolder>
	struct unique<ListHolder<>> : type_constant<ListHolder<>> {};
	
	template<class List>
	using unique_t = typename unique<List>::type;
} // namespace lmpl

#endif // LMPL_UNIQUE_HPP_
