#ifndef LMPL_VOIDER_HPP_
#define LMPL_VOIDER_HPP_

#include "constant.hpp"

namespace lmpl {
	namespace detail_voider_hpp_ {
		template<class...>
		struct voider : type_constant<void> {};
		
		template<template<class...> class...>
		struct template_voider : type_constant<void> {};
	} // namespace detail_voider_hpp_
	
	template<template<class...> class... T>
	using template_void_t =
		typename detail_voider_hpp_::template_voider<T...>::type;
	
	template<class... T>
	using void_t = typename detail_voider_hpp_::voider<T...>::type;
} // namespace lmpl

#endif // LMPL_VOIDER_HPP_
