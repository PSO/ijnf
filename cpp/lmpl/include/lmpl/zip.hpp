#ifndef LMPL_ZIP_HPP_
#define LMPL_ZIP_HPP_

#include "list.hpp"
#include "constant.hpp"
#include "range.hpp"
#include "repack.hpp"

namespace lmpl {
	namespace detail_zip_hpp_ {
		template<class L1, class L2, class = void>
		struct zip_dispatch {};
		
		template
			<template<class...> class Holder
			,class... Content1
			,class... Content2
			>
		struct zip_dispatch
			<Holder<Content1...>
			,Holder<Content2...>
			,typename std::enable_if
				<sizeof...(Content1) == sizeof...(Content2)>::type
			>
		: type_constant<Holder<list<Content1, Content2>...>> {};
	} // namespace detail_zip_hpp_
	
	template<class L1, class L2>
	struct zip : detail_zip_hpp_::zip_dispatch<L1, L2> {};
	
	template<class L1, class L2>
	using zip_t = typename zip<L1, L2>::type;
	
	template<class List>
	struct number_list {};
	
	template<template<class...> class Holder, class... Content>
	struct number_list<Holder<Content...>>
	: zip
		<Holder<Content...>
		,repack_t
			<range_t
				<std::integral_constant<std::size_t, std::size_t(0)>
				,std::integral_constant<std::size_t, sizeof...(Content)>
				>
			,pack_template<Holder>
			>
		> {};
	
	template<class List>
	using number_list_t = typename number_list<List>::type;
} // namespace lmpl

#endif // LMPL_ZIP_HPP_
