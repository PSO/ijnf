#include "types.hpp"
#include "matrix_parser.hpp"
#include "intermediate_matrix_decomposition.hpp"
#include "laguerre.hpp"
#include "gershgorin.hpp"
#include <iomanip>
#include <cmath>
#include <sstream>

template<class T>
T cast_coefficient(const mpq_wrapper& x);

template<class T>
T cast_coefficient(const rational_type& x) {
	return T(x.numerator()) / T(x.denominator());
}

template<>
double cast_coefficient<double>(const mpq_wrapper& x) {
	return double(x);
}

template<>
complex_type cast_coefficient<complex_type>(const rational_type& x) {
	return complex_type(cast_coefficient<real_type>(x));
}

template<class T>
T cast_polynomial(const rational_polynomial_type&);

template<>
real_polynomial_type
cast_polynomial<real_polynomial_type>(const rational_polynomial_type& p) {
	return p.cast_with<real_field>
		       (static_cast<real_type(*)(const rational_type&)>
			        (cast_coefficient<real_type>)
		       );
}

template<>
complex_polynomial_type
cast_polynomial<complex_polynomial_type>(const rational_polynomial_type& p) {
	return p.cast_with<complex_field>
		       (static_cast<complex_type(*)(const rational_type&)>
			        (cast_coefficient<complex_type>)
		       );
}

template<>
interval_polynomial_type
cast_polynomial<interval_polynomial_type>(const rational_polynomial_type& p) {
	return p.cast_with<interval_pseudofield>
		       (static_cast<interval_type(*)(const rational_type&)>
			        (cast_coefficient<interval_type>)
		       );
}

interval_matrix_type cast_matrix(const rational_matrix_type& m) {
	return m.cast_with<interval_pseudofield>
		       (static_cast<interval_type(*)(const rational_type&)>
			        (cast_coefficient<interval_type>)
		       );
}

struct in_nanosecond_type { std::ostream& stream; };

struct in_nanosecond_tag {} in_nanosecond;

in_nanosecond_type operator<<(std::ostream& o, in_nanosecond_tag) {
	return in_nanosecond_type{o};
}

template<class CountT>
std::ostream& operator<<(in_nanosecond_type&& o, CountT t) {
	CountT nanosecond = 1;
	CountT microsecond = nanosecond*1000;
	CountT millisecond = microsecond*1000;
	CountT second = millisecond*1000;
	CountT minute = second*60;
	CountT hour = minute*60;
	
	CountT units[] =
		{hour, minute, second, millisecond, microsecond, nanosecond};
	const char* symbols[] = {"h", "mn", "s", "ms", "µs", "ns"};
	std::size_t i = 0;
	for(CountT u : units) {
		CountT value = t / u;
		t %= u;
		if(value > 0) o.stream << value << symbols[i];
		++i;
	}
	return o.stream;
}

namespace detail {
	template<class T>
	struct jblock_wrap {
		T root;
		std::size_t sz;
	};
	
	template<class T>
	std::ostream& operator<<(std::ostream& o, const jblock_wrap<T>& block) {
		return o << "jordan_block(" << block.root << ", " << block.sz << ')';
	}
	
	template<class T>
	jblock_wrap<T> jordan_block(const T& root, std::size_t sz) {
		return{root, sz};
	}
	
	template<class T>
	struct rblock_wrap {
		T root;
		std::size_t sz;
	};
	
	// outputs an interval [m, M] such that m*Pi <= theta <= M*Pi
	// for some value theta = arg(z)[2Pi]
	// may be unsound !!! TODO
	// also, perhaps too many cases?
	template<class T>
	interval<T> complex_arg(const cartesian_complex<interval<T>>& z) {
		using std::nexttoward;
#	define CST_pi 3.14159265358979323846264338327950288419716939937510
		// should be constexpr, but nexttoward isn't
		static const T pi_min = nexttoward(static_cast<T>(CST_pi), 0);
		static const T pi_max = nexttoward(static_cast<T>(CST_pi), 4);
#	undef CST_pi
		using std::atan2; // !!! not necessarily correctly rounded ! TODO
		using std::max;
		using std::min;
		const auto a = real(z);
		const auto b = imag(z);
		if(contains_zero_strict(a) && contains_zero_strict(b))
			return interval<T>{0, 2};
		if(infimum(b) >= 0) {
			const auto bb = infimum(b);
			return interval<T>
				       {atan2(bb, supremum(a))/pi_max                     // [0;1]
				       ,min<T>(1, atan2(bb, infimum(a))/pi_min)           // [0;1]
				       };
		}
		if(supremum(b) <= 0) {
			const auto bb = supremum(b);
			return interval<T>
				       {max<T>(-1, atan2(bb, infimum(a))/pi_min)         // [-1;0]
				       ,atan2(bb, supremum(a))/pi_max                    // [-1;0]
				       };
		}
		if(infimum(a) >= 0) {
			const auto aa = infimum(a);
			return interval<T>
				       {max<T>(-0.5, atan2(supremum(b), aa)/pi_min)     // [-0.5; 0]
				       ,min<T>(0.5, atan2(infimum(b), aa)/pi_min)       // [0; 0.5]
				       };
		}
		if(supremum(a) <= 0) {
			const auto aa = supremum(a);
			return interval<T>
				       {max<T>(0.5, atan2(infimum(b), aa)/pi_max)       // [0.5; 1]
				       ,min<T>(-0.5, atan2(supremum(b), aa)/pi_max) + 2 // [-1; -0.5] +2
				       };
		}
		assert(false);
		return interval<T>::top();
	}
	
	/* outputs "real_block(r, theta, sz)"
	 * where
	 * a) r*e^(i*theta) is the given complex root in cartesian form
	 * b) sz is the multiplicity of that root
	 *    (and the number of such diagonal blocks, with identity matrices on the
	 *    second diagonal)
	 *
	 * the corresponding "real jordan block" is r*R_theta
	 * where R_theta is the 2d rotation matrix of angle theta, i.e.
	 *           ( cos(theta)   -sin(theta) )
	 * R_theta = (                          )
	 *           ( sin(theta)    cos(theta) )
	 */
	template<class T>
	std::ostream& operator<<(std::ostream& o, const rblock_wrap<T>& block) {
		return o << "real_block(" << abs(block.root)
		         << ", "          << complex_arg(block.root)
		         << ", "          << block.sz << ')';
	}
	
	template<class T>
	rblock_wrap<T> real_block(const T& root, std::size_t sz) {
		return{root, sz};
	}
} // namespace detail
using detail::jordan_block;
using detail::real_block;

void perform(const rational_matrix_type& m, bool show_product);

int checked_perform(const rational_matrix_type& m, bool show_product) {
	static constexpr int gershgorin_failure = 11;
	static constexpr int unknown_failure = 12;
	static constexpr int generic_failure = 13;
	try {
		perform(m, show_product);
		return 0;
	} catch(const gershgorin_exception& e) {
		std::cerr << e.what() << std::endl;
		return gershgorin_failure;
	} catch(const std::runtime_error& e) {
		std::cerr << e.what() << std::endl;
		return generic_failure;
	} catch(...) {
		std::cerr << "unknown exception" << std::endl;
		return unknown_failure;
	}
}

void perform(const rational_matrix_type& m, bool show_product) {
	const std::size_t n = m.width();
	
	auto decomposition = intermediate_decomposition(m);
	interval_matrix_type transformation(n, n, interval_type(0));
	interval_matrix_type inverse_transformation(n, n, interval_type(0));
	std::size_t k = 0;
	bool not_first = false;
	std::ostringstream output;
	output << "{ 'J':" << std::endl;
	output << "diag(";
	output << std::setprecision(24);
	for(const auto& block : decomposition.diagonal) {
		if(block.kind == diagonal_block::COMPANION_MATRIX) {
			auto charpoly =
				cast_polynomial<complex_polynomial_type>(block.block.charpoly);
			const std::size_t dg = charpoly.degree();
			std::vector<complex_type> roots;
			roots.reserve(charpoly.degree());
			auto root_finder = laguerre_root_finder(charpoly);
			for(const auto& r : root_finder) { roots.emplace_back(r); }
			auto bounded_roots =
				gershgorin
					(roots
					,cast_polynomial<real_polynomial_type>(block.block.charpoly)
					);
			auto interval_charpoly =
				cast_polynomial<interval_polynomial_type>(block.block.charpoly);
			std::size_t row = k;
			for(const auto& r : bounded_roots.first) {
				if(not_first) output << ", ";
				not_first = true;
				output << jordan_block(r, block.size);
				interval_type coef(1);
				for(std::size_t col = k ; col < k + dg ; ++col, coef *= r)
					{ transformation.at(row, col) = coef; }
				auto column_poly =
					interval_charpoly
					/ interval_polynomial_type({-r, interval_type(1)});
				const auto alpha = column_poly.evaluate(r);
				for(auto& column_poly_coef : column_poly)
					{ column_poly_coef /= alpha; }
				for(std::size_t col = 0 ; col < dg ; ++col) {
					inverse_transformation.at(k + col, row) = column_poly[col];
				}
				// recopy as many times as needed
				const auto src_row = row, src_en = k + dg;
				auto col = k + dg;
				++row;
				for(std::size_t bid = 1 ; bid < block.size ; ++bid, ++row) {
					for(std::size_t src_col = k; src_col < src_en; ++col, ++src_col) {
						transformation.at(row, col) =
							transformation.at(src_row, src_col);
						inverse_transformation.at(col, row) =
							inverse_transformation.at(src_col, src_row);
					}
				}
			}
			for(const auto& z : bounded_roots.second) {
				if(not_first) output << ", ";
				not_first = true;
				/*
				 * in the following, we compute two polynomials pi and pr whose
				 * coefficients are the coefficients of the corresponding
				 * columns of the inverse transformation matrix
				 * (the transformation matrix being an altered vandermonde
				 * matrix with row pairs whose coefficients are Re(z^column_id)
				 * and Im(z^column_id) respectively)
				 *
				 * we have pr = pp * ppr, pi = pp * ppi where
				 * 0) pp = p/sigma
				 * 1) sigma is the real quadratic polynomial with complex root z
				 * 2) and ppr, ppi have degree 0 or 1
				 *
				 * a, b, c and d are reals introduced for convenience such as:
				 * 3)     z = a + ib
				 * 4) pp(z) = c + id
				 */
				const auto a = real(z), b = imag(z);
				output << real_block(z, block.size);
				const interval_polynomial_type
				sigma{norm(z), -2 * a, interval_type(1)};
				const auto pp = interval_charpoly / sigma;
				auto rem = pp % sigma;
				// if the polynomial really is square-free, rem≠0
				assert(nonzero(rem));
				const auto c =
				rem.degree() > 0 ? (a * rem[1] + rem[0]) : rem[0];
				const auto d = rem.degree() > 0 ? b * rem[1] : interval_type(0);
				const auto denom = b * (square(c) + square(d));
				const interval_polynomial_type ppr({(b*c + a*d)/denom, -d/denom});
				const interval_polynomial_type ppi({(b*d - a*c)/denom, c/denom});
				const interval_polynomial_type pr = pp * ppr;
				const interval_polynomial_type pi = pp * ppi;
				assert(nonzero(pr));
				assert(nonzero(pi));
				complex_interval_type coef(1);
				for(std::size_t col = k ; col < k + dg ; ++col, coef *= z) {
					transformation.at(row, col) = real(coef);
					transformation.at(row + 1, col) = imag(coef);
				}
				for(std::size_t i = 0 ; i <= pr.degree() ; ++i)
					{ inverse_transformation.at(k + i, row) = pr[i]; }
				for(std::size_t i = 0 ; i <= pi.degree() ; ++i)
					{ inverse_transformation.at(k + i, row + 1) = pi[i]; }
				// recopy as many times as needed
				const auto src_row = row, src_en = k + dg;
				auto col = k + dg;
				row+=2;
				for(std::size_t bid = 1 ; bid < block.size ; ++bid, row+=2) {
					for(std::size_t src_col = k; src_col < src_en; ++col, ++src_col) {
						transformation.at(row, col) =
							transformation.at(src_row, src_col);
						transformation.at(row+1, col) =
							transformation.at(src_row+1, src_col);
						inverse_transformation.at(col, row) =
							inverse_transformation.at(src_col, src_row);
						inverse_transformation.at(col, row+1) =
							inverse_transformation.at(src_col, src_row+1);
					}
				}
			}
			k += dg * block.size;
		} else {
			if(not_first) output << ", ";
			not_first = true;
			output << block;
			for(std::size_t i = 0 ; i < block.size ; ++i, ++k) {
				transformation.at(k, k) = interval_type(1);
				inverse_transformation.at(k, k) = interval_type(1);
			}
		}
	}
	output << "),\n";
	transformation =
		transformation.multiply(cast_matrix(decomposition.inverse_transformation));
	inverse_transformation =
		cast_matrix(decomposition.transformation).multiply(inverse_transformation);
	output << "  'Q':" << std::endl;
	output << transformation << "," << std::endl;
	output << "  'P':" << std::endl;
	output << inverse_transformation;
	if(show_product) {
		const auto product =
			transformation.multiply(cast_matrix(m)).multiply(inverse_transformation);
		output << ',' << std::endl;
		output << "  'Q*M*P':" << std::endl;
		output << product;
	}
	output << '}' << std::endl;
	std::cout << output.str() << std::flush;
}

int main(int argc, char* argv[]) {
	if(argc > 1) {
		bool show_product = false;
		if(argc == 3 && std::string(argv[2]) == "--show-product") {
			show_product = true;
		}
		rational_matrix_type m = parse(argv[1]);
		if(m.width() != m.height()) {
			std::cerr << "matrix isn't square" << std::endl;
			return 2;
		}
		return checked_perform(m, show_product);
	}
	std::cerr << "usage: ijnf matrix [--show-product]" << std::endl;
	return 1;
}
