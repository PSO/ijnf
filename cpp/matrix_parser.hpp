#ifndef MATRIX_PARSER_HPP_
#define MATRIX_PARSER_HPP_

#include "types.hpp"
#include "configuration.hpp"
#include <iostream>

void ignore_chars(std::istream& i, unsigned int n) { i.ignore(n); }
void ignore_chars(const char*& src, unsigned int n) { src += n; }
bool get_char(std::istream& i, char& c) { return static_cast<bool>(i.get(c)); }
bool get_char(const char*& i, char& c) { return (c = *i++) != '\0'; }

template<class InputStream>
rational_matrix_type parse_aux(InputStream& i) {
	ignore_chars(i, 8); // Matrix([
	unsigned int state = 1;
	unsigned long int numerator = 0;
	unsigned long int denominator = 0;
	unsigned long int *x = &numerator;
	unsigned int nrow = 0u - 1u;
	unsigned int n = 0;
	std::vector<rational_type> coefs;
	char c;
	bool minus = false;
	const auto commit = [&]() {
		coefs.emplace_back(numerator, denominator?denominator:1ul);
		if(minus) coefs.back() = -coefs.back();
		numerator = 0;
		denominator = 0;
		minus = false;
		x = &numerator;
		++n;
	};
	// (there's a final ')' after state 0
	while(get_char(i, c) && state) {
		switch(c) {
			case '[':
				assert(state == 1);
				state = 12; // 1 -> 12
				break;
			case ']':
				assert(state == 1 || state == 2 || state == 9 || state == 12);
				if(state == 9) commit();
				state >>= 2; // 1 -> 0, 2 -> 0, 8 -> 2, 12 -> 2
				++nrow;
				break;
			case ',':
				assert(state == 2 || state == 8 || state == 9);
				if(state != 2) {
					commit();
					state = 52; // 8 -> 4(skip3), 9 -> 4(skip3)
				} else
					state = 28; // 2 -> 12(skip1)
				break;
			case '/':
				assert(state == 8);
				x = &denominator;
				state = 53; // 8 -> 5(skip3)
				break;
			case '\'':
				assert(state == 6 || state == 7);
				state += 18u; // 6 -> 8(skip1), 7 -> 9(skip1)
				break;
			case 'S':
				assert(state == 12);
				state = 36; // 12 -> 4(skip2)
				break;
			case '-':
				minus = true;
#ifdef CXX17
				[[fallthrough]];
#elif (defined(GCC_COMPILER) && REQUIRE_LATER_COMPILER_VERSION(7, 0, 0))
				[[gnu::fallthrough]];
#endif
			case '+':
				assert(state == 4 || state == 5);
				state += 2u; // 4 -> 6, 5 -> 7
				break;
			default: {
				assert(state == 4 || state == 6 || state == 5 || state == 7);
				assert('0' <= c && c <= '9');
				*x *= 10u;
				*x += static_cast<unsigned int>(c - '0');
				state |= 2u;
			}
		}
		ignore_chars(i, state >> 4);
		state &= 0xFu;
	}
	return rational_matrix_type(nrow, n/nrow, coefs.data());
}

rational_matrix_type parse(std::istream& i) { return parse_aux(i); }
rational_matrix_type parse(const char* src) { return parse_aux(src); }

#endif // MATRIX_PARSER_HPP_
