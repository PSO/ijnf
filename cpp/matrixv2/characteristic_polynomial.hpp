#ifndef CHARACTERISTIC_POLYNOMIAL_HPP_
#define CHARACTERISTIC_POLYNOMIAL_HPP_

#include "iterable_base.hpp"
#include "dense_square_matrix.hpp"
#include <cassert>

namespace ergl {
template<class T>
T matrix_trace(const dense_square_matrix<T>& m) {
	T result(0);
	const std::size_t n = m.width();
	for(std::size_t k = 0 ; k < n ; ++k) { result += m[k][k]; }
	return result;
}

template<class It, class CIt, class D, class T>
iterable_base<It, CIt, D, Mutability::Mutable>&
characteristic_polynomial
	(iterable_base<It, CIt, D, Mutability::Mutable>& dst
	,const dense_square_matrix<T>& m
	)
{
	const std::size_t n = m.width();
	assert(n == m.height());
	assert(size(dst) > n);
	using value_type = T;
	int tmp_idx = 1, matseq_idx = 0;
	dense_square_matrix<value_type> workspace[] =
		{{n, value_type(0)}, dense_square_matrix<value_type>{n}};
	
	dst[n] = value_type(1);
	for(std::size_t i = 0, k = n ; i < n ; ) {
		for(std::size_t j = 0 ; j < n ; ++j)
			{ (workspace[matseq_idx])[j][j] += dst[k]; }
		zero_matrix(workspace[tmp_idx]);
		inplace_matrix_multiplication
			(workspace[tmp_idx], m, workspace[matseq_idx]);
		matseq_idx = tmp_idx, tmp_idx = 1 - tmp_idx;
		--k, ++i;
		dst[k] = -matrix_trace(workspace[matseq_idx]) / value_type(i);
	}
	return dst;
}

template<class It, class CIt, class D, class T>
iterable_base<It, CIt, D, Mutability::Moved>&
characteristic_polynomial
	(iterable_base<It, CIt, D, Mutability::Moved>& dst
	,const dense_square_matrix<T>& m
	) {
	return move(characteristic_polynomial(unmove(dst), m));
}

template<class It, class CIt, class D, class T, Mutability M>
iterable_base<It, CIt, D, M>&&
characteristic_polynomial
	(iterable_base<It, CIt, D, M>&& dst
	,const dense_square_matrix<T>& m
	) {
	return std::move(characteristic_polynomial(dst, m));
}

} // namespace ergl

#endif // CHARACTERISTIC_POLYNOMIAL_HPP_
