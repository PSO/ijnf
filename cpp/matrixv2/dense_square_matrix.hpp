#ifndef DENSE_SQUARE_MATRIX_HPP_
#define DENSE_SQUARE_MATRIX_HPP_

#include "range_iterator.hpp"
#include "iterable_base.hpp"
#include <cassert>
#include <iterator>
#include <vector>

namespace ergl {

template<class T, bool IsConst>
using row_iterator_base =
	range_iterator_base
		<T*
		,const T*
		,IsConst
		,inner_stride_policy<static_value<1>>
		,stride_policy<dynamic_value>
		,end_range_policy
			 <stride_away<static_value<1>, stride_policy<dynamic_value>>>
		>;

template<class StridePolicy>
struct stride_stride_away {
	template<class Iterator, class Content>
	Iterator operator()(Iterator st, const Content& content) const noexcept {
		return st
		       + (static_cast<const StridePolicy&>(content)()
		          * static_cast<const StridePolicy&>(content)()
		         );
	}
};
	
template<class T, bool IsConst>
using dense_square_matrix_column_iterator_base =
	range_iterator_base
		<T*
		,const T*
		,IsConst
		,inner_stride_policy<dynamic_value>
		,stride_policy<static_value<1>>
		,end_range_policy
			 <stride_stride_away<inner_stride_policy<dynamic_value>>>
		>;
	
template<class T, bool IsConst>
using dense_square_matrix_submatrix_row_iterator_base =
	range_iterator_base
		<T*
		,const T*
		,IsConst
		,inner_stride_policy<static_value<1>>
		,stride_policy<dynamic_value>
		,end_range_policy
			 <stride_away<dynamic_value, inner_stride_policy<static_value<1>>>>
		>;

template<class T>
struct dense_square_matrix
: iterable_base
	  <row_iterator_base<T, false>
	  ,row_iterator_base<T, true>
	  ,dense_square_matrix<T>
	  >
{
public:
	using coefficient_type            = T;
	using coefficient_reference       = coefficient_type&;
	using const_coefficient_reference = const coefficient_type&;
	using iterator        = row_iterator_base<T, false>;
	using const_iterator  = row_iterator_base<T, true>;
	using reference       = typename std::iterator_traits<iterator>::reference;
	using const_reference = typename std::iterator_traits<const_iterator>::reference;
	
	using base =
		iterable_base<iterator, const_iterator, dense_square_matrix<T>>;
	
	dense_square_matrix()
	: base()
	, n(0)
	, content() {};
	
	explicit dense_square_matrix(std::size_t init_n)
	: base()
	, n(init_n)
	, content(n * n) {};
	
	dense_square_matrix(std::size_t init_n, const_coefficient_reference x)
	: base()
	, n(init_n)
	, content(n * n, x) {}
	
	template<class TT>
	dense_square_matrix(std::initializer_list<std::initializer_list<TT>> init)
	: base()
	, n(init.size())
	, content() {
		content.reserve(n * n);
		for(const auto& row : init) {
			assert(row.size() == n);
			for(const auto& c : row) { content.emplace_back(c); }
		}
	}
	
	template<class It, class CIt, class D, Mutability M>
	dense_square_matrix
		(std::size_t init_n, const iterable_base<It, CIt, D, M>& view)
	: base()
	, n(init_n)
	, content() {
		content.reserve(n * n);
		for(auto&& x : view) { content.emplace_back(conditional_move<M>(x)); }
	}
	
	template<class It, class CIt, class D, Mutability M>
	explicit dense_square_matrix(iterable_base<It, CIt, D, M>& view)
	: base()
	, n(size(view))
	, content() {
		content.reserve(n * n);
		for(const auto& row : view) {
			for(auto&& x : row) {
				content.emplace_back(conditional_move<M>(x));
			}
		}
	}
	
	template<class It, class CIt, class D, Mutability M>
	explicit dense_square_matrix(iterable_base<It, CIt, D, M>&& view)
	: dense_square_matrix(view) {}
	
	template<class It, class CIt, class D, Mutability M>
	explicit dense_square_matrix(const iterable_base<It, CIt, D, M>& view)
	: dense_square_matrix(const_iterable(cast_const_away(view))) {}
	
	dense_square_matrix(const dense_square_matrix&) = default;
	dense_square_matrix(dense_square_matrix&&) = default;
	
	dense_square_matrix& operator=(const dense_square_matrix&) = default;
	dense_square_matrix& operator=(dense_square_matrix&&) = default;
	
	template<class It, class CIt, class D, Mutability M>
	dense_square_matrix& operator=(const iterable_base<It, CIt, D, M>& rhs) {
		return static_cast<dense_square_matrix&>
			       (base::template operator=<>(rhs));
	}
	template<class It, class CIt, class D, Mutability M>
	dense_square_matrix& operator=(iterable_base<It, CIt, D, M>& rhs) {
		return static_cast<dense_square_matrix&>
			       (base::template operator=<>(rhs));
	}
	template<class It, class CIt, class D, Mutability M>
	dense_square_matrix& operator=(iterable_base<It, CIt, D, M>&& rhs) {
		return operator=<>(rhs);
	}
	
	std::size_t width() const noexcept {
		return n;
	}
	std::size_t height() const noexcept {
		return n;
	}
	
	iterator begin() noexcept {
		return iterator
			       (content.data()
			       ,{}, {static_cast<std::ptrdiff_t>(n)}, {}
			       );
	}
	const_iterator begin() const noexcept {
		return const_cast<dense_square_matrix&>(*this).begin();
	}
	const_iterator cbegin() const noexcept {
		return begin();
	}
	
	iterator end() noexcept {
		return iterator(content.data() + content.size()
		               ,{}, {static_cast<std::ptrdiff_t>(n)}, {}
		               );
	}
	const_iterator end() const noexcept {
		return const_cast<dense_square_matrix&>(*this).end();
	}
	const_iterator cend() const noexcept {
		return end();
	}
	reference operator[](std::size_t i) noexcept {
		assert(i < height());
		coefficient_type* start = content.data() + (n * i);
		return {{start, {}}, {start + n, {}}};
	}
	const_reference operator[](std::size_t i) const noexcept {
		return const_cast<dense_square_matrix&>(*this)[i];
	}
	
	template<Mutability M>
	using column_major_view_base =
		iterator_range_view_base
			<dense_square_matrix_column_iterator_base<coefficient_type, false>
			,dense_square_matrix_column_iterator_base<coefficient_type, true>
			,M
			>;
	using const_column_major_view =
		column_major_view_base<Mutability::Constant>;
	using expiring_column_major_view =
		column_major_view_base<Mutability::Expiring>;
	using column_major_view =
		column_major_view_base<Mutability::Mutable>;
	
private:
	template<Mutability M>
	column_major_view_base<M> get_column_major_view() const noexcept {
		const std::ptrdiff_t stride = static_cast<std::ptrdiff_t>(n);
		dense_square_matrix& self = const_cast<dense_square_matrix&>(*this);
		return column_major_view_base<M>
			       ({self.content.data(), {stride}, {}, {}}
			       ,{self.content.data() + stride, {stride}, {}, {}}
			       );
	}
	
public:
	const_column_major_view column_major() const & noexcept {
		return get_column_major_view<Mutability::Constant>();
	}
	column_major_view column_major() & noexcept {
		return get_column_major_view<Mutability::Mutable>();
	}
	expiring_column_major_view column_major() && noexcept {
		return get_column_major_view<Mutability::Expiring>();
	}
	
	template<Mutability M>
	using submatrix_view_base =
		iterator_range_view_base
			<dense_square_matrix_submatrix_row_iterator_base<T, false>
			,dense_square_matrix_submatrix_row_iterator_base<T, true>
			,M
			>;
	using const_submatrix_view =
		submatrix_view_base<Mutability::Constant>;
	using expiring_submatrix_view =
		submatrix_view_base<Mutability::Expiring>;
	using submatrix_view =
		submatrix_view_base<Mutability::Mutable>;
private:
	template<Mutability M>
	submatrix_view_base<M> get_submatrix_view
		(std::size_t row_st, std::size_t col_st
		,std::size_t row_en, std::size_t col_en
		)
	const noexcept {
		std::ptrdiff_t offset =
			static_cast<std::ptrdiff_t>((row_st * n) + col_st);
		std::ptrdiff_t row_size =
			static_cast<std::ptrdiff_t>(col_en)
			- static_cast<std::ptrdiff_t>(col_st);
		std::ptrdiff_t nrow =
			static_cast<std::ptrdiff_t>(row_en)
			- static_cast<std::ptrdiff_t>(row_st);
		std::ptrdiff_t stride = static_cast<std::ptrdiff_t>(n);
		dense_square_matrix& self = const_cast<dense_square_matrix&>(*this);
		return submatrix_view_base<M>
			       ({self.content.data() + offset, {}, {stride}, {row_size}}
			       ,{self.content.data() + (offset + n * nrow)
			        ,{}, {stride}, {row_size}
			        }
			       );
	}
public:
	const_submatrix_view submatrix
		(std::size_t row_st, std::size_t col_st
		,std::size_t row_en, std::size_t col_en
		)
	const & noexcept {
		return get_submatrix_view<Mutability::Constant>
			       (row_st, col_st, row_en, col_en);
	}
	
	submatrix_view submatrix
		(std::size_t row_st, std::size_t col_st
		,std::size_t row_en, std::size_t col_en
		)
	& noexcept {
		return get_submatrix_view<Mutability::Mutable>
			       (row_st, col_st, row_en, col_en);
	}
	
	expiring_submatrix_view submatrix
		(std::size_t row_st, std::size_t col_st
		,std::size_t row_en, std::size_t col_en
		)
	&& noexcept {
		return get_submatrix_view<Mutability::Expiring>
			       (row_st, col_st, row_en, col_en);
	}
	
private:
	std::size_t n;
	std::vector<coefficient_type> content;
};
	
} // namespace ergl

#endif // DENSE_SQUARE_MATRIX_HPP_
