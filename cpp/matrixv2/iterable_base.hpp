#ifndef ITERABLE_BASE_HPP_
#define ITERABLE_BASE_HPP_

#include <iterator>
#include <type_traits>
#include <cassert>
#include <iostream>

namespace ergl {
	
/* const casts ****************************************************************/

template<class T>
constexpr T& cast_const_away(const T& x) noexcept { return const_cast<T&>(x); }

template<class T>
constexpr auto add_const(T&& x) noexcept ->
const typename std::remove_reference<T>::type& { return x; }

/******************************************************************************/

enum class Mutability { Moved, Expiring, Mutable, Constant };

template
	<class Iterator, class ConstIterator, class Derived
	,Mutability M = Mutability::Mutable
	>
struct iterable_base;

template<Mutability M, class T, class = void>
struct conditional_move_type_trait /* undefined */;

template<Mutability M, class T>
constexpr auto conditional_move(T&& x) noexcept ->
decltype(conditional_move_type_trait<M, T>::invoke(std::forward<T>(x))) {
	return conditional_move_type_trait<M, T>::invoke(std::forward<T>(x));
}

template<class Iterator, class ConstIterator, class Derived>
struct iterable_base<Iterator, ConstIterator, Derived, Mutability::Constant> {
	using iterator        = Iterator;
	using const_iterator  = ConstIterator;
	using value_type      = typename std::iterator_traits<iterator>::value_type;
	using reference       = typename std::iterator_traits<iterator>::reference;
	using pointer         = typename std::iterator_traits<iterator>::pointer;
	using const_reference = typename std::iterator_traits<const_iterator>::reference;
	using const_pointer   = typename std::iterator_traits<const_iterator>::pointer;
	using difference_type = typename std::iterator_traits<iterator>::difference_type;
	
	using const_iterable_type =
		iterable_base<Iterator, ConstIterator, Derived, Mutability::Constant>;
	using expiring_iterable_type =
		iterable_base<Iterator, ConstIterator, Derived, Mutability::Expiring>;
	using moved_iterable_type =
		iterable_base<Iterator, ConstIterator, Derived, Mutability::Moved>;
	using derived_type = Derived;
	
	const_iterator cbegin() const noexcept {
		return begin();
	}
	const_iterator cend() const noexcept {
		return end();
	}
	const Derived& self() const noexcept {
		// C-style cast necessary to bypass access modifiers
		return (const Derived&)(*this);
	}
	Derived& self() noexcept {
		return const_cast<Derived&>(add_const(*this).self());
	}
	const_iterator begin() const noexcept {
		return self().begin();
	}
	const_iterator end() const noexcept {
		return self().end();
	}
	const_reference operator[](std::size_t i) const {
		return self()[i];
	}
};
	
template<class Iterator, class ConstIterator, class Derived>
struct iterable_base<Iterator, ConstIterator, Derived, Mutability::Moved>
: iterable_base<Iterator, ConstIterator, Derived, Mutability::Constant> {
	using base =
		iterable_base<Iterator, ConstIterator, Derived, Mutability::Constant>;
	using typename base::iterator;
	using typename base::reference;
	
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(const iterable_base<It, CIt, D, M>& rhs) {
		assert(size(rhs) == size(*this));
		const auto en = end();
		auto j = rhs.begin();
		for(auto i = begin() ; i != en ; ++i, ++j) {
			*i = *j;
		}
		return *this;
	}
	
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(iterable_base<It, CIt, D, M>& rhs) {
		assert(size(rhs) == size(*this));
		const auto en = end();
		auto j = rhs.begin();
		for(auto i = begin() ; i != en ; ++i, ++j) {
			*i = conditional_move<M>(*j);
		}
		return *this;
	}
	
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(iterable_base<It, CIt, D, M>&& rhs) {
		return operator=(rhs);
	}
	
	iterable_base& operator=(const iterable_base& rhs) {
		return operator=<>(rhs);
	}
	
	using base::self;
	using base::begin;
	using base::end;
	using base::operator[];
	iterator begin() noexcept { return base::self().begin(); }
	iterator end()   noexcept { return base::self().end(); }
	reference operator[](std::size_t i) {
		return self()[i];
	}
};

template<class Iterator, class ConstIterator, class Derived>
struct iterable_base<Iterator, ConstIterator, Derived, Mutability::Mutable>
: private iterable_base<Iterator, ConstIterator, Derived, Mutability::Moved> {
	using base =
		iterable_base<Iterator, ConstIterator, Derived, Mutability::Moved>;
	using typename base::iterator;
	using typename base::const_iterator;
	using typename base::value_type;
	using typename base::reference;
	using typename base::pointer;
	using typename base::difference_type;
	
	using typename base::const_iterable_type;
	using typename base::expiring_iterable_type;
	using typename base::moved_iterable_type;
	using typename base::derived_type;
	
	using base::self;
	using base::begin;
	using base::end;
	using base::operator[];
	
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(iterable_base<It, CIt, D, M>& rhs) {
		return static_cast<iterable_base&>(base::operator=(rhs));
	}
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(iterable_base<It, CIt, D, M>&& rhs) {
		return operator=(rhs);
	}
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(const iterable_base<It, CIt, D, M>& rhs) {
		return static_cast<iterable_base&>(base::operator=(rhs));
	}
	iterable_base& operator=(const iterable_base& rhs) {
		return operator=<>(rhs);
	}
	
	static constexpr auto upcast_to_moved(const iterable_base& x) noexcept ->
	const moved_iterable_type& {
		return static_cast<const moved_iterable_type&>(x);
	}
	
	static constexpr auto upcast_to_moved(iterable_base& x) noexcept ->
	moved_iterable_type& {
		return static_cast<moved_iterable_type&>(x);
	}
	
	static constexpr auto upcast_to_moved(iterable_base&& x) noexcept ->
	moved_iterable_type&& {
		return static_cast<const moved_iterable_type&>(x);
	}
	
	static constexpr auto downcast_to_mutable(const moved_iterable_type& x)
	noexcept -> const iterable_base& {
		return static_cast<const iterable_base&>(x);
	}
	
	static constexpr auto downcast_to_mutable(moved_iterable_type& x)
	noexcept -> iterable_base& {
		return static_cast<iterable_base&>(x);
	}
	
	static constexpr auto downcast_to_mutable(moved_iterable_type&& x)
	noexcept -> iterable_base&& {
		return static_cast<const iterable_base&>(x);
	}
};

template<class Iterator, class ConstIterator, class Derived>
struct iterable_base<Iterator, ConstIterator, Derived, Mutability::Expiring>
: iterable_base<Iterator, ConstIterator, Derived, Mutability::Mutable> {
	using base =
		iterable_base<Iterator, ConstIterator, Derived, Mutability::Mutable>;
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(iterable_base<It, CIt, D, M>& rhs) {
		return static_cast<iterable_base&>(base::operator=(rhs));
	}
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(iterable_base<It, CIt, D, M>&& rhs) {
		return operator=(rhs);
	}
	template<class It, class CIt, class D, Mutability M>
	iterable_base& operator=(const iterable_base<It, CIt, D, M>& rhs) {
		return static_cast<iterable_base&>(base::operator=(rhs));
	}
	iterable_base& operator=(const iterable_base& rhs) {
		return operator=<>(rhs);
	}
};

template<class Iterator, class ConstIterator, class Derived>
using const_iterable_base =
	iterable_base<Iterator, ConstIterator, Derived, Mutability::Constant>;
template<class Iterator, class ConstIterator, class Derived>
using expiring_iterable_base =
	iterable_base<Iterator, ConstIterator, Derived, Mutability::Expiring>;

/* is_iterable ****************************************************************/

template<class It, class CIt, class D, Mutability M>
auto is_iterable_helper(const iterable_base<It, CIt, D, M>&) ->
std::true_type /* undefined */;

auto is_iterable_helper(...) -> std::false_type /* undefined */;


template<class T>
constexpr bool is_iterable() noexcept {
	return decltype(is_iterable_helper(std::declval<const T&>()))::value;
}

template<class T>
constexpr bool is_iterable(const T&) noexcept {
	return is_iterable<T>();
}

/* const_iterable *************************************************************/

template<class It, class CIt, class D, Mutability M>
constexpr const iterable_base<It, CIt, D, Mutability::Constant>&
const_iterable(const iterable_base<It, CIt, D, M>& x) {
	return static_cast<const iterable_base<It, CIt, D, Mutability::Constant>&>(move(x));
}

/* unmove *********************************************************************/

template<class It, class CIt, class D>
constexpr auto unmove_const(const iterable_base<It, CIt, D, Mutability::Mutable>& x)
noexcept -> const iterable_base<It, CIt, D, Mutability::Mutable>& { return x; }

template<class It, class CIt, class D>
constexpr auto unmove_const(const iterable_base<It, CIt, D, Mutability::Constant>& x)
noexcept -> const iterable_base<It, CIt, D, Mutability::Constant>& { return x; }

template<class It, class CIt, class D>
constexpr auto unmove_const(const iterable_base<It, CIt, D, Mutability::Expiring>& x)
noexcept -> const iterable_base<It, CIt, D, Mutability::Mutable>& {
	return static_cast<const iterable_base<It, CIt, D, Mutability::Mutable>&>(x);
}

template<class It, class CIt, class D>
constexpr auto unmove_const(const iterable_base<It, CIt, D, Mutability::Moved>& x)
noexcept -> const iterable_base<It, CIt, D, Mutability::Mutable>& {
	return iterable_base<It, CIt, D, Mutability::Mutable>::downcast_to_mutable(x);
}

// const ref
template<class It, class CIt, class D>
constexpr auto unmove(const iterable_base<It, CIt, D, Mutability::Mutable>& x)
noexcept -> decltype(unmove_const(x)) { return unmove_const(x); }

template<class It, class CIt, class D>
constexpr auto unmove(const iterable_base<It, CIt, D, Mutability::Constant>& x)
noexcept -> decltype(unmove_const(x)) { return unmove_const(x); }

template<class It, class CIt, class D>
constexpr auto unmove(const iterable_base<It, CIt, D, Mutability::Expiring>& x)
noexcept -> decltype(unmove_const(x)) { return unmove_const(x); }

template<class It, class CIt, class D>
constexpr auto unmove(const iterable_base<It, CIt, D, Mutability::Moved>& x)
noexcept -> decltype(unmove_const(x)) { return unmove_const(x); }

// nonconst lvalue_ref
template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Mutable>& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Constant>& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Expiring>& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}
	
template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Moved>& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}

// rvalue ref
template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Mutable>&& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Constant>&& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Expiring>&& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto unmove(iterable_base<It, CIt, D, Mutability::Moved>&& x)
noexcept -> decltype(cast_const_away(unmove_const(x))) {
	return cast_const_away(unmove_const(add_const(x)));
}

/* move ***********************************************************************/
	
template<class It, class CIt, class D>
constexpr const iterable_base<It, CIt, D, Mutability::Constant>&
move_const(const iterable_base<It, CIt, D, Mutability::Constant>& x) noexcept {
	return x;
}

template<class It, class CIt, class D>
constexpr const iterable_base<It, CIt, D, Mutability::Moved>&
move_const(const iterable_base<It, CIt, D, Mutability::Expiring>& x) noexcept {
	return move_const(unmove(x));
}

template<class It, class CIt, class D>
constexpr const iterable_base<It, CIt, D, Mutability::Moved>&
move_const(const iterable_base<It, CIt, D, Mutability::Mutable>& x) noexcept {
	return iterable_base<It, CIt, D, Mutability::Mutable>::upcast_to_moved(x);
}

template<class It, class CIt, class D>
constexpr const iterable_base<It, CIt, D, Mutability::Moved>&
move_const(const iterable_base<It, CIt, D, Mutability::Moved>& x) noexcept {
	return x;
}

// const ref
template<class It, class CIt, class D>
constexpr auto move(const iterable_base<It, CIt, D, Mutability::Mutable>& x)
noexcept -> decltype(move_const(x)) { return move_const(x); }

template<class It, class CIt, class D>
constexpr auto move(const iterable_base<It, CIt, D, Mutability::Constant>& x)
noexcept -> decltype(move_const(x)) { return move_const(x); }

template<class It, class CIt, class D>
constexpr auto move(const iterable_base<It, CIt, D, Mutability::Expiring>& x)
noexcept -> decltype(move_const(x)) { return move_const(x); }

template<class It, class CIt, class D>
constexpr auto move(const iterable_base<It, CIt, D, Mutability::Moved>& x)
noexcept -> decltype(move_const(x)) { return move_const(x); }

// nonconst lvalue_ref
template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Mutable>& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Constant>& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Expiring>& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Moved>& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

// rvalue ref
template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Mutable>&& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Constant>&& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Expiring>&& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

template<class It, class CIt, class D>
constexpr auto move(iterable_base<It, CIt, D, Mutability::Moved>&& x)
noexcept -> decltype(cast_const_away(move_const(x))) {
	return cast_const_away(move_const(add_const(x)));
}

/* conditional move implementation ********************************************/

template<Mutability M, class T>
struct conditional_move_noniterable_type_trait /* undefined */;

template<class T>
struct conditional_move_noniterable_type_trait<Mutability::Constant, T> {
	using type = const typename std::remove_reference<T>::type &;
};

template<class T>
struct conditional_move_noniterable_type_trait<Mutability::Moved, T> {
	using type = typename std::remove_reference<T>::type &&;
};
template<class T>
struct conditional_move_noniterable_type_trait<Mutability::Mutable, T> {
	using type = typename std::remove_reference<T>::type &;
};
template<class T>
struct conditional_move_noniterable_type_trait<Mutability::Expiring, T>
: conditional_move_type_trait<Mutability::Moved, T> {};

template<Mutability M, class T>
using conditional_move_noniterable_type =
	typename conditional_move_noniterable_type_trait<M, T>::type;

template<Mutability M, class T>
struct conditional_move_type_trait
	<M, T, typename std::enable_if<!is_iterable<T>()>::type> {
	static constexpr conditional_move_noniterable_type<M, T> invoke(T&& x)
	noexcept {
		return static_cast<conditional_move_noniterable_type<M, T>>(x);
	}
};

template<class T>
struct conditional_move_type_trait
	<Mutability::Constant, T
	,typename std::enable_if<is_iterable<T>()>::type
	> {
	static constexpr auto invoke(T&& x) noexcept ->
	decltype(const_iterable(std::forward<T>(x))) {
		return const_iterable(std::forward<T>(x));
	}
};

template<class T>
struct conditional_move_type_trait
	<Mutability::Mutable, T
	,typename std::enable_if<is_iterable<T>()>::type
	> {
	static constexpr auto invoke(T&& x) noexcept ->
	decltype(unmove(std::forward<T>(x))) {
		return unmove(std::forward<T>(x));
	}
};

template<class T>
struct conditional_move_type_trait
	<Mutability::Moved, T
	,typename std::enable_if<is_iterable<T>()>::type
	> {
	static constexpr auto invoke(T&& x) noexcept ->
	decltype(move(std::forward<T>(x))) {
		return move(std::forward<T>(x));
	}
};

template<class T>
struct conditional_move_type_trait
	<Mutability::Expiring, T
	,typename std::enable_if<is_iterable<T>()>::type
	>
: conditional_move_type_trait<Mutability::Moved, T> {};

/******************************************************************************/

template<class Iterator, class ConstIterator, Mutability M>
struct iterator_range_view_base
: iterable_base
	<Iterator
	,ConstIterator
	,iterator_range_view_base<Iterator, ConstIterator, M>
	,M
	> {
	using base =
		iterable_base
			<Iterator
			,ConstIterator
			,iterator_range_view_base<Iterator, ConstIterator, M>
			,M
			>;
	
	using typename base::iterator;
	using typename base::const_iterator;
	using qualified_iterator_type =
		typename std::conditional
			<M == Mutability::Constant, const_iterator, iterator>::type;
	
	constexpr iterator_range_view_base
		(qualified_iterator_type init_start
		,qualified_iterator_type init_end
		)
	noexcept
	: range_start(init_start)
	, range_end(init_end) {}
	
	template<class It, class CIt, class D, Mutability MM>
	iterator_range_view_base&
	operator=(const iterable_base<It, CIt, D, MM>& rhs) {
		return static_cast<iterator_range_view_base&>(base::operator=(rhs));
	}
		
	template<class It, class CIt, class D, Mutability MM>
	iterator_range_view_base&
	operator=(iterable_base<It, CIt, D, MM>& rhs) {
		return static_cast<iterator_range_view_base&>(base::operator=(rhs));
	}
	
	template<class It, class CIt, class D, Mutability MM>
	iterator_range_view_base& operator=(iterable_base<It, CIt, D, MM>&& rhs) {
		return operator=(rhs);
	}
	
	iterator_range_view_base& operator=(const iterator_range_view_base& rhs) {
		return operator=<>(rhs);
	}
	
	using const_range_type =
		iterator_range_view_base<Iterator, ConstIterator, Mutability::Constant>;
	constexpr operator const_range_type()
	const noexcept {
		return {range_start, range_end};
	}
	
	constexpr qualified_iterator_type begin() const noexcept {
		return range_start;
	}
	
	constexpr qualified_iterator_type end() const noexcept {
		return range_end;
	}
	
	auto operator[](std::size_t n) const noexcept ->
	decltype(*std::declval<const qualified_iterator_type&>()) {
		assert(n < size(*this));
		qualified_iterator_type i = range_start;
		i += n;
		return *i;
	}
private:
	const qualified_iterator_type range_start;
	const qualified_iterator_type range_end;
};

template<class It, class CIt>
using const_iterator_range_view =
	iterator_range_view_base<It, CIt, Mutability::Constant>;
template<class It, class CIt>
using iterator_range_view =
	iterator_range_view_base<It, CIt, Mutability::Mutable>;
template<class It, class CIt>
using expiring_iterator_range_view =
	iterator_range_view_base<It, CIt, Mutability::Expiring>;

template<class It, class CIt, class D, Mutability M>
std::ptrdiff_t size(const iterable_base<It, CIt, D, M>& view) {
	return std::distance(view.begin(), view.end());
}

template
	<class LIt, class LCIt, class LD
	,class RIt, class RCIt, class RD
	>
void swap
	(iterable_base<LIt, LCIt, LD, Mutability::Mutable>& lhs
	,iterable_base<RIt, RCIt, RD, Mutability::Mutable>& rhs
	)
noexcept {
	assert(size(lhs) == size(rhs));
	using std::swap;
	auto rit = rhs.begin();
	const auto len = lhs.end();
	for(auto lit = lhs.begin() ; lit != len ; ++lit, ++rit)
		swap(*lit, *rit);
}
	
template
	<class LIt, class LCIt, class LD, Mutability LM
	,class RIt, class RCIt, class RD, Mutability RM
	>
void swap
	(iterable_base<LIt, LCIt, LD, LM>& lhs
	,iterable_base<RIt, RCIt, RD, RM>& rhs
	)
noexcept { return swap(unmove(lhs), unmove(rhs)); }

template
	<class LIt, class LCIt, class LD, Mutability LM
	,class RIt, class RCIt, class RD, Mutability RM
	>
void swap
	(iterable_base<LIt, LCIt, LD, LM>&& lhs
	,iterable_base<RIt, RCIt, RD, RM>& rhs
	)
noexcept { return swap(lhs, rhs); }
	
template
	<class LIt, class LCIt, class LD, Mutability LM
	,class RIt, class RCIt, class RD, Mutability RM
	>
void swap
	(iterable_base<LIt, LCIt, LD, LM>& lhs
	,iterable_base<RIt, RCIt, RD, RM>&& rhs
	)
noexcept { return swap(lhs, rhs); }

template
	<class LIt, class LCIt, class LD, Mutability LM
	,class RIt, class RCIt, class RD, Mutability RM
	>
void swap
	(iterable_base<LIt, LCIt, LD, LM>&& lhs
	,iterable_base<RIt, RCIt, RD, RM>&& rhs
	)
noexcept { return swap(lhs, rhs); }
} // namespace ergl

#endif // ITERABLE_BASE_HPP_
