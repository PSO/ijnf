#ifndef ITERABLE_IO_HPP_
#define ITERABLE_IO_HPP_

#include "iterable_base.hpp"
#include <iostream>
namespace ergl {

template<class It, class CIt, class D>
std::ostream& operator<<
	(std::ostream& o
	,const iterable_base<It, CIt, D, Mutability::Constant>& view
	) {
	o << '[';
	bool not_first = false;
	for(const auto& x : view) {
		if(not_first) { o << ", "; }
		o << x;
		not_first = true;
	}
	return o << ']';
}

template<class It, class CIt, class D>
	std::ostream& operator<<
	(std::ostream& o
	,const iterable_base<It, CIt, D, Mutability::Mutable>& view
	) {
	return o << const_iterable(view);
}
} // namespace ergl

#endif // ITERABLE_IO_HPP_
