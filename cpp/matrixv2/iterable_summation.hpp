#ifndef ITERABLE_SUMMATION_HPP_
#define ITERABLE_SUMMATION_HPP_

#include "iterable_base.hpp"
#include "lazy_transform.hpp"
#include <cassert>

namespace ergl {
/* operator += ****************************************************************/

template
	<class LIt, class LCIt, class LD
	,class RIt, class RCIt, class RD
	,Mutability RM
	>
LD& operator+=(iterable_base<LIt, LCIt, LD, Mutability::Mutable>& lhs
              ,const iterable_base<RIt, RCIt, RD, RM>& rhs
              ) {
	assert(size(lhs) == size(rhs));
	const auto en = lhs.end();
	auto j = rhs.begin();
	for(auto i = lhs.begin() ; i != en ; ++i, ++j) {
		*i += conditional_move<RM>(*j);
	}
	return static_cast<LD&>(lhs);
}

template
	<class LIt, class LCIt, class LD
	,class RIt, class RCIt, class RD
	,Mutability RM
	>
LD& operator+=(iterable_base<LIt, LCIt, LD, Mutability::Moved>& lhs
              ,const iterable_base<RIt, RCIt, RD, RM>& rhs
              ) {
	return unmove(lhs) += rhs;
}

template
	<class LIt, class LCIt, class LD, Mutability LM
	,class RIt, class RCIt, class RD, Mutability RM
	>
LD&& operator+=(iterable_base<LIt, LCIt, LD, LM>&& lhs
               ,const iterable_base<RIt, RCIt, RD, RM>& rhs
               ) {
	return std::move(lhs += rhs);
}

/* operator -= ****************************************************************/

template
	<class LIt, class LCIt, class LD
	,class RIt, class RCIt, class RD
	,Mutability RM
	>
LD& operator-=(iterable_base<LIt, LCIt, LD, Mutability::Mutable>& lhs
              ,const iterable_base<RIt, RCIt, RD, RM>& rhs
              ) {
	assert(size(lhs) == size(rhs));
	const auto en = lhs.end();
	auto j = rhs.begin();
	for(auto i = lhs.begin() ; i != en ; ++i, ++j) {
		*i -= conditional_move<RM>(*j);
	}
	return static_cast<LD&>(lhs);
}
	
template
	<class LIt, class LCIt, class LD
	,class RIt, class RCIt, class RD, Mutability RM
	>
LD& operator-=(iterable_base<LIt, LCIt, LD, Mutability::Moved>& lhs
              ,const iterable_base<RIt, RCIt, RD, RM>& rhs
              ) {
	return unmove(lhs) -= rhs;
}
	
template
	<class LIt, class LCIt, class LD, Mutability LM
	,class RIt, class RCIt, class RD, Mutability RM
	>
LD&& operator-=(iterable_base<LIt, LCIt, LD, LM>&& lhs
               ,const iterable_base<RIt, RCIt, RD, RM>& rhs
               ) {
	return std::move(lhs -= rhs);
}

/* details ********************************************************************/

namespace detail_iterable_summation_hpp_ {
	template
		<class Transformer, Mutability LM, Mutability RM, class LIt, class RIt>
	lazy_transform_view
		<Transformer, decorated_iterator<LIt, LM>, decorated_iterator<RIt, RM>>
	make_lazy_binary_transform_view(LIt lst, LIt len, RIt rst, RIt ren) {
		return {{lst, rst}, {len, ren}};
	}
	
	template
		<class Transformer
		,class LIt, class LCIt, class LD, Mutability LM
		,class RIt, class RCIt, class RD, Mutability RM
		>
	auto make_lazy_binary_transform_view
		(const iterable_base<LIt, LCIt, LD, LM>& lhs
		,const iterable_base<RIt, RCIt, RD, RM>& rhs
		) ->
	decltype
		(make_lazy_binary_transform_view<Transformer, LM, RM>
			 (lhs.begin(), lhs.end(), rhs.begin(), rhs.end())
		) {
		return make_lazy_binary_transform_view<Transformer, LM, RM>
			       (lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}
	
	struct addition_transformer {
		template<class Lhs, class Rhs>
		auto operator()(Lhs&& lhs, Rhs&& rhs) const ->
		decltype(std::forward<Lhs>(lhs) + std::forward<Rhs>(rhs)) {
			return std::forward<Lhs>(lhs) + std::forward<Rhs>(rhs);
		}
	};
	
	struct substraction_transformer {
		template<class Lhs, class Rhs>
		auto operator()(Lhs&& lhs, Rhs&& rhs) const ->
		decltype(std::forward<Lhs>(lhs) - std::forward<Rhs>(rhs)) {
			return std::forward<Lhs>(lhs) - std::forward<Rhs>(rhs);
		}
	};
} // namespace detail_iterable_summation_hpp_

/******************************************************************************/

template<class LIt, class LCIt, class LD, class RIt, class RCIt, class RD>
auto operator+
	(const iterable_base<LIt, LCIt, LD, Mutability::Mutable>& lhs
	,const iterable_base<RIt, RCIt, RD, Mutability::Mutable>& rhs
	) ->
decltype
	(detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		 <detail_iterable_summation_hpp_::addition_transformer>(lhs, rhs)
	) {
	return detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		       <detail_iterable_summation_hpp_::addition_transformer>(lhs, rhs);
}

template<class LIt, class LCIt, class LD, class RIt, class RCIt, class RD>
auto operator-
	(const iterable_base<LIt, LCIt, LD, Mutability::Mutable>& lhs
	,const iterable_base<RIt, RCIt, RD, Mutability::Mutable>& rhs
	) ->
decltype
	(detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		 <detail_iterable_summation_hpp_::substraction_transformer>(lhs, rhs)
	) {
	return detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		       <detail_iterable_summation_hpp_::substraction_transformer
		       >(lhs, rhs);
}

template<class LIt, class LCIt, class LD, class RIt, class RCIt, class RD>
auto operator-
	(const iterable_base<LIt, LCIt, LD, Mutability::Constant>& lhs
	,const iterable_base<RIt, RCIt, RD, Mutability::Mutable>& rhs
	) ->
decltype
	(detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		 <detail_iterable_summation_hpp_::substraction_transformer>(lhs, rhs)
	) {
	return detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		       <detail_iterable_summation_hpp_::substraction_transformer
		       >(lhs, rhs);
}

template<class LIt, class LCIt, class LD, class RIt, class RCIt, class RD>
auto operator-
	(const iterable_base<LIt, LCIt, LD, Mutability::Mutable>& lhs
	,const iterable_base<RIt, RCIt, RD, Mutability::Constant>& rhs
	) ->
decltype
	(detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		 <detail_iterable_summation_hpp_::substraction_transformer>(lhs, rhs)
	) {
	return detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		       <detail_iterable_summation_hpp_::substraction_transformer
		       >(lhs, rhs);
}

template<class LIt, class LCIt, class LD, class RIt, class RCIt, class RD>
auto operator-
	(const iterable_base<LIt, LCIt, LD, Mutability::Constant>& lhs
	,const iterable_base<RIt, RCIt, RD, Mutability::Constant>& rhs
	) ->
decltype
	(detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		 <detail_iterable_summation_hpp_::substraction_transformer>(lhs, rhs)
	) {
	return detail_iterable_summation_hpp_::make_lazy_binary_transform_view
		       <detail_iterable_summation_hpp_::substraction_transformer
		       >(lhs, rhs);
}

} // namespace ergl

#endif // ITERABLE_SUMMATION_HPP_
