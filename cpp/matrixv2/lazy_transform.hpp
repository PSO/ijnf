#ifndef LAZY_TRANSFORM_HPP_
#define LAZY_TRANSFORM_HPP_

#include "iterable_base.hpp"
#include <iterator>
#include <type_traits>
#include <tuple>

namespace ergl {

template<class It, Mutability M>
struct decorated_iterator {
	using iterator = It;
	static constexpr Mutability modifier = M;
};

namespace detail {
	template<class T>
	constexpr Mutability mutability_modifier() noexcept {
		return T::modifier;
	}
	
	template<class T>
	using iterator_type = typename T::iterator;
	
	template<std::size_t... Idx>
	struct integer_sequence {};
	
	template
		<std::size_t Count, std::size_t N = 0, class Acc = integer_sequence<>>
	struct make_integer_sequence;
	
	template<std::size_t Count, std::size_t N, std::size_t... Idx>
	struct make_integer_sequence<Count, N, integer_sequence<Idx...>>
	: make_integer_sequence<Count - 1, N + 1, integer_sequence<Idx..., N>> {};
	
	template<std::size_t N, std::size_t... Idx>
	struct make_integer_sequence<0, N, integer_sequence<Idx...>> {
		using type = integer_sequence<Idx...>;
	};
	
	template<class Transformer, class Indices, class... Iterator>
	struct lazy_transform_iterator_helper /* undefined */;
	
	template
		<class Transformer, std::size_t... Indices, class... DecoratedIterator>
	struct lazy_transform_iterator_helper
		<Transformer, integer_sequence<Indices...>, DecoratedIterator...>
	{
		static auto invoke
			(const Transformer& transform
			,const std::tuple<iterator_type<DecoratedIterator>...>& tpl
			) ->
		decltype
			(transform
				 (conditional_move<mutability_modifier<DecoratedIterator>()>
					  (*std::get<Indices>(tpl))...
				 )
			)
		{
			return transform
				       (conditional_move<mutability_modifier<DecoratedIterator>()>
					        (*std::get<Indices>(tpl))...
				       );
		}
		
		template<class... Args>
		static void dummy_function_call(Args&&...) {}
		
		static void advance
			(std::ptrdiff_t step
			,std::tuple<iterator_type<DecoratedIterator>...>& tpl
			) noexcept {
			dummy_function_call((std::get<Indices>(tpl) += step)...);
		}
	};

	template<class Transformer, class... It>
	struct lazy_transform_iterator {
		using iterator_category = std::input_iterator_tag;
		using reference =
			decltype
				(lazy_transform_iterator_helper
					 <Transformer
					 ,typename make_integer_sequence<sizeof...(It)>::type
					 ,It...
					 >::invoke
						 (std::declval<Transformer&>()
						 ,std::declval<std::tuple<iterator_type<It>...>&>()
						 )
				);
		using value_type = typename std::decay<reference>::type;
		using pointer    = proxy_pointer<value_type>;
		using difference_type = std::ptrdiff_t;
		
		lazy_transform_iterator() = default;
		
		lazy_transform_iterator
			(iterator_type<It>... init_it
			,const Transformer& transform = Transformer()
			)
		noexcept
		: content(init_it..., transform) {}
		
		lazy_transform_iterator& operator+=(difference_type n) noexcept {
			lazy_transform_iterator_helper
				<Transformer
				,typename make_integer_sequence<sizeof...(It)>::type
				,It...
				>::advance(n, content.it);
			return *this;
		}
		
		lazy_transform_iterator& operator++() noexcept {
			return operator+=(1);
		}
		
		lazy_transform_iterator operator++(int) noexcept {
			lazy_transform_iterator copy(*this);
			operator++();
			return copy;
		}
		
		lazy_transform_iterator operator+(difference_type n) const noexcept{
			lazy_transform_iterator copy(*this);
			copy += n;
			return copy;
		}
		
		reference operator*() const {
			return lazy_transform_iterator_helper
				       <Transformer
				       ,typename make_integer_sequence<sizeof...(It)>::type
				       ,It...
				       >::invoke(content, content.it);
		}
		
		pointer operator->() const {
			return pointer(**this);
		}
		
		difference_type operator-(const lazy_transform_iterator& rhs)
		const noexcept {
			return std::get<0>(content.it) - std::get<0>(rhs.content.it);
		}
		
		bool operator==(const lazy_transform_iterator& rhs) const noexcept {
			return std::get<0>(content.it) == std::get<0>(rhs.content.it);
		}
		
		bool operator!=(const lazy_transform_iterator& rhs) const noexcept {
			return std::get<0>(content.it) != std::get<0>(rhs.content.it);
		}
	private:
		struct content_type : Transformer {
			content_type
				(iterator_type<It>... init_it, const Transformer& transform)
			noexcept
			: Transformer(transform)
			, it(init_it...) {}
			
			std::tuple<iterator_type<It>...> it;
		} content;
	};
} // namespace detail

using detail::lazy_transform_iterator;

template<class Transformer, class... DecoratedIterator>
using lazy_transform_view =
	iterator_range_view_base
		<lazy_transform_iterator<Transformer, DecoratedIterator...>
		,lazy_transform_iterator<Transformer, DecoratedIterator...>
		,Mutability::Constant
		>;

} // namespace ergl

#endif // LAZY_TRANSFORM_HPP_
