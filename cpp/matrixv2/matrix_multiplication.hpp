#ifndef MATRIX_MULTIPLICATION_HPP_
#define MATRIX_MULTIPLICATION_HPP_

#include "dense_square_matrix.hpp"
#include "iterable_summation.hpp"
#include "scalar_iterable_multiplication.hpp"
#include <cassert>

namespace ergl {

template<class T>
dense_square_matrix<T>& zero_matrix(dense_square_matrix<T>& m) {
	const T zero_coef(0);
	for(auto&& row : m) { for(auto&& coef : row) { coef = zero_coef; } }
	return m;
}

// dst is assumed to be a zero matrix
template<class T, class LhsT, class RhsT>
dense_square_matrix<T>& inplace_matrix_multiplication
	(dense_square_matrix<T>& dst
	,const dense_square_matrix<LhsT>& lhs
	,const dense_square_matrix<RhsT>& rhs
	) {
	assert(lhs.width() == rhs.width());
	auto product_row_it = dst.begin();
	for(const auto& lhs_row : lhs) {
		auto rhs_row_it = rhs.begin();
		for(const auto& lhs_coef : lhs_row) {
			*product_row_it += lhs_coef * (*rhs_row_it);
			++rhs_row_it;
		}
		++product_row_it;
	}
	return dst;
}

template<class T>
dense_square_matrix<T> operator*
	(const dense_square_matrix<T>& lhs, const dense_square_matrix<T>& rhs) {
	std::size_t n = lhs.width();
	assert(n == rhs.width());
	using value_type = T;
	dense_square_matrix<T> product(n, value_type(0));
	inplace_matrix_multiplication(product, lhs, rhs);
	return product;
}

} // namespace ergl

#endif // MATRIX_MULTIPLICATION_HPP_
