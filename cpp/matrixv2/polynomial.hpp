#ifndef POLYNOMIAL_HPP_
#define POLYNOMIAL_HPP_

#include "iterable_base.hpp"
#include <iterator>
#include <vector>

namespace ergl {

template<class T>
struct polynomial : iterable_base<T*, const T*, polynomial<T>> {
public:
	using base = iterable_base<T*, const T*, polynomial<T>>;
	
	using coefficient_type            = T;
	using coefficient_reference       = coefficient_type&;
	using const_coefficient_reference = const coefficient_type&;
	using iterator                    = T*;
	using const_iterator              = const T*;
	using reference                   = typename std::iterator_traits<iterator>::reference;
	using const_reference             = typename std::iterator_traits<const_iterator>::reference;
	
	polynomial() = default;
	
	explicit polynomial(std::size_t init_deg)
	: base()
	, content(init_deg + 1, coefficient_type(0)) {
		content[init_deg] = coefficient_type(1);
	}
	
	polynomial(const polynomial&) = default;
	polynomial(polynomial&&) = default;
	
	polynomial& operator=(const polynomial&) = default;
	polynomial& operator=(polynomial&&) = default;
	
	template<class It, class CIt, class D, Mutability M>
	polynomial& operator=(iterable_base<It, CIt, D, M>& rhs) {
		return (base::template operator=(rhs)).self();
	}
	
	template<class It, class CIt, class D, Mutability M>
	polynomial& operator=(const iterable_base<It, CIt, D, M>& rhs) {
		return (base::template operator=(rhs)).self();
	}
	
	template<class It, class CIt, class D, Mutability M>
	polynomial& operator=(iterable_base<It, CIt, D, M>&& rhs) {
		return operator=<>(rhs);
	}
	
	template<class TT>
	explicit polynomial(std::initializer_list<TT> init)
	: base()
	, content() {
		using std::begin;
		std::size_t n = -1;
		std::size_t i = 0;
		for(const auto& x : init)
			{ (n = (x != coefficient_type(0) ? i : n)), ++i; }
		if(n == std::size_t(-1)) return;
		content.reserve(n);
		i = 0;
		for(auto it = begin(init) ; i < n ; ++i, ++it)
			{ content.emplace_back(*it); }
	}
	
	std::size_t degree() const noexcept { return content.size() - 1; }
	
	void normalize() {
		std::size_t n = content.size();
		if(n == 0) return;
		while(n != 0 && content[--n] == T(0));
		if(content[n] != T(0)) ++n;
		content.resize(n);
	}
	
	iterator begin() noexcept {
		return content.data();
	}
	const_iterator begin() const noexcept {
		return const_cast<polynomial&>(*this).begin();
	}
	const_iterator cbegin() const noexcept { return begin(); }
	iterator end() noexcept {
		return content.data() + content.size();;
	}
	const_iterator end() const noexcept {
		return const_cast<polynomial&>(*this).end();
	}
	const_iterator cend() const noexcept { return end(); }
	
	reference operator[](std::size_t i) noexcept {
		assert(i < content.size());
		return content[i];
	}
	const_reference operator[](std::size_t i) const noexcept {
		return const_cast<polynomial&>(*this)[i];
	}
	
	void swap(polynomial& other) noexcept {
		using std::swap;
		swap(content, other.content);
	}
	
private:
	std::vector<coefficient_type> content;
};

template<class T>
void swap(polynomial<T>& lhs, polynomial<T>& rhs) noexcept {
	lhs.swap(rhs);
}

} // namespace ergl

#endif // POLYNOMIAL_HPP_
