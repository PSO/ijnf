#ifndef RANGE_ITERATOR_HPP_
#define RANGE_ITERATOR_HPP_

#include "iterable_base.hpp"
#include <iterator>

namespace ergl {

template<std::ptrdiff_t N>
struct static_value {
	constexpr std::ptrdiff_t operator()() const noexcept { return N; }
};

struct dynamic_value {
	dynamic_value(std::ptrdiff_t init_n) noexcept : n(init_n) {}
	std::ptrdiff_t operator()() const noexcept { return n; }
private:
	std::ptrdiff_t n;
};

template<class Policy>
struct stride_policy : Policy { using Policy::Policy; };

template<class It, class CIt, bool IsConst, class StridePolicy>
struct stride_iterator_base {
public:
	using internal_iterator_type =
		typename std::conditional<IsConst, CIt, It>::type;
	using internal_traits = std::iterator_traits<internal_iterator_type>;
	using iterator_category = std::input_iterator_tag;
	using value_type        = typename internal_traits::value_type;
	using pointer           = typename internal_traits::pointer;
	using reference         = typename internal_traits::reference;
	using difference_type   = typename internal_traits::difference_type;
	
	stride_iterator_base() = default;
	
	stride_iterator_base
		(internal_iterator_type init_ptr, const StridePolicy& init_stride)
	noexcept
	: content(init_ptr, init_stride) {}
	
	operator stride_iterator_base<It, CIt, true, StridePolicy>()
	const noexcept {
		return stride_iterator_base<It, CIt, true, StridePolicy>
			       (content.ptr, static_cast<const StridePolicy&>(content));
	}
	
	stride_iterator_base& operator++() noexcept {
		content.ptr += static_cast<const StridePolicy&>(content)();
		return *this;
	}
	
	stride_iterator_base& operator+=(difference_type n) noexcept {
		content.ptr += n * static_cast<const StridePolicy&>(content)();
		return *this;
	}
	
	stride_iterator_base operator++(int) const noexcept {
		stride_iterator_base copy(*this);
		++(*this);
		return copy;
	}
	
	reference operator*() const noexcept {
		return *content.ptr;
	}
	
	internal_iterator_type operator->() const noexcept {
		return content.ptr;
	}
	
	template<bool C>
	difference_type operator-
		(const stride_iterator_base<It, CIt, C, StridePolicy>& rhs)
	const noexcept {
		return (content.ptr - rhs.content.ptr)
		       / static_cast<const StridePolicy&>(content)();
	}
	
	template<bool C>
	bool operator==(const stride_iterator_base<It, CIt, C, StridePolicy>& rhs)
	const noexcept {
		return content.ptr == rhs.content.ptr;
	}
	
	template<bool C>
	bool operator!=(const stride_iterator_base<It, CIt, C, StridePolicy>& rhs)
	const noexcept {
		return content.ptr != rhs.content.ptr;
	}
	
private:
	struct content_type : StridePolicy {
		content_type() = default;
		content_type
			(internal_iterator_type init_ptr
			,const StridePolicy& init_stride
			)
		noexcept
		: StridePolicy(init_stride)
		, ptr(init_ptr) {}
		
		internal_iterator_type ptr;
	} content;
};

template<class Policy>
struct inner_stride_policy : Policy { using Policy::Policy; };

template<class Policy>
struct end_range_policy : Policy { using Policy::Policy; };

template<class ValuePolicy, class StridePolicy>
	struct stride_away : ValuePolicy {
	using ValuePolicy::ValuePolicy;
	template<class Iterator, class Content>
	Iterator operator()(Iterator st, const Content& content)
	const noexcept {
		return st
		       + (ValuePolicy::operator()()
		          * static_cast<const StridePolicy&>(content)()
		         );
	}
};

template<class T>
struct proxy_pointer {
	template<class... Args>
	proxy_pointer(Args&&... args)
	: content(std::forward<Args>(args)...) {}
	
	T* operator->() noexcept {
		return &content;
	}
	
private:
	T content;
};

template
	<class It, class CIt, bool IsConst
	,class InnerStridePolicy, class StridePolicy, class EndRangePolicy
	>
struct range_iterator_base {
public:
	template<bool C>
	using self_type =
		range_iterator_base
			<It, CIt, C, InnerStridePolicy, StridePolicy, EndRangePolicy>;
	using internal_iterator_type =
		typename std::conditional<IsConst, CIt, It>::type;
	template<bool C>
	using sub_iterator_base =
		stride_iterator_base<It, CIt, C, InnerStridePolicy>;
	using sub_iterator      = sub_iterator_base<IsConst>;
	using iterator_category = std::input_iterator_tag;
	using value_type        =
		iterator_range_view_base
			<sub_iterator_base<false>
			,sub_iterator_base<true>
			,IsConst ? Mutability::Constant : Mutability::Mutable
			>;
	using pointer           = proxy_pointer<value_type>;
	using reference         = value_type;
	using difference_type   =
		typename std::iterator_traits<internal_iterator_type>::difference_type;
	
	range_iterator_base() = default;
	
	range_iterator_base
		(internal_iterator_type init_ptr
		,const InnerStridePolicy& init_inner_stride
		,const StridePolicy& init_stride
		,const EndRangePolicy& init_endrange
		)
	noexcept
	: content(init_ptr, init_inner_stride, init_stride, init_endrange) {}
	
	operator self_type<true>() const noexcept {
		return self_type<true>
			       (content.ptr
			       ,static_cast<const InnerStridePolicy&>(content)
			       ,static_cast<const StridePolicy&>(content)
			       ,static_cast<const EndRangePolicy&>(content)
			       );
	}
	
	range_iterator_base& operator++() noexcept {
		content.ptr += static_cast<const StridePolicy&>(content)();
		return *this;
	}
	
	range_iterator_base& operator+=(difference_type n) noexcept {
		content.ptr += n * static_cast<const StridePolicy&>(content)();
		return *this;
	}
	
	range_iterator_base operator++(int) const noexcept {
		range_iterator_base copy(*this);
		++(*this);
		return copy;
	}
	
	value_type operator*() const noexcept {
		return value_type
			       (sub_iterator(content.ptr, content)
			       ,sub_iterator
				        (static_cast<const EndRangePolicy&>(content)
					         (content.ptr, content)
				        ,content
				        )
			       );
	}
	
	pointer operator->() const noexcept {
		return pointer(**this);
	}
	
	template<bool C>
	difference_type operator-(const self_type<C>& rhs)
	const noexcept {
		return (content.ptr - rhs.content.ptr)
		       / static_cast<const StridePolicy&>(content);
	}
	
	template<bool C>
	bool operator==(const self_type<C>& rhs)
	const noexcept {
		return content.ptr == rhs.content.ptr;
	}
	
	template<bool C>
	bool operator!=(const self_type<C>& rhs)
	const noexcept {
		return content.ptr != rhs.content.ptr;
	}
	
private:
	struct content_type : InnerStridePolicy, StridePolicy, EndRangePolicy {
		content_type() = default;
		content_type
			(internal_iterator_type init_ptr
			,const InnerStridePolicy& init_inner_stride
			,const StridePolicy& init_stride
			,const EndRangePolicy& init_endrange
			)
		noexcept
		: InnerStridePolicy(init_inner_stride)
		, StridePolicy(init_stride)
		, EndRangePolicy(init_endrange)
		, ptr(init_ptr) {}
		
		internal_iterator_type ptr;
	} content;
};

} // namespace ergl

#endif // RANGE_ITERATOR_HPP_
