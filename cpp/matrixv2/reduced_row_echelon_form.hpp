#ifndef REDUCED_ROW_ECHELON_FORM_HPP_
#define REDUCED_ROW_ECHELON_FORM_HPP_

#include "iterable_base.hpp"
#include "iterable_summation.hpp"
#include "scalar_iterable_multiplication.hpp"
#include <cassert>

namespace ergl {

// gauss-jordan elimination
// note that this implementation could make coefficients grow exponentially
// on some matrices
template<class It, class CIt, class D>
iterable_base<It, CIt, D, Mutability::Moved>&
reduced_row_echelon_form(iterable_base<It, CIt, D, Mutability::Moved>& m) {
	using value_type = typename std::decay<decltype(*m.begin()->begin())>::type;
	const value_type zero_coef(0ul);
	const value_type one_coef(1ul);
	
	const auto row_en = m.end();
	auto row_it = m.begin();
	if(row_it == row_en) return m;
	std::size_t col_id = -1;
	const std::size_t max_col_id = size(*row_it);
	for( ; row_it != row_en ; ++row_it) {
		// find pivot
		auto pivot_it = row_it;
		do {
			if((++col_id) >= max_col_id) return m;
			for(pivot_it = row_it
			   ;pivot_it != row_en && (*pivot_it)[col_id] == 0
			   ;++pivot_it
			   );
		} while(pivot_it == row_en);
		if(row_it != pivot_it) { swap(*row_it, *pivot_it); } else { ++pivot_it; }
		// normalize pivot
		{	const auto alpha = one_coef/(*row_it)[col_id];
			*row_it = alpha * (*row_it);
		}
		// normalize next rows
		for( ; pivot_it != row_en ; ++pivot_it) {
			const auto alpha = (*pivot_it)[col_id];
			if(alpha != zero_coef) { *pivot_it -= alpha * (*row_it); }
		}
		// normalize previous rows
		for(auto pivot_it = m.begin() ; pivot_it != row_it ; ++pivot_it) {
			const auto alpha = (*pivot_it)[col_id];
			if(alpha != zero_coef) { *pivot_it -= alpha * (*row_it); }
		}
	}
	return m;
}

template<class It, class CIt, class D>
iterable_base<It, CIt, D, Mutability::Moved>&&
reduced_row_echelon_form(iterable_base<It, CIt, D, Mutability::Moved>&& m) {
	return std::move(reduced_row_echelon_form(m));
}

template<class It, class CIt, class D>
iterable_base<It, CIt, D, Mutability::Moved>&&
reduced_row_echelon_form(iterable_base<It, CIt, D, Mutability::Expiring>&& m) {
	return std::move(reduced_row_echelon_form(move(m)));
}

template<class It, class CIt, class D>
iterable_base<It, CIt, D, Mutability::Moved>&
reduced_row_echelon_form(iterable_base<It, CIt, D, Mutability::Expiring>& m) {
	return reduced_row_echelon_form(move(m));
}
	
} // namespace ergl

#endif // REDUCED_ROW_ECHELON_FORM_HPP_
