#ifndef SCALAR_ITERABLE_MULTIPLICATION_HPP_
#define SCALAR_ITERABLE_MULTIPLICATION_HPP_

#include "iterable_base.hpp"
#include "lazy_transform.hpp"

namespace ergl {

namespace detail_scalar_iterable_multiplication_hpp_ {
	template<Mutability M, class Transformer, class It>
	lazy_transform_view<Transformer, decorated_iterator<It, M>>
	make_lazy_transform_view(const Transformer& transform, It st, It en) {
		return {{st, transform}, {en, transform}};
	}
	
	template<class Transformer, class It, class CIt, class D, Mutability M>
	auto make_lazy_transform_view
		(const Transformer& transform, const iterable_base<It, CIt, D, M>& v)
	noexcept ->
	decltype(make_lazy_transform_view<M>(transform, v.begin(), v.end())) {
		return make_lazy_transform_view<M>(transform, v.begin(), v.end());
	}

	template<class Scalar>
	struct scalar_div {
	private:
		const Scalar& alpha;
	public:
		scalar_div(const Scalar& init_alpha) noexcept
		: alpha(init_alpha) {}
		
		template<class Lhs>
		auto operator()(Lhs&& lhs) const ->
		decltype(std::forward<Lhs>(lhs) / this->alpha) {
			return std::forward<Lhs>(lhs) / alpha;
		}
	};
	
	template<class Scalar>
	struct scalar_mul {
	private:
		const Scalar& alpha;
	public:
		scalar_mul(const Scalar& init_alpha) noexcept
		: alpha(init_alpha) {}
		
		template<class Rhs>
		auto operator()(Rhs&& rhs) const ->
		decltype(this->alpha * std::forward<Rhs>(rhs)) {
			return alpha * std::forward<Rhs>(rhs);
		}
	};
	
	template<class Scalar, class It, class CIt, class D>
	auto operator*
		(const Scalar& alpha
		,const iterable_base<It, CIt, D, Mutability::Constant>& v
		)
	noexcept ->
	decltype(make_lazy_transform_view(scalar_mul<Scalar>(alpha), v)) {
		return make_lazy_transform_view(scalar_mul<Scalar>(alpha), v);
	}
	
	template<class Scalar, class It, class CIt, class D>
	auto operator*
		(const Scalar& alpha
		,const iterable_base<It, CIt, D, Mutability::Mutable>& v
		)
	noexcept ->
	decltype(make_lazy_transform_view(scalar_mul<Scalar>(alpha), v)) {
		return make_lazy_transform_view(scalar_mul<Scalar>(alpha), v);
	}
	
	template<class Scalar, class It, class CIt, class D>
	auto operator/
		(const iterable_base<It, CIt, D, Mutability::Constant>& v
		,const Scalar& alpha
		)
	noexcept ->
	decltype(make_lazy_transform_view(scalar_div<Scalar>(alpha), v)) {
		return make_lazy_transform_view(scalar_div<Scalar>(alpha), v);
	}
	
	template<class Scalar, class It, class CIt, class D>
	auto operator/
		(const iterable_base<It, CIt, D, Mutability::Mutable>& v
		,const Scalar& alpha
		)
	noexcept ->
	decltype(make_lazy_transform_view(scalar_div<Scalar>(alpha), v)) {
		return make_lazy_transform_view(scalar_div<Scalar>(alpha), v);
	}
} // namespace detail_scalar_iterable_multiplication_hpp_

using detail_scalar_iterable_multiplication_hpp_::operator*;
using detail_scalar_iterable_multiplication_hpp_::operator/;

} // namespace ergl

#endif // SCALAR_ITERABLE_MULTIPLICATION_HPP_
