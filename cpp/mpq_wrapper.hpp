#ifndef MPQ_WRAPPER_HPP_
#define MPQ_WRAPPER_HPP_

#include "mpz_wrapper.hpp"
#include <gmp.h>
#include <iostream>

struct mpq_wrapper {
	mpq_wrapper() noexcept { mpq_init(content); }
	explicit mpq_wrapper(double d) noexcept : mpq_wrapper() {
		mpq_set_d(content, d);
	}
	mpq_wrapper(unsigned long int num, unsigned long int denom) noexcept
	: mpq_wrapper() {
		mpq_set_ui(content, num, denom);
	}
	mpq_wrapper(signed long int num, signed long int denom) noexcept
	: mpq_wrapper() {
		mpq_set_si(content, num, denom);
	}
	
	~mpq_wrapper() { mpq_clear(content); }
	
	mpq_wrapper(const mpq_wrapper& other) noexcept : mpq_wrapper() {
		*this = other;
	}
	
	mpq_wrapper(const mpz_wrapper& other) noexcept : mpq_wrapper() {
		mpq_set_z(content, other.content);
	}
	
	mpq_wrapper(mpq_wrapper&& other) noexcept : mpq_wrapper() {
		*this = std::move(other);
	}
	
	mpq_wrapper& operator=(const mpq_t& rhs) noexcept {
		mpq_set(content, rhs);
		return *this;
	}
	mpq_wrapper& operator=(const mpq_wrapper& rhs) noexcept {
		return (*this) = rhs.content;
	}
	
	mpq_wrapper& operator=(const mpz_t& rhs) noexcept {
		mpq_set_z(content, rhs);
		return *this;
	}
	mpq_wrapper& operator=(const mpz_wrapper& rhs) noexcept {
		return (*this) = rhs.content;
	}
	
	mpq_wrapper& operator=(mpq_wrapper&& rhs) noexcept {
		swap(rhs);
		return *this;
	}
	void swap(mpq_wrapper& rhs) noexcept {
		mpq_swap(content, rhs.content);
	}
	void canonicalize() noexcept {
		mpq_canonicalize(content);
	}
	mpq_wrapper/*&&*/  operator-() && noexcept {
		mpq_neg(content, content);
		return std::move(*this);
	}
	mpq_wrapper operator-() const & noexcept {
		mpq_wrapper copy(*this);
		mpq_neg(copy.content, content);
		return copy;
	}
	mpq_wrapper/*&&*/  abs() && noexcept {
		mpq_abs(content, content);
		return std::move(*this);
	}
	mpq_wrapper abs() const & noexcept {
		mpq_wrapper copy(*this);
		mpq_abs(copy.content, content);
		return copy;
	}
	mpq_wrapper inv() && noexcept {
		mpq_inv(content, content);
		return std::move(*this);
	}
	mpq_wrapper inv() const & noexcept {
		mpq_wrapper copy(*this);
		mpq_inv(copy.content, content);
		return copy;
	}
	
	int sign() const noexcept {
		return mpq_sgn(content);
	}
	
	bool operator==(const mpq_wrapper& rhs) const noexcept {
		return mpq_equal(content, rhs.content) != 0;
	}
	bool operator!=(const mpq_wrapper& rhs) const noexcept {
		return !(*this == rhs);
	}
	
	bool operator<(const mpq_wrapper& rhs) const noexcept {
		return mpq_cmp(content, rhs.content) < 0;
	}
	bool operator>(const mpq_wrapper& rhs) const noexcept {
		return mpq_cmp(content, rhs.content) > 0;
	}
	bool operator>=(const mpq_wrapper& rhs) const noexcept {
		return !(mpq_cmp(content, rhs.content) < 0);
	}
	bool operator<=(const mpq_wrapper& rhs) const noexcept {
		return !(mpq_cmp(content, rhs.content) > 0);
	}
	
	mpz_wrapper numerator() const noexcept {
		mpz_wrapper result;
		result = mpq_numref(content);
		return result;
	}
	
	mpz_wrapper denominator() const noexcept {
		mpz_wrapper result;
		result = mpq_denref(content);
		return result;
	}
	
	explicit operator double() const noexcept {
		return mpq_get_d(content);
	}
	
#	define CANONICALIZE
#	define GMP_OPERATOR_DEF_tuple (mpq, add, +)
#	include "gmp_operator.def.hpp"
#	define CANONICALIZE
#	define GMP_OPERATOR_DEF_tuple (mpq, sub, -)
#	include "gmp_operator.def.hpp"
#	define CANONICALIZE
#	define GMP_OPERATOR_DEF_tuple (mpq, mul, *)
#	include "gmp_operator.def.hpp"
#	define CANONICALIZE
#	define GMP_OPERATOR_DEF_tuple (mpq, div, /)
#	include "gmp_operator.def.hpp"
/*
#	define GMP_OPERATOR_DEF_tuple (mul_2exp, <<) // rhs is mp_bitcnt_t!
#	include "gmp_operator.def.hpp"
#	define GMP_OPERATOR_DEF_tuple (div_2exp, >>) // rhs is mp_bitcnt_t!
#	include "gmp_operator.def.hpp"
//*/
private:
	friend std::ostream& operator<<(std::ostream& o, const mpq_wrapper& q) {
		char* str = mpq_get_str(nullptr, 10, q.content);
		o << str;
		void (*free_function)(void*, size_t);
		mp_get_memory_functions(nullptr, nullptr, &free_function);
		free_function(str, 0 /* ignored */);
		return o;
	}
	
	mpq_t content;
};

/*
namespace Eigen {
	template<>
	struct NumTraits<mpq_wrapper> {
		using Real       = mpq_wrapper;
		using NonInteger = mpq_wrapper;
		using Nested     = mpq_wrapper;
		
		enum {
			IsComplex = 0,
			IsInteger = 0,
			IsSigned = 1,
			RequiredInitialization = 1,
			ReadCost = 5, // cpu cycles of a move (rough estimate)
			AddCost = 20, // cpu cycles of an add (rough estimate)
			MulCost = 20 // cpu cycles of a mul (rough estimate)
		};
		
		static constexpr Real epsilon() {
			//return ?;
		}
		
		static constexpr Real dummy_precision() {
			//return ?;
		}
		
		static constexpr Real highest() {
			//return ?;
		}
		
		static constexpr Real lowest() {
			//return ?;
		}
	}
} // namespace Eigen
//*/

#endif
