#ifndef MPZ_WRAPPER_HPP_
#define MPZ_WRAPPER_HPP_

#include <gmp.h>
#include <iostream>

struct mpq_wrapper;

struct mpz_wrapper {
	mpz_wrapper() { mpz_init(content); }
	explicit mpz_wrapper(unsigned long int uli) {
		mpz_init_set_ui(content, uli);
	}
	explicit mpz_wrapper(signed long int sli) {
		mpz_init_set_si(content, sli);
	}
	explicit mpz_wrapper(double d) {
		mpz_init_set_d(content, d);
	}
	
	~mpz_wrapper() { mpz_clear(content); }
	
	mpz_wrapper(const mpz_wrapper& rhs) {
		mpz_init_set(content, rhs.content);
	}
	mpz_wrapper(mpz_wrapper&& other) noexcept {
		mpz_init(content);
		swap(other);
	}
	mpz_wrapper& operator=(const mpz_t rhs) noexcept {
		mpz_set(content, rhs);
		return *this;
	}
	mpz_wrapper& operator=(const mpz_wrapper& rhs) {
		return (*this) = rhs.content;
	}
	mpz_wrapper& operator=(mpz_wrapper&& rhs) noexcept {
		swap(rhs);
		return *this;
	}
	void swap(mpz_wrapper& rhs) noexcept {
		mpz_swap(content, rhs.content);
	}
	mpz_wrapper&& operator-() && {
		mpz_neg(content, content);
		return std::move(*this);
	}
	mpz_wrapper operator-() const & {
		mpz_wrapper copy(*this);
		mpz_neg(copy.content, content);
		return copy;
	}
	mpz_wrapper&& abs() && {
		mpz_abs(content, content);
		return std::move(*this);
	}
	mpz_wrapper abs() const & {
		mpz_wrapper copy(*this);
		mpz_abs(copy.content, content);
		return copy;
	}
	
	int sign() const noexcept {
		return mpz_sgn(content);
	}
	
	bool operator==(const mpz_wrapper& rhs) const noexcept {
		return mpz_cmp(content, rhs.content) == 0;
	}
	bool operator!=(const mpz_wrapper& rhs) const noexcept {
		return !(*this == rhs);
	}
	
	bool operator<(const mpz_wrapper& rhs) const noexcept {
		return mpz_cmp(content, rhs.content) < 0;
	}
	bool operator>(const mpz_wrapper& rhs) const noexcept {
		return mpz_cmp(content, rhs.content) > 0;
	}
	bool operator>=(const mpz_wrapper& rhs) const noexcept {
		return !(mpz_cmp(content, rhs.content) < 0);
	}
	bool operator<=(const mpz_wrapper& rhs) const noexcept {
		return !(mpz_cmp(content, rhs.content) > 0);
	}
	
	static mpz_wrapper gcd(const mpz_wrapper& lhs, const mpz_wrapper& rhs)
	noexcept {
		mpz_wrapper result;
		mpz_gcd(result.content, lhs.content, rhs.content);
		return result;
	}
	
	static mpz_wrapper&& gcd(mpz_wrapper&& lhs, const mpz_wrapper& rhs)
	noexcept {
		mpz_gcd(lhs.content, lhs.content, rhs.content);
		return std::move(lhs);
	}
	
	static mpz_wrapper&& gcd(mpz_wrapper&& lhs, mpz_wrapper&& rhs)
	noexcept {
		return gcd(std::move(lhs), static_cast<const mpz_wrapper&>(rhs));
	}
	static mpz_wrapper&& gcd(const mpz_wrapper& lhs, mpz_wrapper&& rhs)
	noexcept {
		return gcd(std::move(rhs), lhs);
	}
	
	explicit operator unsigned long int() const noexcept {
		return mpz_get_ui(content);
	}
	
	explicit operator signed long int() const noexcept {
		return mpz_get_si(content);
	}
	
	explicit operator double() const noexcept {
		return mpz_get_d(content);
	}
	
	/*
	mpz_t numerator() const noexcept {
		return mpz_numref(content);
	}
	mpz_t denominator() const noexcept {
		return mpz_denref(content);
	}*/
	
#	define GMP_OPERATOR_DEF_tuple (mpz, add, +)
#	include "gmp_operator.def.hpp"
#	define GMP_OPERATOR_DEF_tuple (mpz, sub, -)
#	include "gmp_operator.def.hpp"
#	define GMP_OPERATOR_DEF_tuple (mpz, mul, *)
#	include "gmp_operator.def.hpp"
#	define GMP_OPERATOR_DEF_tuple (mpz, tdiv_q, /)
#	include "gmp_operator.def.hpp"
#	define GMP_OPERATOR_DEF_tuple (mpz, tdiv_r, %)
#	include "gmp_operator.def.hpp"
/*
#	define GMP_OPERATOR_DEF_tuple (mul_2exp, <<) // rhs is mp_bitcnt_t!
#	include "gmp_operator.def.hpp"
#	define GMP_OPERATOR_DEF_tuple (tdiv_2exp, >>) // rhs is mp_bitcnt_t!
#	include "gmp_operator.def.hpp"
//*/
private:
	friend struct mpq_wrapper;
	friend std::ostream& operator<<(std::ostream& o, const mpz_wrapper& q) {
		char* str = mpz_get_str(nullptr, 10, q.content);
		o << str;
		void (*free_function)(void*, size_t);
		mp_get_memory_functions(nullptr, nullptr, &free_function);
		free_function(str, 0 /* ignored */);
		return o;
	}
	/*
	friend std::istream& operator>>(std::istream& i, const mpz_wrapper& q) {
		//0x / 0X -> hexadecimal
		//0b / 0B -> binary
		//0 -> octal
		mpq_set_str (q, );
		return i;
	}*/
	
	mpz_t content;
};

/*
namespace Eigen {
	template<>
	struct NumTraits<mpz_wrapper> {
		using Real       = mpz_wrapper;
		using NonInteger = ?;
		using Nested     = mpz_wrapper;
		
		enum {
			IsComplex = 0,
			IsInteger = 0,
			IsSigned = 1,
			RequiredInitialization = 1,
			ReadCost = 5, // cpu cycles of a move (rough estimate)
			AddCost = 10, // cpu cycles of an add (rough estimate)
			MulCost = 10 // cpu cycles of a mul (rough estimate)
		};
		
		static constexpr Real epsilon() {
			//return ?;
		}
		
		static constexpr Real dummy_precision() {
			//return ?;
		}
		
		static constexpr Real highest() {
			//return ?;
		}
		
		static constexpr Real lowest() {
			//return ?;
		}
	}
} // namespace Eigen
//*/

#endif // MPZ_WRAPPER_HPP_
