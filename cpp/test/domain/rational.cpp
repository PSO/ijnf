#include "domain/rational.hpp"
#include "traits/field_traits.hpp"
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include <random>
#include <iosfwd>

namespace {
	using rational_domain = field_of<rational<std::int_least64_t>>;
	using rational_t      = typename rational_domain::type;
	using traits          = field_traits<rational_domain>;
	
	template
		<class Q
		,class DividendDist = std::uniform_int_distribution<int>
		,class DivisorDist  = std::uniform_int_distribution<int>
		>
	struct rational_distribution {
		using dividend_distribution_t = DividendDist;
		using divisor_distribution_t  = DivisorDist;
		using domain_type = Q;
		using traits      = field_traits<domain_type>;
		using result_type = typename traits::type;
		using dividend_param_type =
			typename dividend_distribution_t::param_type;
		using divisor_param_type  =
			typename divisor_distribution_t::param_type;
		using param_type = std::pair<dividend_param_type, divisor_param_type>;
		
		rational_distribution() = default;
		rational_distribution
			(const dividend_param_type& pdividend
			,const divisor_param_type& pdivisor
			)
		: dividend_distribution(pdividend)
		, divisor_distribution(pdivisor) {
			assert(divisor_distribution.min() > 0);
		}
		
		param_type param() const noexcept {
			return std::make_pair
				       (dividend_distribution.param()
				       ,divisor_distribution.param()
				       );
		}
		void param(const param_type& p) {
			dividend_distribution.param(p.first);
			divisor_distribution.param(p.second);
			assert(divisor_distribution.min() > 0);
		}
		
		template<class UniformRandomBitGenerator>
		result_type operator()(UniformRandomBitGenerator& rng) {
			return result_type
				       (dividend_distribution(rng)
				       ,divisor_distribution(rng)
				       );
		}
		
		result_type max() const noexcept {
			return result_type
				       (dividend_distribution.max()
				       ,divisor_distribution.min()
				       );
		}
		
		result_type min() const noexcept {
			return result_type
				       (dividend_distribution.min()
				       ,dividend_distribution.min() < 0
					        ? divisor_distribution.min()
					        : divisor_distribution.max()
				       );
		}
		
		void reset() {
			divisor_distribution.reset();
			dividend_distribution.reset();
		}
		
		bool operator==(const rational_distribution& other) const noexcept {
			return divisor_distribution == other.divisor_distribution
			       && dividend_distribution == other.dividend_distribution;
		}
		
		bool operator!=(const rational_distribution& other) const noexcept {
			return divisor_distribution != other.divisor_distribution
			       || dividend_distribution != other.dividend_distribution;
		}
	private:
		friend std::ostream& operator<<
			(std::ostream& o, const rational_distribution& d) {
			return o << d.dividend_distribution << ' '
			         << d.divisor_distribution;
		}
		friend std::istream& operator<<
			(std::istream& i, rational_distribution& d) {
			return i >> d.dividend_distribution >> d.divisor_distribution;
		}
		dividend_distribution_t dividend_distribution;
		divisor_distribution_t  divisor_distribution;
	};
	
	namespace bdata = boost::unit_test::data;
	
	using param_type = typename std::uniform_int_distribution<int>::param_type;
	auto rdgen =
		bdata::random((bdata::seed = 100UL
		              ,bdata::distribution =
			               rational_distribution<rational_domain>
				               (param_type(-100000, 100000)
				               ,param_type(1, 100000)
				               )
		              ));
	auto sample_size = bdata::xrange(100);
} // anonymous namespace

BOOST_AUTO_TEST_SUITE(rational_arithmetic)

BOOST_DATA_TEST_CASE(zero_is_identity, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((traits::eval(domain, x_s + zerosymb == x_s)));
	BOOST_TEST((traits::eval(domain, zerosymb + x_s == x_s)));
	BOOST_TEST((traits::eval(domain, x_s - zerosymb == x_s)));
}

BOOST_DATA_TEST_CASE(successor_inequality, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((traits::eval(domain, x_s + onesymb != x_s)));
}

BOOST_DATA_TEST_CASE
	(addition_commutativity, rdgen ^ rdgen ^ sample_size, x, y, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	BOOST_TEST((traits::eval(domain, x_s + y_s == y_s + x_s)));
}

BOOST_DATA_TEST_CASE
	(addition_associativity
	,rdgen ^ rdgen ^ rdgen ^ sample_size
	,x, y, z, idx
	) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	auto z_s = mksymb(z);
	BOOST_TEST((traits::eval(domain, (x_s + y_s) + z_s == x_s + (y_s + z_s))));
}

BOOST_DATA_TEST_CASE
	(opposite_involutivity, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((traits::eval(domain, -(-x_s) == x_s)));
}

BOOST_DATA_TEST_CASE
	(add_sub_is_identity, rdgen ^ rdgen ^ sample_size, x, y, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	BOOST_TEST((traits::eval(domain, (x_s + y_s) - y_s == x_s)));
	BOOST_TEST((traits::eval(domain, (x_s - y_s) + y_s == x_s)));
}

BOOST_DATA_TEST_CASE(opposite_is_zero_sub, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((traits::eval(domain, zerosymb - x_s == -x_s)));
}

BOOST_DATA_TEST_CASE
	(multiplication_commutativity
	,rdgen ^ rdgen ^ sample_size
	,x, y, idx
	) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	BOOST_TEST((traits::eval(domain, x_s * y_s == y_s * x_s)));
}

BOOST_DATA_TEST_CASE
	(multiplication_associativity
	,rdgen ^ rdgen ^ rdgen ^ sample_size
	,x, y, z, idx
	) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	auto z_s = mksymb(z);
	BOOST_TEST((traits::eval(domain, (x_s * y_s) * z_s == x_s * (y_s * z_s))));
}

BOOST_DATA_TEST_CASE(zero_is_absorbant, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((traits::eval(domain, x_s * zerosymb == zerosymb)));
	BOOST_TEST((traits::eval(domain, zerosymb * x_s == zerosymb)));
}

BOOST_DATA_TEST_CASE(one_is_identity, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((traits::eval(domain, x_s * onesymb == x_s)));
	BOOST_TEST((traits::eval(domain, onesymb * x_s == x_s)));
	BOOST_TEST((traits::eval(domain, x_s / onesymb == x_s)));
}

BOOST_DATA_TEST_CASE(inverse_is_involutive, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((traits::eval(domain, x_s == zerosymb || inv(inv(x_s)) == x_s)));
}

BOOST_DATA_TEST_CASE
	(mul_div_is_identity, rdgen ^ rdgen ^ sample_size, x, y, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	BOOST_TEST((
		traits::eval(domain, y_s == zerosymb || (x_s * y_s) / y_s == x_s)
	));
	BOOST_TEST((
		traits::eval(domain, y_s == zerosymb || (x_s / y_s) * y_s == x_s)
	));
}

BOOST_DATA_TEST_CASE(inverse_is_one_div, rdgen ^ sample_size, x, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	BOOST_TEST((
		traits::eval(domain, x_s == zerosymb || onesymb / x_s == inv(x_s))
	));
}

BOOST_DATA_TEST_CASE
	(left_distributivity, rdgen ^ rdgen ^ rdgen ^ sample_size, x, y, a, idx) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	auto a_s = mksymb(a);
	BOOST_TEST((
		traits::eval(domain, a_s * (x_s + y_s) == (a_s * x_s) + (a_s * y_s))
	));
}

BOOST_DATA_TEST_CASE
	(right_distributivity
	,rdgen ^ rdgen ^ rdgen ^ sample_size
	,x, y, a, idx
	) {
	(void) idx;
	rational_domain domain;
	auto x_s = mksymb(x);
	auto y_s = mksymb(y);
	auto a_s = mksymb(a);
	BOOST_TEST((
		traits::eval(domain, (x_s + y_s) * a_s == (x_s * a_s) + (y_s * a_s))
	));
}
BOOST_AUTO_TEST_SUITE_END() // rational_arithmetic
