#ifndef JORDAN_TYPES_HPP_
#define JORDAN_TYPES_HPP_

#include "domain/complex.hpp"
#include "domain/interval.hpp"
#include "domain/polynomial.hpp"
#include "domain/dense_matrix.hpp"
#include "traits/field_traits.hpp"
#include "traits/vector_space_traits.hpp"

#include "mpq_wrapper.hpp"
#include "configuration.hpp"

//using rational_type         = rational<int64_t>;
using rational_type         = mpq_wrapper;
using real_type             = double;
using complex_type          = cartesian_complex<real_type>;
using interval_type         = interval<real_type>;
using complex_interval_type = cartesian_complex<interval_type>;

using rational_field               = field_of<rational_type>;
using real_field                   = field_of<real_type>;
using complex_field                = field_of<complex_type>;
using interval_pseudofield         = field_of<interval_type>;
using complex_interval_pseudofield = field_of<complex_interval_type>;

using rational_polynomial_type  = polynomial<rational_field>;
using real_polynomial_type      = polynomial<real_field>;
using complex_polynomial_type   = polynomial<complex_field>;
using interval_polynomial_type  = polynomial<interval_pseudofield>;
using complex_interval_polynomial_type =
	polynomial<complex_interval_pseudofield>;

using rational_matrix_type = dense_matrix<rational_field>;
using module               = vector_space_traits<matrix_set<rational_matrix_type>>;
using interval_matrix_type = dense_matrix<interval_pseudofield>;

#endif // JORDAN_TYPES_HPP_
