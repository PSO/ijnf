
module MathPP = struct
  type t = [`PrintOne | `Print of (Format.formatter -> unit -> unit)]
  let product =
    List.fold_left
      (fun acc term ->
        match acc, term with
        | `PrintOne, x
          | x, `PrintOne -> x
        | `Print pp1, `Print pp2 ->
           `Print (fun fmt () ->
               Format.fprintf fmt "%a * %a"
                 pp1 () pp2 ())) `PrintOne
  let printer math_pp fmt x = match math_pp x with
    | `PrintOne -> Format.pp_print_char fmt '1'
    | `Print pp -> pp fmt ()
end

module JordanCoeff = struct
  type t = {
      lambda : Real.t;
      diag : int;
      trigo : [`Cos | `Sin];
      angle : Real.t;
    }
  let basic l k = {
      lambda = l;
      diag = k;
      angle = Real.zero;
      trigo = `Cos; }
  let trigo f t l k = {
      lambda = l;
      diag = k;
      angle = t;
      trigo = f; }
  let math_pp c =
    let pp_n_minus_k fmt () =
      if c.diag = 0 then Format.pp_print_char fmt 'n'
      else Format.fprintf fmt "(n-%d)" c.diag in
    let pp_exp =
      if Real.is_one c.lambda then `PrintOne
      else `Print (fun fmt () -> Format.fprintf fmt "%a**%a"
                               Real.print c.lambda pp_n_minus_k ()) in
    let pp_comb =
      if c.diag = 0 then `PrintOne
      else `Print (fun fmt () -> if c.diag = 1 then Format.pp_print_char fmt 'n'
                                 else Format.fprintf fmt "(%d among n)" c.diag) in
    let pp_trig =
      if c.trigo = `Cos && Real.is_zero c.angle then `PrintOne
      else `Print (fun fmt () -> Format.fprintf fmt "%s(pi*%a*%a)"
                               (match c.trigo with `Cos -> "cos" | `Sin -> "sin")
                               Real.print c.angle pp_n_minus_k ()) in
    MathPP.product [pp_exp; pp_comb; pp_trig]
end

module JordanTerm = struct
  type t = Real.t * JordanCoeff.t
  let basic l k = Real.one, JordanCoeff.basic l k
  let trigo f t l k = Real.one, JordanCoeff.trigo f t l k
  let neg (x, c) = Real.neg x, c
  let mul x (y, c) = Real.mul x y, c
  let math_pp (x, c) =
    let pp_x = if Real.is_one x then `PrintOne else `Print (fun fmt () -> Real.print fmt x) in
    MathPP.product [pp_x; JordanCoeff.math_pp c]
  let print = MathPP.printer math_pp
end

module JordanBlock = struct
  type lambda = Lambda of Real.t
  type theta = Theta of Real.t
  type t = {
      size : int;
      nature : [`Single of lambda
               |`Pair of lambda * theta] }
  let of_output = function
    | `JB (c, l) -> { size = c; nature = `Single (Lambda l) }
    | `RJB (c, l, a) ->
       { size = 2 * c; nature = `Pair (Lambda l, Theta a) }
  let exp_matrix b =
    let n = b.size in
    let e meta_row meta_col ~row ~col =
      if meta_row > meta_col
      then []
      else
        let k = meta_col - meta_row in
        let t = match b.nature with
          | `Single (Lambda l) -> JordanTerm.basic l k
          | `Pair (Lambda l, Theta t) ->
             match (row, col) with
             | (0,0)
             | (1,1) -> JordanTerm.trigo `Cos t l k
             | (0,1) -> JordanTerm.neg (JordanTerm.trigo `Sin t l k)
             | (1,0) -> JordanTerm.trigo `Sin t l k
             | _ -> assert false in
        [t] in
    let f ~row ~col = match b.nature with
      | `Single _ -> e row col
                       ~row:0 ~col:0
      | `Pair _ -> e (row / 2) (col / 2)
                     ~row:(row mod 2) ~col:(col mod 2) in
    Matrix_utilities.matrix_of_function (n, n) f
end

let pp_array pp_cell fmt a =
  Format.pp_print_char fmt '[';
  for i = 0 to Array.length a - 1 do
    if i <> 0 then Format.pp_print_char fmt ',';
    pp_cell fmt a.(i)
  done;
  Format.pp_print_char fmt ']'

module Input = struct
  type cell = Mpqf.t
  type t = cell array array
  let format input =
    let pp_rational fmt q = Format.fprintf fmt "S('%s')/S('%s')"
                              (Mpzf.to_string (Mpqf.get_num q))
                              (Mpzf.to_string (Mpqf.get_den q)) in
    Format.asprintf "Matrix(%a)"
      (pp_array (pp_array pp_rational)) input

end

module OutputCell = struct
  type t = JordanTerm.t list
  let zero = []
  let term t = [t]
  let rec add_term (x, c) = function
    | [] -> [(x, c)]
    | (y, d) :: r when c = d ->
       let z = Real.add x y in
       if Real.is_zero z then r else  (z, c) :: r
    | t :: r -> t :: add_term (x, c) r
  let rec add l = function
    | [] -> l
    | t :: r -> add (add_term t l) r
  let add = List.append
  let mul r c =
    if Real.is_zero r
    then zero
    else List.map (JordanTerm.mul r) c
  let print fmt l =
    if l = [] then Format.pp_print_char fmt '0'
    else Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt " +@ ")
           JordanTerm.print fmt l
end

module Output = struct
  module Cell = OutputCell
  type cell = Cell.t
  type t = cell array array

  let print = Matrix_utilities.print_matrix Cell.print
  let of_jnf_result (p, q, jbs) =
    let open Matrix_utilities in
    let passage m =
      matrix_of_nested_lists m in
    let p = passage p in
    let q = passage q in
    let j =
      matrix_of_diagonal_blocks
        Cell.zero
        (List.map (fun jb ->
             JordanBlock.exp_matrix
               (JordanBlock.of_output jb)) jbs) in
    Format.eprintf "J = %a@\n@?" print j;
    Format.eprintf "P = %a@\n@?" (Matrix_utilities.print_matrix Real.print) p;
    let p_j = matrix_product
                ~add:Cell.add
                ~mul:Cell.mul
                p j in
    Format.eprintf "P * J = %a@\n@?" print p_j;
    Format.eprintf "Q = %a@\n@?" (Matrix_utilities.print_matrix Real.print) q;
    let p_j_q = matrix_product
                     ~add:Cell.add
                     ~mul:(fun cell real -> Cell.mul real cell)
                     p_j q in
    Format.eprintf "P * J * Q = %a@\n@?" print p_j_q;
    p_j_q
end

exception AccelerationError of string

let perform_jnf input =
  let command_name = "ijnf" in
  let cmd = Printf.sprintf "%s \"%s\"" command_name input in
  let ic = Unix.open_process_in cmd in
  let res = Parse_jnf_output.parse ic "<pipe>" in
  match Unix.close_process_in ic with
  | Unix.WEXITED 0 -> begin
      match res with
      | `Success res -> res
      | `Failure msg -> raise (AccelerationError msg)
    end
  | Unix.WEXITED n -> raise (AccelerationError (Printf.sprintf "%s exited with code %d" command_name n))
  | _ -> raise (AccelerationError (Printf.sprintf "%s terminated abnormaly" command_name))

let accelerate input =
  let input = Input.format input in
  Format.eprintf "INPUT: %s@\n@?" input;
  let jnf_res = perform_jnf input in
  Output.of_jnf_result jnf_res
