(**
Symbolic computation of M{^n}.

@version 0.9
@author Marc Pasqualetto
@author Pascal Sotin
 *)

(** Input of the acceleration. *)
module Input : sig
  (** The content of an input cell is a {i rational}. *)
  type cell = Mpqf.t

  (** The input of the acceleration is a {i square} matrix of input cells (array of {i rows}). *)
  type t = cell array array

end

(** Jordan coefficients appearing in the acceleration output. *)
module JordanCoeff : sig
  (** A Jordan coefficient stands for: [(lambda ^ (n - diag)) * binomial(n, diag) * trigo(pi * angle * (n - diag))] where [n] represents the exponent.
   *)
  type t = {
      lambda : Real.t;
      diag : int;
      trigo : [`Cos | `Sin];
      angle : Real.t;
    }
end

(** Output of the acceleration. *)
module Output : sig
  (** The content of an output cell is a {i weighted sum} of Jordan coefficients. *)
  type cell = (Real.t * JordanCoeff.t) list
  (** The output of the acceleration is a {i square} matrix of output cells (array of {i rows}). *)
  type t = cell array array
  (** Pretty-printer. *)
  val print : Format.formatter -> t -> unit
end

(** Denotes an error during the acceleration. Presently, the acceleration may fail because:
- The root-finding algorithm (Jenkins-Traub or Laguerre) failed to approximate the roots of a polynomial,
- The root-bounding algorithm (Gershgorin) failed to isolate the roots of a polynomial,
 *)
exception AccelerationError of string

(** Computes a symbolic output matrix M{^n} for an input matrix M.

@raise InvalidArgument if the input is malformed,
@raise AccelerationError if the acceleration failed.
*)
val accelerate : Input.t -> Output.t
