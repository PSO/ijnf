(** Real numbers (or {i approximation} thereof)
    used by the Jordan terms. *)

let list_max cmp =
  let rec aux_list_max cur = function
    | [] -> cur
    | alt :: tail ->
       let cur = if cmp alt cur > 0 then alt else cur in
       aux_list_max cur tail
  in
  function
  | h :: t -> aux_list_max h t
  | [] -> invalid_arg "empty list"

let list_min cmp = list_max (fun a b -> ~- (cmp a b))

(** Rational numbers. *)
module Rational = struct
  type t = Mpqf.t
  let zero = Mpqf.of_int 0
  let one = Mpqf.of_int 1
  let neg = Mpqf.neg
  let sign q = match Mpqf.sgn q with
    | 1 -> `Pos
    | 0 -> `Zero
    | _ -> `Neg
  let is_zero q = (sign q = `Zero)
  let is_one q = Mpqf.equal q one
  let of_number n = Mpqf.of_mpz (Mpz.of_string n)
  let of_fraction n d = Mpqf.of_mpz2 (Mpz.of_string n) (Mpz.of_string d)
  let of_mpq = Mpqf.of_mpq
  let mul = Mpqf.mul
  let add = Mpqf.add
  let print = Mpqf.print
end

(** Interval approximation of real numbers. *)
module Interval = struct
  type t = Mpfrf.t * Mpfrf.t
  let monotone f (l, u) =
    let l = f l Mpfr.Down in
    let u = f u Mpfr.Up in
    assert (Mpfrf.cmp l u <= 0);
    (l, u)
  let antitone f (l, u) = monotone f (u, l)
  let of_strings = monotone Mpfrf.of_string
  let of_rational q =
    let q = Mpqf._mpq q in
    monotone Mpfrf.of_mpq (q, q)
  let neg = antitone Mpfrf.neg
  let mul (l1, u1) (l2, u2) =
    let pairs = [(l1, l2); (l1, u2); (u1, l2); (u1, u2)] in
    let products rnd = List.map (fun (x, y) -> Mpfrf.mul x y rnd) pairs in
    (list_min Mpfrf.cmp (products Mpfr.Down),
     list_max Mpfrf.cmp (products Mpfr.Up))
  let add (l1, u1) (l2, u2) =
    monotone (fun (x, y) -> Mpfrf.add x y) ((l1, l2), (u1, u2))
  let print fmt (l, u) =
    let summarize_or_print (trunk_b, lu) other_b =
      let open Mpfr_utilities in
      let trk = Truncation.make trunk_b in
      let prec = Truncation.precision trk in
      let open Mpfrf in
      if match lu with
         | `Upper -> cmp (sub trunk_b prec Mpfr.Up) other_b <= 0
         | `Lower -> cmp other_b (add trunk_b prec Mpfr.Down) <= 0
      then Truncation.print fmt trk
      else Format.fprintf fmt "interval(%a,%a)"
             Mpfrf.print l Mpfrf.print u in
    if Mpfrf.sgn u >= 0
    then summarize_or_print (u, `Upper) l
    else summarize_or_print (l, `Lower) u
end

(** Represents a real number,
    either by an exact representation (a rational number)
    or by a range of values containing it (an interval delimited by floating point numbers). *)
type t =
  | Exact of Rational.t
  | Range of Interval.t
let of_range l u = Range (Interval.of_strings (l, u))
let of_number n = Exact (Rational.of_number n)
let of_fraction n d = Exact (Rational.of_fraction n d)
let of_float f = Range (Interval.of_strings (f,f))
let zero = Exact (Rational.zero)
let one = Exact (Rational.one)
let is_zero = function
  | Exact q -> Rational.is_zero q
  | Range _ -> false
let is_one = function
  | Exact q -> Rational.is_one q
  | Range _ -> false
let neg = function
  | Exact q -> Exact (Rational.neg q)
  | Range r -> Range (Interval.neg r)
let as_interval = function
  | Range r -> r
  | Exact q -> (Interval.of_rational q)
let print fmt = function
  | Exact q -> Rational.print fmt q
  | Range r -> Interval.print fmt r
let mul c1 c2 = match c1, c2 with
  | Exact q1, Exact q2 -> Exact (Rational.mul q1 q2)
  | Exact q, Range r
    | Range r, Exact q -> begin
      if Rational.is_zero q
      then zero
      else Range (Interval.mul r (Interval.of_rational q))
    end
  | Range r1, Range r2 -> Range (Interval.mul r1 r2)
let add c1 c2 = match c1, c2 with
  | Exact q1, Exact q2 -> Exact (Rational.add q1 q2)
  | _ -> Range (Interval.add (as_interval c1) (as_interval c2))

