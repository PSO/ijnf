open JordanAcceleration

let m1 = [| [|0.; 1.; 1.;|];
            [|1.; 0.; 5.;|];
            [|0.; 0.; 1.|]; |]

let as_input = Array.map (Array.map Mpqf.of_float)

let acc m =
  let res = accelerate (as_input m) in
  Format.printf "Acceleration réussie:@\n%a@\n"
    Output.print res

let _ = acc m1
