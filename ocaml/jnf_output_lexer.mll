{
open Jnf_output_parser
open Lexing

exception SyntaxError of string

let next_line lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_bol = lexbuf.lex_curr_pos;
               pos_lnum = pos.pos_lnum + 1
    }
}

let digit = ['0'-'9']
let int = '-'? digit+
let frac = '.' digit*
let exp = ['e' 'E'] ['-' '+']? digit+
let float = '-'? digit* frac? exp?

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

rule read =
  parse
  | white { read lexbuf }
  | newline { next_line lexbuf; read lexbuf }
  | int { INT (Lexing.lexeme lexbuf) }
  | float { FLOAT (Lexing.lexeme lexbuf) }
  | "'P'" { P }
  | "'J'" { J }
  | "'Q'" { Q }
  | "diag" { DIAG }
  | "jordan_block" { JORDAN_BLOCK }
  | "real_block" { REAL_BLOCK }
  | "interval" { INTERVAL }
  | '[' { LEFT_BRACKET }
  | ']' { RIGHT_BRACKET }
  | '(' { LEFT_PAREN }
  | ')' { RIGHT_PAREN }
  | '{' { LEFT_CURLY }
  | '}' { RIGHT_CURLY }
  | '/' { SLASH }
  | ',' { COMMA }
  | ':' { COLUMN }
  | _ { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
  | eof { EOF }
