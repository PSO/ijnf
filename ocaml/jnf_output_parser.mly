%token <string> INT
%token <string> FLOAT
%token P Q J
%token DIAG
%token JORDAN_BLOCK REAL_BLOCK
%token INTERVAL
%token LEFT_BRACKET RIGHT_BRACKET
%token LEFT_PAREN RIGHT_PAREN
%token LEFT_CURLY RIGHT_CURLY
%token COMMA
%token COLUMN
%token SLASH
%token EOF

%start < Real.t list list * Real.t list list * [`JB of int * Real.t | `RJB of int * Real.t * Real.t] list> output

%%

plist(X):
| xs = delimited(LEFT_PAREN, separated_nonempty_list(COMMA, X), RIGHT_PAREN) { xs }

blist(X):
| xs = delimited(LEFT_BRACKET, separated_nonempty_list(COMMA, X), RIGHT_BRACKET) { xs }

output:
  LEFT_CURLY
  J COLUMN DIAG j = plist(jordan_block) COMMA
  Q COLUMN q = dense_matrix COMMA
  P COLUMN p = dense_matrix
  RIGHT_CURLY
  EOF { (p, q, j) }

jordan_block:
  | JORDAN_BLOCK LEFT_PAREN eigen = cell COMMA card = INT RIGHT_PAREN { `JB (int_of_string card, eigen) }
  | REAL_BLOCK LEFT_PAREN eigen = cell COMMA angle = cell COMMA card = INT RIGHT_PAREN { `RJB (int_of_string card, eigen, angle) }

dense_matrix:
  m = blist(blist(cell)) { m }

cell:
  | c = interval { c }
  | c = rational { c }
  | c = float { c }

interval:
  INTERVAL LEFT_PAREN lb = FLOAT COMMA ub = FLOAT RIGHT_PAREN { Real.of_range lb ub }

rational:
  | n = INT SLASH d = INT { Real.of_fraction n d }
  | n = INT { Real.of_number n }

float:
  f = FLOAT { Real.of_float f }
