(** Utilities on matrices represented as ['a array array] *)

let matrix_of_nested_lists l =
  Array.of_list (List.map Array.of_list l)

let matrix_map f m =
  Array.map (Array.map f) m

let matrix_of_function (rows, cols) f =
  let f00 = f ~row:0 ~col:0 in
  let a = Array.make_matrix rows cols f00 in
  for row = 0 to rows - 1 do
    for col = 0 to cols - 1 do
      if (row, col) <> (0, 0)
      then a.(row).(col) <- f ~row ~col
    done
  done;
  a

(** Builds a square matrix from the provided square diagonal blocks. *)
let matrix_of_diagonal_blocks zero blocks =
  let n, f =
    List.fold_left (fun (n, f) m ->
        let n' = n + Array.length m in
        let f' ~row ~col =
          if n <= row && row < n' && n <= col && col < n'
          then m.(row - n).(col - n)
          else f ~row ~col in
        n', f') (0, fun ~row ~col -> zero) blocks in
  matrix_of_function (n, n) f

let matrix_product ~add ~mul m1 m2 =
  let rows = Array.length m1 in
  let cols = Array.length m2.(0) in
  let n = Array.length m2 in
  let n' = Array.length m1.(0) in
  if n <> n' then raise (Invalid_argument
                           (Printf.sprintf "matrix_product: (%d,%d) by (%d,%d)" rows n' n cols));
  let f ~row ~col =
    let mk_mul i = mul m1.(row).(i) m2.(i).(col) in
    let x = ref (mk_mul 0) in
    for i = 1 to n - 1 do
      x := add !x (mk_mul i)
    done;
    !x in
  matrix_of_function (rows, cols) f

let print_matrix pp_cel fmt m =
  Format.fprintf fmt "[@[<v>";
  let rows = Array.length m in
  let cols = Array.length m.(0) in
  for row = 0 to rows - 1 do
    if row <> 0 then Format.fprintf fmt "@]],@,";
    Format.fprintf fmt "[@[";
    for col = 0 to cols - 1 do
      if col <> 0 then Format.pp_print_string fmt ", ";
      pp_cel fmt m.(row).(col)
    done;
  done;
  Format.fprintf fmt "]]@]@]"
