
module Truncation = struct

  type sign = [`Plus | `Minus]

  (** To be interpreted as [sign] 0.[digits] x 10^[e]. *)
  type t = {
      sign : sign;
      digits : string;
      e : int;
    }

  let print fmt a =
    begin match a.sign with
    | `Plus -> ()
    | `Minus -> Format.pp_print_char fmt '-'
    end;
    if a.e <= 0 then
      Format.fprintf fmt "0.%s%s" (String.make (-a.e) '0') a.digits
    else if a.e <= String.length a.digits then
      Format.fprintf fmt "%s.%s"
        (String.sub a.digits 0 a.e)
        (String.sub a.digits a.e ((String.length a.digits) - a.e))
    else
      Format.fprintf fmt "%c.%sE%d"
        (String.get a.digits 0)
        (String.sub a.digits 1 ((String.length a.digits) - 1))
        a.e

  let precision a =
    let rank_last_digit = a.e - String.length a.digits in
    Mpfrf.pow_int (Mpfrf.of_int 10 Mpfr.Up) rank_last_digit Mpfr.Up

  let make ?(width=3) x =
    let txt = Mpfrf.to_string x in
    let sign = if String.get txt 0 = '-' then `Minus else `Plus in
    let id = String.index txt '.' in
    let ie = String.index txt 'E' in
    let digits = String.sub txt (id + 1) (ie - id - 1) in
    let e = int_of_string (String.sub txt (ie + 1) ((String.length txt) - ie - 1)) in
    let digits, e =
      if e <= 0 then
        let e_limit = 2 - width in
        let digits_length = width + e - 1 in
        if digits_length <= 0 then "", e_limit
        else String.sub digits 0 digits_length, e
      else String.sub digits 0 width, e in
    { sign = sign; digits = digits; e = e }
end
