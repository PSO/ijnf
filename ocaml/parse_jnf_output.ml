module Lexer = Jnf_output_lexer
module Parser = Jnf_output_parser

let print_position outx lexbuf =
  let open Lexing in
  let pos = lexbuf.lex_curr_p in
  Format.fprintf outx "%s:%d:%d" pos.pos_fname
    pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse_with_error lexbuf =
  try `Success (Parser.output Lexer.read lexbuf) with
  | Lexer.SyntaxError msg ->
     `Failure (Format.asprintf "%a: %s\n" print_position lexbuf msg)
  | Parser.Error ->
     `Failure (Format.asprintf "%a: syntax error\n" print_position lexbuf)

let parse ic filename =
  let lexbuf = Lexing.from_channel ic in
  Lexing.(lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename });
  parse_with_error lexbuf

let main () =
  let file = "output.txt" in
  match parse (open_in file) file with
  | `Success (p, q, j) -> begin
      Format.printf "Parsing successful (%d lines in P, %d lines in Q, %d blocks int J)@\n"
                    (List.length p)
                    (List.length q)
                    (List.length j)
    end
  | `Failure r ->
     Format.printf "Parsing failed@\n%s@\n" r
