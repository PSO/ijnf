"""
Reporting what is ok or not during some verification process.
"""
import sys
import traceback
from abc import ABC, abstractmethod
from io import StringIO
from typing import TextIO, Any, List


class Item(ABC):

    @abstractmethod
    def print_report(self, border: str, file: TextIO):
        pass

    @staticmethod
    def print_value(border: str, label: str, value: Any, file: TextIO):
        if isinstance(value, Exception):
            print(border, label, file=file)
            traceback.print_exception(value.__class__, value, value.__traceback__, file=file)
        else:
            value_lines = str(value).splitlines()
            assert len(value_lines) != 0
            if len(value_lines) == 1:
                print(border, label, value_lines[0], file=file)
            else:
                print(border, label, file=file)
                for li in value_lines:
                    print(li, file=file)


class Info(Item):

    def __init__(self, label: str, value: Any, relation: str = '='):
        self.label = label
        self.value = value
        self.relation = relation

    def print_report(self, border: str, file: TextIO):
        if self.value is None:
            print(border, self.label, file=file)
        else:
            Item.print_value(border, f'{self.label} {self.relation}', self.value, file=file)


# TODO: (urgent) build with reason + **kwargs
class Error(Item):

    def __init__(self, reason: str, **kwargs):
        self.reason = reason
        self.fields = kwargs

    def print_report(self, border: str, file: TextIO):
        border = f'{border}!'
        print(border, 'Error:', self.reason, file=file)
        for label, value in self.fields.items():
            Item.print_value(border, f'{label}:', value, file=file)


class SubReport(Item):

    def __init__(self, report: 'Report'):
        self.sub_report = report

    def print_report(self, border: str, file: TextIO):
        self.sub_report.print_report(file=file, outer_border=border)


class Report(object):
    """
    A report, with its items, error count and an optional identifier.
    Sub-reports might be nested.
    """

    def __init__(self, title=None, border='#'):
        self._title = title
        self.border = border
        self.items: List[Item] = []

    @property
    def errors(self):
        n = 0
        for item in self.items:
            if isinstance(item, Error):
                n += 1
            elif isinstance(item, SubReport) and item.sub_report.has_errors():
                n += 1
        return n

    def has_errors(self):
        return self.errors != 0

    def __bool__(self):
        return not self.has_errors()

    @property
    def cumulated_errors(self):
        n = 0
        for item in self.items:
            if isinstance(item, Error):
                n += 1
            elif isinstance(item, SubReport):
                n += item.sub_report.cumulated_errors
        return n

    @property
    def title(self) -> str:
        if self._title is None:
            return 'Report'
        elif isinstance(self._title, str):
            return self._title
        else:
            return f'Report {self._title}'

    @property
    def error_status(self) -> str:
        if self.errors == 0:
            return 'no errors'
        elif self.errors == 1:
            return '1 error'
        else:
            return f'{self.errors} errors'

    def print_report(self, file: TextIO = sys.stderr, outer_border=''):
        border = f'{outer_border}{self.border}'
        width = max(1, 5 - len(border))
        upper_border = self.border * width
        print(outer_border + upper_border, f'{self.title} ({self.error_status})', file=file)
        for it in self.items:
            it.print_report(border, file)

    def __repr__(self):
        buffer = StringIO()
        self.print_report(file=buffer)
        return buffer.getvalue()

    def add_item(self, item: Item):
        self.items.append(item)

    def register(self, label: str, value=None):
        self.add_item(Info(label, value))

    def blame(self, problem: str, **kwargs):
        self.add_item(Error(problem, **kwargs))

    def blame_value(self, problem: str, got: Any, expected: Any = None):
        self.add_item(Error(problem, got=got, expected=expected))

    def blame_failure(self, action: str, exn: Exception):
        self.add_item(Error(f'{action} failed with {exn}', got=exn))

    def sub_report(self, label: str, border='+'):
        new_rep = Report(label, border=border)
        self.add_item(SubReport(new_rep))
        return new_rep
