import random
from inspect import signature
from numbers import Number
from typing import List, Union, Callable, Sequence

CellTemplate = Union[Number, Sequence[Number]]
MatrixTemplate = List[List[CellTemplate]]


def number_of_template(x: CellTemplate, prev_lines, prev_cells):
    if isinstance(x, Number):
        return x
    elif isinstance(x, Sequence):
        return number_of_template(random.choice(x), prev_lines, prev_cells)
    elif isinstance(x, Callable):
        params = signature(x).parameters
        kwargs = {}
        if 'prev_cells' in params:
            kwargs['prev_cells'] = prev_cells
        if 'prev_lines' in params:
            kwargs['prev_lines'] = prev_lines
        y = x(**kwargs)
        return number_of_template(y, prev_lines, prev_cells)
    else:
        raise TypeError(f'No number out of: {type(x)}')


def matrix_of_template(template: MatrixTemplate) -> List[List[Number]]:
    res = []
    for template_line in template:
        line = []
        for template_cell in template_line:
            num = number_of_template(template_cell, prev_lines=res, prev_cells=line)
            line.append(num)
        res.append(line)
    return res


def matrix_of_cells(rows, cols, gen_cell):
    template = [[gen_cell(r, c) for c in range(cols)] for r in range(rows)]
    return matrix_of_template(template)
