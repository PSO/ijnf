from enum import Enum
from functools import total_ordering
from typing import Callable


@total_ordering
class Verbose(Enum):
    """
    Controlling the verbosity of the application.
    """
    QUIET = 0
    LITTLE = 1
    VERY = 2

    @classmethod
    def make(cls, txt: str) -> 'Verbose':
        upper = txt.upper()
        for mode in Verbose:
            if mode.name == upper:
                return mode
        raise ValueError(txt)

    def __lt__(self, other: 'Verbose'):
        return self.value < other.value

    def log(self, level: 'Verbose', *args, **kwargs) -> None:
        """
        Log a value, or not, according to the verbosity level.

        :param level: Verbosity level of the printed information
        :param args: The information. Functions in it will be executed without parameters.
        :param kwargs: Directly passed to print.
        """
        if level <= self:
            expanded_args = []
            for a in args:
                if isinstance(a, Callable):
                    a = a()
                expanded_args.append(a)
            print(*expanded_args, **kwargs)
