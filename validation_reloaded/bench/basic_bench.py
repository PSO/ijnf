from timeit import timeit

crash_sympy_not_CRootof = """
m = Matrix([
 [0, 1, 0, 2, 1, 1, 1, 0, 0, 0],
 [0, 0, 0, 1, 2, 1, 2, 0, 1, 0],
 [1, 2, 1, 1, 0, 0, 1, 0, 0, 2],
 [2, 1, 1, 2, 1, 1, 1, 1, 1, 1],
 [0, 2, 1, 1, 2, 0, 1, 0, 2, 0],
 [1, 2, 1, 0, 1, 0, 1, 0, 0, 0],
 [0, 0, 0, 0, 1, 0, 2, 0, 1, 2],
 [0, 1, 0.5, 2, 0, 0.5, 0, 0, 2, 1],
 [0, 0, 1, 1, 0, 0, 2, 1, 1, 0],
 [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]])
""" # Sympy fails

sympy_TO = """
m = Matrix([
 [2, 0, 0, 0, 0, -1],
 [1, 0, -1, 0, 1, 1],
 [0, 0, 1, 0, 0, 0],
 [0, 0.5, 0, 0, 1, 0],
 [1, 0.5, 1, -1, 0, 1],
 [0, 0, 0, 0, 0, 1]])
"""  # Sympy stalls (even on 1 decomposition)

matrix5 = """
m = Matrix([
 [2, 0, 0, 0, 0],
 [1, 0, 0, 1, 0],
 [1, 2, 0, 1, 0],
 [1, 2, 1, 0, 0],
 [1, 0, 0, 0, 1]])
"""  # IJNF twice faster

matrix5_sqrt = """
m = Matrix([
 [0, 1, 2, 0, 0],
 [2, 0, 0, 1, 0],
 [0, 0, 0, -1, 1],
 [0, 0, 2, 1, -1],
 [0, 0, 0, 0, 1]])
 """  # IJNF more than twice faster

matrix8 = """
Matrix([
 [0, 2, 0, 0, 0, 1, 1, 0],
 [2, 2, 1, 0, 2, 0, 0.5, 2],
 [1, 1, 1, 0, 1, 0.5, 1, 0],
 [1, 1, 0, 0, 0, 0, 2, 0],
 [0.5, 0, 0, 1, 1, 1, 1, 2],
 [2, 0, 2, 1, 0, 1, 0, 1],
 [1, 0, 2, 1, 1, 1, 1, 1],
 [0, 0, 0, 0, 0, 0, 0, 1]])
"""  # Sympy fails

if __name__ == '__main__':
    n = 100
    matrix = matrix5_sqrt

    t = timeit(stmt='m.jnf()', setup='from lib.ijnf import Matrix' + matrix, number=n)
    print('IJNF:', t, 's')

    t = timeit(stmt='m.jordan_form()', setup='from sympy import Matrix' + matrix, number=n)
    print('Sympy:', t, 's')
