from lib.ijnf import *

# M is not in P*J*Q

Matrix([
 [2, 0, 2, 0],
 [2, 2, 2, 2],
 [0, 0, 1, 0],
 [1, 0, 0, 1]])

Matrix([
 [2, 0, 0, 1],
 [1, 1, 0, 0],
 [1, 0, 2, 1],
 [0, 0, 0, 1]])

# J is ok (same as Sympy). P and Q are such that Q*P=Id but are not ok.
