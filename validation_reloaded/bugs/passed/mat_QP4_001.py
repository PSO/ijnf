from lib.ijnf import *

Matrix([
 [0, 2, 1, 2],
 [1, 0, 1, 0],
 [1, 0, 0, 1],
 [0, 0, 0, 1]])

# Q*P not in Id
# (Q*P)[1,2] is almost 0 but slightly negative.
