from lib.ijnf import *

Matrix([
 [2, 0, 0, 0],
 [0, 1, 0, 1],
 [0, 0, 2, 2],
 [0, 0, 0, 1]])

# ijnf: ./intermediate_matrix_decomposition.hpp:175:
# void intermediate_decomposition_algorithm::refactor_and_resume(
#   const rational_polynomial_type&, std::size_t,
#   const rational_polynomial_type&):
# Assertion `squarefree_subfactor.degree() < squarefree_factor.degree()' failed.

Matrix([
 [1/2, 0, 0, 2],
 [0, 1, 0, 1/2],
 [0, 0, 1, 0],
 [0, 0, 0, 1/2]])

