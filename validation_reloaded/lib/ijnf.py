"""
Binding for IJNF, by call and pipe.
"""
import subprocess
import sys
from math import cos, sin, pi
from numbers import Number, Real
from typing import Tuple, List

from sympy import Rational, S

from lib.intervals import Interval
from lib.matrices import SquareMatrix


class Matrix(SquareMatrix):

    def __init__(self, rows: List[List[Number]]):
        super().__init__(rows)
        self._ijnf_output = None

    @property
    def ijnf_output(self):
        if self._ijnf_output is None:
            return None
        return self._ijnf_output.decode(encoding='ascii', errors='replace')

    @property
    def ijnf_input(self):
        """
        Produces strings like:
        "Matrix([[S('1')/S('1'),S('2')/S('1')],[S('3')/S('1'),S('4')/S('1')]])"
        """
        res = "Matrix(["
        first_row = True
        for row in self.rows:
            if not first_row:
                res += ","
            res += "["
            first_cell = True
            for cell in row:
                if not first_cell:
                    res += ","
                as_rational = Rational(cell)
                res += f"S('{as_rational.p}')/S('{as_rational.q}')"
                first_cell = False
            res += "]"
            first_row = False
        res += "])"
        return res

    @classmethod
    def jordan_block(cls, x: Number, size: int) -> 'Matrix':
        def cell(i, j):
            if i == j:
                return x
            elif j == i + 1:
                return 1
            else:
                return 0

        return cls([[cell(i, j) for j in range(size)] for i in range(size)])

    @classmethod
    def real_jordan_block(cls, x: Real, angle: Real, half_size: int) -> 'Matrix':
        theta = pi * angle
        x_cos_theta = x * (theta.cos if isinstance(theta, Interval) else cos(theta))
        x_sin_theta = x * (theta.sin if isinstance(theta, Interval) else sin(theta))
        trig_mat = [[x_cos_theta, -x_sin_theta],
                    [x_sin_theta, x_cos_theta]]
        id_mat = [[1, 0],
                  [0, 1]]

        def cell(i, j):
            meta_i = i // 2
            meta_j = j // 2
            if meta_i == meta_j:
                return trig_mat[i % 2][j % 2]
            elif meta_j == meta_i + 1:
                return id_mat[i % 2][j % 2]
            else:
                return 0

        size = 2 * half_size
        return cls([[cell(i, j) for j in range(size)] for i in range(size)])

    @classmethod
    def from_text(cls, matrix_txt: str) -> 'Matrix':
        eval_env = {
            'Matrix': Matrix,
            'S': S,
        }
        try:
            return eval(matrix_txt, eval_env)
        except Exception as e:
            raise ValueError(f'Error while processing matrix text: {matrix_txt}') from e

    def jnf(self) -> Tuple['Matrix', 'Matrix', 'Matrix']:
        """
        Jordan Normal Form.

        :return: (P, J, Q) such that self = P * J * Q and Q * P = ident
        """
        command_result = subprocess.run(['ijnf', self.ijnf_input], stdout=subprocess.PIPE, check=True)
        self._ijnf_output = command_result.stdout

        eval_env = {
            'ident': Matrix.identity,
            'diag': Matrix.diag,
            'jordan_block': Matrix.jordan_block,
            'real_block': Matrix.real_jordan_block,
            'interval': Interval,
        }
        try:
            dec = eval(command_result.stdout, eval_env)
        except Exception as e:
            print('Error while processing ijnf output:', file=sys.stderr)
            print(command_result.stdout.decode(encoding='ascii', errors='replace'), file=sys.stderr)
            raise e

        p = Matrix(dec['P'])
        j = dec['J']
        q = Matrix(dec['Q'])
        return p, j, q
