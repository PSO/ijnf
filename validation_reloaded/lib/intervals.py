from abc import ABC
from numbers import Number, Real
from typing import Union

from bigfloat import BigFloat, RoundTowardNegative, RoundTowardPositive, cos, \
    quadruple_precision, const_pi, half_precision, double_precision, single_precision
from sympy.core.numbers import Number as SympyNumber

BoundType = Union[Real, BigFloat]


# TODO: consider having a class for bounds (wrapper around int or BigFloat)
# TODO: add to this class a pretty print for close enough intervals?
# TODO: find floats such that bound -> repr with float -> bound = Id


class Bound(ABC):

    precisions = {
        'half': half_precision,
        'single': single_precision,
        'double': double_precision,
        'quadruple': quadruple_precision,
    }
    precision = precisions['double']  # MPRF precision

    lower = precision + RoundTowardNegative

    upper = precision + RoundTowardPositive

    @staticmethod
    def make(arg: BoundType, side):
        if isinstance(arg, int):
            # int: arbitrary precision
            return arg
        elif isinstance(arg, BigFloat):
            # BigFloat: assumed to be sound
            return arg
        elif isinstance(arg, (float, str)):
            # float or str: assumed imprecise
            return BigFloat(arg, context=side)
        elif isinstance(arg, SympyNumber):
            # Sympy: kept as is
            # TODO: check why I get some Sympy here...
            return arg
        raise TypeError

    @staticmethod
    def make_lower(arg: BoundType):
        return Bound.make(arg, Bound.lower)

    @staticmethod
    def make_upper(arg: BoundType):
        return Bound.make(arg, Bound.upper)


class Interval(Number):
    """
    Numeric intervals between two reals, with usual interval arithmetic.
    
    Rounding might be incorrect (e.g. 0 not in Interval.make(0).sin).
    """

    def __init__(self, lower: BoundType, upper: BoundType):
        if lower > upper:
            raise ValueError(f'lower bound ({lower}) > upper bound ({upper})')
        self.lower = Bound.make_lower(lower)
        self.upper = Bound.make_upper(upper)

    @classmethod
    def make(cls, *args):
        if len(args) == 1:
            arg, = args
            if isinstance(arg, Interval):
                return arg
            else:
                return cls(arg, arg)
        elif len(args) == 2:
            lower, upper = args
            return cls(lower, upper)
        raise ValueError(f'Cannot make a {cls.__name__} of {args}')

    def __repr__(self):
        return self.__class__.__name__ + repr((self.lower, self.upper))

    def __add__(self, other):
        try:
            other = Interval.make(other)
        except ValueError:
            return NotImplemented

        with Bound.lower:
            lower = self.lower + other.lower
        with Bound.upper:
            upper = self.upper + other.upper
        return Interval(lower, upper)

    def __radd__(self, other):
        return self.__add__(other)  # Addition is reflexive

    def __mul__(self, other):
        try:
            other = Interval.make(other)
        except ValueError:
            return NotImplemented

        with Bound.lower:
            candidates = [self.lower * other.lower, self.lower * other.upper,
                          self.upper * other.lower, self.upper * other.upper]
            lower = min(candidates)
        with Bound.upper:
            candidates = [self.lower * other.lower, self.lower * other.upper,
                          self.upper * other.lower, self.upper * other.upper]
            upper = max(candidates)
        return Interval(lower, upper)

    def __rmul__(self, other):
        return self.__mul__(other)  # Multiplication is reflexive

    def __neg__(self):
        return Interval(-self.upper, -self.lower)

    def __sub__(self, other):
        try:
            other = Interval.make(other)
        except ValueError:
            return NotImplemented

        with Bound.lower:
            lower = self.lower - other.upper
        with Bound.upper:
            upper = self.upper - other.lower
        return Interval(lower, upper)

    def __rsub__(self, other):
        minus_self = self.__neg__()
        return minus_self.__add__(other)

    def __contains__(self, item):
        try:
            item = Interval.make(item)
        except ValueError:
            return NotImplemented

        return self.lower <= item.lower and item.upper <= self.upper

    def contains(self, item):
        return item in self

    @classmethod
    def pi(cls):
        with Bound.lower:
            lower = const_pi()
        with Bound.upper:
            upper = const_pi()
        return cls.make(lower, upper)

    @property
    def cos(self):
        # Sections determination (position w.r.t. multiples of pi)
        pi = Interval.pi()
        with Bound.lower:
            lower_section = min(self.lower // pi.lower,
                                self.lower // pi.upper)
        with Bound.upper:
            upper_section = max(self.upper // pi.lower,
                                self.upper // pi.upper)

        # Result determination
        if lower_section == upper_section:  # Same section
            if lower_section % 2 == 0:  # Descending
                with Bound.lower:
                    lower = cos(self.upper)
                with Bound.upper:
                    upper = cos(self.lower)
                return Interval(lower, upper)
            else:  # Ascending
                with Bound.lower:
                    lower = cos(self.lower)
                with Bound.upper:
                    upper = cos(self.upper)
                return Interval(lower, upper)
        elif lower_section == upper_section - 1:  # Distinct adjacent sections
            if lower_section % 2 == 0:  # Containing a multiple of 2.pi
                with Bound.upper:
                    upper = max(cos(self.lower), cos(self.upper))
                return Interval(-1, upper)
            else:
                with Bound.lower:
                    lower = min(cos(self.lower), cos(self.upper))
                return Interval(lower, 1)
        else:  # Interval larger than 2 * pi
            return Interval(-1, 1)

    @property
    def sin(self):
        pi = Interval.pi()
        return (self - (pi * 0.5)).cos

    def __hash__(self) -> int:
        if self.lower == self.upper:
            return hash(self.lower)  # Equal numbers should have same hashcode
        else:
            return hash((self.lower, self.upper))
