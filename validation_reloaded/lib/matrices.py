import os

from numbers import Number
from typing import List

from lib.intervals import Interval


class SquareMatrix(object):

    def __init__(self, rows: List[List[Number]]):
        if not rows:
            raise ValueError(f'No rows')
        self.size = len(rows)
        for r in rows:
            if len(r) != self.size:
                raise ValueError(f'Square matrix with {self.size} rows containing a row of {len(r)} cells')
        self.rows = rows

    def __getitem__(self, key):
        row, col = key
        return self.rows[row][col]

    def __repr__(self):
        res = self.__class__.__name__ + '(['
        res += ','.join([os.linesep + ' ' + repr(r) for r in self.rows])
        res += '])'
        return res

    def cell(self, i_row, i_col):
        return self.rows[i_row][i_col]

    def __add__(self, other):
        if isinstance(other, Number):
            def cell(i, j): return self.cell(i, j) + other
        elif isinstance(other, SquareMatrix):
            if self.size != other.size:
                raise ValueError(f'Different matrix sizes ({self.size} vs. {other.size})')

            def cell(i, j): return self.cell(i, j) + other.cell(i, j)
        else:
            return NotImplemented

        return SquareMatrix([[cell(i, j) for j in range(self.size)] for i in range(self.size)])

    def __radd__(self, other):
        return self.__add__(other)

    def __mul__(self, other):
        if isinstance(other, Number):
            def cell(i, j): return self.cell(i, j) * other
        elif isinstance(other, SquareMatrix):
            if self.size != other.size:
                raise ValueError(f'Different matrix sizes ({self.size} vs. {other.size})')

            def cell(i, j): return sum(self.rows[i][v] * other.rows[v][j] for v in range(self.size))
        else:
            return NotImplemented

        return SquareMatrix([[cell(i, j) for j in range(self.size)] for i in range(self.size)])

    def __rmul__(self, other):
        return self.__mul__(other)

    def __pow__(self, power, modulo=None):
        if modulo is None and isinstance(power, int) and power >= 0:
            res = self.identity(self.size)
            for _ in range(power):
                res *= self
            return res
        else:
            return NotImplemented

    @classmethod
    def identity(cls, size):
        def cell(i, j):
            if i == j:
                return 1
            else:
                return 0

        return cls([[cell(i, j) for j in range(size)] for i in range(size)])

    def refines(self, loose_matrix: 'SquareMatrix'):
        other = loose_matrix
        if self.size != other.size:
            raise ValueError(f'Different matrix sizes ({self.size} vs. {other.size})')

        def valid_cell(i, j):
            loose_cell = loose_matrix.cell(i, j)
            if isinstance(loose_cell, Interval):
                return loose_cell.contains(self.cell(i, j))
            else:
                return loose_cell == self.cell(i, j)

        return all(valid_cell(i, j) for i in range(self.size) for j in range(self.size))

    def contains(self, other: 'SquareMatrix'):
        return other.refines(self)

    @classmethod
    def diag(cls, *blocks: 'SquareMatrix'):
        size = sum(b.size for b in blocks)

        def cell(i, j):
            block_start = 0
            for b in blocks:
                block_end = block_start + b.size
                if i < block_end:
                    if block_start <= j < block_end:
                        return b.cell(i - block_start, j - block_start)
                    else:
                        break
                block_start = block_end
            return 0

        return cls([[cell(i, j) for j in range(size)] for i in range(size)])
