import doctest
import sys


sys.path.append('../..')
for f in "intervals_spec.txt", "matrices_spec.txt", "contains_spec.txt":
    print(f'#### Checking {f} ####')
    doctest.testfile(f)
print("### Done ####")
