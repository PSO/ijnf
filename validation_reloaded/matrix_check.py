"""
Application for verifying the results of the decomposition tool on a given matrix.

Usage:
__file__ [options] FILE_OR_MATRIX...
"""
import sys
from pathlib import Path
from typing import List

from docopt import docopt

from app.failure import Report
from app.verbose import Verbose
from lib.ijnf import Matrix
from lib.intervals import Interval

check_doc = """
Output options:
--verbose=[very|little|quiet]  Level of display [default: little]

Exit codes:
- 0: everything went fine
- 1: something went wrong (Python Exception traceback displayed)
- 2: invalid command syntax
- 3: a bug was found!
"""


# TODO: identify and distance from soundness
def verify_interval_inclusion(report, i1_name, i1, i2_name, i2):
    if isinstance(i2, Interval):
        if not i2.contains(i1):
            report.blame(f'{i1_name} not in {i2_name}', **{i1_name: i1, i2_name: i2})
    elif i2 != i1:
        report.blame(f'{i1_name} not equal to {i2_name}', **{i1_name: i1, i2_name: i2})


def interval_inclusion(i1, i2):
    report = Report('interval inclusion')
    verify_interval_inclusion(report, 'i1', i1, 'i2', i2)
    return report


def verify_matrix_inclusion(report,
                            m1_name, m1_cell_names, m1,
                            m2_name, m2_cell_names, m2):
    if m1.size != m2.size:
        report.blame(f'size mismatch',
                     **{f'{m1_name}.size': m1.size,
                        f'{m2_name}.size': m2.size})
        return
    n = m1.size

    for i in range(n):
        for j in range(n):
            verify_interval_inclusion(report,
                                      m1_cell_names(i, j), m1[i, j],
                                      m2_cell_names(i, j), m2[i, j])


def matrix_inclusion(m1, m2):
    report = Report('matrix inclusion')
    verify_matrix_inclusion(report,
                            'm1', lambda i, j: f'm1[{i},{j}]', m1,
                            'm2', lambda i, j: f'm2[{i},{j}]', m2)
    return report


def verify_qp_soundness(report, q, p):
    qp = q * p
    report.register('Q*P', qp)
    verify_matrix_inclusion(report.sub_report('Id in P*Q'),
                            'Id', lambda i, j: None, Matrix.identity(qp.size),
                            'QP', lambda i, j: f'QP[{i},{j}]', qp)


def verify_pjq_soundness(report, m, p, j, q):
    pjq = p * j * q
    report.register('P * J * Q', pjq)
    verify_matrix_inclusion(report.sub_report('M in P*J*Q'),
                            'M', lambda row, col: f'M[{row},{col}]', m,
                            'PJQ', lambda row, col: f'PJQ[{row},{col}]', pjq)


class IjnfDecompositionException(Exception):
    pass


def verify_ijnf(report: Report, m: Matrix, check_qp=True, check_pjq=True):
    report.register('IJNF input', m.ijnf_input)
    try:
        p, j, q = m.jnf()
    except Exception as e:
        report.blame_failure('M.jnf()', e)
        raise IjnfDecompositionException from e
    finally:
        report.register('IJNF output', m.ijnf_output)
    report.register('P', p)
    report.register('J', j)
    report.register('Q', q)
    if check_qp:
        verify_qp_soundness(report, q, p)
    if check_pjq:
        verify_pjq_soundness(report, m, p, j, q)


def check_matrix(m: Matrix, v=Verbose.VERY, report: Report = None, check_pjq=True, check_qp=True) -> Report:
    if report is None:
        report = Report()

    v.log(Verbose.VERY, "Checking:", m)
    verify_ijnf(report, m, check_qp=check_qp, check_pjq=check_pjq)
    v.log(Verbose.VERY, "Pass!")

    return report


def check_str_matrix(m_txt: str, report: Report = None, v=Verbose.VERY) -> Report:
    if report is None:
        report = Report()

    m_ref = Matrix.from_text(m_txt)
    report.register("M", m_ref)
    check_matrix(m_ref, report=report, v=v)

    return report


def main(args: List[str]) -> int:
    doc = __doc__ + check_doc
    doc = doc.replace('__file__', __file__)
    options = docopt(doc, args)

    # Verbosity
    v = Verbose.make(options['--verbose'])
    v.log(Verbose.VERY, f"Verbose mode: {v}")
    v.log(Verbose.VERY, options)

    # Targets
    files = options['FILE_OR_MATRIX']

    for f in files:
        report = Report()
        if f.startswith('Matrix([['):
            expect_failure = False
            m_txt = f
        else:
            f = Path(f)
            expect_failure = f.name.startswith('fail')
            report.register("File", f)
            m_q_txt, = open(f, 'r').readlines()
            m_txt = eval(m_q_txt)
        report.register('Matrix text', m_txt)
        try:
            v.log(Verbose.LITTLE, f'Checking {f}')
            check_str_matrix(m_txt, report=report, v=v)
            if report.has_errors():
                report.print_report()
                v.log(Verbose.QUIET, f"Buggy decomposition!")
                return 3
        except IjnfDecompositionException as e:
            if expect_failure:
                v.log(Verbose.LITTLE, f'Failure in {f} (as expected).')
                continue
            else:
                raise e

    return 0


if __name__ == '__main__':
    command_args = sys.argv[1:]
    code = main(command_args)
    sys.exit(code)
