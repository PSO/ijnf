"""
Application generating matrices and checking their JNF decomposition.

Usage:
__file__ [options]

Matrix options:
--size=<n>  Dimension of the generated square matrices [default: 4]

Generation options:
--limit=NUM  Number of generations [default: unlimited]
--pause=NUM  Pause in seconds between generations [default: 0]

Testing options:
--no-checks       Do not perform soundness checks (M in P*J*Q, Id in Q*J)
--ignore-failure  Do not halt on failure
"""
import sys
import time
from datetime import datetime, timedelta
from typing import List, Union

from docopt import docopt

from app.failure import Report
from app.verbose import Verbose
from matrix_check import check_matrix, check_doc, IjnfDecompositionException
from lib.ijnf import Matrix
from app.random_matrix import matrix_of_template, MatrixTemplate


class Template(object):

    def __init__(self, size):
        self.size = int(size)
        self.template: MatrixTemplate = [
            [(0,) * self.size * 2
             + (
              1, 1, 1, -1,
              2, 2,
              3,
              0.5) for _i in range(self.size)] for _j in range(self.size - 1)]
        self.template.append([0] * (self.size - 1) + [1])

    def random_matrix(self) -> Matrix:
        grid = matrix_of_template(self.template)
        return Matrix(grid)


class LiveStats(object):

    def __init__(self, first_display=3, display_interval=10):
        """
        Start the statistics.
        """
        self.total_counter = 0
        self.stage_begin = datetime.now()
        self.stage_counter = 0
        self.stage_end = self.stage_begin + timedelta(seconds=first_display)
        self.interval = timedelta(seconds=display_interval)

    def __iadd__(self, other):
        self.total_counter += other
        self.stage_counter += other
        return self

    def print_if_needed(self):
        now = datetime.now()
        if now < self.stage_end:
            return

        # Printing needed
        duration = (now - self.stage_begin).total_seconds()
        if self.stage_counter <= 1:
            stage = f'{self.stage_counter} matrix in {duration:.1f} seconds'
        else:
            stage = f'{self.stage_counter / duration:.1f} matrices per second'
        print(f'{stage}; totalizing {self.total_counter} checks')
        self.stage_begin = now
        self.stage_end = self.stage_begin + self.interval
        self.stage_counter = 0


# TODO: make cleaner control flow + penser les différents usages (bench vs test)
def perform_checks(template: Template,
                   limit: Union[None, int], pause: float,
                   v: Verbose,
                   ignore_failure=False,
                   check_pjq=True,
                   check_qp=True) -> int:
    """
    :param template: Matrices template
    :param limit: Limited number of generations
    :param pause: Pause between generations
    :param v: Logger
    :param ignore_failure: Continue upon decomposition failures
    :param check_pjq: Perform M in P*J*Q soundness check
    :param check_qp: Perform Id in Q*P soundness check
    :return: 3 if a bug was found, 0 otherwise
    """
    v.log(Verbose.VERY, f"Starting {'unlimited' if limit is None else limit} random checks.")
    n = 0
    n_fail = 0
    stats = LiveStats()
    try:
        while limit is None or n < limit:
            n += 1
            report = Report(n)
            v.log(Verbose.VERY, f'Drawing Matrix {n}')
            matrix = template.random_matrix()
            v.log(Verbose.VERY, f'M = {matrix}')
            report.register('M', matrix)
            try:
                stats += 1
                check_matrix(matrix, v=v, report=report, check_pjq=check_pjq, check_qp=check_qp)
                if report.has_errors():
                    report.print_report()
                    v.log(Verbose.QUIET, f" bug found after {n} checks")
                    return 3
            except KeyboardInterrupt as e:
                raise e
            except IjnfDecompositionException as e:
                n_fail += 1
                if ignore_failure:
                    v.log(Verbose.LITTLE, f" failure encountered at check {n}")
                    continue
                else:
                    raise e
            if v == Verbose.LITTLE:
                stats.print_if_needed()
            if pause != 0:
                time.sleep(pause)
    except KeyboardInterrupt:
        v.log(Verbose.LITTLE, "Halting by keyboard interrupt")
    except IjnfDecompositionException as e:
        v.log(Verbose.QUIET, f" failure encountered after {n} checks")
        raise e
    failure = f' ({n_fail} failed)' if n_fail else ''
    v.log(Verbose.QUIET, f" {n - n_fail} checks were performed successfully{failure}")
    return 0


def main(args: List[str]) -> int:
    doc = __doc__ + check_doc
    doc = doc.replace('__file__', __file__)
    options = docopt(doc, args)

    # Verbosity
    v = Verbose.make(options['--verbose'])
    v.log(Verbose.VERY, f"Verbose mode: {v}")
    v.log(Verbose.VERY, options)

    # Number of random draws
    limit = options['--limit']
    if limit == 'unlimited':
        limit = None
    else:
        limit = int(limit)

    # Slowing the generations
    pause = float(options['--pause'])

    # Matrix configuration
    template = Template(options['--size'])

    # Testing options
    ignore_failure = options['--ignore-failure']
    check_pjq = check_qp = not options['--no-checks']

    return perform_checks(template, limit, pause, v,
                          ignore_failure=ignore_failure,
                          check_pjq=check_pjq,
                          check_qp=check_qp)


if __name__ == '__main__':
    command_args = sys.argv[1:]
    code = main(command_args)
    sys.exit(code)
